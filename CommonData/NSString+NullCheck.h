//
//  NSString+NullCheck.h
//  MaTrackHOSV1
//
//  Created by sieva networks on 10/27/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (NullCheck)
-(id)NullCheck;
- (BOOL)validateEmailWithString:(NSString*)email;
@end
