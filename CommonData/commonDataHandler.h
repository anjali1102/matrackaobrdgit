//
//  commonDataHandler.h
//  MaTrackHOSV1
//
//  Created by sieva networks on 10/27/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//
#import "DBManager.h"
#import <Foundation/Foundation.h>

@interface commonDataHandler : NSObject
@property (nonatomic, strong) NSString *strUserName;
@property (nonatomic, strong) NSString *strAuthCode;
@property (nonatomic, strong) NSString *strUserId;
@property (nonatomic) BOOL isReAuth;
+ (commonDataHandler *)sharedInstance;
-(NSString*)constructInput:(NSMutableDictionary*)listValues;
-(NSMutableArray*)parseResponse:(NSString*)responseString;
-(void)reAuthenticateUser:(void (^)(BOOL finished))completion;
-(NSString *)getCurrentTimeInLocalTimeZone;
-(NSString *)getTimeDifferenceForGMT;
-(NSString *)calculateStatus:(NSString *)today vehicleStatus:(NSString*)vehicleStatus driverStaus:(NSString *)driverStatus associatedVehicle:(NSString *)associatedVehicle;
-(NSString*)modifyData:(NSString *)statusString date:(NSString *)date;
-(NSDate *)getCurrentTimeForPickerInLocalTimeZone;
@end
