DROP TABLE IF EXISTS "Carrier";
CREATE TABLE Carrier(  
id   INTEGER AUTO_INCREMENT PRIMARY KEY,  
name VARCHAR(45) ,
mainAddress VARCHAR(255),
mainCity VARCHAR(45),
mainState VARCHAR(2),
mainZip VARCHAR(5),
homeAddress VARCHAR(255),
homeCity VARCHAR(45),
homeState VARCHAR(2),
homeZip VARCHAR(5));

DROP TABLE IF EXISTS "user2carrier";
CREATE TABLE user2carrier(  
id     INTEGER AUTO_INCREMENT PRIMARY KEY,   
userid INTEGER ,  
carrierid INTEGER,
FOREIGN KEY (userid) REFERENCES User(id),
FOREIGN KEY (carrierid) REFERENCES Carrier(id));

DROP TABLE IF EXISTS "DVIR";
CREATE TABLE DVIR(  
id     INTEGER AUTO_INCREMENT PRIMARY KEY,
logid  INTEGER,
type VARCHAR(10),
defectCode INTEGER,
driverSign VARCHAR(255),
mechSign VARCHAR(255),
FOREIGN KEY(logid) REFERENCES Log(id)
);

DROP TABLE IF EXISTS "DVIR2Defects";
CREATE TABLE DVIR2Defects(  
id     INTEGER AUTO_INCREMENT PRIMARY KEY,
dvirid INTEGER,
reason VARCHAR(45),
dvirProperty INTEGER ,
FOREIGN KEY(dvirProperty) REFERENCES DVIRDefects(id),
FOREIGN KEY(dvirid) REFERENCES DVIR(id));

DROP TABLE IF EXISTS "DVIRDefects";
CREATE TABLE DVIRDefects(  
id     INTEGER AUTO_INCREMENT PRIMARY KEY,
defectdescription VARCHAR(255));

DROP TABLE IF EXISTS "Log";
CREATE TABLE Log(  
id     INTEGER AUTO_INCREMENT PRIMARY KEY,   
userid INTEGER ,  
carrierid INTEGER,
distance INTEGER,
codriver VARCHAR(45),
destination VARCHAR(45),
notes TEXT,
date VARCHAR(10),
timezone VARCHAR(45),
driverStatus VARCHAR(96),
annotation TEXT,
vehicleStatus VARCHAR(96),
calculatedStatus VARCHAR(96),
orgin VARCHAR(255),
FOREIGN KEY (userid) REFERENCES User(id),
FOREIGN KEY (carrierid) REFERENCES Carrier(id));

DROP TABLE IF EXISTS "Log2shippingdoc";
CREATE TABLE Log2shippingdoc(  
id     INTEGER AUTO_INCREMENT PRIMARY KEY,  
logid INTEGER ,
shippingdocid INTEGER,
FOREIGN KEY (logid) REFERENCES Log(id)
FOREIGN KEY (shippingdocid) REFERENCES Shippingdoc(id));

DROP TABLE IF EXISTS "Log2trailors";
CREATE TABLE Log2trailors(  
id     INTEGER AUTO_INCREMENT PRIMARY KEY,  
logid INTEGER , 
trailerid INTEGER,
FOREIGN KEY (logid) REFERENCES Log(id)
FOREIGN KEY (trailerid) REFERENCES Trailer(id));

DROP TABLE IF EXISTS "Log2vehicle";
CREATE TABLE Log2vehicle(  
id     INTEGER AUTO_INCREMENT PRIMARY KEY,  
logid INTEGER ,  
vehicleid INTEGER,
FOREIGN KEY (logid) REFERENCES Log(id)
FOREIGN KEY (vehicleid) REFERENCES Vehicle(id));

DROP TABLE IF EXISTS "Shippingdoc";
CREATE TABLE Shippingdoc(  
id     INTEGER AUTO_INCREMENT PRIMARY KEY,  
shippingdocno INTEGER
);

DROP TABLE IF EXISTS "Trailer";
CREATE TABLE Trailer(  
id   INTEGER AUTO_INCREMENT PRIMARY KEY,  
name VARCHAR(45));

DROP TABLE IF EXISTS "User";
CREATE TABLE User(  
id   INTEGER AUTO_INCREMENT PRIMARY KEY,  
username VARCHAR(45) UNIQUE ,
password VARCHAR(45),
firstname VARCHAR(45),
lastname VARCHAR(45),
email VARCHAR(45) UNIQUE,
mobile VARCHAR(15),
driverid VARCHAR(45),
homeTimezone INTEGER,
cycle INTEGER,
license no VARCHAR(45)
);

DROP TABLE IF EXISTS "Vehicle";
CREATE TABLE Vehicle(  
id   INTEGER AUTO_INCREMENT PRIMARY KEY, 
imei VARCHAR(20) UNIQUE 
name VARCHAR(45) ,
odometerUnit VARCHAR(45));

DROP TABLE IF EXISTS "pendingUpdate";
CREATE TABLE pendingUpdate(  
id     INTEGER AUTO_INCREMENT PRIMARY KEY,
operation VARCHAR(10),
data TEXT);
