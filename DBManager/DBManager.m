//
//  DBManager1.m
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 8/8/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//

#import "DBManager.h"
//static sqlite3 *database=nil;
NSString *databasePathNew=@"";
@implementation DBManager

-(instancetype)initWithDatabaseFilename:(NSString *)dbFilename{
    self = [super init];
    if (self) {
        
        // Set the documents directory path to the documentsDirectory property.
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        self.documentsDirectory = [paths objectAtIndex:0];
        
        // Keep the database filename.
        self.databaseFilename = dbFilename;
   // [self createDB:self.databaseFilename];
        //BFLog(@"succes or not:%@",sucess);
        // Copy the database file into the documents directory if necessary.
        [self copyDatabaseIntoDocumentsDirectory];
        
    }
    return self;
}
-(void)copyDatabaseIntoDocumentsDirectory{
    // Check if the database file exists in the documents directory.
    NSString *destinationPath = [self.documentsDirectory stringByAppendingPathComponent:self.databaseFilename];
   // BFLog(@"destination:%@",destinationPath);
    if (![[NSFileManager defaultManager] fileExistsAtPath:destinationPath]) {
        
        //BFLog(@"DB not exists");
        // The database file does not exist in the documents directory, so copy it from the main bundle now.
        NSString *sourcePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:self.databaseFilename];
        NSError *error;
        [[NSFileManager defaultManager] copyItemAtPath:sourcePath toPath:destinationPath error:&error];
        
        // Check if any error occurred during copying and display it.
        if (error != nil) {
           // BFLog(@"Error:%@", [error localizedDescription]);
        }
    }
}
-(void)copyDatabaseback{
    // Check if the database file exists in the documents directory.
    NSString *sourcePath = [self.documentsDirectory stringByAppendingPathComponent:self.databaseFilename];
     NSString *destinationPath = [[[NSBundle mainBundle] resourcePath]stringByAppendingPathComponent:self.databaseFilename];
    if (![[NSFileManager defaultManager] fileExistsAtPath:destinationPath]) {
        
       // BFLog(@"DB not exists");
        // The database file does not exist in the documents directory, so copy it from the main bundle now.
       
        NSError *error;
        [[NSFileManager defaultManager] copyItemAtPath:sourcePath toPath:destinationPath error:&error];
        
        // Check if any error occurred during copying and display it.
        if (error != nil) {
         //   BFLog(@"Error:%@", [error localizedDescription]);
        }
    }
}

-(void)runQuery:(const char *)query isQueryExecutable:(BOOL)queryExecutable{
    // Create a sqlite object.
    sqlite3 *sqlite3Database;
    
    // Set the database file path.
    databasePathNew = [self.documentsDirectory stringByAppendingPathComponent:self.databaseFilename];
   // NSFileManager *fileManager=[NSFileManager defaultManager];
    
    // Initialize the results array.
    if (self.arrResults != nil) {
        [self.arrResults removeAllObjects];
        self.arrResults = nil;
    }
    self.arrResults = [[NSMutableArray alloc] init];
    
    // Initialize the column names array.
    if (self.arrColumnNames != nil) {
        [self.arrColumnNames removeAllObjects];
        self.arrColumnNames = nil;
    }
    self.arrColumnNames = [[NSMutableArray alloc] init];
    
    // Open the database.
    BOOL openDatabaseResult = sqlite3_open([databasePathNew UTF8String], &sqlite3Database);
    if(openDatabaseResult == SQLITE_OK) {
        // Declare a sqlite3_stmt object in which will be stored the query after having been compiled into a SQLite statement.
        sqlite3_stmt *compiledStatement;
        
        // Load all data from database to memory.
       // const char  *query1="SELECT * FROM Carrier";
        BOOL prepareStatementResult = sqlite3_prepare_v2(sqlite3Database, query , -1, &compiledStatement, NULL);
//        NSLog(@"SQlite Error:%s",sqlite3_errmsg(sqlite3Database));
        if(prepareStatementResult == SQLITE_OK) {
            // Check if the query is non-executable.
            if (!queryExecutable){
                // In this case data must be loaded from the database.
                
                // Declare an array to keep the data for each fetched row.
                NSMutableArray *arrDataRow;
              //  NSLog(@"CompiledStatement: %@",compiledStatement);
                // Loop through the results and add them to the results array row by row.
                //int compileCode=sqlite3_step(compiledStatement);
                while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
                    // Initialize the mutable array that will contain the data of a fetched row.
                    arrDataRow = [[NSMutableArray alloc] init];
                    
                    // Get the total number of columns.
                    int totalColumns = sqlite3_column_count(compiledStatement);
                    
                    // Go through all columns and fetch each column data.
                    for (int i=0; i<totalColumns; i++){
                        // Convert the column data to text (characters).
                        char *dbDataAsChars = (char *)sqlite3_column_text(compiledStatement, i);
                        
                        // If there are contents in the currenct column (field) then add them to the current row array.
                        if (dbDataAsChars != NULL) {
                            // Convert the characters to string.
                            [arrDataRow addObject:[NSString  stringWithUTF8String:dbDataAsChars]];
                       }
                        else
                            [arrDataRow addObject:[NSString  stringWithFormat:@""]];
                        
                        // Keep the current column name.
                        if (self.arrColumnNames.count != totalColumns) {
                            dbDataAsChars = (char *)sqlite3_column_name(compiledStatement, i);
                            [self.arrColumnNames addObject:[NSString stringWithUTF8String:dbDataAsChars]];
                        }
                    }
                    
                    // Store each fetched data row in the results array, but first check if there is actually data.
                    if (arrDataRow.count > 0) {
                        [self.arrResults addObject:arrDataRow];
                    }
                }
//                NSLog(@"SQlite Error:%s",sqlite3_errmsg(sqlite3Database));
                
            }
            else {
                // This is the case of an executable query (insert, update, ...).
                
                // Execute the query.
               int executeQueryResults = sqlite3_step(compiledStatement);
//                NSLog(@"SQlite Error:%s",sqlite3_errmsg(sqlite3Database));
                if (executeQueryResults == SQLITE_DONE) {
                    // Keep the affected rows.
                    self.affectedRows = sqlite3_changes(sqlite3Database);
                    
                    // Keep the last inserted row ID.
                    self.lastInsertedRowID = sqlite3_last_insert_rowid(sqlite3Database);
                }
                else {
                    // If could not execute the query show the error message on the debugger.
                    //BFLog(@"DB Error: %s", sqlite3_errmsg(sqlite3Database));
                }
            }
        }
        else {
            // In the database cannot be opened then show the error message on the debugger.
           // BFLog(@"Error1:%s", sqlite3_errmsg(sqlite3Database));
        }
        
        // Release the compiled statement from memory.
        sqlite3_finalize(compiledStatement);
        
    }
    
    // Close the database.
    sqlite3_close(sqlite3Database);
}
-(void)loadDataFromDB:(NSString *)query{
    // Run the query and indicate that is not executable.
    // The query string is converted to a char* object.
    [self runQuery:[query UTF8String] isQueryExecutable:NO];
    
    // Returned the loaded results.
   // return (NSArr)self.arrResults;
}

-(void)executeQuery:(NSString *)query{
    // Run the query and indicate that is executable.
    [self runQuery:[query UTF8String] isQueryExecutable:YES];
    [self copyDatabaseback];
}

-(NSString *)getRecords:(NSString*) tablename
{
    NSString *name;
   NSString *filePath = [self.documentsDirectory stringByAppendingPathComponent:self.databaseFilename];
    //NSMutableArray * students =[[NSMutableArray alloc] init];
    //BFLog(@"filepath:%@",filePath);
    
    sqlite3* db = NULL;
    sqlite3_stmt* stmt =NULL;
    int rc=0;
    rc = sqlite3_open_v2([filePath UTF8String], &db, SQLITE_OPEN_READONLY , NULL);
    if (SQLITE_OK != rc)
    {
        sqlite3_close(db);
        //BFLog(@"Failed to open db connection");
    }
    else
    {
        
       tablename = [NSString stringWithFormat:@"\'%@\'",tablename];
        NSString *query_first = [NSString stringWithFormat:@"SELECT tbl_name FROM sqlite_master WHERE type='table' AND name=%@;",tablename];
       // BFLog(@"query:%@",query_first);
        rc =sqlite3_prepare_v2(db, [query_first UTF8String], -1, &stmt, NULL);
        if(rc == SQLITE_OK)
        {
            while (sqlite3_step(stmt) == SQLITE_ROW) //get each row in loop
            {
                if((char*)sqlite3_column_text(stmt, 1) != NULL)
                {
             // NSString *name1 =[NSString stringWithUTF8String:(const char *)sqlite3_column_text(stmt, 1)];
                
                    //BFLog(@"name: %@",name1);
                }
                
                
            }
            //BFLog(@"Done");
            name=@"YES";
            sqlite3_finalize(stmt);
        }
        else
        {
            //BFLog(@"Failed to prepare statement with rc:%d,%s",rc,sqlite3_errmsg(db));
        }
        sqlite3_close(db);
   }
    
    return name;
    
}

-(void)insertIntoTable:(id)table{
    NSString *query_insert;
    if([table isKindOfClass:[Suggestion class]])
    {
      Suggestion *objSugg=(Suggestion *)table;
          NSString *date=[self convertDate:[NSString stringWithFormat:@"%@ 00:00",objSugg.date]];
      query_insert = [NSString stringWithFormat:@"insert into suggestion(username,date,slot,repeat,status,suggestedBy) values ('%@','%@',%@,%@,'%@','%@')",objSugg.username,date,objSugg.slot,objSugg.repeat,objSugg.status,objSugg.suggestedby];
    }
    else if([table isKindOfClass:[Carrier class]]){
        Carrier *objCarrier = (Carrier*)table;
     query_insert = [NSString stringWithFormat:@"INSERT INTO carrier ('name','mainAddress','mainCity', 'mainState', 'mainZip', 'homeAddress', 'homeCity', 'homestate', 'homeZip') VALUES ('%@','%@','%@','%@','%@','%@','%@','%@','%@')",objCarrier.name,objCarrier.mainAddress, objCarrier.mainCity, objCarrier.mainState, objCarrier.mainZip, objCarrier.homeAddress, objCarrier.homeCity, objCarrier.homeState, objCarrier.homeZip];
        
    }
    else if([table isKindOfClass:[DVIR class]]){
        DVIR *obj = (DVIR*)table;
        query_insert = [NSString stringWithFormat:@"INSERT INTO dvir ('id','logid','type','defectCode','driverSign','mechSign') VALUES ('%@','%@','%@','%@','%@','%@')",obj.identifier,obj.logid,obj.type,obj.defectCode,obj.driverSign,obj.mechSign];
        
    }
    else if([table isKindOfClass:[DVIR2Defects class]]){
        DVIR2Defects*obj = (DVIR2Defects*)table;
        query_insert = [NSString stringWithFormat:@"INSERT INTO dvir2defect ('id','driverid','reason','dvirProperty') VALUES ('%@','%@','%@','%@')",obj.identifier,obj.driverid,obj.reason,obj.dvirProperty];
        
    }
    else if([table isKindOfClass:[DVIRDefects class]]){
        DVIRDefects*obj = (DVIRDefects*)table;
        query_insert = [NSString stringWithFormat:@"INSERT INTO dvirdefect ('id','defectdescription') VALUES ('%@','%@')",obj.identifier,obj.defectdescription];
        
    }
    
    else if([table isKindOfClass:[Log class]]){
        Log*obj = (Log*)table;
        query_insert = [NSString stringWithFormat:@"INSERT INTO log ('id','userid','carrierid','distance', 'codriver', 'destination', 'notes', 'date', 'timezone', 'driverStatus','annotation','vehicleStatus','calculateStatus','Orgin') VALUES ('%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@')",obj.identifier,obj.carrierid,obj.distance, obj.codriver,obj.destination,obj.notes,obj.date,obj.timezone,obj.driverStatus,obj.annotation,obj.vehicleStatus,obj.calculateStatus,obj.orgin];
        
    }
    else if([table isKindOfClass:[Log2shippingdoc class]]){
        Log2shippingdoc*obj = (Log2shippingdoc*)table;
        query_insert = [NSString stringWithFormat:@"INSERT INTO Log2shippingdoc ('id','logid','shippingdocid') VALUES ('%@','%@','%@')",obj.identifier,obj.logid, obj.shippingdocid];
        
    }
    else if([table isKindOfClass:[Log2trailors class]]){
        Log2trailors*obj = (Log2trailors*)table;
        query_insert = [NSString stringWithFormat:@"INSERT INTO Log2trailors ('id','logid','trailerid') VALUES ('%@','%@','%@')",obj.identifier,obj.logid,obj.trailerid];
        
    }
    else if([table isKindOfClass:[Log2Vehicle class]]){
        Log2Vehicle*obj = (Log2Vehicle*)table;
        query_insert = [NSString stringWithFormat:@"INSERT INTO Log2vehicle ('id','logid','vehicleid') VALUES ('%@','%@','%@')",obj.identifier,obj.logid,obj.vehicleid];
        
    }
    else if([table isKindOfClass:[pendingUpdate class]]){
        pendingUpdate*obj = (pendingUpdate*)table;
        query_insert = [NSString stringWithFormat:@"INSERT INTO pendingUpdate ('id','operation','data') VALUES ('%@','%@','%@')",obj.identifier,obj.operation,obj.data];
        
    }
    else if([table isKindOfClass:[Shippingdoc class]]){
        Shippingdoc*obj = (Shippingdoc*)table;
        query_insert = [NSString stringWithFormat:@"INSERT INTO Shippingdoc ('id','shippingdocno') VALUES ('%@','%@')",obj.identifier,obj.shippingdocno];
        
    }
    else if([table isKindOfClass:[Trailer class]]){
        Trailer*obj = (Trailer*)table;
        query_insert = [NSString stringWithFormat:@"INSERT INTO Trailer ('id','name') VALUES ('%@','%@')",obj.identifier,obj.name];
        
    }
      else if([table isKindOfClass:[User class]]){
        User*obj = (User*)table;
         // query_insert=[NSString stringWithFormat:@"INSERT INTO user ('username','password','firstname','lastname','email','mobile','driverid','dotNumber','homeTimezone','cycle','cargotype','odounit','restarthours','breakhours','licenseno','loggedIn','firstTimeUser','loggingSwitch','authCode','shippingdocno') VALUES ('%@','%@','%@','','email','mobile','driverid','dotNumber','homeTimezone','cycle','cargotype','odounit','restarthours','breakhours','licenseno','loggedIn','firstTimeUser','loggingSwitch','authCode','shippingdocno')",obj.username,obj.password,obj.firstname ];
        query_insert = [NSString stringWithFormat:@"INSERT INTO user ('username','password','firstname','lastname','mobile','driverid','homeTimezone','cycle','licenseNo','loggedIn','dotNumber','authCode') VALUES ('%@','%@','%@','%@','%@','%@','%@',%@,%@,'%@',%@,'%@')",obj.username,obj.password,obj.firstname,obj.lastname,obj.mobile,obj.driverid,obj.homeTimezone,obj.cycle,obj.licenseNo,obj.loggedIn,obj.dotNo,obj.authCode];
        
    }
    else if([table isKindOfClass:[user2carrier class]]){
        user2carrier*obj = (user2carrier*)table;
        query_insert = [NSString stringWithFormat:@"INSERT INTO user2carrier ('userid','carrierid') VALUES ('%@','%@')",obj.userid,obj.carrierid];
        
    }
    else if([table isKindOfClass:[Vehicle class]]){
        Vehicle*obj = (Vehicle*)table;
        query_insert = [NSString stringWithFormat:@"INSERT INTO vehicle('id','imei','name','odometerUnit') VALUES ('%@','%@','%@','%@')",obj.identifier,obj.imei,obj.name,obj.odometerUnit];
        
    }
    else if([table isKindOfClass:[preferences class]]){
        preferences *obj=(preferences *)table;
        query_insert=[NSString stringWithFormat:@"INSERT INTO preferences('uerid','preference','val') VALUES (%@,'%@','%@')",obj.userid,obj.preference,obj.val];
    }
    
    
//    [self initWithDatabaseFilename:@"hosDB.sql"];
 //   [self initWithDatabaseFilename:@"hosDB.sqlite"];
    [self executeQuery:query_insert];
    if(self.affectedRows!=0){
         NSLog(@"Inserted Succesfully");
    }
    else{
        NSLog(@"Error");
    }
    
        
    
}

-(void)userToVehicle:(id)table{
    NSString *query;
    if([table isKindOfClass:[user2vehicle class]]){
        user2vehicle* obj=(user2vehicle *)table;
        query=[NSString stringWithFormat:@"SELECT * FROM user2vehicle WHERE userid=%@",obj.userid];
        [self loadDataFromDB:query];
        if([_arrResults count]!=0){
           
        query=[NSString stringWithFormat:@"UPDATE user2vehicle SET vehicleid='%@' WHERE userid='%@'",obj.vehicleid,obj.userid];
     
        }
        else{
          query=[NSString stringWithFormat:@"INSERT INTO user2vehicle ('userid','vehicleid') VALUES (%@,'%@')",obj.userid,obj.vehicleid];
        }
        [self executeQuery:query];
        if(self.affectedRows!=0){
            NSLog(@"Inserted Succesfully");
        }
        else{
            NSLog(@"Error");
        }
    }
}
-(void)updateIntoTable:(id)table whereToken:(NSMutableDictionary*)whereToken{
    NSString *query_insert;
    if([table isKindOfClass:[Carrier class]]){
        Carrier *objCarrier = (Carrier*)table;
        query_insert = [NSString stringWithFormat:@"UPDATE carrier SET name='%@',mainAddress='%@',mainCity='%@', mainState='%@', mainZip='%@', homeAddress='%@', homeCity='%@', homestate='%@', homeZip='%@' WHERE %@='%@'",objCarrier.name,objCarrier.mainAddress, objCarrier.mainCity, objCarrier.mainState, objCarrier.mainZip, objCarrier.homeAddress, objCarrier.homeCity, objCarrier.homeState, objCarrier.homeZip,[whereToken objectForKey:@"columnName"],[whereToken objectForKey:@"entry"]];
        
    }
    else if([table isKindOfClass:[DVIR class]]){
        DVIR *obj = (DVIR*)table;
        query_insert = [NSString stringWithFormat:@"UPDATE dvir SET logid='%@',type='%@',defectCode='%@',driverSign='%@',mechSign='%@' WHERE %@=%@",obj.logid,obj.type,obj.defectCode,obj.driverSign,obj.mechSign,[whereToken objectForKey:@"columnName"],[whereToken objectForKey:@"entry"]];
        
    }
    else if([table isKindOfClass:[DVIR2Defects class]]){
        DVIR2Defects*obj = (DVIR2Defects*)table;
        query_insert = [NSString stringWithFormat:@"UPDATE dvir2defect SET driverid='%@',reason='%@',dvirProperty='%@' WHERE %@='%@'",obj.driverid,obj.reason,obj.dvirProperty,[whereToken objectForKey:@"columnName"],[whereToken objectForKey:@"entry"]];
        
    }
    else if([table isKindOfClass:[DVIRDefects class]]){
        DVIRDefects*obj = (DVIRDefects*)table;
        query_insert = [NSString stringWithFormat:@"UPDATE dvirdefect SET defectdescription='%@' WHERE %@='%@'",obj.defectdescription,[whereToken objectForKey:@"columnName"],[whereToken objectForKey:@"entry"]];
        
    }
    
    else if([table isKindOfClass:[Log class]]){
        Log*obj = (Log*)table;
        query_insert = [NSString stringWithFormat:@"UPDATE log SET  userid='%@',carrierid='%@',distance='%@', codriver='%@', destination='%@', notes='%@', date='%@', timezone='%@', driverStatus='%@',annotation='%@',vehicleStatus='%@',calculateStatus='%@',Orgin='%@' WHERE %@='%@'",obj.userid,obj.carrierid,obj.distance, obj.codriver,obj.destination,obj.notes,obj.date,obj.timezone,obj.driverStatus,obj.annotation,obj.vehicleStatus,obj.calculateStatus,obj.orgin,[whereToken objectForKey:@"columnName"],[whereToken objectForKey:@"entry"]];
        
    }
    else if([table isKindOfClass:[Log2shippingdoc class]]){
        Log2shippingdoc*obj = (Log2shippingdoc*)table;
        query_insert = [NSString stringWithFormat:@"UPDATE Log2shippingdoc SET logid='%@',shippingdocid='%@' WHERE %@='%@'",obj.logid, obj.shippingdocid,[whereToken objectForKey:@"columnName"],[whereToken objectForKey:@"entry"]];
        
    }
    else if([table isKindOfClass:[Log2trailors class]]){
        Log2trailors*obj = (Log2trailors*)table;
        query_insert = [NSString stringWithFormat:@"UPDATE Log2trailors SET logid='%@',trailerid='%@' WHERE %@='%@'",obj.logid,obj.trailerid,[whereToken objectForKey:@"columnName"],[whereToken objectForKey:@"entry"]];
        
    }
    else if([table isKindOfClass:[Log2Vehicle class]]){
        Log2Vehicle*obj = (Log2Vehicle*)table;
        query_insert = [NSString stringWithFormat:@"UPDATE Log2vehicle SET logid='%@',vehicleid='%@' WHERE %@='%@'",obj.logid,obj.vehicleid,[whereToken objectForKey:@"columnName"],[whereToken objectForKey:@"entry"]];
        
    }
    else if([table isKindOfClass:[pendingUpdate class]]){
        pendingUpdate*obj = (pendingUpdate*)table;
        query_insert = [NSString stringWithFormat:@"UPDATE  pendingupdate SET operation='%@',data='%@' WHERE %@='%@'",obj.operation,obj.data,[whereToken objectForKey:@"columnName"],[whereToken objectForKey:@"entry"]];
        
    }
    else if([table isKindOfClass:[Shippingdoc class]]){
        Shippingdoc*obj = (Shippingdoc*)table;
        query_insert = [NSString stringWithFormat:@"UPDATE Shippingdoc SET shippingdocno='%@' WHERE %@='%@'",obj.shippingdocno,[whereToken objectForKey:@"columnName"],[whereToken objectForKey:@"entry"]];
        
    }
    else if([table isKindOfClass:[Trailer class]]){
        Trailer*obj = (Trailer*)table;
        query_insert = [NSString stringWithFormat:@"UPDATE  Trailer SET name='%@' WHERE %@='%@'",obj.name,[whereToken objectForKey:@"columnName"],[whereToken objectForKey:@"entry"]];
        
    }
    else if([table isKindOfClass:[User class]]){
        User*obj = (User*)table;
        
        query_insert = [NSString stringWithFormat:@"UPDATE user SET firstname='%@',lastname='%@',email='%@',mobile='%@',driverid='%@',licenseno='%@',dotNumber='%@'WHERE %@='%@'",obj.firstname,obj.lastname,obj.email,obj.mobile,obj.driverid,obj.licenseNo,obj.dotNo,[whereToken objectForKey:@"columnName"],[whereToken objectForKey:@"entry"]];
        
    }
    else if([table isKindOfClass:[user2carrier class]]){
        user2carrier*obj = (user2carrier*)table;
        query_insert = [NSString stringWithFormat:@"UPDATE user2carrier SET userid='%@',carrierid='%@' WHERE %@='%@'",obj.userid,obj.carrierid,[whereToken objectForKey:@"columnName"],[whereToken objectForKey:@"entry"]];
        
    }
    else if([table isKindOfClass:[Vehicle class]]){
        Vehicle*obj = (Vehicle*)table;
        query_insert = [NSString stringWithFormat:@"UPDATE vehicle SET imei='%@',name='%@',odometerUnit='%@' WHERE %@='%@'",obj.imei,obj.name,obj.odometerUnit,[whereToken objectForKey:@"columnName"],[whereToken objectForKey:@"entry"]];
        
    }
    else if([table isKindOfClass:[preferences class]]){
        preferences *obj=(preferences*)table;
        query_insert=[NSString stringWithFormat:@"UPDATE preferences SET preference='%@',val='%@' WHERE %@=%@",obj.preference,obj.val,[whereToken objectForKey:@"columnName"],[whereToken objectForKey:@"entry"]];
    }
    
    
    
    //    [self initWithDatabaseFilename:@"hosDB.sql"];
    [self executeQuery:query_insert];
    if(_affectedRows!=0){
        NSLog(@"Inserted Succesfully");
    }
    else{
        NSLog(@"Error");
    }
    
    
    
}

-(NSArray*)selectFromTable:(id)table whereToken:(NSMutableDictionary*)whereToken{
    NSString *query_insert;
    if([table isKindOfClass:[Carrier class]]){
        if([[whereToken objectForKey:@"columnName"]isEqualToString:@"id"])
            query_insert = [NSString stringWithFormat:@"SELECT * FROM carrier WHERE %@=%@",[whereToken objectForKey:@"columnName"],[whereToken objectForKey:@"entry"]];
        else
        query_insert = [NSString stringWithFormat:@"SELECT * FROM carrier WHERE %@='%@'",[whereToken objectForKey:@"columnName"],[whereToken objectForKey:@"entry"]];
       
    }
    else if([table isKindOfClass:[DVIR class]]){
        query_insert = [NSString stringWithFormat:@"SELECT * FROM dvir WHERE %@='%@'",[whereToken objectForKey:@"columnName"],[whereToken objectForKey:@"entry"]];
        
    }
    else if([table isKindOfClass:[DVIR2Defects class]]){
        query_insert = [NSString stringWithFormat:@"SELECT * FROM dvir2defect WHERE %@='%@'",[whereToken objectForKey:@"columnName"],[whereToken objectForKey:@"entry"]];
        
    }
    else if([table isKindOfClass:[DVIRDefects class]]){
        query_insert = [NSString stringWithFormat:@"SELECT * FROM dvirdefect WHERE %@='%@'",[whereToken objectForKey:@"columnName"],[whereToken objectForKey:@"entry"]];
        
    }
    
    else if([table isKindOfClass:[Log class]]){
        query_insert = [NSString stringWithFormat:@"SELECT * FROM log WHERE %@='%@'",[whereToken objectForKey:@"columnName"],[whereToken objectForKey:@"entry"]];
        
    }
    else if([table isKindOfClass:[Log2shippingdoc class]]){
        query_insert = [NSString stringWithFormat:@"SELECT * FROM Log2shippingdoc WHERE %@='%@'",[whereToken objectForKey:@"columnName"],[whereToken objectForKey:@"entry"]];
        
    }
    else if([table isKindOfClass:[Log2trailors class]]){
        query_insert = [NSString stringWithFormat:@"SELECT * FROM Log2trailors WHERE %@='%@'",[whereToken objectForKey:@"columnName"],[whereToken objectForKey:@"entry"]];
        
    }
    else if([table isKindOfClass:[Log2Vehicle class]]){
        query_insert = [NSString stringWithFormat:@"SELECT * FROM Log2Vehicle WHERE %@='%@'",[whereToken objectForKey:@"columnName"],[whereToken objectForKey:@"entry"]];
        
    }
    else if([table isKindOfClass:[pendingUpdate class]]){
        query_insert = [NSString stringWithFormat:@"SELECT * FROM pendingUpdate WHERE %@='%@'",[whereToken objectForKey:@"columnName"],[whereToken objectForKey:@"entry"]];
        
    }
    else if([table isKindOfClass:[Shippingdoc class]]){
        query_insert = [NSString stringWithFormat:@"SELECT * FROM Shippingdoc WHERE %@='%@'",[whereToken objectForKey:@"columnName"],[whereToken objectForKey:@"entry"]];
        
    }
    else if([table isKindOfClass:[Trailer class]]){
        query_insert = [NSString stringWithFormat:@"SELECT * FROM Trailer WHERE %@='%@'",[whereToken objectForKey:@"columnName"],[whereToken objectForKey:@"entry"]];
        
    }
    else if([table isKindOfClass:[User class]]){
        query_insert = [NSString stringWithFormat:@"SELECT * FROM user WHERE %@='%@'",[whereToken objectForKey:@"columnName"],[whereToken objectForKey:@"entry"]];
        
    }
    else if([table isKindOfClass:[user2carrier class]]){
        query_insert = [NSString stringWithFormat:@"SELECT * FROM user2carrier WHERE %@=%@",[whereToken objectForKey:@"columnName"],[whereToken objectForKey:@"entry"]];
        
    }
    else if([table isKindOfClass:[Vehicle class]]){
        query_insert = [NSString stringWithFormat:@"SELECT * FROM vehicle WHERE %@='%@'",[whereToken objectForKey:@"columnName"],[whereToken objectForKey:@"entry"]];
        
    }
    else if([table isKindOfClass:[preferences class]]){
        query_insert = [NSString stringWithFormat:@"SELECT * FROM preferences WHERE %@='%@'",[whereToken objectForKey:@"columnName"],[whereToken objectForKey:@"entry"]];
        
    }
    else if([table isKindOfClass:[user2vehicle class]]){
        user2vehicle* obj=(user2vehicle *)table;
        query_insert=[NSString stringWithFormat:@"SELECT * FROM user2vehicle WHERE userid=%@",obj.userid];
    }
    //    [self initWithDatabaseFilename:@"hosDB.sql"];
    [self loadDataFromDB:query_insert];
    
    return (NSArray *)self.arrResults;
    
    
    
}

-(void)deleteFromTable:(id)table whereToken:(NSMutableDictionary*)whereToken{
    NSString *query_insert;
    if([table isKindOfClass:[Suggestion class]]){
        query_insert=[NSString stringWithFormat:@"DELETE FROM suggestion WHERE %@='%@'",[whereToken objectForKey:@"columnName"],[whereToken objectForKey:@"entry"]];
                      
    }
    else if([table isKindOfClass:[Carrier class]]){
       // Carrier *obj = (Carrier*)table;
        query_insert = [NSString stringWithFormat:@"DELETE FROM carrier WHERE %@='%@'",[whereToken objectForKey:@"columnName"],[whereToken objectForKey:@"entry"]];
    }
    else if([table isKindOfClass:[DVIR class]]){
       // DVIR *obj = (DVIR*)table;
        query_insert = [NSString stringWithFormat:@"DELETE FROM dvir WHERE %@='%@'",[whereToken objectForKey:@"columnName"],[whereToken objectForKey:@"entry"]];
        
    }
    else if([table isKindOfClass:[DVIR2Defects class]]){
       // DVIR2Defects*obj = (DVIR2Defects*)table;
        query_insert = [NSString stringWithFormat:@"DELETE FROM dvir2defect WHERE %@='%@'",[whereToken objectForKey:@"columnName"],[whereToken objectForKey:@"entry"]];
        
    }
    else if([table isKindOfClass:[DVIRDefects class]]){
      //  DVIRDefects*obj = (DVIRDefects*)table;
        query_insert = [NSString stringWithFormat:@"DELETE FROM dvirdefect WHERE %@='%@'",[whereToken objectForKey:@"columnName"],[whereToken objectForKey:@"entry"]];
        
    }
    
    else if([table isKindOfClass:[Log class]]){
       // Log*obj = (Log*)table;
        query_insert = [NSString stringWithFormat:@"DELETE FROM log WHERE %@='%@'",[whereToken objectForKey:@"columnName"],[whereToken objectForKey:@"entry"]];
        
    }
    else if([table isKindOfClass:[Log2shippingdoc class]]){
       // Log2shippingdoc*obj = (Log2shippingdoc*)table;
        query_insert = [NSString stringWithFormat:@"DELETE FROM Log2shippingdoc WHERE %@='%@'",[whereToken objectForKey:@"columnName"],[whereToken objectForKey:@"entry"]];
        
    }
    else if([table isKindOfClass:[Log2trailors class]]){
        //Log2trailors*obj = (Log2trailors*)table;
        query_insert = [NSString stringWithFormat:@"DELETE FROM Log2trailors WHERE %@='%@'",[whereToken objectForKey:@"columnName"],[whereToken objectForKey:@"entry"]];
        
    }
    else if([table isKindOfClass:[Log2Vehicle class]]){
        //Log2Vehicle*obj = (Log2Vehicle*)table;
        query_insert = [NSString stringWithFormat:@"DELETE FROM Log2Vehicle WHERE %@='%@'",[whereToken objectForKey:@"columnName"],[whereToken objectForKey:@"entry"]];
        
    }
    else if([table isKindOfClass:[pendingUpdate class]]){
       // pendingUpdate*obj = (pendingUpdate*)table;
        query_insert = [NSString stringWithFormat:@"DELETE FROM pendingUpdate WHERE %@='%@'",[whereToken objectForKey:@"columnName"],[whereToken objectForKey:@"entry"]];
        
    }
    else if([table isKindOfClass:[Shippingdoc class]]){
        //Shippingdoc*obj = (Shippingdoc*)table;
        query_insert = [NSString stringWithFormat:@"DELETE FROM Shippingdoc WHERE %@='%@'",[whereToken objectForKey:@"columnName"],[whereToken objectForKey:@"entry"]];
        
    }
    else if([table isKindOfClass:[Trailer class]]){
        //Trailer*obj = (Trailer*)table;
        query_insert = [NSString stringWithFormat:@"DELETE FROM Trailer WHERE %@='%@'",[whereToken objectForKey:@"columnName"],[whereToken objectForKey:@"entry"]];
        
    }
    else if([table isKindOfClass:[User class]]){
        //User*obj = (User*)table;
        query_insert = [NSString stringWithFormat:@"DELETE FROM user WHERE %@='%@'",[whereToken objectForKey:@"columnName"],[whereToken objectForKey:@"entry"]];
        
    }
    else if([table isKindOfClass:[user2carrier class]]){
        //user2carrier*obj = (user2carrier*)table;
        query_insert = [NSString stringWithFormat:@"DELETE FROM user2carrier WHERE %@='%@'",[whereToken objectForKey:@"columnName"],[whereToken objectForKey:@"entry"]];
        
    }
    else if([table isKindOfClass:[Vehicle class]]){
       // Vehicle*obj = (Vehicle*)table;
        query_insert = [NSString stringWithFormat:@"DELETE FROM vehicle WHERE %@='%@'",[whereToken objectForKey:@"columnName"],[whereToken objectForKey:@"entry"]];
        
    }
    
    
 //   [self initWithDatabaseFilename:@"hosDB.sql"];
   //[self initWithDatabaseFilename:@"hosDB.sql"];
    [self executeQuery:query_insert];
    if(_affectedRows!=0){
        NSLog(@"Inserted Succesfully");
    }
    else{
        NSLog(@"Error");
    }
    
    
    
}

-(void)removeLoggedIn{
    NSString* query = [NSString stringWithFormat:@"UPDATE user SET loggedIn=0 WHERE loggedIn=1"];
    [self executeQuery:query];
    
}
-(NSArray*)getUserId{
    NSString* query_insert = [NSString stringWithFormat:@"SELECT * FROM user WHERE loggedIn=1"];
    [self loadDataFromDB:query_insert];
    //NSLog(@"%@",self.arrResults[0][0]);
    return self.arrResults;
}

-(NSArray *)getCarrierId:(NSMutableDictionary*)whereToken{
    NSString* query_insert = [NSString stringWithFormat:@"SELECT carrierid FROM user2carrier WHERE userid=%@",[whereToken objectForKey:@"entry"]];
    [self loadDataFromDB:query_insert];
    return self.arrResults;
}

-(int)getInsertId:(NSString *)table{
    NSString* query_insert;
    if([table isEqualToString:@"Carrier"]){
       query_insert = [NSString stringWithFormat:@"SELECT id from carrier ORDER BY id DESC LIMIT 1"];
    }
    else if([table isEqualToString:@"Suggestion"]){
        query_insert = [NSString stringWithFormat:@"SELECT id from suggestion ORDER BY id DESC LIMIT 1"];
    }
    [self loadDataFromDB:query_insert];
    if([self.arrResults count]!=0){
        
//    NSLog(@"%@",self.arrResults[0][0]);
        return [self.arrResults[0][0] intValue];
    }
    else
        return 1;
}
-(NSArray *)getVehDataPerDay:(NSMutableDictionary *)whereToken{
    NSString* query_log;
    commonDataHandler * common=[commonDataHandler new];
    NSString* date;
    if([[whereToken objectForKey:@"date"]  isEqualToString:[common getCurrentTimeInLocalTimeZone]]){
   date=[self convertDate:[NSString stringWithFormat:@"%@ 00:00",[whereToken objectForKey:@"date"]]];
    }
    else{
        date=[self convertDate:[NSString stringWithFormat:@"%@ %@",[whereToken objectForKey:@"date"],[self getTimeForSlot:96]]];
    }
    NSArray  *dateTime=[date componentsSeparatedByString:@" "];
    NSString *time=dateTime[1];
    NSString *slot=[self getSlot:time];
    query_log=[NSString stringWithFormat:@"SELECT  loggraph.associatedvehicle,loggraph.associatedtrailer from loggraph where userid=%@ and slot='%@' and strftime('%@',loggraph.date)=strftime('%@','%@')",[whereToken objectForKey:@"entry"],slot,@"%m/%d/%Y",@"%m/%d/%Y",date];
    [self loadDataFromDB:query_log];
    return _arrResults;
}

-(void)insertDistance:whereToken{
    NSString* query_log;
    commonDataHandler * common=[commonDataHandler new];
    NSString* date;
    if([[whereToken objectForKey:@"date"]  isEqualToString:[common getCurrentTimeInLocalTimeZone]]){
        date=[self convertDate:[NSString stringWithFormat:@"%@ 00:00",[whereToken objectForKey:@"date"]]];
    }
    else{
        date=[self convertDate:[NSString stringWithFormat:@"%@ %@",[whereToken objectForKey:@"date"],[self getTimeForSlot:96]]];
    }
    NSArray  *dateTime=[date componentsSeparatedByString:@" "];
    NSString *time=dateTime[1];
    NSString *slot=[self getSlot:time];
    query_log=[NSString stringWithFormat:@"SELECT  loggraph.odo from loggraph where userid=%@ and slot='%@' and strftime('%@',loggraph.date)=strftime('%@','%@')",[whereToken objectForKey:@"entry"],slot,@"%m/%d/%Y",@"%m/%d/%Y",date];
}
-(NSArray *)getLogData:(NSMutableDictionary*)whereToken{
    NSMutableArray *arr=[[NSMutableArray alloc]init];
    [self insertDistance:whereToken];
    NSString* query_log;
    NSString* date=[self convertDate:[NSString stringWithFormat:@"%@ 00:00",[whereToken objectForKey:@"date"]]];
    NSArray  *dateTime=[date componentsSeparatedByString:@" "];
    NSString *time=dateTime[1];
    NSString *slot=[self getSlot:time];
    query_log=[NSString stringWithFormat:@"SELECT driverid,firstName,lastName from user WHERE id=%@",[whereToken objectForKey:@"entry"]];
    [self loadDataFromDB:query_log];
    if([_arrResults count]!=0){
        for(int i=0;i<[_arrResults[0] count];i++){
            [arr addObject:_arrResults[0][i]];
        }
    }
    else{
        for(int i=0;i<3;i++){
            [arr addObject:@""];
        }
    }

    query_log=[NSString stringWithFormat:@"SELECT  log.id,log.isapproved,log.formnotes,log.shippingdocno,log.carriername,log.carrierhome,log.carriermain, log.codriver,log.destination,log.distance,log.origin from log WHERE log.userid=%@ and strftime('%@',log.date)=strftime('%@','%@')",[whereToken objectForKey:@"entry"],@"%m/%d/%Y",@"%m/%d/%Y",date];
    [self loadDataFromDB:query_log];
    if([_arrResults count]!=0){
        for(int i=0;i<[_arrResults[0] count];i++){
            [arr addObject:_arrResults[0][i]];
        }
    }
    else{
        for(int i=0;i<11;i++){
            [arr addObject:@""];
        }
    }

    query_log=[NSString stringWithFormat:@"SELECT associatedvehicle,associatedtrailer,odo from loggraph WHERE userid=%@ and strftime=%@",[whereToken objectForKey:@"entry"],date];
    [self loadDataFromDB:query_log];
    if([_arrResults count]!=0){
        for(int i=0;i<[_arrResults[0] count];i++){
            [arr addObject:_arrResults[0][i]];
        }
    }
    else{
        for(int i=0;i<3;i++){
            [arr addObject:@""];
        }
    }

   /* query_log=[NSString stringWithFormat:@"SELECT  log.id,strftime('%@',log.date), log.isapproved,loggraph.associatedvehicle,loggraph.associatedtrailer,loggraph.odo,loggraph.origin,log.formnotes,log.shippingdocno,User.driverid,User.firstName,User.lastName,log.carriername,log.carrierhome,log.carriermain, log.codriver,log.destination,log.distance,log.origin from log LEFT JOIN User ON User.id=log.userid LEFT JOIN loggraph ON loggraph.userid=log.userid WHERE log.userid=%@ and strftime('%@',log.date)=strftime('%@','%@')",@"%m/%d/%Y",[whereToken objectForKey:@"entry"],@"%m/%d/%Y",@"%m/%d/%Y",date];
    [self loadDataFromDB:query_log];*/
   // NSInteger *logId;
    //NSString *query_dvir=[NSString stringWithFormat:@"SELECT DVIR.id, DVIR2Defects.dvirProperty,DVIRDefects.defectdescription FROM  DVIR LEFT JOIN DVIR2Defects ON DVIR2Defects.dvirid=DVIR.id LEFT JOIN DVIRDefects ON DVIRDefects.id=DVIR2Defects.dvirProperty where logid=%@",logId];
    return arr;
    
}

-(NSArray *)getUnapprovedList:(NSMutableDictionary*)whereToken{
    NSString *date= [self convertDate:[NSString stringWithFormat:@"%@ %@",[whereToken objectForKey:@"entry"],[self getTimeForSlot:1]]];
    NSString *query_unapprovedList=[NSString stringWithFormat:@"SELECT strftime('%@', date), isapproved FROM log WHERE %@=%@  AND isapproved=0 AND date BETWEEN DATETIME('now','-14 days') and DATETIME('now','localtime') ORDER BY date",@"%m-%d-%Y",[whereToken objectForKey:@"columnName"],[whereToken objectForKey:@"entry"]];
    [self loadDataFromDB:query_unapprovedList];
    return self.arrResults;
}
-(NSArray *)getLogEditData:(NSMutableDictionary*)whereToken{
    int userId=[[whereToken objectForKey:@"userid"] intValue];
    NSString *query;
    NSMutableArray *data=[[NSMutableArray alloc]init];
    
    for(int i=1;i<=96;i++){
        
    NSString* date=[self convertDate:[NSString stringWithFormat:@"%@ %@",[whereToken objectForKey:@"date"],[self getTimeForSlot:i]]];
    NSArray  *dateTime=[date componentsSeparatedByString:@" "];
    NSString *time=dateTime[1];
    NSString *slot=[self getSlot:time];
    //    NSLog(@"date:%@ slot:%@",date,slot);
    query=[NSString stringWithFormat:@"select isapproved,calculatedstatus,location,annotation,slot,associatedvehicle,associatedtrailer,loggraph.origin,loggraph.date,loggraph.odo from log left join loggraph on loggraph.userid=log.userid  WHERE log.userid=%d and loggraph.slot=%@ and loggraph.date='%@'",userId,slot,date];
        [self loadDataFromDB:query];
        //NSLog(@"logedit:%@",self.arrResults[0]);
        if([self.arrResults count]!=0)
        [data addObject:[self.arrResults[0] mutableCopy]];
        else
        {
        NSArray *emptyArr=[[NSArray alloc]initWithObjects:@"",@"",@"",@"",slot,@"",@"",@"",date, nil];
        [data addObject:[emptyArr mutableCopy]];
        }
        
    }
    
    return data;
}

-(NSArray *)getlogSettings:(NSMutableDictionary*)whereToken{
    NSString* query_log=[NSString stringWithFormat:@"select user.id, cycle.name,timezone.name, user.restarthours,user.breakhours ,cargo.name,odometerunit.name from user left join cycle on user.cycle=cycle.id left join timezone on User.homeTimezone=timezone.id  left join odometerunit on user.odounit=odometerunit.id left join cargo on user.cargotype=cargo.id where user.id=%@" ,[whereToken objectForKey:@"entry"]];
    [self loadDataFromDB:query_log];
    return self.arrResults;
}


//-(NSArray*)getByteData:(NSMutableDictionary *)whereToken{
//    NSString * query_log=[NSString stringWithFormat:@"SELECT log.id,loggraph.calculatedStatus,loggraph.associatedvehicle,loggraph.location from log  left join loggraph on loggraph.logid=log.id where strftime('%m/%d/%Y',loggraph.date)= strftime('%m/%d/%Y',log.date)"];
//    return self.arrResults;
//}
-(NSString *)convertDate:(NSString *)date{
    NSDateFormatter *dateFormat=[[NSDateFormatter alloc]init];
    NSString *timezone;
    NSDictionary *token;
    NSMutableDictionary *whereToken;
    NSArray *userInfoArray=[[NSArray alloc]initWithArray: [self getUserId]];
    if([userInfoArray count]!=0){
        int userId = [userInfoArray[0][0] intValue];
        token=@{@"columnName":@"id",@"entry":[NSString stringWithFormat:@"%d",userId]};
        ;
        whereToken=[[NSMutableDictionary alloc] initWithDictionary:token];
    [self getlogSettings:whereToken];
        if([_arrResults count]!=0){
            timezone=_arrResults[0][2];
            [dateFormat setTimeZone:[NSTimeZone timeZoneWithAbbreviation:timezone]];
        }
        else
            [dateFormat setTimeZone:[NSTimeZone localTimeZone]];
    }
    
    [dateFormat setDateFormat:@"MM/dd/yyyy HH:mm"];
    NSDate *dateTime=[dateFormat dateFromString:date];
    dateFormat=[[NSDateFormatter alloc]init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [dateFormat setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    NSString *dateNew=[dateFormat stringFromDate:dateTime];
    return dateNew;
}

/*-(NSString *)convertDateToLocal:(NSString *)date slotFromServer:(int)slotFromServer{
    NSString *dateTemp=[NSString stringWithFormat:@"%@ %@",date,[self getTimeForSlot:slotFromServer]];
    NSDateFormatter *dateFormat=[[NSDateFormatter alloc]init];
    NSString *timezone;
    NSDictionary *token;
    NSMutableDictionary *whereToken;
    NSArray *userInfoArray=[[NSArray alloc]initWithArray: [self getUserId]];
    if([userInfoArray count]!=0){
        int userId = [userInfoArray[0][0] intValue];
        token=@{@"columnName":@"id",@"entry":[NSString stringWithFormat:@"%d",userId]};
        ;
        whereToken=[[NSMutableDictionary alloc] initWithDictionary:token];
        [dateFormat setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
        
    }
    [dateFormat setDateFormat:@"MM/dd/yyyy HH:mm"];
    NSDate *dateTime=[dateFormat dateFromString:dateTemp];
    dateFormat=[[NSDateFormatter alloc]init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        [self getlogSettings:whereToken];
        if([_arrResults count]!=0){
            timezone=_arrResults[0][2];
            [dateFormat setTimeZone:[NSTimeZone timeZoneWithAbbreviation:timezone]];
        }
        else
            [dateFormat setTimeZone:[NSTimeZone localTimeZone]];
    
    NSString *dateNew=[dateFormat stringFromDate:dateTime];
    dateNewArr=[date]
    return dateNew;
}*/
-(void)saveGeneralForm:(id)table{
    
    formGeneral*obj = (formGeneral *)table;
    NSString *date=[self convertDate:[NSString stringWithFormat:@"%@ %@",obj.date,[self getTimeForSlot:1]]];
    NSArray  *dateTime=[date componentsSeparatedByString:@" "];
    NSString *time=dateTime[1];
    NSString *slot=[self getSlot:time];
    NSString *query=[NSString stringWithFormat:@"update user set firstname='%@' , lastname='%@', driverid='%@' where id=%@",obj.firstname,obj.lastname,obj.driverid,obj.userid];
    [self executeQuery:query];
    if(_affectedRows!=0){
        NSLog(@"Inserted Succesfully");
    }
    else{
        NSLog(@"Error");
    }
    query=[NSString stringWithFormat:@"select * from loggraph where strftime('%@',date)=strftime('%@','%@') and userid=%@",@"%m/%d/%Y",
           @"%m/%d/%Y",date,obj.userid];
    [self loadDataFromDB:query];
    /* if([self.arrResults count]!=0)
        query=[NSString stringWithFormat:@"update loggraph set associatedvehicle='%@' , associatedtrailer='%@' where userid=%@ and strftime('%@',date)=strftime('%@','%@')",obj.vehicle,obj.trailer,obj.userid,@"%m/%d/%Y",@"%m/%d/%Y",date];
    else
        query=[NSString stringWithFormat:@"insert into loggraph (userid,associatedvehicle, associatedtrailer,date,slot) values(%@,'%@','%@','%@','%@')",obj.userid,obj.vehicle,obj.trailer,date,slot];
    [self executeQuery:query];
    if(_affectedRows!=0){
        NSLog(@"Inserted Succesfully");
    }
    else{
        NSLog(@"Error");
    }
     query=[NSString stringWithFormat:@"update log set distance='%@' , shippingdocno='%@' where userid=%@ and strftime('%@',date)=strftime('%@','%@')",obj.distance,obj.shippingdoc,obj.userid,@"%m/%d/%Y",@"%m/%d/%Y",date];*/
    query=[NSString stringWithFormat:@"select * from log where strftime('%@',date)=strftime('%@','%@') and userid=%@",@"%m/%d/%Y",@"%m/%d/%Y",date,obj.userid];
    [self loadDataFromDB:query];
    if([self.arrResults count]!=0)
       query=[NSString stringWithFormat:@"update log set shippingdocno='%@' where userid=%@ and strftime('%@',date)=strftime('%@','%@')",obj.shippingdoc,obj.userid,@"%m/%d/%Y",@"%m/%d/%Y",date];
     else
       query=[NSString stringWithFormat:@"insert into log (shippingdocno,userid,date) values('%@','%@','%@')",obj.shippingdoc,obj.userid,date];
    [self executeQuery:query];
    if(_affectedRows!=0){
        NSLog(@"Inserted Succesfully");
    }
    else{
        NSLog(@"Error");
    }

}

-(void)saveCarrierForm:(id)table{
    Log *obj=(Log *)table;
    NSString *date=[self convertDate:[NSString stringWithFormat:@"%@ 00:00",obj.date]];
    NSString *query=[NSString stringWithFormat:@"select * from log where strftime('%@',date)=strftime('%@','%@') and userid=%@",@"%m/%d/%Y",@"%m/%d/%Y",date,obj.userid];
    [self loadDataFromDB:query];
    if([self.arrResults count]!=0)
        query=[NSString stringWithFormat:@"update log set carriername='%@' , carrierhome='%@',carriermain='%@' where userid=%@ and strftime('%@',date)=strftime('%@','%@')",obj.carriername,obj.carrierhome,obj.carriermain,obj.userid,@"%m/%d/%Y",@"%m/%d/%Y",date];
    else
        query=[NSString stringWithFormat:@"insert into log (carriername,carrierhome,carriermain,date,userid) values('%@','%@','%@','%@',%@)",obj.carriername,obj.carrierhome,obj.carriermain,date,obj.userid];
    [self executeQuery:query];
    if(_affectedRows!=0){
        NSLog(@"Inserted Succesfully");
    }
    else{
        NSLog(@"Error");
    }

}

-(void)saveOtherForm:(id)table{
    Log *obj=(Log *)table;
    NSString *date=[self convertDate:[NSString stringWithFormat:@"%@ 00:00",obj.date]];
    NSString *query=[NSString stringWithFormat:@"select * from log where strftime('%@',date)=strftime('%@','%@') and userid=%@",@"%m/%d/%Y",@"%m/%d/%Y",date,obj.userid];
    [self loadDataFromDB:query];
    if([self.arrResults count]!=0)
        query=[NSString stringWithFormat:@"update log set codriver='%@' , destination='%@',formnotes='%@',origin='%@' where userid=%@ and strftime('%@',date)=strftime('%@','%@')",obj.codriver,obj.destination,obj.notes,obj.orgin,obj.userid,@"%m/%d/%Y",@"%m/%d/%Y",date];
    else
        query=[NSString stringWithFormat:@"insert into log (codriver,destination,formnotes,date,userid,origin) values('%@','%@','%@','%@',%@,'%@')",obj.codriver,obj.destination,obj.notes,date,obj.userid,obj.orgin];
    [self executeQuery:query];
    if(_affectedRows!=0){
        NSLog(@"Inserted Succesfully");
    }
    else{
        NSLog(@"Error");
    }
    
}
-(NSArray *)getLast14Days:(int)i{
    NSDate *now=[NSDate date];
    NSDateFormatter *dateFormat=[[NSDateFormatter alloc]init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDictionary*token;NSMutableDictionary *whereToken;
    NSString* timezone;
    NSArray *userInfoArray=[[NSArray alloc]initWithArray: [self getUserId]];
    if([userInfoArray count]!=0){
        int userId = [userInfoArray[0][0] intValue];
        token=@{@"columnName":@"id",@"entry":[NSString stringWithFormat:@"%d",userId]};
        ;
        whereToken=[[NSMutableDictionary alloc] initWithDictionary:token];
        [self getlogSettings:whereToken];
        if([_arrResults count]!=0){
            timezone=_arrResults[0][2];
            if([timezone isEqualToString:@""])
                [dateFormat setTimeZone:[NSTimeZone localTimeZone]];
            else
                [dateFormat setTimeZone:[NSTimeZone timeZoneWithAbbreviation:timezone]];
        }
        else
            [dateFormat setTimeZone:[NSTimeZone localTimeZone]];
    }
    
    NSString *dateNew=[dateFormat stringFromDate:now];
    NSString *query=[NSString stringWithFormat:@"select strftime('%@',DATETIME('%@','-%d days'))",@"%m/%d/%Y",dateNew,i];
    [self loadDataFromDB:query];
    return self.arrResults;
}

-(NSArray *)getLast14DaysFromDate:(int)i date:dateNew{
    NSString *query=[NSString stringWithFormat:@"select strftime('%@',DATETIME('%@','-%d days'))",@"%m/%d/%Y",dateNew,i];
    [self loadDataFromDB:query];
    return self.arrResults;
}

-(void)updateCred:(id)table{
    User *obj=(User *)table;
    NSString* query_insert = [NSString stringWithFormat:@"UPDATE user SET loggedIn='%@',authCode='%@'WHERE username='%@'",obj.loggedIn,obj.authCode,obj.username];
    [self executeQuery:query_insert];
    if(_affectedRows!=0){
        NSLog(@"Inserted Succesfully");
    }
    else{
        NSLog(@"Error");
    }
    
}
-(void)updateDistance:(id)table dist:(int)dist{
    MobileCheckIn *objMob=(MobileCheckIn *)table;
    NSString *query=[NSString stringWithFormat:@"select * from log where userid=%@ and strftime('%@',date)=strftime('%@','%@')",objMob.userId, @"%m/%d/%Y", @"%m/%d/%Y", objMob.date];
    [self loadDataFromDB:query];
    if([_arrResults count]==0){
        query=[NSString stringWithFormat:@"insert into log (date,userid,distance) values('%@',%@,%d)",objMob.date,objMob.userId, dist];
    }

    else
        query=[NSString stringWithFormat:@"update log set distance=%d where userid=%@ and strftime('%@',date)=strftime('%@','%@')",dist,objMob.userId,@"%m/%d/%Y",@"%m/%d/%Y",objMob.date];
    [self executeQuery:query];
    if(_affectedRows!=0){
        NSLog(@"Insertion sucessfully");
    }
    else
        NSLog(@"Error");
}

-(void)saveStatus:(id)table{
    MobileCheckIn *objMob=(MobileCheckIn *)table;
     NSString *query=[NSString stringWithFormat:@"select * from lateststatus where userid=%@",objMob.userId];
    [self loadDataFromDB:query];
    if([_arrResults count]==0){
        query=[NSString stringWithFormat:@"insert into lateststatus (slot,date,userid,status) values(%@,'%@',%@,'%@')",objMob.slot,objMob.date,objMob.userId,objMob.calculatedstatus];
    }
    else{
        query=[NSString stringWithFormat:@"update lateststatus set slot=%@, date='%@', status='%@' where userid=%@",objMob.slot,objMob.date,objMob.calculatedstatus,objMob.userId];
    }
    [self executeQuery:query];
    if(_affectedRows!=0){
        NSLog(@"Insertion sucessfully");
    }
    else
        NSLog(@"Error");
}
-(void)mobileCheckinUpdation:(id)table{
    MobileCheckIn *objMob=(MobileCheckIn *)table;
    NSArray *dateList=[[NSArray alloc] initWithArray:[objMob.date componentsSeparatedByString:@"/"]];
    NSString *date=[NSString stringWithFormat:@"%@-%@-%@ %@",dateList[2],dateList[0],dateList[1],[self getTimeForSlot:[objMob.slot intValue]]];
   //NSString *date=[self convertDate:[NSString stringWithFormat:@"%@ %@",objMob.date,objMob.slot]];
    NSString *query=[NSString stringWithFormat:@"select * from loggraph where userid=%@ and slot=%@ and strftime('%@',date)=strftime('%@','%@')",objMob.userId,objMob.slot,@"%m/%d/%Y",@"%m/%d/%Y",date];
    [self loadDataFromDB:query];
    if([_arrResults count]==0){
        query=[NSString stringWithFormat:@"insert into loggraph (slot,date,associatedvehicle,vehiclestatus,odo,userid,calculatedstatus,driverstatus) values(%@,'%@','%@','%@',%@,'%@','%@','%@')",objMob.slot,date,objMob.associatedvehicle,objMob.vehiclestatus,objMob.odo,objMob.userId,objMob.calculatedstatus,objMob.driverstatus];
    }
    else{
        query=[NSString stringWithFormat:@"update loggraph set vehiclestatus='%@', calculatedstatus='%@', driverstatus='%@' where userid=%@ and slot=%@ and strftime('%@',date)=strftime('%@','%@')",objMob.vehiclestatus,objMob.calculatedstatus,objMob.driverstatus ,objMob.userId,objMob.slot,@"%m/%d/%Y",@"%m/%d/%Y",date];
    }
    [self executeQuery:query];
    if(_affectedRows!=0){
        NSLog(@"Insertion sucessfully");
    }
    else
        NSLog(@"Error");
}

-(void)logout:(int)userId{
    NSString *query=[NSString stringWithFormat:@"update user set loggedIn=0 where id=%d",userId];
    [self executeQuery:query];
    if(_affectedRows!=0){
        NSLog(@"logged out sucessfully");
    }
    else
        NSLog(@"Error");
}

-(NSArray *)getApproveLog:(NSMutableDictionary *)whereToken{
    NSString *date=[self convertDate:[NSString stringWithFormat:@"%@ 00:00",[whereToken objectForKey:@"date"]]];
    
    NSString *query=[NSString stringWithFormat:@"select isapproved from log where userid=%@ and strftime('%@',date)=strftime('%@','%@')",[whereToken objectForKey:@"userid"], @"%m/%d/%Y", @"%m/%d/%Y", date];
    [self loadDataFromDB:query];
    return _arrResults;
}

-(void)insertPastStatus:(id)table{
    loggraph *objLog=(loggraph *)table;
    NSString *date=[self convertDate:[NSString stringWithFormat:@"%@ %@",objLog.date, [self getTimeForSlot:[objLog.slot intValue]]]];
    NSArray  *dateTime=[date componentsSeparatedByString:@" "];
  //  NSLog(@"data:%@",objLog.calculatedStatus);
    NSString *time=dateTime[1];
    int slot=[[self getSlot:time] intValue];
    NSString *query;
    if([objLog.slot isEqualToString:@"1"]){
    query=[NSString stringWithFormat:@"select * from log where userid=%@ and strftime('%@',date)=strftime('%@','%@')",objLog.userId, @"%m/%d/%Y", @"%m/%d/%Y", date];
    [self loadDataFromDB:query];
    if([_arrResults count]==0){
        query=[NSString stringWithFormat:@"insert into log (date,userid,isapproved) values('%@',%@,%@)",date,objLog.userId,objLog.approve];
    }
    else{
        query=[NSString stringWithFormat:@"update log set isapproved=%@ where userid=%@ and strftime('%@',date)=strftime('%@','%@')",objLog.approve,objLog.userId, @"%m/%d/%Y", @"%m/%d/%Y", date];
    }
    [self executeQuery:query];
    }
    query=[NSString stringWithFormat:@"select * from loggraph where userid=%@ and slot=%d and strftime('%@',date)=strftime('%@','%@')",objLog.userId,slot,@"%m/%d/%Y",@"%m/%d/%Y",date];
    [self loadDataFromDB:query];
    if([_arrResults count]==0){
        query=[NSString stringWithFormat:@"insert into loggraph (slot,date,driverstatus,calculatedstatus,location,annotation,userid) values(%d,'%@','%@','%@','%@','%@',%@)",slot,date,objLog.driverStatus,objLog.calculatedStatus,objLog.location, objLog.notes,objLog.userId];
    }
    else{
        if([objLog.insert isEqualToString:@"1"])
            query=[NSString stringWithFormat:@"update loggraph set driverstatus='%@', calculatedstatus='%@',location='%@', annotation='%@' where userid=%@ and slot=%d and strftime('%@',date)=strftime('%@','%@')",objLog.driverStatus,objLog.calculatedStatus,objLog.location,objLog.notes,objLog.userId,slot,@"%m/%d/%Y",@"%m/%d/%Y",date];
        else
            query=[NSString stringWithFormat:@"update loggraph set driverstatus='%@', calculatedstatus='%@' where userid=%@ and slot=%d and strftime('%@',date)=strftime('%@','%@')",objLog.driverStatus,objLog.calculatedStatus,objLog.userId,slot,@"%m/%d/%Y",@"%m/%d/%Y",date];
    }
    [self executeQuery:query];
    if(_affectedRows!=0){
        NSLog(@"Insertion sucessfully");
    }
    else
        NSLog(@"Error");

}

-(void)insertLogSettings:(id)table{
    rules *obj=(rules *)table;
    NSString *query=[NSString stringWithFormat:@"insert into user (homeTimezone, cycle, restarthours, breakhours,cargotype,odounit)  values (%@,%@,%@,%@,%@,%@)",obj.timezone,obj.cycle,obj.restart,obj.breakhour, obj.cargotype,obj.odounit];
    [self executeQuery:query];
    
}
-(void)updateLogSettings:(id)table{
    rules *obj=(rules *)table;
    NSString *query=[NSString stringWithFormat:@"update user  set homeTimezone=%@, cycle=%@, restarthours=%@, breakhours=%@ ,cargotype=%@, odounit=%@ where id=%@",obj.timezone,obj.cycle,obj.restart,obj.breakhour,obj.cargotype,obj.odounit,obj.userid];
    [self executeQuery:query];
    
}

-(NSArray *)getPreviousStatus:(id)modal{
     MobileCheckIn *obj=(MobileCheckIn *)modal;
    NSArray *dateList=[[NSArray alloc] initWithArray:[obj.date componentsSeparatedByString:@"/"]];
    NSString *date=[NSString stringWithFormat:@"%@-%@-%@ %@",dateList[2],dateList[0],dateList[1],[self getTimeForSlot:[obj.slot intValue]]];
    //NSArray  *dateTime=[date componentsSeparatedByString:@" "];
    //NSString *time=dateTime[1];
    //obj.slot=[self getSlot:time];
    NSString * query;
    int number;
    if([obj.slot intValue]>1)
        number=(int)([obj.slot intValue]-1);
    else{
        number=96;
        query=[NSString stringWithFormat:@"select strftime('%@',DATETIME('%@','-1 days'))",@"%m/%d/%Y",date];
        
        [self loadDataFromDB:query];
        date=_arrResults[0][0];
    }
    query=[NSString stringWithFormat:@"select id, slot,driverstatus,vehiclestatus,calculatedstatus,associatedvehicle from loggraph where strftime('%@',date)=strftime('%@','%@') and slot = %d   and userid=%@",@"%m/%d/%Y",@"%m/%d/%Y",date,number,obj.userId];
    [self loadDataFromDB:query];
    return self.arrResults;
}
-(NSArray *)lastStatus:(NSMutableDictionary *)whereToken{
    NSString * userId=[whereToken objectForKey:@"userid"];
    NSString *date=[whereToken objectForKey:@"date"] ;
    int slot=[[whereToken objectForKey:@"slot"] intValue];
    NSString * query;
    int number;
    if(slot>1)
        number=(int)(slot-1);
    else{
        number=96;
        query=[NSString stringWithFormat:@"select strftime('%@',DATETIME('%@','-1 days'))",@"%m/%d/%Y",date];
        
        [self loadDataFromDB:query];
        date=_arrResults[0][0];
    }
    query=[NSString stringWithFormat:@"select id, slot,driverstatus,vehiclestatus,calculatedstatus,associatedvehicle from loggraph where strftime('%@',date)=strftime('%@','%@') and slot = %d   and userid=%@",@"%m/%d/%Y",@"%m/%d/%Y",date,number,userId];
    [self loadDataFromDB:query];
    return self.arrResults;
}
-(NSArray*)fetchSuggestionWithwhereToken:(NSMutableDictionary*)whereToken{
    NSString *query_insert;
   // if([table isKindOfClass:[Suggestion class]]){
    NSString *date=[self convertDate:[NSString stringWithFormat:@"%@ 00:00",[whereToken valueForKey:@"date"]]];
        query_insert = [NSString stringWithFormat:@"SELECT * FROM suggestion WHERE username='%@' and strftime('%@',date)=strftime('%@','%@') ",[whereToken objectForKey:@"username"],@"%m/%d/%Y",@"%m/%d/%Y",date];
 //   }
    [self loadDataFromDB:query_insert];
    return self.arrResults;
}

-(NSArray *)getCurrentStatus:(int) userid{
    NSString *query=[NSString stringWithFormat:@"SELECT slot,date, calculatedstatus from loggraph where userid =%d and calculatedstatus<>'0' order by date desc ,slot desc LIMIT 1",userid];
    [self loadDataFromDB:query];
    return self.arrResults;
}

-(NSString *)getTimeForSlot:(int)slot{
    NSString * time=@"";
    int hour=(slot/4);
    NSString *minutes=@"0";
    if(slot%4==1){
        minutes=@"00";
    }
    else if(slot%4==2){
        minutes=@"15";
    }
    else if(slot%4==3){
        minutes=@"30";
    }
    else if(slot%4==0){
        minutes=@"45";
        hour=hour-1;
    }
    if(hour>9){
        time=[NSString stringWithFormat:@"%d:%@",hour,minutes];
    }
    else{
        time=[NSString stringWithFormat:@"0%d:%@",hour,minutes];
    }
    //NSLog(@"time:%@",time);
    return time;
}

-(NSString *)getSlot:(NSString * )time {
    //Need unit testing
    int slot = 0;
    NSArray *timeSplit=[time componentsSeparatedByString:@":"];
        int hour = [timeSplit[0] intValue];
        int min = [timeSplit[1] intValue];;
        
        slot =  hour * 4;
        if (min > 0)
            slot = slot + (min / 15);
    return [NSString stringWithFormat:@"%d",(int)(slot+1)];
    
}

-(NSMutableDictionary *)getDOTInspectionData:(NSMutableDictionary*)whereToken {
    NSMutableDictionary *data=[[NSMutableDictionary alloc]init];
    NSString *date=[self convertDate:[NSString stringWithFormat:@"%@ 00:00",[whereToken objectForKey:@"date"]]];
    NSString * query;
    query=[NSString stringWithFormat:@"select firstname, lastname, cycle.name, timezone.name from user LEFT JOIN cycle on cycle.id=user.cycle left join timezone on timezone.id=user.homeTimezone where user.id=%@",[whereToken objectForKey:@"id"]];
    [self loadDataFromDB:query];
    if([_arrResults count]!=0)
    {
        [data setValue:_arrResults[0][0] forKey:@"firstname"];
        [data setValue:_arrResults[0][1] forKey:@"lastname"];
        [data setValue:_arrResults[0][2] forKey:@"cycle"];
        [data setValue:_arrResults[0][3] forKey:@"timezone"];
    }
    else
    {
        
        [data setValue:@"" forKey:@"firstname"];
        [data setValue:@"" forKey:@"lastname"];
        [data setValue:@"" forKey:@"cycle"];
        [data setValue:@"" forKey:@"timezone"];
    }
    query=[NSString stringWithFormat:@"select carriername, carrierhome,carriermain,timeDiff,distance,codriver,shippingdocno from log where userid=%@ and strftime('%@',date)= strftime('%@','%@')",[whereToken objectForKey:@"id"],@"%m/%d/%Y",@"%m/%d/%Y",date];
    [self loadDataFromDB:query];
    if([_arrResults count]!=0)
    {
        [data setValue:_arrResults[0][0] forKey:@"carriername"];
        [data setValue:_arrResults[0][1] forKey:@"carrierhome"];
        [data setValue:_arrResults[0][2] forKey:@"carriermain"];
        [data setValue:_arrResults[0][3] forKey:@"timeDiff"];
        [data setValue:_arrResults[0][4] forKey:@"distance"];
        [data setValue:_arrResults[0][5] forKey:@"codriver"];
        [data setValue:_arrResults[0][6] forKey:@"shippingdocno"];
    }
    else{
        [data setValue:@"" forKey:@" carriername"];
        [data setValue:@"" forKey:@"carrierhome"];
        [data setValue:@"" forKey:@"carriermain"];
        [data setValue:@"" forKey:@"timeDiff"];
        [data setValue:@"" forKey:@"distance"];
        [data setValue:@"" forKey:@"codriver"];
        [data setValue:@"" forKey:@"shippingdocno"];
    }
    return data;
}
-(NSString *)findRestart:whereToken{
    NSString *query;
    NSString *date=[self convertDate:[NSString stringWithFormat:@"%@ %@",[whereToken objectForKey:@"date"], [self getTimeForSlot:[[whereToken objectForKey:@"slot"] intValue]]]];
    NSArray  *dateTime=[date componentsSeparatedByString:@" "];
    NSString *time=dateTime[1];
    int slot=[[self getSlot:time] intValue];
    NSString *statusString=@"";
    int count=0;
    for(int i=1;i<=136;i++){
        slot=slot-1;
        if(slot==0)
        {
            slot=96;
            query=[NSString stringWithFormat:@"select strftime('%@',DATETIME('%@','-1 days'))",@"%m/%d/%Y",date];
            
            [self loadDataFromDB:query];
            date=_arrResults[0][0];
        }
        //date=[self convertDate:[NSString stringWithFormat:@"%@ %@",[whereToken objectForKey:@"date"], [self getTimeForSlot:[[whereToken objectForKey:@"slot"] intValue]]]];
        query=[NSString stringWithFormat:@"select calculatedStatus from loggraph where strftime('%@',date)=strftime('%@','%@') and slot=%d and userid=%@",@"%m/%d/%Y",@"%m/%d/%Y",date,slot,[whereToken objectForKey:@"userid"]];
        [self loadDataFromDB:query];
        NSString *tempChar;
        if([_arrResults count]!=0){
            tempChar=_arrResults[0][0];
        }
        else
            tempChar=@"1";
        if([tempChar isEqualToString:@"1"]){
            count++;
        }

        
    }
    if(count ==136 )
        return @"9";
    else
        return @"7";
        

}
-(NSString *)returnRestart:(NSMutableDictionary*) whereToken{
    NSString *dateNew=@"";
    NSString *query=[NSString stringWithFormat:@"SELECT date  FROM loggraph where userid=%@ and calculatedstatus='9' order by date desc limit 1",[whereToken objectForKey:@"userid"]];
    [self loadDataFromDB:query];
    if([_arrResults count]!=0){
    NSDateFormatter *dateFormat=[[NSDateFormatter alloc]init];
    
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [dateFormat setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    NSDate *dateTime=[dateFormat dateFromString:_arrResults[0][0]];
    dateFormat=[[NSDateFormatter alloc]init];
    [dateFormat setDateFormat:@"MM/dd/yyyy"];
    NSString *timezone;
    NSDictionary *token;
    
    NSArray *userInfoArray=[[NSArray alloc]initWithArray: [self getUserId]];
    if([userInfoArray count]!=0){
        int userId = [userInfoArray[0][0] intValue];
        token=@{@"columnName":@"id",@"entry":[NSString stringWithFormat:@"%d",userId]};
        ;
        whereToken=[[NSMutableDictionary alloc] initWithDictionary:token];
        [self getlogSettings:whereToken];
        if([_arrResults count]!=0){
            timezone=_arrResults[0][2];
            [dateFormat setTimeZone:[NSTimeZone timeZoneWithAbbreviation:timezone]];
        }
        else
            [dateFormat setTimeZone:[NSTimeZone localTimeZone]];
    }
    dateNew=[dateFormat stringFromDate:dateTime];
    }
    return dateNew;
}

@end
