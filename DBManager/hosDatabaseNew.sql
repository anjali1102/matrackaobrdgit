CREATE TABLE Carrier(
id   INTEGER PRIMARY KEY AUTOINCREMENT,
name VARCHAR(45) ,
mainAddress VARCHAR(255),
mainCity VARCHAR(45),
mainState VARCHAR(2),
mainZip VARCHAR(5),
homeAddress VARCHAR(255),
homeCity VARCHAR(45),
homeState VARCHAR(2),
homeZip VARCHAR(5));

CREATE TABLE Preferences(
id INTEGER PRIMARY KEY AUTOINCREMENT,
userid INTEGER ,  
preference VARCHAR(45),
val VARCHAR(50),
FOREIGN KEY (userid) REFERENCES User(id),
);

CREATE TABLE user2carrier(  
id     INTEGER PRIMARY KEY AUTOINCREMENT,
userid INTEGER ,  
carrierid INTEGER,
FOREIGN KEY (userid) REFERENCES User(id),
FOREIGN KEY (carrierid) REFERENCES Carrier(id));

CREATE TABLE DVIR(  
id     INTEGER PRIMARY KEY AUTOINCREMENT,
logid  INTEGER,
type VARCHAR(10),
defectCode INTEGER,
driverSign VARCHAR(255),
mechSign VARCHAR(255),
FOREIGN KEY(logid) REFERENCES Log(id)
);

CREATE TABLE DVIR2Defects(  
id     INTEGER PRIMARY KEY AUTOINCREMENT,
dvirid INTEGER,
reason VARCHAR(45),
dvirProperty INTEGER ,
FOREIGN KEY(dvirProperty) REFERENCES DVIRDefects(id),
FOREIGN KEY(dvirid) REFERENCES DVIR(id));

CREATE TABLE DVIRDefects(  
id     INTEGER PRIMARY KEY AUTOINCREMENT,
defectdescription VARCHAR(255));

CREATE TABLE Log(  
id     INTEGER PRIMARY KEY AUTOINCREMENT,
userid INTEGER,
carrierid INTEGER,
distance INTEGER,
codriver VARCHAR(45),
destination VARCHAR(45),
notes TEXT,
date VARCHAR(10),
timezone VARCHAR(45),
driverStatus VARCHAR(96),
annotation TEXT,
vehicleStatus VARCHAR(96),
calculatedStatus VARCHAR(96),
origin VARCHAR(255),
approved INTEGER,
FOREIGN KEY (userid) REFERENCES User(id),
FOREIGN KEY (carrierid) REFERENCES Carrier(id));

CREATE TABLE approval(
id     INTEGER AUTOINCREMENT,
userid INTEGER ,
date varchar(15),
isapproved bit default 0,
PRIMARY KEY (id),
FOREIGN KEY (logid) REFERENCES Log(id)
FOREIGN KEY (vehicleid) REFERENCES Vehicle(id));

CREATE TABLE Log2shippingdoc(  
id     INTEGER PRIMARY KEY AUTOINCREMENT,
logid INTEGER ,
shippingdocid INTEGER,
FOREIGN KEY (logid) REFERENCES Log(id)
FOREIGN KEY (shippingdocid) REFERENCES Shippingdoc(id));

CREATE TABLE Log2trailers(
id     INTEGER PRIMARY KEY AUTOINCREMENT ,
logid INTEGER , 
trailerid INTEGER,
FOREIGN KEY (logid) REFERENCES Log(id)
FOREIGN KEY (trailerid) REFERENCES Trailer(id));

CREATE TABLE Log2vehicle(  
id     INTEGER PRIMARY KEY AUTOINCREMENT,
logid INTEGER ,  
vehicleid INTEGER,
FOREIGN KEY (logid) REFERENCES Log(id)
FOREIGN KEY (vehicleid) REFERENCES Vehicle(id));

CREATE TABLE Shippingdoc(  
id     INTEGER PRIMARY KEY AUTOINCREMENT,
shippingdocno INTEGER
);

CREATE TABLE Vehicle(
id   INTEGER PRIMARY KEY AUTOINCREMENT,
name VARCHAR(45) ,
odometerUnit VARCHAR(45));

CREATE TABLE Trailer(
id   INTEGER PRIMARY KEY AUTOINCREMENT ,
name VARCHAR(45));

CREATE TABLE User(  
id   INTEGER PRIMARY KEY AUTOINCREMENT,
username VARCHAR(45) UNIQUE ,
password VARCHAR(45),
firstname VARCHAR(45),
lastname VARCHAR(45),
email VARCHAR(45) UNIQUE,
mobile VARCHAR(15),
driverid VARCHAR(45),
dotNumber VARCHAR(45),
homeTimezone INTEGER,
cycle INTEGER,
licenseno VARCHAR(45),
loggedIn INTEGER,
firstTimeUser INTEGER,
loggingSwitch INTEGER,
authCode VARCHAR(45)
);


create table cycle (
	id int NOT NULL AUTO_INCREMENT,
	name varchar(255),
        PRIMARY KEY (id)
);

CREATE TABLE timezone (
    id int NOT NULL AUTOINCREMENT,
    name varchar(255) ,
    timediff varchar(10),
    PRIMARY KEY (ID)
);

INSERT INTO timezone(name, timediff ) VALUES
( "PST", "UTC-8" ), ( "PDT", "UTC-7" ), ("MST","UTC-7"),("MDT","UTC-6"),("CST","UTC-6"),("CDT","UTC-5"),("EST","UTC-5"),("EDT","UTC-4");

CREATE TABLE pendingUpdate(  
id    INTEGER PRIMARY KEY AUTOINCREMENT,
operation VARCHAR(10),
data TEXT);

