//
//  CarrierTwoTextTableViewCell.h
//  MaTrackHOSV1
//
//  Created by sieva networks on 11/3/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SectionRows.h"
@class CarrierTwoTextTableViewCell;
@protocol CarrierTwoTextTableViewCellDelegate<NSObject>
-(void)onTapStateForCell:(CarrierTwoTextTableViewCell *)objCell withRow:(SectionRows *)objRow andTextField:(UITextField *)txtActiveField;
-(void)onSelectStateForCell:(CarrierTwoTextTableViewCell *)objCell withRow:(SectionRows *)objRow andTextField:(UITextField *)txtActiveField;
-(void)sendChangedRowDataCarrier:(SectionRows *)objRow fortheCell:(CarrierTwoTextTableViewCell *)objCell withTextField:(UITextField *)txtActiveField;
-(void)scrollTableViewForTextFieldCarrier:(UITextField *)txtActiveField withCell:(CarrierTwoTextTableViewCell *)objCell;
@end
@interface CarrierTwoTextTableViewCell : UITableViewCell<UITextFieldDelegate, UIPickerViewDataSource, UIPickerViewDelegate>
{
    NSArray *states, *statecodes;
    SectionRows *objCurrentRow;
    int selectedRow;
}
@property (nonatomic, strong) IBOutlet UITextField *txtState;
@property (nonatomic, strong) IBOutlet UITextField *txtZip;
@property (nonatomic, strong) IBOutlet UITextField *txtCity;
@property (nonatomic, weak) id<CarrierTwoTextTableViewCellDelegate> delegate;
@property (nonatomic, strong) UIPickerView *statePickerView;
-(void)setCellWithData:(SectionRows *)objRow;
@end
