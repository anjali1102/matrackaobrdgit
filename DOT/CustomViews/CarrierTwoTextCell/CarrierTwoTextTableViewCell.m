//
//  CarrierTwoTextTableViewCell.m
//  MaTrackHOSV1
//
//  Created by sieva networks on 11/3/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import "CarrierTwoTextTableViewCell.h"
#import "customPickerView.h"

@implementation CarrierTwoTextTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    states = [[NSArray alloc] initWithObjects:@"Alabama",@"Alaska",@"Arizona",@"Arkansas",@"California",@"Colorado",@"Connecticut",@"Delaware",@"District Of Columbia",@"Florida",@"Georgia",@"Hawaii",@"Idaho",@"Illinois",@"Indiana",@"Iowa",@"Kansas",@"Kentucky",@"Louisiana",@"Maine",@"Maryland",@"Massachusetts",@"Michigan",@"Minnesota",@"Mississippi",@"Missouri",@"Montana",@"Nebraska",@"Nevada",@"New Hampshire",@"New Jersey",@"New Mexico",@"New York",@"North Carolina",@"North Dakota",@"Ohio",@"Oklahoma",@"Oregon",@"Pennsylvania",@"Rhode Island",@"South Carolina",@"South Dakota",@"Tennessee",@"Texas",@"Utah",@"Vermont",@"Virginia",@"Washington",@"West Virginia",@"Wisconsin",@"Wyoming",nil];
    
    statecodes = [[NSArray alloc] initWithObjects:@"AL",@"AK",@"AZ",@"AR",@"CA",@"CO",@"CT",@"DE",@"DC",@"FL",@"GA",@"HI",@"ID",@"IL",@"IN",@"IA",@"KS",@"KY",@"LA",@"ME",@"MD",@"MA",@"MI",@"MN",@"MS",@"MO",@"MT",@"NE",@"NV",@"NH",@"NJ",@"NM",@"NY",@"NC",@"ND",@"OH",@"OK",@"OR",@"PA",@"RI",@"SC",@"SD",@"TN",@"TX",@"UT",@"VT",@"VA",@"WA",@"WV",@"WI",@"WY", nil];
    _statePickerView    =   [[UIPickerView alloc] init];
    _statePickerView.delegate = self;
    _statePickerView.dataSource = self;
    _txtState.inputView = _statePickerView;
    selectedRow = 0;
    
    UIToolbar *toolBar1=[[UIToolbar alloc]initWithFrame:CGRectMake(0,0,230,44)];
    [toolBar1 setTintColor:[UIColor colorWithRed:0.125 green:0.525 blue:0.125 alpha:1.0]];
    UIBarButtonItem *doneBtn1=[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(doneAction)];
    UIBarButtonItem *spac1=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [toolBar1 setItems:[NSArray arrayWithObjects:spac1, doneBtn1,nil]];
    [_txtState  setInputAccessoryView:toolBar1];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)setCellWithData:(SectionRows *)objRow{
    objCurrentRow = objRow;
    _txtCity.text = objRow.strRowTitle;
    _txtState.text = objRow.strPlaceHolder;
    _txtZip.text = objRow.strRowContent;
}
- (void)doneAction{
    objCurrentRow.strPlaceHolder = statecodes[selectedRow];
    objCurrentRow.strRowTitle = _txtCity.text;
    objCurrentRow.strRowContent = _txtZip.text;
    [self.delegate onSelectStateForCell:self withRow:objCurrentRow andTextField:_txtState];
    [_txtState resignFirstResponder];

}
#pragma mark    - <UITextField Delegate>
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
//    if(textField == _txtState){
//        objCurrentRow.strRowTitle = _txtCity.text;
//        objCurrentRow.strRowContent = _txtZip.text;
//        [self.delegate onSelectStateForCell:self withRow:objCurrentRow andTextField:textField];
//    }else{
            [self.delegate scrollTableViewForTextFieldCarrier:textField withCell:self];

  //  }
    return YES;
}
- (void)textFieldDidEndEditing:(UITextField *)textField{
//    if(textField == _txtState )
//        objCurrentRow.strPlaceHolder = textField.text;
//    else if (textField == _txtCity)
//        objCurrentRow.strRowTitle = textField.text;
//    else if (textField == _txtZip)
//        objCurrentRow.strRowContent = textField.text;
  //  objCurrentRow.strPlaceHolder = _txtState.text;
   // if(textField != _txtState){
        objCurrentRow.strPlaceHolder = _txtState.text;
        objCurrentRow.strRowContent = _txtZip.text;
        objCurrentRow.strRowTitle =  _txtCity.text;
    
  //  }
    if(textField.tag == 1234){
        
    }else{
          [self.delegate sendChangedRowDataCarrier:objCurrentRow fortheCell:self withTextField:textField];
    }
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
#pragma mark    - <UIPickerView Delegate & DataSource>
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return states.count;
}
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    return states[row];
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    selectedRow =(int) row;
    objCurrentRow.strPlaceHolder = statecodes[row];
    _txtState.text =  statecodes[row];
    objCurrentRow.strPlaceHolder = statecodes[row];//_txtState.text;
    objCurrentRow.strRowContent = _txtZip.text;
    objCurrentRow.strRowTitle = _txtCity.text;
    
  //  [self.delegate onSelectStateForCell:self withRow:objCurrentRow andTextField:_txtState];
}
@end
