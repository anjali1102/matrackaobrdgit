//
//  customPickerView.h
//  MaTrackHOSV1
//
//  Created by sieva networks on 11/3/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol customPickerViewDelegate <NSObject>
-(void)onSelectState:(NSString *)strState withAbbreviation:(NSString *)strStateAbbr;
@end
@interface customPickerView : UIView<UIPickerViewDelegate, UIPickerViewDataSource>
@property (nonatomic, strong) IBOutlet UIPickerView *carrierPickerView;
@property (nonatomic, strong) NSArray  *arrayPickerData;
@property (nonatomic, strong) NSArray  *arrayPickerDataAbbr;
@property (nonatomic, weak) id<customPickerViewDelegate> delegate;
@end
