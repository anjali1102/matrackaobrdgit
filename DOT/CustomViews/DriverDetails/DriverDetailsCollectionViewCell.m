//
//  DriverDetailsCollectionViewCell.m
//  MaTrackHOSV1
//
//  Created by sieva networks on 11/10/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import "DriverDetailsCollectionViewCell.h"

@implementation DriverDetailsCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(void)setCellWithData:(SectionRows *)objRow{
    switch (objRow.rowType) {
        case row_type_dot_report_driver:
            lblKey.text = @"Driver";
            lblValue.text = objRow.strRowContent;
            break;
        case row_type_dot_report_distance:
            lblKey.text = @"Distance";
            lblValue.text = objRow.strRowContent;
            break;
        case row_type_dot_report_cycle:
            lblKey.text = @"Cycle";
            lblValue.text = objRow.strRowContent;
            break;
        case row_type_dot_report_codriver:
            lblKey.text = @"Co-driver";
            lblValue.text = objRow.strRowContent;
            break;
        case row_type_dot_report_shipping:
            lblKey.text = @"Shipping Docs";
            lblValue.text = objRow.strRowContent;
            break;
        case row_type_dot_report_total_hours:
            lblKey.text = @"Total Hours";
            lblValue.text = objRow.strRowContent;
            break;
        case row_type_dot_report_available_hours:
            lblKey.text = @"Remaining Hours";
            lblValue.text = objRow.strRowContent;
            break;
        case row_type_dot_report_carrier:
            lblKey.text = @"Carrier";
            lblValue.text = objRow.strRowContent;
            break;
        case row_type_dot_report_vehicle:
            lblKey.text = @"Vehicle";
            lblValue.text = objRow.strRowContent;
            break;
        case row_type_dot_report_main_office:
            lblKey.text = @"Main Office";
            lblValue.text = objRow.strRowContent;
            break;
        case row_type_dot_report_trailer:
            lblKey.text = @"Trailer";
            lblValue.text = objRow.strRowContent;
            break;
        case row_type_dot_report_restart:
            lblKey.text = @"Restart";
            lblValue.text = objRow.strRowContent;
            break;
        case row_type_dot_report_worked_today:
            lblKey.text = @"Worked Today";
            lblValue.text = objRow.strRowContent;
            break;
        default:
            break;
    }
}
@end
