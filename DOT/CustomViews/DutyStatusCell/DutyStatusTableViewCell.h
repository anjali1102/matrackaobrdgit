//
//  DutyStatusTableViewCell.h
//  MaTrackHOSV1
//
//  Created by sieva networks on 10/30/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SectionRows.h"
@interface DutyStatusTableViewCell : UITableViewCell
{
    IBOutlet UILabel *lblDutyStatus;
    IBOutlet UIImageView *imgvwDutyStatus;
    IBOutlet UIView *bgView;
}
-(void)setCellData:(SectionRows *)objRow;
@end
