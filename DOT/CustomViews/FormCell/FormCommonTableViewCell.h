//
//  FormCommonTableViewCell.h
//  MaTrackHOSV1
//
//  Created by sieva networks on 10/31/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SectionRows.h"
@class FormCommonTableViewCell;

@protocol FormCommonTableViewCellDelegate<NSObject>
-(void)sendChangedRowData:(SectionRows *)objRow fortheCell:(FormCommonTableViewCell *)objCell withTextField:(UITextField *)txtActiveField;
-(void)scrollTableViewForTextField:(UITextField *)txtActiveField withCell:(FormCommonTableViewCell *)objCell;
@end

@interface FormCommonTableViewCell : UITableViewCell<UITextFieldDelegate>
{
    IBOutlet UILabel *lblRowTitle;
    IBOutlet UITextField *txtRowContent;
    IBOutlet NSLayoutConstraint *constraintlblHeight;
    SectionRows *row;
}
@property (nonatomic) id<FormCommonTableViewCellDelegate> delegate;
-(void)setCellWithData:(SectionRows *)objRow;
@end
