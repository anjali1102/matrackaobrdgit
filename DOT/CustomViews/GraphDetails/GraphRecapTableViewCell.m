//
//  GraphRecapTableViewCell.m
//  MaTrackHOSV1
//
//  Created by sieva networks on 10/23/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import "GraphRecapTableViewCell.h"

@implementation GraphRecapTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    flag_in1=0;
    is_editing=0;
    multiplier1=0;
    _tblRecap.layer.borderWidth = 1.0f;
    _tblRecap.layer.borderColor = [UIColor blackColor].CGColor;
    [_tblRecap registerNib:[UINib nibWithNibName:@"RecapTableViewCell" bundle:nil] forCellReuseIdentifier:@"RecapTableViewCell"];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
#pragma mark    - <UITableView Delegates & DataSources>
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return arrayTableData.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    Section *objSec = [arrayTableData objectAtIndex:section];
    return objSec.arrayRows.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    Section *objSec = [arrayTableData objectAtIndex:indexPath.section];
    SectionRows *objRow = [objSec.arrayRows objectAtIndex:indexPath.row];
    RecapTableViewCell *recapCell = [tableView dequeueReusableCellWithIdentifier:@"RecapTableViewCell" forIndexPath:indexPath];
    if(recapCell == nil){
        recapCell = [[RecapTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"RecapTableViewCell"];
    }
    recapCell.lblRecapText.text = objRow.strRowContent;
    recapCell.selectionStyle = UITableViewCellSelectionStyleNone;
    return recapCell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 20;
}

-(void)setCellWithData:(SectionRows *)objRow{
    dictRecap = objRow.rowDictionary;
    [self createTableData];
}
#pragma mark    - <Create TableData>

-(void)createTableData{
    arrayTableData = [NSMutableArray new];
  NSArray *  recapTableArray = [dictRecap valueForKey:@"cycleRecap"];
    Section *objSec = [Section new];
    SectionRows *objRow;
    objRow = [SectionRows new];
    objRow.strRowContent = @"RECAP";
    [objSec.arrayRows addObject:objRow];
    
    for (NSArray *arrayRecap in recapTableArray) {
        objRow = [SectionRows new];
        objRow.strRowContent = [NSString stringWithFormat:@"%@  %@", arrayRecap[0],arrayRecap[1]];
        [objSec.arrayRows addObject:objRow];
    }
    [arrayTableData addObject:objSec];
    [_graph setNeedsDisplay];
    [_tblRecap reloadData];
}
@end
