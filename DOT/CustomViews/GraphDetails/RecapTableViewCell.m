//
//  RecapTableViewCell.m
//  MaTrackHOSV1
//
//  Created by sieva networks on 11/8/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import "RecapTableViewCell.h"

@implementation RecapTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [vwRecapCell.layer setBorderWidth:1.0f];
    [vwRecapCell.layer setBorderColor:[UIColor blackColor].CGColor];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
