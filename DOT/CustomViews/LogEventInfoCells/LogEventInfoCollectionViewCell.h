//
//  LogEventInfoCollectionViewCell.h
//  MaTrackHOSV1
//
//  Created by sieva networks on 11/1/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SectionRows.h"
@interface LogEventInfoCollectionViewCell : UICollectionViewCell
{
    IBOutlet UILabel *lblContent;
    IBOutlet UIView *vwContent;
}
-(void)setCellWithData:(SectionRows *)objRow;
@end
