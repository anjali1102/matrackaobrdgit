//
//  LogEventInfoCollectionViewCell.m
//  MaTrackHOSV1
//
//  Created by sieva networks on 11/1/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import "LogEventInfoCollectionViewCell.h"

@implementation LogEventInfoCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(void)setCellWithData:(SectionRows *)objRow{
    lblContent.text = objRow.strRowTitle;
    if([objRow.strRowContent isEqualToString:@"suggestion"]){
        vwContent.layer.backgroundColor = [UIColor yellowColor].CGColor;
    }else{
        vwContent.layer.backgroundColor = [UIColor whiteColor].CGColor;
    }
    vwContent.layer.borderWidth = 1.0;
}
@end
