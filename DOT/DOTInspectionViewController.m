//
//  DOTInspectionViewController.m
//  MaTrackHOSV1
//
//  Created by sieva networks on 10/24/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import "DOTInspectionViewController.h"
#import "DBManager.h"
#import "HOSRecap.h"
#import "DOTInspection.h"
#import "GraphPoint.h"
#import "NSString+NullCheck.h"
#import "MBProgressHUD.h"
@interface DOTInspectionViewController ()<UIPopoverControllerDelegate>
{
    NSDate *sevenDays;
    BOOL pickerVisible;
    MBProgressHUD *hud;
    NSMutableArray *statusArray;
    NSArray *recapArray;
}
@end

@implementation DOTInspectionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    count1=0;
    next1.enabled=NO;
    pickerVisible = NO;
    dates1 = [[NSMutableArray alloc] init];
    dates_titles = [[NSMutableArray alloc] init];

    next1 = [[UIBarButtonItem alloc]init];
    next1.title=@"Next";
    next1.action=@selector(nextBtn);
    next1.tintColor=[UIColor blackColor];
    next1.image=[UIImage imageNamed:@"datepicker.png"];

    next1.tintColor=[UIColor blackColor];
    back1 = [[UIBarButtonItem alloc] init];
    back1.title=@"Back";
    back1.tintColor=[UIColor blackColor];
    back1.action=@selector(gotomenu);
    
    vwForDatePicker.layer.borderColor = [UIColor colorWithRed:0.125 green:0.525 blue:0.125 alpha:1.0].CGColor;
    vwForDatePicker.layer.borderWidth = 1.0;

    _navigation1.leftBarButtonItems=@[back1,next1];
    commonDataHandler *common=[commonDataHandler new];
    
    NSDate * now = [NSDate date];
    NSDateFormatter *outputFormatter_dot1 = [[NSDateFormatter alloc] init];
    [outputFormatter_dot1 setDateFormat:@"MM/dd/YYYY"];
    NSString *today = [common getCurrentTimeInLocalTimeZone];//[outputFormatter_dot1 stringFromDate:now];
    NSDateFormatter *outputFormatter_dot11 = [[NSDateFormatter alloc] init];
    [outputFormatter_dot11 setDateFormat:@"MM/dd/YYYY"];
    NSDateFormatter *outputFormatter_dot12 = [[NSDateFormatter alloc] init];
    [outputFormatter_dot12 setDateFormat:@"MM-dd-YYYY"];

    titleLabel=[UIButton buttonWithType:UIButtonTypeCustom];
    [titleLabel setTitle:today forState:UIControlStateNormal];
    lblLogDate.text = today;
    titleLabel.frame=CGRectMake(0, 0, 200, 30);
    [titleLabel setTintColor:[UIColor blackColor]];
//    _navigation1.titleView.intrinsicContentSize = UILayoutFittingExpandedSize;
    _navigation1.titleView=titleLabel;
    
    [self getDataForDOT:today];
    
    [_tblDOTInspection registerNib:[UINib nibWithNibName:@"DOTDriverDetailsTableViewCell" bundle:nil] forCellReuseIdentifier:@"DOTDriverDetailsTableViewCell"];
     [_tblDOTInspection registerNib:[UINib nibWithNibName:@"DriverStatusTableViewCell" bundle:nil] forCellReuseIdentifier:@"DriverStatusTableViewCell"];
    [_tblDOTInspection registerNib:[UINib nibWithNibName:@"GraphRecapTableViewCell" bundle:nil] forCellReuseIdentifier:@"GraphRecapTableViewCell"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark    - <UITableView Delegates & DataSources>
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return arrayOfTableData.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    Section *objSec = [arrayOfTableData objectAtIndex:section];
    return objSec.arrayRows.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    Section *objSec = [arrayOfTableData objectAtIndex:indexPath.section];
    SectionRows *objRow = [objSec.arrayRows objectAtIndex:indexPath.row];
    switch (objRow.rowType) {
        case row_type_driver_details:
        {
            DOTDriverDetailsTableViewCell *cell = (DOTDriverDetailsTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"DOTDriverDetailsTableViewCell" forIndexPath:indexPath];
            if (cell == nil) {
                cell = [[DOTDriverDetailsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                          reuseIdentifier:@"DOTDriverDetailsTableViewCell"];
            }
            [cell setCellWithData:objRow];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
        }
        case row_type_graph_recap:
        {
            GraphRecapTableViewCell *cell = (GraphRecapTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"GraphRecapTableViewCell" forIndexPath:indexPath];
            if (cell == nil) {
                cell = [[GraphRecapTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                            reuseIdentifier:@"GraphRecapTableViewCell"];
            }
            [cell setCellWithData:objRow];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
        }
        case row_type_driver_status:
        {
            DriverStatusTableViewCell *cell = (DriverStatusTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"DriverStatusTableViewCell" forIndexPath:indexPath];
            if (cell == nil) {
                cell = [[DriverStatusTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                      reuseIdentifier:@"DriverStatusTableViewCell"];
            }
            [cell setCellWithData:objRow];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
        }
        default:
        {
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
            return cell;
        }
    }
    
    
  //  return nil;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    Section *objSec = [arrayOfTableData objectAtIndex:indexPath.section];
    SectionRows *objRow = [objSec.arrayRows objectAtIndex:indexPath.row];
    if(objRow.rowType == row_type_driver_details)
        return 220;
    else if (objRow.rowType == row_type_graph_recap)
        return (170+recapArray.count*20);
    else if (objRow.rowType == row_type_driver_status)
        return 50+(statusArray.count*50);
    else
        return UITableViewAutomaticDimension;
}
- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}
#pragma mark    - <Save as PDF>
-(IBAction)saveAsPDFAction:(id)sender{
  /*      NSMutableData* pdf = [NSMutableData data];
        UIGraphicsBeginPDFContextToData(pdf, CGRectZero, nil);
        UIGraphicsBeginPDFPage();
        CGContextRef context = UIGraphicsGetCurrentContext();

    NSMutableArray *arrayViews = [NSMutableArray new];
    [arrayViews addObject:vwHeading];
    [arrayViews addObject:_tblDOTInspection];
    for(UIView *view in arrayViews) {
        [view.layer renderInContext:context];
        CGContextTranslateCTM(context, 0, view.bounds.size.height);
    }
    UIGraphicsEndPDFContext();
   */
}
-(IBAction)backAction:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}
#pragma mark    - <Custom methods>
-(void)createTableDataForDot:(NSMutableDictionary *)dotDict recapValues:(NSMutableDictionary *)recapDict andStatusValues:(NSString *)strStatus withArray:(NSArray *)dbArray{

    NSString *strDrivername = [NSString stringWithFormat:@"%@ %@", [[dotDict valueForKey:@"firstname"] NullCheck], [[dotDict valueForKey:@"lastname"] NullCheck]];
    NSMutableDictionary *dictDriverDetails = [NSMutableDictionary new];
    [dictDriverDetails setValue:[[dotDict valueForKey:@"carriername"] NullCheck] forKey:@"carrier"];
    [dictDriverDetails setValue:strDrivername forKey:@"driver"];
    [dictDriverDetails setValue:[[dotDict valueForKey:@"distance"] NullCheck] forKey:@"distance"];
    [dictDriverDetails setValue:[[dotDict valueForKey:@"cycle"] NullCheck] forKey:@"cycle"];
    [dictDriverDetails setValue:[[dotDict valueForKey:@"carriermain"] NullCheck] forKey:@"mainOffice"];
    [dictDriverDetails setValue:[[dotDict valueForKey:@"codriver"] NullCheck] forKey:@"codriver"];
    [dictDriverDetails setValue:[[dotDict valueForKey:@"shippingdocno"] NullCheck] forKey:@"shippingdocno"];
    [dictDriverDetails setValue:dbArray[0][6] forKey:@"trailer"];
    [dictDriverDetails setValue:dbArray[0][5] forKey:@"vehicle"];
   // NSArray *recapTableArray;
    if(recapDict!= nil){
        NSString *strRestartDate = [[[recapDict valueForKey:@"restartDate"] valueForKey:@"date"] NullCheck];
        [dictDriverDetails setValue:[[recapDict valueForKey:@"remaining"] NullCheck] forKey:@"hoursAvailable"];
        [dictDriverDetails setValue:[[recapDict valueForKey:@"workedHours"] NullCheck] forKey:@"workedToday"];
        [dictDriverDetails setValue:[[recapDict valueForKey:@"cycleHours"] NullCheck] forKey:@"totalHours"];
        [dictDriverDetails setValue:strRestartDate forKey:@"restart"];
        recapArray = [recapDict valueForKey:@"cycleRecap"];
       
    }

    arrayOfTableData = [NSMutableArray new];
    Section *objSec = [Section new];
    
    SectionRows *objRow;
    
    objRow = [SectionRows new];
    objRow.rowType = row_type_driver_details;
    objRow.rowDictionary = dictDriverDetails;
    [objSec.arrayRows addObject:objRow];
    
    objRow = [SectionRows new];
    objRow.rowType = row_type_graph_recap;
    objRow.rowDictionary = recapDict;
    [objSec.arrayRows addObject:objRow];
    
    objRow = [SectionRows new];
    objRow.rowType = row_type_driver_status;
    objRow.rowArray = dbArray;
    objRow.rowMutbleArray = statusArray;
    [objSec.arrayRows addObject:objRow];
    
    [arrayOfTableData addObject:objSec];
    dispatch_async(dispatch_get_main_queue(), ^{
        [hud hideAnimated:YES];
        [_tblDOTInspection reloadData];
    });
}
#pragma mark    - <Date picker & help - Old>
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    if([[self.view subviews] containsObject:transparentView_dotins]){
        [transparentView_dotins removeFromSuperview];
        
    }
}

-(void)gotomenu{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self dismissViewControllerAnimated:YES completion:nil];
    });
    
}

-(IBAction)datePickerDoneAction:(id)sender{
    
    _addBannerDistanceFromBottomConstraint.constant = -200;
    [UIView animateWithDuration:7
                     animations:^{
                         [vwForDatePicker setNeedsLayout];
                         NSDate *date = DatePicker.date;
                         
                         NSDateFormatter *_dateFormat = [[NSDateFormatter alloc]init];
                         
                         [_dateFormat setDateFormat:@"MMM dd yyyy"];
                         NSLog(@"%@", [_dateFormat stringFromDate:date]);
                         lblLogDate.text = [_dateFormat stringFromDate:date];
                         [titleLabel setTitle:[_dateFormat stringFromDate:date] forState:UIControlStateNormal];
                     }];
    NSDate *date = DatePicker.date;
    
    NSDateFormatter *_dateFormat = [[NSDateFormatter alloc]init];
    
    [_dateFormat setDateFormat:@"MMM dd yyyy"];
    NSLog(@"%@", [_dateFormat stringFromDate:date]);
    lblLogDate.text = [_dateFormat stringFromDate:date];
    [titleLabel setTitle:[_dateFormat stringFromDate:date] forState:UIControlStateNormal];
    [_dateFormat setDateFormat:@"MM/dd/YYYY"];
    NSString *selectedDate = [_dateFormat stringFromDate:date];
    [self getDataForDOT:selectedDate];
}
-(IBAction)datePickerChanged:(id)sender{
//    NSDate *date = DatePicker.date;
//
//    NSDateFormatter *_dateFormat = [[NSDateFormatter alloc]init];
//
//    [_dateFormat setDateFormat:@"MMM dd yyyy"];
//    NSLog(@"%@", [_dateFormat stringFromDate:date]);
//    lblLogDate.text = [_dateFormat stringFromDate:date];
//    [titleLabel setTitle:[_dateFormat stringFromDate:date] forState:UIControlStateNormal];
//    [_dateFormat setDateFormat:@"MM/dd/YYYY"];
//    NSString *selectedDate = [_dateFormat stringFromDate:date];
//    [self getDataForDOT:selectedDate];
    
}

-(void)nextBtn{
    sevenDays = [[NSDate date] dateByAddingTimeInterval:-60*60*24*7];

    [DatePicker setDatePickerMode:UIDatePickerModeDate];
    [DatePicker setMaximumDate:[NSDate date]];
    [DatePicker setMinimumDate:sevenDays];
    DatePicker.backgroundColor = [UIColor whiteColor];
    _addBannerDistanceFromBottomConstraint.constant = 0;
    [UIView animateWithDuration:4
                     animations:^{
                         [vwForDatePicker setNeedsLayout];
                     }];
}
-(void)getDataForDOT:(NSString *)dateValue{
    dispatch_async(dispatch_get_main_queue(), ^{
        hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    });
 //   __weak __typeof(self) weakSelf = self;  // New C99 uses __typeof(..)
    
    // Heavy work dispatched to a separate thread
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSDictionary *token;
        NSMutableDictionary *whereToken;
        DBManager *objDB= [[DBManager alloc] initWithDatabaseFilename:@"hosDB.sql"];
        NSArray *userInfoArray=[[NSArray alloc]initWithArray: [objDB getUserId]];
        if([userInfoArray count]!=0){
            int userId = [userInfoArray[0][0] intValue];
            token=@{@"date":[NSString stringWithFormat:@"%@",dateValue],@"id":[NSString stringWithFormat:@"%d",userId],};
            whereToken=[[NSMutableDictionary alloc] initWithDictionary:token];
            NSMutableDictionary * dotValues=[[NSMutableDictionary alloc]initWithDictionary:[objDB getDOTInspectionData:whereToken]];
            token=@{@"columnName":@"id",@"entry":[NSString stringWithFormat:@"%d",userId]};
            whereToken=[[NSMutableDictionary alloc] initWithDictionary:token];

            token=@{@"userid":[NSString stringWithFormat:@"%d",userId],@"date":[NSString stringWithFormat:@"%@",dateValue]};
            ;
            whereToken=[[NSMutableDictionary alloc] initWithDictionary:token];
            NSArray* dataFromDB=[[NSArray alloc]initWithArray:[objDB getLogEditData:whereToken]];
            NSString *statusString=@"";
            for(int j=0;j<[dataFromDB count];j++){
                    if([dataFromDB[j][1] isEqualToString:@""])
                        statusString= [statusString stringByAppendingString:[NSString stringWithFormat:@"1"]];
                    else
                        statusString= [statusString stringByAppendingString:[NSString stringWithFormat:@"%@",[[dataFromDB objectAtIndex:j] objectAtIndex:1]]];
            }
            NSString* charStr=[@"" stringByPaddingToLength:96 withString:@"1" startingAtIndex:0];
            if([dataFromDB count]==0)
                statusString=charStr;
            else
            {
                if([statusString isEqualToString:@""])
                    statusString=charStr;
                
            }
            commonDataHandler *common=[commonDataHandler new];
            statusString=[common modifyData:statusString date:dateValue];
            HOSRecap *objRecap=[HOSRecap new];
            NSArray*dateList=[[NSArray alloc] initWithArray:[dateValue componentsSeparatedByString:@"/"]];
            [objRecap getRecap:[NSString stringWithFormat:@"%@-%@-%@ 00:00:00",dateList[2],dateList[0],dateList[1]]];
            NSMutableDictionary * recapForCycle=[[NSMutableDictionary alloc]initWithDictionary:[objRecap returnRecapData:[NSString stringWithFormat:@"%@-%@-%@ 00:00:00",dateList[2],dateList[0],dateList[1]]]];
            
            [self generateGraphPoint:statusString];
            [self createTableDataForDot:dotValues recapValues:recapForCycle andStatusValues:statusString withArray:dataFromDB];
            
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [hud hideAnimated:YES];
                [_tblDOTInspection reloadData];
            });
        }

    });
    

}

-(void)generateGraphPoint:(NSString *)statusString{
    [dataarray5 removeAllObjects];
    graphDataOperations *obj=[graphDataOperations new];
    NSMutableArray *dict=[obj generateGraphPoints:statusString];
        for (int i=0; i<[dict count]; i++) {
            
            //             NSLog(@"posx:%@,%d",[dict[0] objectForKey:@"posx"],i);
            float posx=[[dict[i] objectForKey:@"posx"] floatValue];
            float x1 = posx - 40.0;
            int x2 = x1/12.25;
            int x3 = x2/4.0;
            int x4 = x2%4;
            
            NSString *minutes;
            if (15*x4 <15) {
                minutes=@"00";
            }
            else if(15*x4 >=15 && 15*x4 <30){
                minutes=@"15";
            }else if(15*x4 >=30 && 15*x4 <45){
                minutes=@"30";
            }else if(15*x4 >=45 && 15*x4 <60){
                minutes=@"45";
            }
            NSString *posx_1 = [NSString stringWithFormat:@"%d:%@",x3,minutes];
            
            // float time = [posx_1 floatValue];
            int event_no =(int)[[dict[i] objectForKey:@"posy"] integerValue];
            NSString * event;
            if (event_no == 245) {
                event=@"ON";
            }
            else if(event_no == 125){
                event=@"SB";
                
            }else if (event_no == 185){
                event = @"D";
            }else if (event_no == 65){
                event = @"OFF";
            }
            NSArray *final_data = [[NSArray alloc] initWithObjects:posx_1,event, nil];
            [dataarray5 addObject:final_data];
        }
    
    int length=(int)[dataarray5 count];
    statusArray = [NSMutableArray new];
    for (int k=0; k<length; k++) {
        if ((k%2==0)&&(k!=length-1)) {
            NSString *event = dataarray5[k][1];
            NSString *start = dataarray5[k][0];
            NSString *end = dataarray5[k+1][0];
            float start_float = [self getPosx:start];
            float end_float = [self getPosx:end];
            float diff = end_float-start_float;
            NSString *duration = [self getDuration:diff];
            NSArray *details = @[start,event,duration,end];
            [statusArray addObject:details];
        }
    }
    NSLog(@"%ld",statusArray.count);
    
    }
-(float)getPosx:(NSString *)time1{
    NSString *x12=time1;
    NSArray *time = [x12 componentsSeparatedByString:@":"];
    int x4 =(int) [time[1] integerValue]/15;
    int x2 =(int) [time[0] integerValue]*4+x4;
    float x1 = 12.25*x2;
    float posx = x1+40.0;
    return posx;
    
}
-(NSString *)getDuration:(float)difference{
    float x1;
    x1 = difference ;
    int x2 = x1/12.25;
    int x3 = x2/4.0;
    int x4 = x2%4;
    
    NSString *minutes;
    if (15*x4 <15) {
        minutes=@"00";
    }
    else if(15*x4 >=15 && 15*x4 <30){
        minutes=@"15";
    }else if(15*x4 >=30 && 15*x4 <45){
        minutes=@"30";
    }else if(15*x4 >=45 && 15*x4 <60){
        minutes=@"45";
    }
    NSString *posx_1 = [NSString stringWithFormat:@"%d:%@hr",x3,minutes];
    return posx_1;
    
    
}
@end
