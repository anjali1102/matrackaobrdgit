//
//  GraphPoint.m
//  MaTrackHOSV1
//
//  Created by sieva networks on 10/23/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import "GraphPoint.h"

@implementation GraphPoint
@synthesize date;
@synthesize slotno;
@synthesize status;
@synthesize location;
@synthesize annotation;
@synthesize origin;
@synthesize vehicle;
@synthesize trailer;

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self initializeData];
    }
    return self;
}
-(void)initializeData{
    date        = @"";
    slotno      = @"";
    status      = @"";
    location    = @"";
    annotation  = @"";
    origin      = @"";
    vehicle     = @"";
    trailer     = @"";
}
@end
