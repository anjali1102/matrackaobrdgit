//
//  Recap.h
//  MaTrackHOSV1
//
//  Created by sieva networks on 10/23/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Recap : NSObject
@property (nonatomic, strong) NSString *sno;
@property (nonatomic, strong) NSString *date;
@property (nonatomic, strong) NSString *hours;
@end
