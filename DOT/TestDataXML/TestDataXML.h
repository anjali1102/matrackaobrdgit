//
//  TestDataXML.h
//  MaTrackHOSV1
//
//  Created by sieva networks on 10/26/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Recap.h"
#import "GraphPoint.h"
@class DOTInspection;
@interface TestDataXML : NSObject
@property (nonatomic, strong) NSString *strXMLTemplate;
- (NSString *)dataFilePath:(BOOL)forSave;
- (NSString *)loadXML;
- (NSString *)saveDOTInspToXML:(DOTInspection *)DotInsp;
@end
