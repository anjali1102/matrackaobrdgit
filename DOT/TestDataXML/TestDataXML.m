//
//  TestDataXML.m
//  MaTrackHOSV1
//
//  Created by sieva networks on 10/26/17.

#import "TestDataXML.h"
#import "GDataXMLNode.h"
#import "DOTInspection.h"
@implementation TestDataXML
- (NSString *)dataFilePath:(BOOL)forSave {
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                         NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0]; //@"/Users/sievanetworks/Desktop/XMLFILES";
    NSString *documentsPath = [documentsDirectory
                               stringByAppendingPathComponent:@"DI.xml"];
    if (forSave || [[NSFileManager defaultManager] fileExistsAtPath:documentsPath]) {
        return documentsPath;
    } else {
        return [[NSBundle mainBundle] pathForResource:@"DI" ofType:@"xml"];
    }
    
}
- (NSString *)loadXML{
    
    NSString *filePath = [self dataFilePath:FALSE];
    NSData *xmlData = [[NSMutableData alloc] initWithContentsOfFile:filePath];
    NSError *error;
    GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithData:xmlData
                                                           options:0 error:&error];
    if (doc == nil) { return nil; }
    
    //NSLog(@"%@", doc.rootElement);
    
     NSString* strXMLHeader = [NSString stringWithFormat:@"<?xml version=\"%@\" encoding=\"%@\"?>", @"1.0", @"UTF-8"];
//    NSString* strXMLHeader = @"<?xml version="1.0" encoding="UTF-8"?>";
    NSString* strResultXML = [strXMLHeader stringByAppendingString:doc.rootElement.XMLString];
    return  strResultXML;
    
}
- (NSString *)saveDOTInspToXML:(DOTInspection *)DotInsp {
    
    GDataXMLElement * DIElement = [GDataXMLNode elementWithName:@"DI"];
    
    //Common Element
    GDataXMLElement * commonElement = [GDataXMLNode elementWithName:@"common"];
    GDataXMLElement * dataElement = [GDataXMLNode elementWithName:@"data" stringValue:@"common"];
    GDataXMLElement * nameElement = [GDataXMLNode elementWithName:@"drivername" stringValue:DotInsp.driverName];
    GDataXMLElement * carrierElement = [GDataXMLNode elementWithName:@"carriername" stringValue:DotInsp.carrierName];
    GDataXMLElement * distanceElement = [GDataXMLNode elementWithName:@"distance" stringValue:DotInsp.distance];
    GDataXMLElement * cycleElement = [GDataXMLNode elementWithName:@"cycle" stringValue:DotInsp.cycle];
    GDataXMLElement * totalhoursElement = [GDataXMLNode elementWithName:@"totalhours" stringValue:DotInsp.totalHours];
    GDataXMLElement * hoursavailabletodayElement = [GDataXMLNode elementWithName:@"hoursavailabletoday" stringValue:DotInsp.hoursAvailableToday];
    GDataXMLElement * mainofficeaddressElement = [GDataXMLNode elementWithName:@"mainofficeaddress" stringValue:DotInsp.mainOfficeAddress];
    GDataXMLElement * hometerminaladdressElement = [GDataXMLNode elementWithName:@"hometerminaladdress" stringValue:DotInsp.homeTerminalAddress];
    GDataXMLElement * restartElement = [GDataXMLNode elementWithName:@"restart" stringValue:DotInsp.restart];
    GDataXMLElement * totalworkhoursElement = [GDataXMLNode elementWithName:@"totalworkhours" stringValue:DotInsp.totalWorkHours];
    GDataXMLElement * commondateElement = [GDataXMLNode elementWithName:@"commondate" stringValue:DotInsp.commonDate];
    GDataXMLElement * timediffElement = [GDataXMLNode elementWithName:@"timediff" stringValue:DotInsp.timeDiff];
    GDataXMLElement * timezoneElement = [GDataXMLNode elementWithName:@"timezone" stringValue:DotInsp.timezone];
    
    
    [commonElement addChild:dataElement];
    [commonElement addChild:nameElement];
    [commonElement addChild:carrierElement];
    [commonElement addChild:distanceElement];
    [commonElement addChild:cycleElement];
    [commonElement addChild:totalhoursElement];
    [commonElement addChild:hoursavailabletodayElement];
    [commonElement addChild:mainofficeaddressElement];
    [commonElement addChild:hometerminaladdressElement];
    [commonElement addChild:restartElement];
    [commonElement addChild:totalworkhoursElement];
    [commonElement addChild:commondateElement];
    [commonElement addChild:timediffElement];
    [commonElement addChild:timezoneElement];
    [DIElement addChild:commonElement];
    
    //Recap Elements
    GDataXMLElement * recapElement = [GDataXMLNode elementWithName:@"recap"];
    for(Recap *objRecap in DotInsp.recapList) {
        
        GDataXMLElement * dayElement =[GDataXMLNode elementWithName:@"day"];
        GDataXMLElement * snoElement =[GDataXMLNode elementWithName:@"sno" stringValue:objRecap.sno];
        GDataXMLElement * dateElement = [GDataXMLNode elementWithName:@"date" stringValue:objRecap.date];
        GDataXMLElement * hoursElement = [GDataXMLNode elementWithName:@"hours" stringValue:objRecap.hours];
        
        [dayElement addChild:snoElement];
        [dayElement addChild:dateElement];
        [dayElement addChild:hoursElement];
        [recapElement addChild:dayElement];
    }
    [DIElement addChild:recapElement];
    
    //Graphpoints
    GDataXMLElement * graphPointsElement = [GDataXMLNode elementWithName:@"graphpoints"];
    for (GraphPoint *objGraphpoints in DotInsp.graphPointList) {
        GDataXMLElement *slotElement = [GDataXMLNode elementWithName:@"slot"];
//        GDataXMLElement *dateElement = [GDataXMLNode elementWithName:@"date" stringValue:objGraphpoints.date];
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat = @"YYYY-MM-dd HH:mm:ss";
        NSDate *string = [formatter dateFromString:objGraphpoints.date];
      //  NSLog(@"Date DB :- %@",string);
        
        NSDateFormatter *formatterFinal = [[NSDateFormatter alloc] init];
        formatterFinal.dateFormat = @"MM/dd/YYYY";
        NSString *stringDate = [formatterFinal stringFromDate:string];
//        NSLog(@"Date Current :- %@",stringDate);
         GDataXMLElement *dateElement = [GDataXMLNode elementWithName:@"date" stringValue:stringDate];
        
        GDataXMLElement *slotnoElement = [GDataXMLNode elementWithName:@"slotno" stringValue:objGraphpoints.slotno];
        GDataXMLElement *statusElement = [GDataXMLNode elementWithName:@"status" stringValue:objGraphpoints.status];
        GDataXMLElement *locationElement;
        if([objGraphpoints.location isEqualToString:@"(null)"]){
            locationElement = [GDataXMLNode elementWithName:@"location" stringValue:@""];
        }else{
            locationElement = [GDataXMLNode elementWithName:@"location" stringValue:objGraphpoints.location];
        }
        GDataXMLElement *annotationElement;
        if([objGraphpoints.annotation isEqualToString:@"(null)"]){
            annotationElement = [GDataXMLNode elementWithName:@"annotation" stringValue:@""];
        }else{
            annotationElement = [GDataXMLNode elementWithName:@"annotation" stringValue:objGraphpoints.annotation];
        }
        GDataXMLElement *originElement = [GDataXMLNode elementWithName:@"origin" stringValue:objGraphpoints.origin];
        GDataXMLElement *vehicleElement = [GDataXMLNode elementWithName:@"vehicle" stringValue:objGraphpoints.vehicle];
        GDataXMLElement *trailerElement = [GDataXMLNode elementWithName:@"trailer" stringValue:objGraphpoints.trailer];
        [slotElement addChild:dateElement];
        [slotElement addChild:slotnoElement];
        [slotElement addChild:statusElement];
        [slotElement addChild:locationElement];
        [slotElement addChild:annotationElement];
        [slotElement addChild:originElement];
        [slotElement addChild:vehicleElement];
        [slotElement addChild:trailerElement];
        
        [graphPointsElement addChild:slotElement];
    }
    [DIElement addChild:graphPointsElement];
    
    GDataXMLDocument *document = [[GDataXMLDocument alloc]
                                  initWithRootElement:DIElement];
    NSData *xmlData = document.XMLData;
    
    NSString *filePath = [self dataFilePath:TRUE];
    [xmlData writeToFile:filePath atomically:YES];
    
    self.strXMLTemplate = [self loadXML];
    NSString *strLocationXML = [self.strXMLTemplate stringByReplacingOccurrencesOfString:@"<location/>" withString:@"<location></location>"];
    NSString *strAnnotationXML = [strLocationXML stringByReplacingOccurrencesOfString:@"<annotation/>" withString:@"<annotation></annotation>"];
    NSString *strOriginXML = [strAnnotationXML stringByReplacingOccurrencesOfString:@"<origin/>" withString:@"<origin></origin>"];
    NSString *strVehicleXML = [strOriginXML stringByReplacingOccurrencesOfString:@"<vehicle/>" withString:@"<vehicle></vehicle>"];
    NSString *strTrailerXML = [strVehicleXML stringByReplacingOccurrencesOfString:@"<trailer/>" withString:@"<trailer></trailer>"];

    return strTrailerXML;//self.strXMLTemplate;
    
}
@end
