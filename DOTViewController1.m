//
//  DOTViewController1.m
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 8/2/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//

#import "DOTViewController1.h"
#import "Reachability.h"
#import "KeychainItemWrapper.h"
#import "Header.h"
#import "commonDataHandler.h"
#import "MobileCheckIn.h"

UIBarButtonItem *previous1;
UIBarButtonItem *next1;
UIBarButtonItem *back1;
UIBarButtonItem *save1;
int count1=0;
NSMutableArray *dates1;
NSMutableArray *dates_titles;
NSString *currentDate;
UIButton *titleLabel;
UIView *transparentView_dotins;


@interface DOTViewController1 ()
{
    // Add this to the interface in the .m file of your view controller
    Reachability *internetReachableFoo;
    NSString *strXMTemplate;
    BOOL isReAuth;
}

@end

@implementation DOTViewController1
@synthesize savebtn;
@synthesize indicator,dbManager;

- (void)viewDidLoad {
    [super viewDidLoad];
    isReAuth = YES;
    
    dbManager = [[DBManager1 alloc] initWithDatabaseFilename:@"HOSDatabase.sql"];

    count1=0;
    savebtn.hidden=YES;
    _web_dot.delegate=self;
    next1.enabled=NO;
    dates1 = [[NSMutableArray alloc] init];
    dates_titles = [[NSMutableArray alloc] init];
    [_scroller_dot1 setScrollEnabled:YES];
    [_scroller_dot1 setContentSize:(CGSizeMake(600, 3500))];
    CGPoint bottomOffset = CGPointMake(0, 3500-_scroller_dot1.bounds.size.height);
    [_scroller_dot1 setContentOffset:bottomOffset animated:YES];
    _web_dot.scrollView.scrollEnabled=TRUE;
    _web_dot.userInteractionEnabled=TRUE;

    
    next1 = [[UIBarButtonItem alloc]init];
    next1.title=@"Next";
    next1.action=@selector(nextBtn);
    next1.tintColor=[UIColor blackColor];
    next1.image=[UIImage imageNamed:@"next2.png"];
    next1.enabled=NO;
    previous1 = [[UIBarButtonItem alloc] init];
    previous1.title = @"Previous";
    previous1.image = [UIImage imageNamed:@"back2.png"];
    previous1.tintColor=[UIColor blackColor];
    previous1.action=@selector(previousBtn);
    next1.tintColor=[UIColor blackColor];
    back1 = [[UIBarButtonItem alloc] init];
    back1.title=@"Back";
    back1.tintColor=[UIColor blackColor];
    back1.action=@selector(gotomenu);
    save1 = [[UIBarButtonItem alloc] init];
    save1.title=@"Save";
    save1.tintColor=[UIColor blackColor];
    save1.image=[UIImage imageNamed:@"icons/info.png"];
    save1.action=@selector(helpAlert);
    next1.tintColor=[UIColor blackColor];
    _navigation1.leftBarButtonItems=@[back1,previous1];
    _navigation1.rightBarButtonItems=@[save1,next1];
    NSDate * now = [NSDate date];
    NSDateFormatter *outputFormatter_dot1 = [[NSDateFormatter alloc] init];
    [outputFormatter_dot1 setDateFormat:@"MMM dd YYYY"];
    NSString *today = [outputFormatter_dot1 stringFromDate:now];
    NSDateFormatter *outputFormatter_dot11 = [[NSDateFormatter alloc] init];
    [outputFormatter_dot11 setDateFormat:@"MM/dd/YYYY"];
    NSString *today1 = [outputFormatter_dot11 stringFromDate:now];
    NSDateFormatter *outputFormatter_dot12 = [[NSDateFormatter alloc] init];
    [outputFormatter_dot12 setDateFormat:@"MM-dd-YYYY"];
    NSString *today2 = [outputFormatter_dot12 stringFromDate:now];
    
    currentDate=today2;
  //  _navigation1.title=today;
   /* UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 300, 30)];
    UIImageView *datepickerImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    datepickerImage.image = [UIImage imageNamed:@"datepicker.png"];*/
    titleLabel=[UIButton buttonWithType:UIButtonTypeCustom];
    [titleLabel setTitle:today forState:UIControlStateNormal];
    titleLabel.frame=CGRectMake(40, 0, 150, 30);
    [titleLabel setTintColor:[UIColor blackColor]];
    [titleLabel addTarget:self action:@selector(showDatepicker:) forControlEvents:UIControlEventTouchUpInside];
    /*[titleView addSubview:datepickerImage];
    [titleView addSubview:titleLabel];
    UITapGestureRecognizer *tapDate = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showDatepicker:)];
    [titleView addGestureRecognizer:tapDate];*/
    _navigation1.titleView=titleLabel;
    
       // Do any additional setup after loading the view.
    [self loadWeb:today1];
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    // UITouch *touch = [touches anyObject];
    if([[self.view subviews] containsObject:transparentView_dotins]){
        [transparentView_dotins removeFromSuperview];
        
    }
}

-(void)helpAlert{
    CGRect screenSize = [[UIScreen mainScreen] bounds];
    transparentView_dotins  = [[UIView alloc] initWithFrame:screenSize];
    transparentView_dotins.backgroundColor=[[UIColor blackColor] colorWithAlphaComponent:0.6];
    UIView *view1=[back1 valueForKey:@"view"];
    UIView *view2=[next1 valueForKey:@"view"];
    UIView *view3=[previous1 valueForKey:@"view"];
    UIView *view4=_navigation1.titleView;
    

    UIImageView *arrow1=[[UIImageView alloc]initWithFrame:CGRectMake(view1.frame.origin.x+view1.frame.size.width/2-10, view1.frame.size.height+view1.frame.origin.y*3 , 60, 120)];
    [arrow1 setImage:[UIImage imageNamed:@"icons/arrow7.png"]];
    UILabel *dashboard  = [[UILabel alloc] initWithFrame:CGRectMake(view1.frame.origin.x+view1.frame.size.width/2, arrow1.frame.origin.y+arrow1.frame.size.height, screenSize.size.width/2, 20)];
    dashboard.text=@"Back to DOT Menu screen";
    dashboard.numberOfLines=2;
    dashboard.textColor=[UIColor whiteColor];
    dashboard.font=[UIFont fontWithName:@"Arial-BoldMT" size:20.0];
    dashboard.minimumScaleFactor=10./dashboard.font.pointSize;
    dashboard.adjustsFontSizeToFitWidth=YES;
    
    UIImageView *arrow2=[[UIImageView alloc]initWithFrame:CGRectMake(screenSize.size.width/2,savebtn.frame.origin.y-savebtn.frame.size.height, 50, 50)];
    [arrow2 setImage:[UIImage imageNamed:@"icons/arrow6.png"]];
    UILabel *sendDot  = [[UILabel alloc] initWithFrame:CGRectMake(screenSize.origin.x+10, arrow2.frame.origin.y-30, screenSize.size.width, 20)];
    sendDot.text=@"Click here to generate the pdf";
    sendDot.numberOfLines=2;
    sendDot.textColor=[UIColor whiteColor];
    sendDot.font=[UIFont fontWithName:@"Arial-BoldMT" size:20.0];
    sendDot.minimumScaleFactor=10./sendDot.font.pointSize;
    sendDot.adjustsFontSizeToFitWidth=YES;
    
    UIImageView *arrow3=[[UIImageView alloc]initWithFrame:CGRectMake(view2.frame.origin.x+view2.frame.size.width/2, view1.frame.size.height+view1.frame.origin.y*3 , 50, 50)];
    [arrow3 setImage:[UIImage imageNamed:@"icons/arrow3.png"]];
    
    UILabel *nextLabel  = [[UILabel alloc] initWithFrame:CGRectMake(view2.frame.origin.x+view2.frame.size.width/2, arrow3.frame.origin.y+arrow3.frame.size.height, screenSize.size.width, 20)];
    nextLabel.text=@"Next date";
    nextLabel.numberOfLines=2;
    nextLabel.textColor=[UIColor whiteColor];
    nextLabel.font=[UIFont fontWithName:@"Arial-BoldMT" size:20.0];
    nextLabel.minimumScaleFactor=10./nextLabel.font.pointSize;
    nextLabel.adjustsFontSizeToFitWidth=YES;
    
    
    UIImageView *arrow4=[[UIImageView alloc]initWithFrame:CGRectMake(view3.frame.origin.x+view3.frame.size.width/2, view3.frame.size.height+view3.frame.origin.y*3 , 50, 50)];
    [arrow4 setImage:[UIImage imageNamed:@"icons/arrow3.png"]];
    
    UILabel *previousLabel  = [[UILabel alloc] initWithFrame:CGRectMake(view3.frame.origin.x+view3.frame.size.width/2, arrow4.frame.origin.y+arrow4.frame.size.height, screenSize.size.width, 20)];
    previousLabel.text=@"Previous date";
    previousLabel.numberOfLines=2;
    previousLabel.textColor=[UIColor whiteColor];
   previousLabel.font=[UIFont fontWithName:@"Arial-BoldMT" size:20.0];
    previousLabel.minimumScaleFactor=10./previousLabel.font.pointSize;
   previousLabel.adjustsFontSizeToFitWidth=YES;
    
    UIImageView *arrow5=[[UIImageView alloc]initWithFrame:CGRectMake(view4.frame.origin.x+view4.frame.size.width/2, view4.frame.size.height+view4.frame.origin.y*3 , 50, 100)];
    [arrow5 setImage:[UIImage imageNamed:@"icons/arrow3.png"]];
    
    UILabel *pickerLabel  = [[UILabel alloc] initWithFrame:CGRectMake(view3.frame.origin.x+view1.frame.size.width, arrow5.frame.origin.y+arrow5.frame.size.height, screenSize.size.width, 20)];
    pickerLabel.text=@"Click here to select any date";
    pickerLabel.numberOfLines=2;
    pickerLabel.textColor=[UIColor whiteColor];
    pickerLabel.font=[UIFont fontWithName:@"Arial-BoldMT" size:20.0];
    pickerLabel.minimumScaleFactor=10./pickerLabel.font.pointSize;
    pickerLabel.adjustsFontSizeToFitWidth=YES;
    
    [transparentView_dotins addSubview:pickerLabel];
    [transparentView_dotins addSubview:arrow5];
    [transparentView_dotins addSubview:previousLabel];
    [transparentView_dotins addSubview:arrow4];
    
    [transparentView_dotins addSubview:nextLabel];
    [transparentView_dotins addSubview:arrow3];
    [transparentView_dotins addSubview:sendDot];
    [transparentView_dotins addSubview:arrow2];
    
    [transparentView_dotins addSubview: arrow1];
    [transparentView_dotins addSubview:dashboard];
    [self.view addSubview:transparentView_dotins];
 
}

- (void)testInternetConnection
{
    internetReachableFoo = [Reachability reachabilityWithHostname:@"www.google.com"];
    
    // Internet is reachable
    internetReachableFoo.reachableBlock = ^(Reachability*reach)
    {
        // Update the UI on the main thread
        dispatch_async(dispatch_get_main_queue(), ^{
            //BFLog(@"Yayyy, we have the interwebs!");
        });
    };
    
    // Internet is not reachable
    internetReachableFoo.unreachableBlock = ^(Reachability*reach)
    {
        // Update the UI on the main thread
        dispatch_async(dispatch_get_main_queue(), ^{
           // BFLog(@"Someone broke the internet :(");
        });
    };
    
    [internetReachableFoo startNotifier];
}
-(IBAction)showDatepicker:(id)sender{
    
    
    count1=0;
    viewForDatePicker=[[UIView alloc] initWithFrame:CGRectMake(25, 150, self.view.bounds.size.width-50, 200)];
    viewForDatePicker.backgroundColor=[UIColor colorWithRed:0.75 green:0.71 blue:0.75 alpha:1.0];
    viewForDatePicker.layer.borderWidth=2.0;
    viewForDatePicker.layer.borderColor=[[UIColor blackColor] CGColor];
    
    UIToolbar *toolBar=[[UIToolbar alloc]initWithFrame:CGRectMake(0,0,viewForDatePicker.bounds.size.width,44)];
    [toolBar setTintColor:[UIColor blackColor]];
    UIBarButtonItem *doneBtn=[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(selectDate:)];
    UIBarButtonItem *space=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [toolBar setItems:[NSArray arrayWithObjects:space, doneBtn,nil]];
    
  

    datepicker=[[UIDatePicker alloc] initWithFrame:CGRectMake(0, toolBar.bounds.origin.y+toolBar.bounds.size.height, viewForDatePicker.bounds.size.width, viewForDatePicker.bounds.size.height-toolBar.bounds.size.height)];
    datepicker.datePickerMode=UIDatePickerModeDate;
    datepicker.hidden=NO;
    datepicker.date=[NSDate date];
    NSDate *minDate = [[NSDate date] dateByAddingTimeInterval:60*60*24*-90];
    [datepicker setMaximumDate:[NSDate date]];
    [datepicker setMinimumDate:minDate];
   // [datepicker addTarget:self action:@selector(selectDate:) forControlEvents:UIControlEventValueChanged];
    [viewForDatePicker addSubview:datepicker];
    [viewForDatePicker addSubview:toolBar];
  //  datepicker.center=viewForDatePicker.center;
    
    dimm = [[UIView alloc] initWithFrame:CGRectMake(0, 72, self.web_dot.bounds.size.width, self.web_dot.bounds.size.height-72)];
    dimm.backgroundColor=[UIColor colorWithRed:0.92 green:0.91 blue:0.91 alpha:1.0];
    [self.view addSubview:dimm];
    [dimm addSubview:viewForDatePicker];
   // viewForDatePicker.center=dimm.center;
    
    
}

-(void)selectDate:(id)sender{
    [dates1 removeAllObjects];
    [dates_titles removeAllObjects];
    
    NSDateFormatter *outputFormatter_dot1 = [[NSDateFormatter alloc] init];
    [outputFormatter_dot1 setDateFormat:@"MMM dd YYYY"];
    NSString *today = [outputFormatter_dot1 stringFromDate:[datepicker date]];
    NSDateFormatter *outputFormatter_dot11 = [[NSDateFormatter alloc] init];
    [outputFormatter_dot11 setDateFormat:@"MM/dd/YYYY"];
    NSString *today1 = [outputFormatter_dot11 stringFromDate:[datepicker date]];
    NSDateFormatter *outputFormatter_dot12 = [[NSDateFormatter alloc] init];
    [outputFormatter_dot12 setDateFormat:@"MM-dd-YYYY"];
    NSString *today2 = [outputFormatter_dot12 stringFromDate:[datepicker date]];
    NSDate *selected =[datepicker date];
    NSDate *nowdate = [NSDate date];
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [calendar components:NSCalendarUnitDay fromDate:selected toDate:nowdate options:0];
    count1=(int)[components day];
    
    for (int i=0; i<count1; i++) {
        
        NSDate *previous = [nowdate dateByAddingTimeInterval:-i*24*60*60];
        NSDateFormatter *outputFormatter1 = [[NSDateFormatter alloc] init];
        [outputFormatter1 setDateFormat:@"MMM dd YYYY"];
        NSString *previousdate = [outputFormatter1 stringFromDate:previous];
        NSDateFormatter *outputFormatter_prev = [[NSDateFormatter alloc] init];
        [outputFormatter_prev setDateFormat:@"MM/dd/YYYY"];
        NSString *newDateString_prev = [outputFormatter_prev stringFromDate:previous];
        NSDateFormatter *outputFormatter_prev1 = [[NSDateFormatter alloc] init];
        [outputFormatter_prev1 setDateFormat:@"MM-dd-YYYY"];
      
        
        [dates1 addObject:newDateString_prev];
        [dates_titles addObject:previousdate];
    }
    
       currentDate=today2;
    next1.enabled=YES;
     [titleLabel setTitle:today forState:UIControlStateNormal];
    [dimm removeFromSuperview];
    viewForDatePicker.hidden=YES;
    [self loadWeb:today1];
    
}
-(void)gotomenu{
    
      dispatch_async(dispatch_get_main_queue(), ^{
//    UIStoryboard *storyBoard = self.storyboard;
//    UIViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"dotmenu"];
//    [self presentViewController:vc animated:YES completion:nil];
          [self dismissViewControllerAnimated:YES completion:nil];
      });
    
}
- (void)previousBtn {
    NSDateFormatter *outputFormatter_dot11 = [[NSDateFormatter alloc] init];
    [outputFormatter_dot11 setDateFormat:@"MM/dd/YYYY"];
    NSDate *today = [NSDate date];
    if (count1==0) {
        NSDateFormatter *outputFormatter_prev1 = [[NSDateFormatter alloc] init];
        [outputFormatter_prev1 setDateFormat:@"MM/dd/YYYY"];
        NSString *newDateString_prev1 = [outputFormatter_prev1 stringFromDate:today];
        [dates1 addObject:newDateString_prev1];
        
        NSDateFormatter *outputFormatter_prev2 = [[NSDateFormatter alloc] init];
        [outputFormatter_prev2 setDateFormat:@"MMM dd"];
        NSString *newDateString_prev2 = [outputFormatter_prev2 stringFromDate:today];
        [dates_titles addObject:newDateString_prev2];
    }
    count1=count1+1;
      next1.enabled=YES;
    
    NSDate *previous = [today dateByAddingTimeInterval:-count1*24*60*60];
    NSDateFormatter *outputFormatter1 = [[NSDateFormatter alloc] init];
    [outputFormatter1 setDateFormat:@"MMM dd YYYY"];
    NSString *previousdate = [outputFormatter1 stringFromDate:previous];
    NSDateFormatter *outputFormatter_prev = [[NSDateFormatter alloc] init];
    [outputFormatter_prev setDateFormat:@"MM/dd/YYYY"];
    NSString *newDateString_prev = [outputFormatter_prev stringFromDate:previous];
    NSDateFormatter *outputFormatter_prev1 = [[NSDateFormatter alloc] init];
    [outputFormatter_prev1 setDateFormat:@"MM-dd-YYYY"];
    NSString *newDateString_prev1 = [outputFormatter_prev1 stringFromDate:previous];

    [dates1 addObject:newDateString_prev];
    [dates_titles addObject:previousdate];
   // BFLog(@"previous:%@",previousdate);
    [titleLabel setTitle:previousdate forState:UIControlStateNormal];
    //_navigation1.title=previousdate;
    currentDate=newDateString_prev1;
    [self loadWeb:newDateString_prev];
}
-(void)nextBtn{
    
    if ([dates1 count]>0) {
        next1.enabled=YES;
        count1=count1-1;
       
        int last=(int)[dates1 count];
         NSString *last1=dates_titles[last-1];
        NSString *current=dates1[last-1];
       // BFLog(@"nextDate: %@ ...", current);
        [dates1 removeLastObject];
        [dates_titles removeLastObject];
       NSDateFormatter *outputFormatter_next = [[NSDateFormatter alloc] init];
         [outputFormatter_next setDateFormat:@"MM/dd/YYYY"];
        [titleLabel setTitle:last1 forState:UIControlStateNormal];
       
        currentDate=current;
        [self loadWeb:current];
        //[self getData:current];
    }else{
        next1.enabled=NO;
        UIAlertController *alert1 = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Next date data not available now!!" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ok1= [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            [alert1 dismissViewControllerAnimated:YES completion:nil];
        }];
        
        
        [alert1 addAction:ok1];
        [self presentViewController:alert1 animated:YES completion:nil];
        
        
    }
}

-(void)loadWeb:(NSString *)date{
    
    //getting authCode
    NSString *query0 = [NSString stringWithFormat:@"select * from authDetails;"];
    NSArray *authData = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query0]];
    NSString *authCode = @"";
    if([authData count]!=0){
        authCode = authData[0][1];
        
    }
    //---

     NSString *urlString = [NSString stringWithFormat:@"http://www.gpstracking-server1.com/gpstracking/hosapp/HOSDotInspection/indexMatrack.php?masterUser=%@&user=%@&date=%@&authCode=%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"masteruser"],[commonDataHandler sharedInstance].strUserName,date,authCode];
//    NSString *urlString = [NSString stringWithFormat:@"http://www.gpstracking-server1.com/gpstracking/hosapp/HOSDotInspection/indexMatrack.php?masterUser=prabootest11hos1&user=%@&date=%@&authCode=%@",[commonDataHandler sharedInstance].strUserName,date,authCode];
   //   NSString *urlString = [NSString stringWithFormat:@"http://23.239.28.100/mydealership/HOSIOSDotInspection/index1.php?masterUser=hosApp&user=%@&date=%@",user_menu,date];
    NSURL *url = [NSURL URLWithString:urlString];
    NSData *data = [NSData dataWithContentsOfURL:url];
    BFLog(@"url:%@",urlString);
    if (data) {
        NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
        dispatch_async(dispatch_get_main_queue(), ^{
            [_web_dot loadRequest:urlRequest];
        });
    }else{
        
        UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"MatrackHOS"
                                                         message:@"No data found"
                                                        delegate:self
                                               cancelButtonTitle:@"Cancel"
                                               otherButtonTitles:@"OK", nil];
        
        [alert1 show];
        
        
    }
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    
    [indicator startAnimating];
    return YES;
}
-(void)webViewDidFinishLoad:(UIWebView *)webView{
    
    
    savebtn.hidden=NO;
    [indicator stopAnimating];
    indicator.hidden=YES;
}

- (IBAction)saveAsPdf:(id)sender {
    
       NSDateFormatter *outputFormatter_log = [[NSDateFormatter alloc] init];
    [outputFormatter_log setDateFormat:@"MM-dd-YYYY"];
   // NSString *today_log = [outputFormatter_log stringFromDate:now];
    NSString *filename = [NSString stringWithFormat:@"dot-%@",currentDate];
    [self createPDFfromUIView:filename];
   
    }
-(void)createPDFfromUIView:(NSString*)aFilename
{
   
    int height, width, header, sidespace;
    
    height = _web_dot.bounds.size.height;
    width = _web_dot.bounds.size.width;
    header = 15;
    sidespace = 30;
    
    // set header and footer spaces
    UIEdgeInsets pageMargins = UIEdgeInsetsMake(header, sidespace, header, sidespace);
    
    _web_dot.viewPrintFormatter.contentInsets = pageMargins;
    
    //Now create an object of UIPrintPageRenderer that is used to draw pages of content that are to be printed, with or without the assistance of print formatters.
    UIPrintPageRenderer *renderer = [[UIPrintPageRenderer alloc] init];
    
    [renderer addPrintFormatter:_web_dot.viewPrintFormatter startingAtPageAtIndex:0];
    
    CGSize pageSize = CGSizeMake(width, height);
    CGRect printableRect = CGRectMake(pageMargins.left,
                                      pageMargins.top,
                                      pageSize.width - pageMargins.left - pageMargins.right,
                                      pageSize.height - pageMargins.top - pageMargins.bottom);
    
    CGRect paperRect = CGRectMake(0, 0, pageSize.width, pageSize.height);
    
    [renderer setValue:[NSValue valueWithCGRect:paperRect] forKey:@"paperRect"];
    [renderer setValue:[NSValue valueWithCGRect:printableRect] forKey:@"printableRect"];
    
    //Now get an NSData object from UIPrintPageRenderer object with specified paper size.
    NSData *pdfData = [self printToPDFWithRenderer: renderer paperRect:paperRect];
    
    // save PDF file
    NSString *saveFileName = [NSString stringWithFormat:@"%@.pdf", aFilename];
     //BFLog(@"Filename :%@",saveFileName);
    NSArray* documentDirectories = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask,YES);
    
    NSString* documentDirectory = [documentDirectories objectAtIndex:0];
    
    NSString* savePath = [documentDirectory stringByAppendingPathComponent:saveFileName];
     //BFLog(@"Filename :%@",savePath);
    if([[NSFileManager defaultManager] fileExistsAtPath:savePath])
    {
        [[NSFileManager defaultManager] removeItemAtPath:savePath error:nil];
    }
    [pdfData writeToFile: savePath atomically: YES];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"PDF File created and saved successfully." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];}


- (NSData*) printToPDFWithRenderer:(UIPrintPageRenderer*)renderer paperRect:(CGRect)paperRect
{
    NSMutableData *pdfData = [NSMutableData data];
    UIGraphicsBeginPDFContextToData( pdfData, paperRect, nil );
    [renderer prepareForDrawingPages: NSMakeRange(0, renderer.numberOfPages)];
    CGRect bounds = UIGraphicsGetPDFContextBounds();
    for ( int i = 0 ; i < renderer.numberOfPages ; i++ ) { UIGraphicsBeginPDFPage(); [renderer drawPageAtIndex:i inRect: bounds]; } UIGraphicsEndPDFContext(); return pdfData;
    
}

#pragma mark    - <Send DOT inspection report>
-(void)dotInspectionAPICall{
    if([commonDataHandler sharedInstance].strUserName.length <= 0 || [commonDataHandler sharedInstance].strAuthCode.length <= 0){
        DBManager *objDB= [[DBManager alloc] initWithDatabaseFilename:@"hosDB.sql"];
        NSArray *userInfoArray=[[NSArray alloc]initWithArray: [objDB getUserId]];
        NSArray *detailsArray = [userInfoArray firstObject];
        [commonDataHandler sharedInstance].strUserId = detailsArray[0];
        [commonDataHandler sharedInstance].strUserName = detailsArray[1];
        if([commonDataHandler sharedInstance].strAuthCode.length <= 0){
            [commonDataHandler sharedInstance].strAuthCode = detailsArray[[detailsArray count]-2];
        }
    }
    
    NSMutableDictionary *requestDict = [[NSMutableDictionary alloc] init];
    [requestDict setValue:OPERATION_DOT forKey:OPERATION];
    [requestDict setValue:[commonDataHandler sharedInstance].strUserName forKey:USER_NAME];
    [requestDict setValue:[Bugfender deviceIdentifier] forKey:DEVICE_ID];
    [requestDict setValue:[commonDataHandler sharedInstance].strAuthCode forKey:AUTH_CODE];
    [requestDict setValue:@"true" forKey:IS_APPROVED];
    [requestDict setValue:strXMTemplate forKey:XML_TEMPLATE];
    [requestDict setValue:[[commonDataHandler sharedInstance] getCurrentTimeInLocalTimeZone] forKey:DATE_IN_CURRENT_TZ];
    NSString *stringParams = [[commonDataHandler sharedInstance] constructInput:requestDict];
    [self getConnection:[NSString stringWithFormat:@"%@%@", WEB_ROOT, SEND_SIGNED_DOT_URL] withParams:stringParams];

}

-(void)getConnection:(NSString *)urlString withParams:(NSString*)paramString{
    __block NSString *responseString = @"";
    NSString *urlStringWithParam = [NSString stringWithFormat:@"payload=%@",paramString];
    NSString *enodedURl = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [NSURL URLWithString:enodedURl] ;
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    
    [request setHTTPBody:[urlStringWithParam dataUsingEncoding:NSUTF8StringEncoding]];
    NSOperationQueue *queue1 = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:request queue:queue1 completionHandler:^(NSURLResponse * _Nullable response, NSData * _Nullable data, NSError * _Nullable connectionError) {
        responseString = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
        if(![responseString isEqualToString:@""]){
            NSMutableArray *arrayResponse = [[NSMutableArray alloc] init];
            NSMutableArray *arrayMobileCheckIn   = [NSMutableArray new];
            [arrayMobileCheckIn removeAllObjects];
            [arrayResponse removeAllObjects];
            arrayResponse =[[commonDataHandler sharedInstance] parseResponse:responseString];
            
            if(arrayResponse!=nil && [arrayResponse count]!=0){
                
                for (NSMutableDictionary *objDict in arrayResponse) {
                    MobileCheckIn *objMobileCheckIn = [MobileCheckIn new];
                    [objMobileCheckIn getDataFromAPI:objDict];
                    [arrayMobileCheckIn addObject:objMobileCheckIn];
                }
                
                if(arrayMobileCheckIn.count > 0){
                    MobileCheckIn *objMobileCheckIn = [arrayMobileCheckIn firstObject];
                    if([objMobileCheckIn.result isEqualToString:@"failed"]){
                        if([objMobileCheckIn.reason isEqualToString:AUTH_CODE_INVALID]){
                            //Re-auth
                            if(isReAuth){
                                isReAuth = NO;
//                                [[commonDataHandler sharedInstance] reAuthenticateUser:^(BOOL finished) {
//                                     [self dotInspectionAPICall];
//                                }];
                            }
                        }else{
                            dispatch_async(dispatch_get_main_queue(), ^{
                                UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"DOT Inspection Report"
                                                                                 message:objMobileCheckIn.reason
                                                                                delegate:nil
                                                                       cancelButtonTitle:@"OK"
                                                                       otherButtonTitles:nil, nil];
                                [alert1 show];
                            });
                        }
                    }else{
                        //Call success
                        dispatch_async(dispatch_get_main_queue(), ^{
                            UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Send DOT Inspection Report"
                                                                             message:objMobileCheckIn.result
                                                                            delegate:nil
                                                                   cancelButtonTitle:@"OK"
                                                                   otherButtonTitles:nil, nil];
                            [alert1 show];
                        });
                    }
                }
            }
        }
        
        
    }];
    
}


@end
