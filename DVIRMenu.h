//
//  DVIRMenu.h
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 9/12/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DVIR_Capture.h"
#import "Trailer_Defects.h"
#import "Vehicle_Defects.h"
#import "DVIR_General.h"
#import "DVIR_Vehicle.h"
#import "previousDayViewController.h"
#import "DBManager1.h"

extern NSString *mechNotes;
extern NSString *odovalue;
extern NSString *miles;
extern NSString *hours;
extern NSInteger inspectionType;
extern NSString *timevalue;
extern NSString *locationValue;
extern NSString *vehicleNumber;
extern NSString *carrierValue;
extern NSString *trailerNumber;
extern int General_done;
extern int vehicle_done;
extern BOOL preInspection;
extern BOOL postInspection;
//extern int DVIR_selected;
@interface DVIRMenu : UIViewController
@property (weak, nonatomic) IBOutlet UIView *container_general;
- (IBAction)selection:(id)sender;
@property (weak, nonatomic) IBOutlet UISegmentedControl *options;
- (IBAction)save:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *container_Vehicle;
@property (weak, nonatomic) IBOutlet UIView *container_sign;
@property (nonatomic, strong) DBManager1 *dbManager;


@end
