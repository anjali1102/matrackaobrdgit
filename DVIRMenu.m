//
//  DVIRMenu.m
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 9/12/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//

#import "DVIRMenu.h"
#import "DVIR_Sign.h"
#import "DVIR_Main.h"
#import "KeychainItemWrapper.h"
#import "insertDutyCycle.h"

NSMutableURLRequest *request3;//int DVIR_selected=0;
DVIR_General *f;
NSString *miles;
NSString *hours;
NSString *odovalue;
NSString *timevalue;
NSString *locationValue;
NSString *carrierValue;
NSString *vehicleNumber;
NSString *trailerNumber;
NSInteger inspectionType=2;
NSString *mechNotes;

NSInteger *counter=0;
BOOL preInspection = true;
BOOL postInspection = false;
int General_done=0;
int vehicle_done=0;
@interface DVIRMenu ()

@end

@implementation DVIRMenu
@synthesize dbManager;

- (void)viewDidLoad {
    [super viewDidLoad];
    

    counter=0;
    dbManager = [[DBManager1 alloc] initWithDatabaseFilename:@"HOSDatabase.sql"];

   f = [[DVIR_General alloc]init];
       DVIR_selected=1;
    sign_id=0;
    form_id=0;
    

    if (sign==1) {
        self.container_general.alpha=0;
        self.container_Vehicle.alpha=0;
        self.container_sign.alpha=1;
        _options.selectedSegmentIndex=2;
    }else if(trailer1==1||vehicle1==1){
        self.container_sign.alpha=0;
        self.container_Vehicle.alpha=1;
        self.container_general.alpha=0;
        _options.selectedSegmentIndex=1;
        
    }else{
        //_date_today.text=newDateString;
        self.container_general.alpha=1;
        self.container_sign.alpha=0;
        self.container_Vehicle.alpha=0;
        _options.selectedSegmentIndex=0;
        }
    
    
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)selection:(id)sender {
    [self.view endEditing:YES];
    if (_options.selectedSegmentIndex == 0) {
        [UIView animateWithDuration:(0.5) animations:^{
            self.container_general.alpha=1;
            self.container_Vehicle.alpha=0;
            self.container_sign.alpha=0;
           
            
        }];
        
    }
    else if(_options.selectedSegmentIndex == 1){
        
        [UIView animateWithDuration:(0.5) animations:^{
            self.container_general.alpha=0;
            self.container_Vehicle.alpha=1;
            self.container_sign.alpha=0;

                   }];
        
        
    }
    else if(_options.selectedSegmentIndex == 2){
        
        [UIView animateWithDuration:(0.5) animations:^{
            self.container_general.alpha=0;
            self.container_Vehicle.alpha=0;
            self.container_sign.alpha=1;
           
        }];
        
        
    }
}


- (IBAction)save:(id)sender {
    autoLoginCount=0;
       
    if(General_done==0 || vehicle_done==0){
        
    if (General_done==0) {
        
        UIAlertView *alert2 = [[UIAlertView alloc] initWithTitle:@""
                                                         message:@"Please hit DONE in the GENERAL screen to confirm your changes!!!"
                                                        delegate:self
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles:nil, nil];
        
        [alert2 show];
        
        
    }

   else  if (vehicle_done==0) {
        
        UIAlertView *alert2 = [[UIAlertView alloc] initWithTitle:@""
                                                         message:@"Please hit DONE in the VEHICLE screen to confirm your changes!!!"
                                                        delegate:self
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles:nil, nil];
        
        [alert2 show];
        
        
   }else{
       
       UIAlertView *alert2 = [[UIAlertView alloc] initWithTitle:@""
                                                        message:@"Please hit DONE in both screens to confirm your changes!!!"
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil, nil];
       
       [alert2 show];
       
   }
}else if(action==1){
        
        
       // BFLog(@"time & location:%@,%@,%@,%@,%@,%@",timevalue,locationValue,vehicleNumber,trailerNumber,trailer_defects1,vehicle_defects);
        NSMutableDictionary *DVIR=[[NSMutableDictionary alloc] init];
        General_done=1;
        vehicle_done=1;
        NSMutableDictionary *vehicle = [[NSMutableDictionary alloc] init];
    [vehicle setValue:vehicleNumber forKey:@"number"];
    [vehicle setValue:defectsDummy forKey:@"defects"];
    [vehicle setValue:miles forKey:@"miles"];
    [vehicle setValue:hours forKey:@"hours"];
    [vehicle setValue:[NSNumber numberWithInteger:inspectionType] forKey:@"inspectionType"];
        NSMutableDictionary *trailer = [[NSMutableDictionary alloc] init];
        [trailer setValue:trailerNumber forKey:@"number"];
        [trailer setValue:trailer_defects1 forKey:@"defects"];
        NSData *data_vehicle = [NSJSONSerialization dataWithJSONObject:vehicle options:NSJSONWritingPrettyPrinted error:nil];
        NSString *jsonStr_vehicle = [[NSString alloc] initWithData:data_vehicle
                                                          encoding:NSUTF8StringEncoding];
       // BFLog(@"JSON:%@",jsonStr_vehicle);
        NSData *data_trailer = [NSJSONSerialization dataWithJSONObject:trailer options:NSJSONWritingPrettyPrinted error:nil];
        NSString *jsonStr_trailer = [[NSString alloc] initWithData:data_trailer
                                                          encoding:NSUTF8StringEncoding];
        //BFLog(@"JSON:%@",jsonStr_trailer);
        //
      //  NSString *query_check =[NSString stringWithFormat:@"select * from DVIR_Report where date='%@';",current_date];
        
        // Get the results.
        
       // NSArray *reports = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query_check]];
        
      // NSInteger Newid=[reports count]+1;
        NSNumber *num1=@(defectsCorrected);
        NSNumber *num2=@(defectsNeednotCorrected);
        NSNumber *num3 = @(isDriverSigned);
        NSNumber *num4 = @(isMechanicSigned);
        NSNumber *num5 =@(selectedDVIRID);
        
        
        NSString *query_insert1 = [NSString stringWithFormat:@"update DVIR_Report set date='%@',time='%@',location='%@',vehicle_defects='%@',trailer_defects='%@',driverSigned='%@',mechanicSigned='%@',defectsCorrected='%@',defectsNotCorrected='%@',odoValue='%@',carrierValue='%@',mechNotes ='%@' where dvirId='%d'",current_date,timevalue,locationValue,jsonStr_vehicle,jsonStr_trailer,num3,num4,num1,num2,odovalue,carrierValue,mechNotes,selectedDVIRID];
        
        // Execute the query.
        
        //NSString *query_del = @"delete from TempTable";
        [dbManager executeQuery:query_insert1];
        
        // If the query was successfully executed then pop the view controller.
        if (dbManager.affectedRows != 0) {
            //BFLog(@"Query was executed successfully. Affected rows = %d", dbManager.affectedRows);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:[NSString stringWithFormat:@"DVIR report updated Successfully"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            editSection=0;
            [alert show];
            [vehicle_defects removeAllObjects];
            [defectsDummy removeAllObjects];
            [trailer_defects1 removeAllObjects];
            [defectsDummyTrailers removeAllObjects];
            [expandedCells removeAllObjects];
            [expandedCellsTrailers removeAllObjects];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"trucknum"];
        
        
            
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"vehicleMiles"];
            
            
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"vehicleHours"];
            
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"trailernum"];

            
        }
        else{
            //BFLog(@"Could not execute the query");
        }
        
        //save to server
        [DVIR setValue:num1 forKey:@"defectsCorrected"];
        [DVIR setValue:num2 forKey:@"defectsNotCorrected"];
        [DVIR setValue:carrierValue forKey:@"carrier"];
        [DVIR setValue:odovalue forKey:@"odometer"];
        [DVIR setValue:timevalue forKey:@"time"];
       // [DVIR setValue:[NSNumber numberWithInteger:inspectionType] forKey:@"inspectionType"];
        [DVIR setValue:locationValue forKey:@"location"];
        [DVIR setObject:jsonStr_vehicle forKey:@"vehicle"];
        [DVIR setObject:jsonStr_trailer forKey:@"trailer"];
        [DVIR setValue:num3 forKey:@"driverSigned"];
        [DVIR setValue:num4 forKey:@"mechanicSigned"];
        [DVIR setValue:num5 forKey:@"dvirId"];
        [DVIR setValue:mechNotes forKey:@"mechNotes"];
//         [self saveToServer:DVIR];
       // BFLog(@"DVIR:%@",DVIR);
        NSError *error;
    if([[DVIR valueForKey:@"location"] isEqual:@""]){
        BFLog(@"data empty");
        
    }
        /*NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration];
    //getting authCode
    NSString *query0 = [NSString stringWithFormat:@"select * from authDetails;"];
    NSArray *authData = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query0]];
    NSString *authCode = @"";
    if([authData count]!=0){
        authCode = authData[0][1];
        
    }
    //---------------------------
            NSString *urlString = [NSString stringWithFormat:@"http://www.gpstracking-server1.com/gpstracking/hosapp1/saveHOSDVIRData.php?masterUser=%@&User=%@&date=%@&action=%d&authCode=%@",masterUser,user,current_date,action,authCode];
       // NSString *urlString = [NSString stringWithFormat:@"http://23.239.28.100/mydealership/HOSIOSDotInspection/saveHOSDVIRData.php?masterUser=hosApp&User=Anjana&date=%@&action=%d",current_date,action];
       // BFLog(@"URL:%@",urlString);
        NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"HEAD"];
    NSURLResponse *response;
    // NSError *error;
    NSData *mydata = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
    if(httpResponse!=nil){
        if ([httpResponse statusCode] ==200 ) {
            NSMutableURLRequest *request1 = [NSMutableURLRequest requestWithURL:url
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:60.0];
        
        [request1 addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request1 addValue:@"application/json" forHTTPHeaderField:@"Accept"];
        
        [request1 setHTTPMethod:@"POST"];
        /* NSDictionary *mapData = [[NSDictionary alloc] initWithObjectsAndKeys: @"TEST IOS", @"name",
         @"IOS TYPE", @"typemap",
         nil];*
        NSData *postData1 = [NSJSONSerialization dataWithJSONObject:DVIR options:0 error:&error];
        [request1 setHTTPBody:postData1];
        
        
        NSURLSessionDataTask *postDataTask1 = [session dataTaskWithRequest:request1 completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            
            NSDictionary *responseObject = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            
            BFLog(@"%@",responseObject);
         
            
        }];
        
        [postDataTask1 resume];

        }else if([httpResponse statusCode]==503){
            
            UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Service Unavailable"
                                                             message:@"Please check your internet connectivity!!!"
                                                            delegate:self
                                                   cancelButtonTitle:@"Cancel"
                                                   otherButtonTitles:@"OK", nil];
            
            [alert1 show];

        }
        
        
        
    }*/
    
        
    }else if(action==0){
        
        
        
        
        //int dvirId=editSection;
        
        //BFLog(@"time & location:%@,%@,%@,%@,%@,%@",timevalue,locationValue,vehicleNumber,trailerNumber,trailer_defects1,vehicle_defects);
        NSMutableDictionary *DVIR1=[[NSMutableDictionary alloc] init];
        General_done=1;
        vehicle_done=1;
        NSMutableDictionary *vehicle = [[NSMutableDictionary alloc] init];
        [vehicle setValue:vehicleNumber forKey:@"number"];
        [vehicle setValue:defectsDummy forKey:@"defects"];
        [vehicle setValue:miles forKey:@"miles"];
        [vehicle setValue:hours forKey:@"hours"];
        [vehicle setValue:[NSNumber numberWithInteger:inspectionType] forKey:@"inspectionType"];

        
        NSMutableDictionary *trailer = [[NSMutableDictionary alloc] init];
        [trailer setValue:trailerNumber forKey:@"number"];
        [trailer setValue:defectsDummyTrailers forKey:@"defects"];
        NSData *data_vehicle = [NSJSONSerialization dataWithJSONObject:vehicle options:NSJSONWritingPrettyPrinted error:nil];
        NSString *jsonStr_vehicle = [[NSString alloc] initWithData:data_vehicle
                                                          encoding:NSUTF8StringEncoding];
        //BFLog(@"JSON:%@",jsonStr_vehicle);
        NSData *data_trailer = [NSJSONSerialization dataWithJSONObject:trailer options:NSJSONWritingPrettyPrinted error:nil];
        NSString *jsonStr_trailer = [[NSString alloc] initWithData:data_trailer
                                                          encoding:NSUTF8StringEncoding];
        //BFLog(@"JSON:%@",jsonStr_trailer);
        
        
        NSString *query_check1 =[NSString stringWithFormat:@"select id from DVIR_Report"];
        NSArray *reports1 = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query_check1]];
        //BFLog(@"Result:%@",reports1);
        //newDVIRID=(int)reports1[0][0];
        
        // Get the results.
        NSInteger Newid;
        //if([reports1 count]!=0){
      Newid=[reports1 count]+1;
       // }else{
         //    Newid=0;
        //}
        NSNumber *num1=@(defectsCorrected);
        NSNumber *num2=@(defectsNeednotCorrected);
        NSNumber *num3 = @(isDriverSigned);
        NSNumber *num4 = @(isMechanicSigned);
        NSNumber *num5 =@(Newid);
        
        
         NSString *query_insert1 = [NSString stringWithFormat:@"insert into DVIR_Report values(null, '%@', '%@', '%@' ,'%@','%@','%@','%@','%@','%@','%@','%@','%@','%@')", current_date,timevalue,locationValue,jsonStr_vehicle,jsonStr_trailer,num5,num3,num4,num1,num2,odovalue,carrierValue,mechNotes];
        
        // Execute the query.
        
        //NSString *query_del = @"delete from TempTable";
        [dbManager executeQuery:query_insert1];
        
        
        if (dbManager.affectedRows != 0) {
            BFLog(@"Query was inserted the data successfully. Affected rows = %d", dbManager.affectedRows);
            [vehicle_defects removeAllObjects];
            [defectsDummy removeAllObjects];
            [trailer_defects1 removeAllObjects];
            [defectsDummyTrailers removeAllObjects];
            [expandedCells removeAllObjects];
            [expandedCellsTrailers removeAllObjects];
            /*UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Updated Successfully!!!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];*/
            // c_flag=1;
        }
        else{
            BFLog(@"Could not execute the query");
            
        }
        
        // If the query was successfully executed then pop the view controller.
       
        //save to server
        [DVIR1 removeAllObjects];
        [DVIR1 setValue:num1 forKey:@"defectsCorrected"];
        [DVIR1 setValue:num2 forKey:@"defectsNotCorrected"];
        [DVIR1 setValue:carrierValue forKey:@"carrier"];
        [DVIR1 setValue:odovalue forKey:@"odometer"];
        [DVIR1 setValue:timevalue forKey:@"time"];
        [DVIR1 setValue:locationValue forKey:@"location"];
        [DVIR1 setObject:jsonStr_vehicle forKey:@"vehicle"];
        [DVIR1 setObject:jsonStr_trailer forKey:@"trailer"];
        [DVIR1 setValue:num3 forKey:@"driverSigned"];
        [DVIR1 setValue:num4 forKey:@"mechanicSigned"];
        [DVIR1 setValue:num5 forKey:@"dvirId"];
        [DVIR1 setValue:mechNotes forKey:@"mechNotes"];
//        [self saveToServer:DVIR1];
       // BFLog(@"DVIR:%@",DVIR);
        NSError *error;
        if([[DVIR1 valueForKey:@"location"] isEqual:@""]){
            BFLog(@"data empty");
       
        }
      /*  NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration];
        //getting authCode
        NSString *query0 = [NSString stringWithFormat:@"select * from authDetails;"];
        NSArray *authData = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query0]];
        NSString *authCode = @"";
        if([authData count]!=0){
            authCode = authData[0][1];
            
        }
        //---------------------------
        NSString *urlString = [NSString stringWithFormat:@"http://www.gpstracking-server1.com/gpstracking/hosapp1/saveHOSDVIRData.php?masterUser=%@&User=%@&date=%@&action=%d&authCode=%@",masterUser,user,current_date,action,authCode];
      //  NSString *urlString = [NSString stringWithFormat:@"http://23.239.28.100/mydealership/HOSIOSDotInspection/saveHOSDVIRData.php?masterUser=hosApp&User=Anjana&date=%@&action=%d",current_date,action];
       // BFLog(@"URL:%@",urlString);
        NSURL *url = [NSURL URLWithString:urlString];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        [request setHTTPMethod:@"HEAD"];
        NSURLResponse *response;
        // NSError *error;
        NSData *mydata = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
        if(httpResponse!=nil){
            if ([httpResponse statusCode] ==200 ) {
                    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:60.0];
        
        [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
        
        [request setHTTPMethod:@"POST"];
        /* NSDictionary *mapData = [[NSDictionary alloc] initWithObjectsAndKeys: @"TEST IOS", @"name",
         @"IOS TYPE", @"typemap",
         nil];*
        NSData *postData = [NSJSONSerialization dataWithJSONObject:DVIR1 options:0 error:&error];
            NSString *jsonString=[[NSString alloc]initWithData:postData encoding:NSUTF8StringEncoding];
            BFLog(@"%@",jsonString);
        [request setHTTPBody:postData];
        
        
        NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            
        }];
        
        [postDataTask resume];
            
            
            if (dbManager.affectedRows != 0) {
                // BFLog(@"Query was executed successfully. Affected rows = %d", dbManager.affectedRows);
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:[NSString stringWithFormat:@"DVIR report added Successfully"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                editSection=0;
                [alert show];
                
                // long long lastRowId = sqlite3_last_insert_rowid(<#sqlite3 *#>)
                
            }
            else{
                // BFLog(@"Could not execute the query");
            }
        }else if([httpResponse statusCode]==503){
            
            UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Service Unavailable"
                                                             message:@"Please check your internet connectivity!!!"
                                                            delegate:self
                                                   cancelButtonTitle:@"Cancel"
                                                   otherButtonTitles:@"OK", nil];
            
            [alert1 show];
            
        }
        
        
        
        }*/
        
        
        
        
    }
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"trailernum"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"trucknum"];
        
}
//-(void)saveToServer:(NSMutableDictionary *)driver_data{
//    
//    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
//    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration];
//    //getting authCode
//    NSString *query0 = [NSString stringWithFormat:@"select * from authDetails;"];
//    NSArray *authData = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query0]];
//    NSString *authCode = @"";
//    if([authData count]!=0){
//        authCode = authData[0][1];
//        
//    }
//    //---------------------------
//     NSString *urlString = [NSString stringWithFormat:@"http://www.gpstracking-server1.com/gpstracking/hosapp/saveHOSDVIRData.php?masterUser=%@&User=%@&date=%@&action=%d&authCode=%@",masterUser,user,current_date,action,authCode];    // NSString *urlString = [NSString stringWithFormat:@"http://23.239.28.100/mydealership/saveHosGraphData_app.php?masterUser=%@&user=%@&date=%@&approve=0",master_menu,user_menu,current_date];
//    NSURL *url = [NSURL URLWithString:urlString];
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
//    [request setHTTPMethod:@"HEAD"];
//    NSURLResponse *response;
//    NSError *error;
//    NSData *mydata = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
//    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
//    if(httpResponse!=nil){
//        if ([httpResponse statusCode] ==200 ) {
//            BFLog(@"Device connected to the internet");
//            NSURL *url = [NSURL URLWithString:urlString];
//            
//            //NSError *error;
//            
//            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
//                                                                   cachePolicy:NSURLRequestUseProtocolCachePolicy
//                                                               timeoutInterval:60.0];
//            
//            [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
//            [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
//            
//            [request setHTTPMethod:@"POST"];
//            /* NSDictionary *mapData = [[NSDictionary alloc] initWithObjectsAndKeys: @"TEST IOS", @"name",
//             @"IOS TYPE", @"typemap",
//             nil];*/
//            
//            //NSMutableDictionary *currentStatus=[[NSMutableDictionary alloc]init];
//            //[currentStatus setValue:status forKey:@"Status"];*/
//            /*  NSMutableArray *inner = [[NSMutableArray alloc] initWithArray:@[@"00:00",@"OFF"]];
//             NSMutableDictionary *data_dict = [[NSMutableDictionary alloc] init];
//             [data_dict setObject:inner forKey:@"payload"];
//             NSError *error;*/
//            NSData *postData = [NSJSONSerialization dataWithJSONObject:driver_data options:0 error:&error];
//            [request setHTTPBody:postData];
//            
//            NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
//                NSError *parseError = nil;
//                  NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:&parseError];
//                NSInteger dvirId = [[json valueForKey:@"dvirId"] integerValue];
////                NSLog(@"Parse Error:%@",parseError);
//                
//                [self saveMechSign:dvirId];
//                if(error==nil){
//                    UIAlertController *alert1 = [UIAlertController alertControllerWithTitle:@"Attention" message:@"New DVIR Added" preferredStyle:UIAlertControllerStyleAlert];
//                    UIAlertAction *ok1= [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
//                        [alert1 dismissViewControllerAnimated:YES completion:nil];
//                    }];
//                    
//                    
//                    [alert1 addAction:ok1];
//                    [self presentViewController:alert1 animated:YES completion:nil];
//             
//                    
//                    
//                }
//                
//            }];
//            
//            [postDataTask resume];
//        }else if([httpResponse statusCode]==403){
//            if(autoLoginCount<=1){
//                insertDutyCycle *obj = [insertDutyCycle new];
//                BOOL success =  [obj autoLogin];
//                
//                if(success){
//                    [self saveToServer:driver_data];
//                    
//                }else{
//                    KeychainItemWrapper *credentials = [[KeychainItemWrapper alloc] initWithIdentifier:@"credentials" accessGroup:nil];
//                    [credentials resetKeychainItem];
//                    UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Session Expired!!"
//                                                                     message:@"Please Login Again"
//                                                                    delegate:self
//                                                           cancelButtonTitle:@"OK"
//                                                           otherButtonTitles:nil, nil];
//                    alert1.tag = 1;
//                    alert1.delegate = self;
//                    
//                    [alert1 show];
//                    
//                }
//            }else{
//                KeychainItemWrapper *credentials = [[KeychainItemWrapper alloc] initWithIdentifier:@"credentials" accessGroup:nil];
//                [credentials resetKeychainItem];
//                UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Session Expired!!"
//                                                                 message:@"Please Login Again"
//                                                                delegate:self
//                                                       cancelButtonTitle:@"OK"
//                                                       otherButtonTitles:nil, nil];
//                alert1.tag = 2;
//                alert1.delegate = self;
//                
//                [alert1 show];
//                
//                
//                
//            }
//            
//        }else if([httpResponse statusCode]==503){
//            BFLog(@"Device not connected to the internet");
//            UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Service Unavailable"
//                                                             message:@"No connectivity with server at this time,it will sync automatically"
//                                                            delegate:self
//                                                   cancelButtonTitle:@"OK"
//                                                   otherButtonTitles:nil, nil];
//            
//            [alert1 show];
//            
//        }
//    }
//    
//}

    
-(BOOL)setParams:(NSInteger)dvirID{
        NSData *signData=[[NSUserDefaults standardUserDefaults] valueForKey:@"mechSign"];
    //getting authCode
    NSString *query0 = [NSString stringWithFormat:@"select * from authDetails;"];
    NSArray *authData = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query0]];
    NSString *authCode = @"";
    if([authData count]!=0){
        authCode = authData[0][1];
        
    }
        NSString *urlString =[NSString stringWithFormat:@"http://www.gpstracking-server1.com/gpstracking/hosapp/saveMechSign.php?dvirId=%ld&authCode=%@",dvirID,authCode];
        //NSString *urlString =[NSString stringWithFormat:@"http://23.239.28.100/mydealership/HOSIOSDotInspection/saveSign.php?user=Anjana"];
        
        if(signData != nil){
            
            
            
            request3 = [NSMutableURLRequest new];
            request3.timeoutInterval = 20.0;
            [request3 setURL:[NSURL URLWithString:urlString]];
            [request3 setHTTPMethod:@"POST"];
            
            NSString *boundary = @"---------------------------14737809831466499882746641449";
            NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
            [request3 addValue:contentType forHTTPHeaderField: @"Content-Type"];
            [request3 setValue:@"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8" forHTTPHeaderField:@"Accept"];
            [request3 setValue:@"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/536.26.14 (KHTML, like Gecko) Version/6.0.1 Safari/536.26.14" forHTTPHeaderField:@"User-Agent"];
            
            NSMutableData *body = [NSMutableData data];
            [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            NSString *filename;
            /*if (![selected_sign isEqualToString:@"driver"]) {
                NSArray *dateParts=[current_date componentsSeparatedByString:@"/"];
                filename=[NSString stringWithFormat:@"mechanic_%@%@%@_%ld",dateParts[0],dateParts[1],dateParts[2],(long)DVIRID];
            }else{*/
                
                //filename=[NSString stringWithFormat:@"%@",user_menu];
                filename=[NSString stringWithFormat:@"demo"];
           // }
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"uploadedfile\"; filename=\"%@.png\"\r\n", filename] dataUsingEncoding:NSUTF8StringEncoding]];
            
            [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            NSData *mechSign = [[NSUserDefaults standardUserDefaults] valueForKey:@"mechSign"];
            [body appendData:[NSData dataWithData:mechSign]];
            
            [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            
            
            [request3 setHTTPBody:body];
            [request3 addValue:[NSString stringWithFormat:@"%lu", (unsigned long)[body length]] forHTTPHeaderField:@"Content-Length"];
            
            return TRUE;
            
        }else{
            
            //BFLog(@"Failed");
            
            return FALSE;
        }
    }
    
-(void)saveMechSign:(NSInteger)dvirId{
    
    if( [self setParams:dvirId]){
        
            NSError *error = nil;
            NSURLResponse *responseStr = nil;
           NSData *syncResData2;
            syncResData2 = [NSURLConnection sendSynchronousRequest:request3 returningResponse:&responseStr error:&error];
            
            
            if(error == nil){
                //BFLog(@"%@",returnString);
            }
            
            
            
        }
    }

    
    
    

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if(alertView.tag ==1 || alertView.tag ==2){
        
        if(buttonIndex==[alertView cancelButtonIndex]){
            
            
            dispatch_async(dispatch_get_main_queue(), ^{
                UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                UIViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"loginscreen"];
                [self presentViewController:vc animated:YES completion:nil];
            });            
        }
        
        
    }
    
    
    
}

@end
