//
//  DVIR_Capture.h
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 9/12/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UviSignatureView.h"
extern int sign;
@protocol CaptureSignatureViewDelegate <NSObject>
@required
- (void)processCompleted:(UIImage*)signImage;
@end
@interface DVIR_Capture : UIViewController{
    // Delegate to respond back
    id <CaptureSignatureViewDelegate> _delegate;
    NSString *userName, *signedDate;
    NSMutableData *_responseData2;

}

@property (nonatomic,strong) id delegate;
-(void)startSampleProcess:(NSString*)text;


@property (weak, nonatomic) IBOutlet UINavigationItem *headerTitle;

@property (weak, nonatomic) IBOutlet UviSignatureView *signature;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *capture;
- (IBAction)CaptureSign:(id)sender;
@end
