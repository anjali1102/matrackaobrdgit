//
//  DVIR_General.h
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 9/12/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DVIRMenu.h"
#import <CoreLocation/CoreLocation.h>

@protocol SaveDetails <NSObject>
- (void)saveEverything;
@end
@interface DVIR_General : UIViewController<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,CLLocationManagerDelegate>{
    CLLocationManager *locationManager;
    UIDatePicker *startPicker;
    UITextField *time;
    UITextField *location ;
    UITextField *carrier;
    UITextField *odo;
    
}
@property (weak, nonatomic) IBOutlet UITableView *general_Info;
@property (weak,nonatomic) id <SaveDetails> _delegate;
@end
