//
//  DVIR_Main.m
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 9/12/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//

#import "DVIR_Main.h"
#import "KeychainItemWrapper.h"
#import "insertDutyCycle.h"


int section_counts;
NSArray *reports;
int editSection=0;
int reportcount=0;
int action=0;
int selectedDVIRID=0;
NSInteger DVIRID=0;
NSInteger newDVIRID;
CGRect toolFrame;
@interface DVIR_Main ()
@property (strong, nonatomic) VCFloatingActionButton *addButton;
@end
#define TEST_USE_MG_DELEGATE 1
NSMutableArray *expandedCells;
NSMutableArray *defectsDummy;
NSMutableArray *expandedCellsTrailers;
NSMutableArray *defectsDummyTrailers;
NSMutableDictionary *jsonDVIR;
NSMutableDictionary *selectedDVIR;
NSInteger editedVehicleDefects=0;
NSInteger editedTrailerDefects=0;
@implementation DVIR_Main
@synthesize addButton;
@synthesize reports_list;
@synthesize scroller_report;
@synthesize dbManager;
@synthesize toolDvir;
- (void)viewDidLoad {
    [super viewDidLoad];
    reports_list.hidden = YES;
    action=0;
    newDVIRID=0;
    expandedCells = [[NSMutableArray alloc] init];
    defectsDummy= [[NSMutableArray alloc] init];
    expandedCellsTrailers = [[NSMutableArray alloc] init];
    defectsDummyTrailers= [[NSMutableArray alloc] init];
    selectedDVIR=[[NSMutableDictionary alloc] init];
    toolFrame=toolDvir.frame;
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"defectsCorrected"]!=nil){
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"defectsCorrected"];
    }
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"defectsNotCorrected"]!=nil){
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"defectsNotCorrected"];
    }
    dbManager = [[DBManager1 alloc] initWithDatabaseFilename:@"HOSDatabase.sql"];
  
        NSString *query_check1 =[NSString stringWithFormat:@"select MAX(id) from DVIR_Report"];
        NSArray *reports1 = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query_check1]];
        //BFLog(@"Result:%@",reports1);
    
    if([reports1 count]!=0){
        DVIRID=(int)reports1[0][0];
    }else{
        DVIRID=0;
    }

                NSString *query_check =[NSString stringWithFormat:@"select * from DVIR_Report where date='%@';",current_date];
                
                // Get the results.
                
                reports = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query_check]];
                //BFLog(@"Result:%@",reports);
                
                if([reports count]==0){
                    
                    section_counts=1;
                    reportcount=0;
                    
                }else{
                    
                    section_counts=(int)[reports count];
                    reportcount=(int)[reports count];
                    
                }
                reports_list.delegate=self;
                reports_list.dataSource=self;
                

                
                
           /* }];
            [dataTask resume];
            sleep(2);
        }

        
    });
    dispatch_group_notify(group, queue, ^{
    
    });*/

    

    CGRect floatFrame = CGRectMake([UIScreen mainScreen].bounds.size.width - 44 - 20, [UIScreen mainScreen].bounds.size.height - 44 - 20, 44, 44);
    
    addButton = [[VCFloatingActionButton alloc]initWithFrame:floatFrame normalImage:[UIImage imageNamed:@"plus"] andPressedImage:[UIImage imageNamed:@"cross"] withScrollview:scroller_report];
    
    //    NSDictionary *optionsDictionary = @{@"fb-icon":@"Facebook",@"twitter-icon":@"Twitter",@"google-icon":@"Google Plus",@"linkedin-icon":@"Linked in"};
    //    addButton.menuItemSet = optionsDictionary;
    
    
    addButton.imageArray = @[@"fb-icon",@"twitter-icon",@"google-icon",@"linkedin-icon"];
    addButton.labelArray = @[@"Facebook",@"Twitter",@"Google Plus",@"Linked in"];
    
    
    
    addButton.hideWhileScrolling = YES;
    addButton.delegate = self;
    
    
    
    
    [self.view addSubview:addButton];

    [scroller_report setScrollEnabled:YES];
    [scroller_report setContentSize:(CGSizeMake(600, 1000))];
    CGPoint bottomOffset = CGPointMake(0, 1000-scroller_report.bounds.size.height);
    [scroller_report setContentOffset:bottomOffset animated:YES];
        
        
        

    // Do any additional setup after loading the view.
}
-(void)refreshTable{
    
    NSString *query_check =[NSString stringWithFormat:@"select * from DVIR_Report where date='%@';",current_date];
    
    // Get the results.
    
    reports = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query_check]];
    //BFLog(@"Result:%@",reports);
    
    if([reports count]==0){
        
        section_counts=1;
        reportcount=0;
        
    }else{
        
        section_counts=(int)[reports count];
        reportcount=(int)[reports count];
        
    }

    
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if([reports count]==0){
        
        section_counts=1;
        
    }else{
        
        section_counts=(int)[reports count];
    }

    return section_counts;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    int rows;
    if ([reports count]==0) {
        rows=1;
    }else{
        
        rows=16;
    }
    return rows;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    
    return 30;
}
/*-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    NSString *title;
    title=[NSString stringWithFormat:@"Report:%ld",(long)section+1];
    return title;
    
    
    
    
}*/

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    CGRect frame = tableView.frame;
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, 30)];
    [headerView setBackgroundColor:[UIColor colorWithRed:0.85 green:0.93 blue:0.84 alpha:1.0]];
    UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, 100, 20)];
    title.text=[NSString stringWithFormat:@"Report:%ld",(long)section+1];
    title.font= [UIFont fontWithName:@"Helvetica-Bold" size:15.0]; ;
    UIButton *edit = [[UIButton alloc] initWithFrame:CGRectMake(frame.size.width-75, 0, 20, 25)];
    edit.tag=section;
    [edit addTarget:self action:@selector(editDVIR:) forControlEvents:UIControlEventTouchUpInside];
    UIButton *delete = [[UIButton alloc] initWithFrame:CGRectMake(frame.size.width-60, 0, 25, 25)];
    delete.tag=section;
    [delete addTarget:self action:@selector(deleteDVIR:) forControlEvents:UIControlEventTouchUpInside];
    UIImage *editIcon = [UIImage imageNamed:@"icons/edit1.png"];
    UIImage *deleteIcon = [UIImage imageNamed:@"icons/delete1.png"];

    //[edit setTitle:@"edit" forState:UIControlStateNormal];
    [edit setImage:editIcon forState:UIControlStateNormal];
    [delete setImage:deleteIcon forState:UIControlStateNormal];
    
    if([reports count]!=0){
    [headerView addSubview:title];
    [headerView addSubview:edit];
    [headerView addSubview:delete];
    }else{
        UILabel *title1 = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, 100, 20)];
        title1.text=[NSString stringWithFormat:@" "];
        title1.font= [UIFont fontWithName:@"Helvetica-Bold" size:15.0];
        [headerView addSubview:title1];
        
    }
    return headerView;
}
-(void)editDVIR:(UIButton *)sender{
    editedVehicleDefects=0;
    editedTrailerDefects=0;
    [vehicle_defects removeAllObjects];
    [trailer_defects1 removeAllObjects];
    //BFLog(@"Sender:%ld",(long)sender.tag);
    NSIndexPath *indexpath= [NSIndexPath indexPathForRow:0 inSection:sender.tag];
    UITableViewCell *cell = [reports_list cellForRowAtIndexPath:indexpath];
    //BFLog(@"cell content:%@",cell.textLabel.text);
    selectedDVIRID=(int)[cell.textLabel.text integerValue];
    editSection=(int)sender.tag+1;
    action=1;
    NSString *query_check =[NSString stringWithFormat:@"select * from DVIR_Report where date='%@' and dvirId='%d';",current_date,selectedDVIRID];
    
    // Get the results.
    
    NSArray *reports1 = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query_check]];
   // BFLog(@"Result:%@",reports1);
    if ([reports1 count]!=0) {
        [selectedDVIR removeAllObjects];
        //[[NSUserDefaults standardUserDefaults] setValue:reports1[0][9]  forKey:@"defectsCorrected"];
        //[[NSUserDefaults standardUserDefaults] setValue:reports1[0][10]  forKey:@"defectsNotCorrected"];
        [selectedDVIR setValue:reports1[0][9] forKey:@"defectsCorrected"];
        [selectedDVIR setValue:reports1[0][10] forKey:@"defectsNotCorrected"];
        [selectedDVIR setValue:reports1[0][12] forKey:@"carrier"];
        [selectedDVIR setValue:reports1[0][11] forKey:@"odometer"];
        [selectedDVIR setValue:reports1[0][2] forKey:@"time"];
        [selectedDVIR setValue:reports1[0][3] forKey:@"location"];
        [selectedDVIR setObject:reports1[0][4] forKey:@"vehicle"];
        [selectedDVIR setObject:reports1[0][5] forKey:@"trailer"];
        [selectedDVIR setValue:reports1[0][7] forKey:@"driverSigned"];
        [selectedDVIR setValue:reports1[0][8]forKey:@"mechanicSigned"];
        [selectedDVIR setValue:reports1[0][6] forKey:@"dvirId"];
         [selectedDVIR setValue:reports1[0][13] forKey:@"mechNotes"];
        NSData *data=[[selectedDVIR objectForKey:@"trailer"] dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *dummy = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
        //trailer_defects1 = dummy[@"defects"];
        defectsDummyTrailers = dummy[@"defects"];
        if([defectsDummyTrailers count]!=0){
        for(int i=0;i<[defectsDummyTrailers count];i++){
            
            [trailer_defects1 addObject:[defectsDummyTrailers[i] valueForKey:@"defects"]];
            
        }
        }
        NSData *data1=[[selectedDVIR objectForKey:@"vehicle"] dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *dummy1 = [NSJSONSerialization JSONObjectWithData:data1 options:NSJSONReadingMutableContainers error:nil];
       // vehicle_defects = dummy1[@"defects"];
        defectsDummy = dummy1[@"defects"];
        if([defectsDummy count]!=0){
            for(int i=0;i<[defectsDummy count];i++){
                
                [vehicle_defects addObject:[defectsDummy[i] valueForKey:@"defects"]];
                
            }
        }
    UIStoryboard *storyBoard = self.storyboard;
    UIViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"DVIRMenu"];
    [self presentViewController:vc animated:YES completion:nil];
    }else{
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Attention" message:@"You can't edit this DVIR now..Please contact your admin" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    
}
-(void)deleteDVIR:(UIButton *)sender{
    autoLoginCount=0;
    
   // BFLog(@"Sender:%ld",(long)sender.tag);
    editSection=(int)sender.tag+1;
    NSIndexPath *indexpath= [NSIndexPath indexPathForRow:0 inSection:sender.tag];
    UITableViewCell *cell = [reports_list cellForRowAtIndexPath:indexpath];
    //BFLog(@"cell content:%@",cell.textLabel.text);
    selectedDVIRID=(int)[cell.textLabel.text integerValue];
    action=2;
    NSString *query_check =[NSString stringWithFormat:@"select * from DVIR_Report where date='%@' and dvirId='%d';",current_date,selectedDVIRID];
    
    // Get the results.
    
    NSArray *reports1 = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query_check]];
    //BFLog(@"Result:%@",reports1);
    if ([reports1 count]!=0) {
        [selectedDVIR removeAllObjects];
    [selectedDVIR setValue:reports1[0][9] forKey:@"defectsCorrected"];
    [selectedDVIR setValue:reports1[0][10] forKey:@"defectsNotCorrected"];
    [selectedDVIR setValue:reports1[0][12] forKey:@"carrier"];
    [selectedDVIR setValue:reports1[0][11] forKey:@"odometer"];
    [selectedDVIR setValue:reports1[0][2] forKey:@"time"];
    [selectedDVIR setValue:reports1[0][3] forKey:@"location"];
    [selectedDVIR setObject:reports1[0][4] forKey:@"vehicle"];
    [selectedDVIR setObject:reports1[0][5] forKey:@"trailer"];
    [selectedDVIR setValue:reports1[0][7] forKey:@"driverSigned"];
    [selectedDVIR setValue:reports1[0][8]forKey:@"mechanicSigned"];
    [selectedDVIR setValue:reports1[0][6] forKey:@"dvirId"];
        [selectedDVIR setValue:reports1[0][13] forKey:@"mechNotes"];
        [self saveToServer:selectedDVIR];
//[selectedDVIR setValue:reports1[0][13] forKey:@"inspectionType"];
   // int dvirId=editSection;
    
   
    
   
    
    
    NSString *query_insert1 = [NSString stringWithFormat:@"delete from DVIR_Report where dvirId=%d",selectedDVIRID];
    
    // Execute the query.
    
    //NSString *query_del = @"delete from TempTable";
    [dbManager executeQuery:query_insert1];
    
    // If the query was successfully executed then pop the view controller.
    if (dbManager.affectedRows != 0) {
        //BFLog(@"Query was executed successfully. Affected rows = %d", dbManager.affectedRows);
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"DVIR report Deleted Successfully" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        [self refreshTable];
        [reports_list reloadData];
        
        
    }
    else{
       // BFLog(@"Could not execute the query");
    }
    
   /* NSError *error;
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration];
        //getting authCode
        NSString *query0 = [NSString stringWithFormat:@"select * from authDetails;"];
        NSArray *authData = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query0]];
        NSString *authCode = @"";
        if([authData count]!=0){
            authCode = authData[0][1];
            
        }
        //---------------------------
        NSString *urlString = [NSString stringWithFormat:@"http://www.gpstracking-server1.com/gpstracking/hosapp1/saveHOSDVIRData.php?masterUser=%@&User=%@&date=%@&action=%d&authCode=%@",masterUser,user,current_date,action,authCode];
   // NSString *urlString = [NSString stringWithFormat:@"http://23.239.28.100/mydealership/HOSIOSDotInspection/saveHOSDVIRData.php?masterUser=hosApp&User=Anjana&date=%@&action=%d",current_date,action];
    //BFLog(@"URL:%@",urlString);
    NSURL *url = [NSURL URLWithString:urlString];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        [request setHTTPMethod:@"HEAD"];
        NSURLResponse *response;
        // NSError *error;
        NSData *mydata = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
        if(httpResponse!=nil){
            if ([httpResponse statusCode] ==200 ) {
        
        
    NSMutableURLRequest *request2 = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    
    [request2 addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request2 addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    [request2 setHTTPMethod:@"POST"];
     NSDictionary *mapData = [[NSDictionary alloc] initWithObjectsAndKeys: @"TEST IOS", @"name",
     @"IOS TYPE", @"typemap",
     nil];
    NSData *postData2 = [NSJSONSerialization dataWithJSONObject:selectedDVIR options:0 error:&error];
    [request2 setHTTPBody:postData2];
    
    
    NSURLSessionDataTask *postDataTask2 = [session dataTaskWithRequest:request2 completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (error!=nil) {
            BFLog(@"error:%@,%@",error,response);
        }else{
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:error.localizedDescription delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
            
        }
        
    }];
    
    [postDataTask2 resume];
            }else{
            
            UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Service Unavailable"
                                                             message:@"Please check your internet connectivity!!!"
                                                            delegate:self
                                                   cancelButtonTitle:@"Cancel"
                                                   otherButtonTitles:@"OK", nil];
            
            [alert1 show];
            
        }
        }*/
    }else{
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Attention" message:@"You can't delete the DVIR now..Please contact your admin" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
    }

    
    
}

-(void)saveToServer:(NSMutableDictionary *)driver_data{
    
//    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
//    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration];
//    //getting authCode
//    NSString *query0 = [NSString stringWithFormat:@"select * from authDetails;"];
//    NSArray *authData = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query0]];
//    NSString *authCode = @"";
//    if([authData count]!=0){
//        authCode = authData[0][1];
//
//    }
//    //---------------------------
//     NSString *urlString = [NSString stringWithFormat:@"http://www.gpstracking-server1.com/gpstracking/hosapp/saveHOSDVIRData.php?masterUser=%@&User=%@&date=%@&action=%d&authCode=%@",masterUser,user,current_date,action,authCode];
//    // NSString *urlString = [NSString stringWithFormat:@"http://23.239.28.100/mydealership/saveHosGraphData_app.php?masterUser=%@&user=%@&date=%@&approve=0",master_menu,user_menu,current_date];
//    NSURL *url = [NSURL URLWithString:urlString];
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
//    [request setHTTPMethod:@"HEAD"];
//    NSURLResponse *response;
//    NSError *error;
//    NSData *mydata = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
//    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
//    if(httpResponse!=nil){
//        if ([httpResponse statusCode] ==200 ) {
//            BFLog(@"Device connected to the internet");
//            NSURL *url = [NSURL URLWithString:urlString];
//
//            //NSError *error;
//
//            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
//                                                                   cachePolicy:NSURLRequestUseProtocolCachePolicy
//                                                               timeoutInterval:60.0];
//
//            [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
//            [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
//
//            [request setHTTPMethod:@"POST"];
//            /* NSDictionary *mapData = [[NSDictionary alloc] initWithObjectsAndKeys: @"TEST IOS", @"name",
//             @"IOS TYPE", @"typemap",
//             nil];*/
//
//            //NSMutableDictionary *currentStatus=[[NSMutableDictionary alloc]init];
//            //[currentStatus setValue:status forKey:@"Status"];*/
//            /*  NSMutableArray *inner = [[NSMutableArray alloc] initWithArray:@[@"00:00",@"OFF"]];
//             NSMutableDictionary *data_dict = [[NSMutableDictionary alloc] init];
//             [data_dict setObject:inner forKey:@"payload"];
//             NSError *error;*/
//            NSData *postData = [NSJSONSerialization dataWithJSONObject:driver_data options:0 error:&error];
//            [request setHTTPBody:postData];
//
//            NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
//
//                NSDictionary *responseData = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
//
//            }];
//
//            [postDataTask resume];
//        }else if([httpResponse statusCode]==403){
//            if(autoLoginCount<=1){
//                insertDutyCycle *obj = [insertDutyCycle new];
//                BOOL success =  [obj autoLogin];
//
//                if(success){
//                    [self saveToServer:driver_data];
//
//                }else{
//                    KeychainItemWrapper *credentials = [[KeychainItemWrapper alloc] initWithIdentifier:@"credentials" accessGroup:nil];
//                    [credentials resetKeychainItem];
//                    UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Session Expired!!"
//                                                                     message:@"Please Login Again"
//                                                                    delegate:self
//                                                           cancelButtonTitle:@"OK"
//                                                           otherButtonTitles:nil, nil];
//                    alert1.tag = 1;
//                    alert1.delegate = self;
//
//                    [alert1 show];
//
//                }
//            }else{
//                KeychainItemWrapper *credentials = [[KeychainItemWrapper alloc] initWithIdentifier:@"credentials" accessGroup:nil];
//                [credentials resetKeychainItem];
//                UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Session Expired!!"
//                                                                 message:@"Please Login Again"
//                                                                delegate:self
//                                                       cancelButtonTitle:@"OK"
//                                                       otherButtonTitles:nil, nil];
//                alert1.tag = 2;
//                alert1.delegate = self;
//
//                [alert1 show];
//
//
//
//            }
//
//        }else if([httpResponse statusCode]==503){
//            BFLog(@"Device not connected to the internet");
//            UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Service Unavailable"
//                                                             message:@"No connectivity with server at this time,it will sync automatically"
//                                                            delegate:self
//                                                   cancelButtonTitle:@"OK"
//                                                   otherButtonTitles:nil, nil];
//
//            [alert1 show];
//
//        }
//    }
    
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if(alertView.tag ==1 || alertView.tag ==2){
        
        if(buttonIndex==[alertView cancelButtonIndex]){
            
            
            dispatch_async(dispatch_get_main_queue(), ^{
                UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                UIViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"loginscreen"];
                [self presentViewController:vc animated:YES completion:nil];
            });            
        }
        
        
    }
    
    
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    CGFloat height = 40.0;
    if(indexPath.row==9){
        NSData *data2= [reports[indexPath.section][4] dataUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *json_log1 = [NSJSONSerialization JSONObjectWithData:data2 options:0 error:nil];
        
        NSArray *defects = [json_log1 objectForKey:@"defects"];
        
        if([defects count]>1){
            
            height = 35*[defects count];
        }
        
    }else if(indexPath.row==13){
        
        NSData *data2= [reports[indexPath.section][5] dataUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *json_log1 = [NSJSONSerialization JSONObjectWithData:data2 options:0 error:nil];
        
        NSArray *defects = [json_log1 objectForKey:@"defects"];
        if([defects count] >1){
            
            
            height = 35*[defects count];
        }
        
        
    }
    return height;
    
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
   
    UITableViewCell *cell ;
    if (cell==nil) {
        
       cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    }
    if ([reports count]==0) {
          cell.textLabel.text=@"NO DVIR Report";
    }
    else{
        switch (indexPath.row) {
            case 0:
            {
                cell.textLabel.text=reports[indexPath.section][6];
                cell.hidden=YES;
                break;
                
            }
            case 1:
            {
                cell.textLabel.text=[NSString stringWithFormat:@"Time:%@",reports[indexPath.section][2]];
                cell.imageView.image=[UIImage imageNamed:@"icons/timeIcon.png"];
                break;
            }
            case 2:
            {
                cell.textLabel.text=[NSString stringWithFormat:@"Location:%@",reports[indexPath.section][3]];
                cell.imageView.image=[UIImage imageNamed:@"icons/location.png"];
                break;
            }
            case 3:{
                
                cell.textLabel.text = @"Vehicle Details";
                cell.imageView.image=[UIImage imageNamed:@"icons/carrier.png"];
                cell.textLabel.font = [UIFont fontWithName:@"Helvetica-bold" size:15];
                break;
                
                
            }
            case 4:
            {     NSData *data2= [reports[indexPath.section][4] dataUsingEncoding:NSUTF8StringEncoding];
                NSMutableDictionary *json_log1 = [NSJSONSerialization JSONObjectWithData:data2 options:0 error:nil];
                
                NSString *text = [NSString stringWithFormat:@"Vehicle Number :%@",[json_log1 objectForKey:@"number"]];
                cell.textLabel.text=text;
                
                break;
            }
            case 5:{
                
                NSData *data2= [reports[indexPath.section][4] dataUsingEncoding:NSUTF8StringEncoding];
                NSMutableDictionary *json_log1 = [NSJSONSerialization JSONObjectWithData:data2 options:0 error:nil];
                
                NSString *text = [NSString stringWithFormat:@"%@",[json_log1 objectForKey:@"miles"]];
                cell.textLabel.text = [NSString stringWithFormat:@"Miles :%@",text];
                break;
                
                
            }
            case 6:{
                NSData *data2= [reports[indexPath.section][4] dataUsingEncoding:NSUTF8StringEncoding];
                NSMutableDictionary *json_log1 = [NSJSONSerialization JSONObjectWithData:data2 options:0 error:nil];
                
                NSString *text = [NSString stringWithFormat:@"%@",[json_log1 objectForKey:@"hours"]];
                cell.textLabel.text = [NSString stringWithFormat:@"Hours :%@",text];
                break;
                
                
            }
            case 7:{
                NSData *data2= [reports[indexPath.section][4] dataUsingEncoding:NSUTF8StringEncoding];
                NSMutableDictionary *json_log1 = [NSJSONSerialization JSONObjectWithData:data2 options:0 error:nil];
                if([[json_log1 valueForKey:@"inspectionType"] integerValue]==2){
                    
                    cell.textLabel.text = [NSString stringWithFormat:@"Inspection : Not Mentioned"];
                    
                    
                }else{
                    NSString *inspection = @"";
                    if([[json_log1 objectForKey:@"inspectionType"] integerValue]==0){
                        
                        inspection = @"Pre";
                        
                    }else{
                        
                        inspection = @"Post";
                    }
                    
                    
                    NSString *text = [NSString stringWithFormat:@"Inspection :%@",inspection];
                    cell.textLabel.text = text;
                }
                break;
                
                
            }
                
            case 8:{
                
                cell.textLabel.text = @"Defects";
                cell.imageView.image=[UIImage imageNamed:@"icons/defects.png"];
                break;
                
            }
                
                
            case 9:
            {
                NSData *data2= [reports[indexPath.section][4] dataUsingEncoding:NSUTF8StringEncoding];
                NSMutableDictionary *json_log1 = [NSJSONSerialization JSONObjectWithData:data2 options:0 error:nil];
                
                NSArray *defects = [json_log1 objectForKey:@"defects"];
                NSString *defects_string = @"";
                if([defects count]!=0){
                    
                    UITextView *defectsView  = [[UITextView alloc] initWithFrame:CGRectMake(10, 0, tableView.bounds.size.width-10, 35*[defects count])];
                    for (int i=0; i<[defects count]; i++) {
                        defects_string =[NSString stringWithFormat:@"%@\r%@(%@)",defects_string,[defects[i] valueForKey:@"defects"],[defects[i] valueForKey:@"info"]];
                    }
                    defectsView.text=defects_string;

                    [cell.contentView addSubview:defectsView];
                    
                }else{
                    
                    cell.textLabel.text = @"No Defects";
                }
                
                /*cell.textLabel.numberOfLines = [defects count]+1;
                for (int i=0; i<[defects count]; i++) {
                    defects_string =[NSString stringWithFormat:@"%@\r%@",defects_string,defects[i]];
                }
                cell.textLabel.text=defects_string*/;
               //cell.imageView.image=[UIImage imageNamed:@"icons/defects.png"];
                break;
            }
            case 10:{
                
                cell.textLabel.text = @"Trailer Details";
               cell.imageView.image=[UIImage imageNamed:@"icons/trailer.png"];
                cell.textLabel.font = [UIFont fontWithName:@"Helvetica-bold" size:15];
                break;
                
            }
            case 11:
            {
                NSData *data2= [reports[indexPath.section][5] dataUsingEncoding:NSUTF8StringEncoding];
                NSMutableDictionary *json_log1 = [NSJSONSerialization JSONObjectWithData:data2 options:0 error:nil];
                
                NSString *text = [NSString stringWithFormat:@"Trailer Number:%@",[json_log1 objectForKey:@"number"]];
                cell.textLabel.text=text;
                
                break;
            }
            case 12:{
                
                
                cell.textLabel.text = @"Defects";
                cell.imageView.image=[UIImage imageNamed:@"icons/defects.png"];
                break;
            }
            case 13:
            {
                NSData *data2= [reports[indexPath.section][5] dataUsingEncoding:NSUTF8StringEncoding];
                NSMutableDictionary *json_log1 = [NSJSONSerialization JSONObjectWithData:data2 options:0 error:nil];
                
                NSArray *defects = [json_log1 objectForKey:@"defects"];
                NSString *defects_string = @"";
                if([defects count]!=0){
                    
                    UITextView *defectsView  = [[UITextView alloc] initWithFrame:CGRectMake(10, 0, tableView.bounds.size.width-10, 35*[defects count])];
                    for (int i=0; i<[defects count]; i++) {
                        defects_string =[NSString stringWithFormat:@"%@\r%@(%@)",defects_string,[defects[i] valueForKey:@"defects"],[defects[i] valueForKey:@"info"]];
                    }
                    defectsView.text=defects_string;
                    
                    [cell.contentView addSubview:defectsView];
                    
                }else{
                    
                    cell.textLabel.text = @"No Defects";
                }
                break;
            }
            case 14:
            {
                NSData *data2= [reports[indexPath.section][4] dataUsingEncoding:NSUTF8StringEncoding];
                NSMutableDictionary *json_log1 = [NSJSONSerialization JSONObjectWithData:data2 options:0 error:nil];
                
                NSArray *defects = [json_log1 objectForKey:@"defects"];
                NSData *data3= [reports[indexPath.section][5] dataUsingEncoding:NSUTF8StringEncoding];
                NSMutableDictionary *json_log2 = [NSJSONSerialization JSONObjectWithData:data3 options:0 error:nil];
                
                NSArray *defects1 = [json_log2 objectForKey:@"defects"];

                if ([reports[indexPath.section][9] integerValue]==1 && [reports[indexPath.section][10] integerValue]==0) {
                    cell.textLabel.text=@"Defects Corrected";
                    cell.textLabel.textColor=[UIColor greenColor];
                    cell.textLabel.textAlignment=NSTextAlignmentCenter;
                }else if([reports[indexPath.section][9] integerValue]==0 && [reports[indexPath.section][10] integerValue]==1){
                    
                    cell.textLabel.text=@"Defects Need Not Be Corrected";
                    cell.textLabel.textColor=[UIColor redColor];
                    cell.textLabel.textAlignment=NSTextAlignmentCenter;
                    
                }else if ([reports[indexPath.section][9] integerValue]==0 && [reports[indexPath.section][10] integerValue]==0 && [defects count]==0 && [defects1 count]==0){
                    
                    cell.textLabel.text=@"No Defects Found";
                    cell.textLabel.textColor=[UIColor purpleColor];
                    cell.textLabel.textAlignment=NSTextAlignmentCenter;
                }
                
                break;
                
                
            }
            case 15:
            {
                
                if ([reports[indexPath.section][7] integerValue]==1 && [reports[indexPath.section][8] integerValue]==1){
               // UIView *driverView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width/2, 30)];
                    UIImageView *image = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,tableView.frame.size.width/2, 30)];
                    //UILabel *text = [[UILabel alloc] initWithFrame:CGRectMake(30,0,(tableView.frame.size.width/2)-25,30)];
                    //text.textAlignment=NSTextAlignmentRight;
                    image.image=[UIImage imageNamed:@"icons/tickDriver.png"];
                    [image setContentMode:UIViewContentModeScaleAspectFit];
                   // text.text=@"Driver";
                   // [driverView addSubview:image];
                  //  [driverView addSubview:text];
                    //UIView *mechanicView = [[UIView alloc] initWithFrame:CGRectMake(tableView.frame.size.width/2+10, 0, tableView.frame.size.width, 30)];
                    UIImageView *image1 = [[UIImageView alloc] initWithFrame:CGRectMake(tableView.frame.size.width/2, 0, tableView.frame.size.width/2, 30)];
                   // UILabel *text1 = [[UILabel alloc] initWithFrame:CGRectMake(30,0,(tableView.frame.size.width/2)-25,30)];
                    image1.image=[UIImage imageNamed:@"icons/tickMechanic.png"];
                     [image1 setContentMode:UIViewContentModeScaleAspectFit];
                   // text1.text=@"Mechanic";
                    //[mechanicView addSubview:image1];
                    //[mechanicView addSubview:text1];
                    [cell.contentView addSubview:image];
                    [cell.contentView addSubview:image1];

                
                }else if([reports[indexPath.section][7] integerValue]==1 && [reports[indexPath.section][8] integerValue]==0){
                    UIImageView *image = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,tableView.frame.size.width/2, 30)];
                    //UILabel *text = [[UILabel alloc] initWithFrame:CGRectMake(30,0,(tableView.frame.size.width/2)-25,30)];
                    //text.textAlignment=NSTextAlignmentRight;
                    image.image=[UIImage imageNamed:@"icons/tickDriver.png"];
                    [image setContentMode:UIViewContentModeScaleAspectFit];
                      [cell.contentView addSubview:image];
                    
                }else if ([reports[indexPath.section][7] integerValue]==0 && [reports[indexPath.section][8] integerValue]==1){
                    UIImageView *image1 = [[UIImageView alloc] initWithFrame:CGRectMake(tableView.frame.size.width/2, 0, tableView.frame.size.width/2, 30)];
                    // UILabel *text1 = [[UILabel alloc] initWithFrame:CGRectMake(30,0,(tableView.frame.size.width/2)-25,30)];
                    image1.image=[UIImage imageNamed:@"icons/tickMechanic.png"];
                    [image1 setContentMode:UIViewContentModeScaleAspectFit];
                    // text1.text=@"Mechanic";
                    //[mechanicView addSubview:image1];
                    //[mechanicView addSubview:text1];
                  
                    [cell.contentView addSubview:image1];
                    
                    
                }else{
                    
                    
                    cell.hidden=YES;
                }
                
                break;
            }
                
            default:
                break;
        }
        
    }
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
   // BFLog(@"Selected section:%ld",(long)indexPath.section);
}
-(void) didSelectMenuOptionAtIndex:(NSInteger)row
{
   // BFLog(@"Floating action tapped index %tu",row);
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
