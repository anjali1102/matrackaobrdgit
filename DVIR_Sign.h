//
//  DVIR_Sign.h
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 9/12/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DVIR_Capture.h"
extern int isDriverSigned;
extern int isMechanicSigned;
extern int defectsCorrected;
extern int defectsNeednotCorrected;
extern NSString *selected_sign;
@interface DVIR_Sign : UIViewController<UIScrollViewDelegate,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>{
    UIImageView *SignView;
    UIImageView *mechView;
    UITextField *mechInfo;
}
@property (weak, nonatomic) IBOutlet UIScrollView *scroller_dvirsign;
@property (weak, nonatomic) IBOutlet UITableView *signTab;

@end
