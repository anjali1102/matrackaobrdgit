//
//  DVIR_Sign.m
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 9/12/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//

#import "DVIR_Sign.h"
#import "DVIR_Main.h"
#import "DVIRMenu.h"


@interface DVIR_Sign ()

@end
int isDriverSigned=0;
int isMechanicSigned=0;
int defectsCorrected=0;
int defectsNeednotCorrected=0;
NSString *selected_sign=@"";
UIImage *sign_image1;
NSInteger corrected=0;
NSInteger notCorrected=0;
@implementation DVIR_Sign

@synthesize signTab;
@synthesize scroller_dvirsign;

- (void)viewDidLoad {
    [super viewDidLoad];
   
   
    scroller_dvirsign.delegate=self;
    [scroller_dvirsign setScrollEnabled:YES];
    [scroller_dvirsign setContentSize:(CGSizeMake(600, 1000))];
    CGPoint bottomOffset = CGPointMake(0, 1000-scroller_dvirsign.bounds.size.height);
    [scroller_dvirsign setContentOffset:bottomOffset animated:YES];

    NSUserDefaults *sign=[NSUserDefaults standardUserDefaults];
    if ([sign objectForKey:@"Driversign"]!=nil) {
        NSString *sign_string = [sign objectForKey:@"Driversign"];
        NSData *data=[[NSData alloc]initWithBase64EncodedString:sign_string options:NSDataBase64DecodingIgnoreUnknownCharacters];
        sign_image1=[UIImage imageWithData:data];
    }
    /*if(action!=1){
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"Mechanicsign"];
        
    }*/
    signTab.delegate=self;
    signTab.dataSource=self;
    

    // Do any additional setup after loading the view.
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 4;
    
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    int rows=1;
    switch (section) {
        case 0:
            rows=1;
            break;
        case 1:
            if([vehicle_defects count]==0 && [trailer_defects1 count]==0){
                rows=1;
            }else{
            rows=2;
            }
            break;
        case 2:
            rows=2;
            break;
        case 3:
            rows=3;
            break;
            
        default:
            break;
    }
    
    
    
    return rows;
    
    
}


-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    
    NSString *title;
    switch (section) {
        case 0:
            title=@"";
            break;
        case 1:
            title=@"";
            break;
        case 2:
            title=@"Driver Signature";
            break;
        case 3:
            title=@"Mechanic Signature";
            break;
            
        default:
            break;
    }
    
    return title;
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat height = 30.0;
    switch (indexPath.section) {
        case 0:
        {
            height=100.0;
            break;
        }
        case 1:
        {
            height=70.0;
            break;
        }
        case 2:{
            if (indexPath.row==0) {
                   height=200;
            }else{
                height=30.0;
            }
         
            break;
        }
        case 3:{
            if (indexPath.row==0) {
                height=200;
            }else{
                height=30.0;
            }
            break;
        }
            
        default:
            break;
    }
    
    return height;
    
}
-(void)clearcell:(UITableViewCell *)cell{
    
    for (UIView * view in[cell.contentView subviews]){
        [view removeFromSuperview];
    }
    cell.textLabel.text=nil;
    cell.imageView.image=nil;
    
    
    
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    mechNotes = mechInfo.text;
    [textField resignFirstResponder];
    return YES;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellIdentifier=@"cell";
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    [self clearcell:cell];
    switch (indexPath.section) {
        case 0:
        {   UITextView *textview1=[[UITextView alloc] initWithFrame:CGRectMake(25, 20,tableView.bounds.size.width-50,80)];
            textview1.text=@"I declare that the vehicle has been inspected in accordance with the applicable requirements of Schedule ! and/or jurisdiction legislation";
            textview1.font=[UIFont fontWithName:@"Helvetica-Bold" size:12.0];
            textview1.textAlignment=NSTextAlignmentJustified;
            [cell.contentView addSubview:textview1];
            break;
        }
        case 1:{
            
            if ([vehicle_defects count]==0 && [trailer_defects1 count]==0) {
                
                 cell.textLabel.text=@"No Defects Found";
                cell.textLabel.textAlignment=NSTextAlignmentCenter;
                
            }else{
            if (indexPath.row==0) {
                cell.textLabel.text=@"Defects Corrected";
                                
                if (action==1) { //if loop action==1
                    if([selectedDVIR count]!=0){
                        
                        if ([selectedDVIR objectForKey:@"defectsCorrected"]==0) {
                            
                             cell.imageView.image=[UIImage imageNamed:@"icons/uncheck.png"];
                            [[NSUserDefaults standardUserDefaults] setValue:0 forKey:@"defectsCorrected"];
                            
                        }else{
                             cell.imageView.image=[UIImage imageNamed:@"icons/checked.png"];
                            [[NSUserDefaults standardUserDefaults] setValue:@(1) forKey:@"defectsCorrected"];
                            
                            
                        }
                        
                    }
                    
                    
                    
                }else{
                    
                    if([[[NSUserDefaults standardUserDefaults] valueForKey:@"defectsCorrected"] integerValue]==1){
                        
                        cell.textLabel.text=@"Defects Corrected";
                        cell.imageView.image=[UIImage imageNamed:@"icons/checked.png"];
                    }
                else if (corrected==0) {
                    cell.imageView.image=[UIImage imageNamed:@"icons/uncheck.png"];
                    [[NSUserDefaults standardUserDefaults] setValue:0 forKey:@"defectsCorrected"];

                }else{
                    cell.imageView.image=[UIImage imageNamed:@"icons/checked.png"];
                    [[NSUserDefaults standardUserDefaults] setValue:@(1) forKey:@"defectsCorrected"];

                    
                }
                }
            }else{
                
                cell.textLabel.text=@"Defects Need Not Be Corrected";
                
                if (action==1) {
                    if([selectedDVIR count]!=0){
                        
                        if ([[selectedDVIR objectForKey:@"defectsNotCorrected"] integerValue]==0) {
                            
                            cell.imageView.image=[UIImage imageNamed:@"icons/uncheck.png"];
                            [[NSUserDefaults standardUserDefaults] setValue:0 forKey:@"defectsNotCorrected"];
                            
                        }else{
                            cell.imageView.image=[UIImage imageNamed:@"icons/checked.png"];
                            [[NSUserDefaults standardUserDefaults] setValue:@(1) forKey:@"defectsNotCorrected"];
                            
                        }
                        
                    }
                    
                    
                    
                }else{
                if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"defectsNotCorrected"] integerValue]==1){
                        
                        cell.textLabel.text=@"Defects Need Not Be Corrected";
                        cell.imageView.image=[UIImage imageNamed:@"icons/checked.png"];
                    }

            
                else if (notCorrected==0) {
                    cell.imageView.image=[UIImage imageNamed:@"icons/uncheck.png"];
                    [[NSUserDefaults standardUserDefaults] setValue:0 forKey:@"defectsNotCorrected"];
                    
                }else{
                    cell.imageView.image=[UIImage imageNamed:@"icons/checked.png"];
                    [[NSUserDefaults standardUserDefaults] setValue:@(1) forKey:@"defectsNotCorrected"];
                    
                    
                }
                }
                
            }
            }
           
            break;
            
        }
        case 2:
        {
            if (indexPath.row==0) {
           
            SignView=[[UIImageView alloc]initWithFrame:CGRectMake(150, 0, 300, 200)];
            
            NSUserDefaults *sign=[NSUserDefaults standardUserDefaults];
            if ([sign objectForKey:@"Driversign"]!=nil) {
                isDriverSigned=1;
                NSString *sign_string = [sign objectForKey:@"Driversign"];
                NSData *data=[[NSData alloc]initWithBase64EncodedString:sign_string options:NSDataBase64DecodingIgnoreUnknownCharacters];
                sign_image1=[UIImage imageWithData:data];
                                CGSize destinationSize = CGSizeMake(300, 300);
                UIGraphicsBeginImageContext(destinationSize);
                [sign_image1 drawInRect:CGRectMake(0, 0, destinationSize.width, destinationSize.height)];
                UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
                UIGraphicsEndImageContext();
                
                SignView.image=newImage;
                CGSize imageSize =newImage.size;
                [SignView sizeThatFits:imageSize];
                CGPoint imageCenter = SignView.center;
                imageCenter.x = CGRectGetMidX(cell.contentView.frame);
                [SignView setCenter:imageCenter];
            }
            
            [cell.contentView addSubview:SignView];
            }else if (indexPath.row==1){
                
                CGRect buttonRect3 = CGRectMake(0, 0,tableView.frame.size.width, 30);
                UIButton *button3 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
                button3.frame = buttonRect3;
                button3.tag=0;
                [button3 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                [button3 addTarget:self action:@selector(getSign:) forControlEvents:UIControlEventTouchUpInside];
                // set the button title here if it will always be the same
                [button3 setTitle:@"Sign DVIR" forState:UIControlStateNormal];
                button3.titleLabel.font=[UIFont fontWithName:@"Helvetica-Bold" size:20.0];                [button3 setTitleColor:[UIColor colorWithRed:0.81 green:0.93 blue:0.34 alpha:1.0] forState:UIControlStateNormal];
                               //[button addTarget:self action:@selector(myAction:) forControlEvents:UIControlEventTouchUpInside];
                [cell.contentView addSubview:button3];
               
                
            }
            break;
            
        }
        case 3:
        {
            if (indexPath.row==0) {
                
                mechView=[[UIImageView alloc]initWithFrame:CGRectMake(150, 0, 300, 200)];
                
                NSUserDefaults *sign=[NSUserDefaults standardUserDefaults];
                if ([sign objectForKey:@"Mechanicsign"]!=nil) {
                    isMechanicSigned=1;
                    NSString *sign_string = [sign objectForKey:@"Mechanicsign"];
                    NSData *data=[[NSData alloc]initWithBase64EncodedString:sign_string options:NSDataBase64DecodingIgnoreUnknownCharacters];
                    sign_image1=[UIImage imageWithData:data];
                    CGSize destinationSize = CGSizeMake(300, 300);
                    UIGraphicsBeginImageContext(destinationSize);
                    [sign_image1 drawInRect:CGRectMake(0, 0, destinationSize.width, destinationSize.height)];
                    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
                    UIGraphicsEndImageContext();

                    mechView.image=newImage;
                    CGSize imageSize =newImage.size;
                    [mechView sizeThatFits:imageSize];
                    CGPoint imageCenter = mechView.center;
                    imageCenter.x = CGRectGetMidX(cell.contentView.frame);
                    [mechView setCenter:imageCenter];
                    
                }
                
                [cell.contentView addSubview:mechView];
                
                
            }else if(indexPath.row==1){
            
                mechInfo = [[UITextField alloc] initWithFrame:CGRectMake(10, 0, tableView.bounds.size.width-20, 30)];
                mechInfo.placeholder=@"Notes";
                mechInfo.layer.borderColor = [[UIColor grayColor] CGColor];
                mechInfo.layer.borderWidth = 2.0;
                mechInfo.delegate=self;
                [cell.contentView addSubview:mechInfo];
                
            }else if (indexPath.row==2){
                
                CGRect buttonRect = CGRectMake(0, 0,tableView.frame.size.width, 30);
                UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
                button.frame = buttonRect;
                [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                button.tag=1;
                [button addTarget:self action:@selector(getSign:) forControlEvents:UIControlEventTouchUpInside];
                // set the button title here if it will always be the same
                [button setTitle:@"Sign DVIR" forState:UIControlStateNormal];
                button.titleLabel.font=[UIFont fontWithName:@"Helvetica-Bold" size:20.0];                [button setTitleColor:[UIColor colorWithRed:0.81 green:0.93 blue:0.34 alpha:1.0] forState:UIControlStateNormal];

                //[button addTarget:self action:@selector(myAction:) forControlEvents:UIControlEventTouchUpInside];
                [cell.contentView addSubview:button];
                
                
            }

            
            
            break;
            
            
        }
        default:
            break;
    }
    
    
    return cell;
    
    
}

-(void)getSign:(id)sender{
    
    UIButton *clicked = (UIButton *)sender;
    if (clicked.tag==0) {
        selected_sign=@"driver";
    }else{
        
        selected_sign=@"mechanic";
    }
    
    UIStoryboard *storyBoard = self.storyboard;
    UIViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"dvirCapture"];
    [self presentViewController:vc animated:YES completion:nil];
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSIndexPath *index1 = [NSIndexPath indexPathForRow:0 inSection:1];
    UITableViewCell *cell =[tableView cellForRowAtIndexPath:index1];
    NSIndexPath *index = [NSIndexPath indexPathForRow:1 inSection:1];
    UITableViewCell *cell2 =[tableView cellForRowAtIndexPath:index];
    

    if ([vehicle_defects count]!=0 || [trailer_defects1 count]!=0) {
        
  
    if (indexPath.section==1) {
        
        if (indexPath.row==0) {
            
            NSData *img1 = UIImagePNGRepresentation(cell.imageView.image);
            NSData *img2 = UIImagePNGRepresentation([UIImage imageNamed:@"icons/checked.png"]);
            BOOL isComapre = [img1 isEqualToData:img2];
           // NSData *img3 = UIImagePNGRepresentation(cell2.imageView.image);
          
          //  BOOL isComapre1 = [img3 isEqualToData:img2];
            
            if(isComapre){
                cell.imageView.image=[UIImage imageNamed:@"icons/uncheck.png"];
                defectsCorrected=0;
                corrected=0;
                //if (!isComapre1) {
                    
                    cell2.imageView.image=[UIImage imageNamed:@"icons/checked.png"];
                    defectsNeednotCorrected=1;
                    notCorrected=1;
                    
               // }

            }else{
                 cell.imageView.image=[UIImage imageNamed:@"icons/checked.png"];
                defectsCorrected=1;
                corrected=1;
               //if (!isComapre1) {
                    
                    cell2.imageView.image=[UIImage imageNamed:@"icons/uncheck.png"];
                    defectsNeednotCorrected=0;
                    notCorrected=0;
                    
                //}

            }
            
        }else{
            
            
            NSData *img1 = UIImagePNGRepresentation(cell2.imageView.image);
            NSData *img2 = UIImagePNGRepresentation([UIImage imageNamed:@"icons/checked.png"]);
            BOOL isComapre = [img1 isEqualToData:img2];
           // NSData *img3 = UIImagePNGRepresentation(cell.imageView.image);
            
           // BOOL isComapre1 = [img3 isEqualToData:img2];
            if(isComapre){
                cell2.imageView.image=nil;
                cell2.imageView.image=[UIImage imageNamed:@"icons/uncheck.png"];
                defectsNeednotCorrected=0;
                notCorrected=0;
               // if (!isComapre1) {
                    cell.imageView.image=nil;
                    cell.imageView.image=[UIImage imageNamed:@"icons/checked.png"];
                    defectsNeednotCorrected=1;
                    notCorrected=1;
                    
                //}
                
            }else{
                cell2.imageView.image=[UIImage imageNamed:@"icons/checked.png"];
                defectsNeednotCorrected=1;
                notCorrected=1;
               // if (!isComapre1) {
                    
                    cell.imageView.image=[UIImage imageNamed:@"icons/uncheck.png"];
                    defectsNeednotCorrected=0;
                    notCorrected=0;
                    
               // }
                

            }
            
        }

    }
    }else{
        
           defectsNeednotCorrected=0;
               defectsCorrected=0;
        
    }
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    NSUserDefaults *sign=[NSUserDefaults standardUserDefaults];
    if ([sign objectForKey:@"Driversign"]!=nil) {
        SignView.image=sign_image1;
    }
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
