//
//  DVIR_Vehicle.m
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 9/12/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//

#import "DVIR_Vehicle.h"
#import "DVIR_Main.h"

@interface DVIR_Vehicle ()

@end

UIButton *preButton;
UIButton *postButton;
@implementation DVIR_Vehicle
@synthesize defects_tables;
//@synthesize vehicle_num;



- (void)viewDidLoad {
    [super viewDidLoad];
   
    //minnu
    //defects_tables.delegate=self;
    
    /*UITapGestureRecognizer *backgroundTapped=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(backgroundTap)];
    [self.view addGestureRecognizer:backgroundTapped];*/
  /*  [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    *///minnu
    
    defects_tables.delegate=self;
    defects_tables.dataSource=self;
    

    // Do any additional setup after loading the view.
}
//minnu

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    CGPoint pointInTable = [textField.superview convertPoint:textField.frame.origin toView:self.defects_tables];
    CGPoint contentOffset = self.defects_tables.contentOffset;
    
    contentOffset.y = (pointInTable.y - textField.inputAccessoryView.frame.size.height)-100;
    
   // BFLog(@"contentOffset is: %@", NSStringFromCGPoint(contentOffset));
    
    [self.defects_tables setContentOffset:contentOffset animated:YES];
    
    return YES;
}


-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    if ([textField.superview.superview isKindOfClass:[UITableViewCell class]])
    {
        UITableViewCell *cell = (UITableViewCell*)textField.superview.superview;
        NSIndexPath *indexPath = [self.defects_tables indexPathForCell:cell];
        
        [self.defects_tables scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:TRUE];
    }
    
    return YES;
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    if(textField.tag==1){
        
        [[NSUserDefaults standardUserDefaults] setValue:textField.text forKey:@"trucknum"];
        
    }
    
    else if(textField.tag==2){
        
        
        [[NSUserDefaults standardUserDefaults] setValue:textField.text forKey:@"vehicleMiles"];
        
    }
    else if(textField.tag==3){
        
        
         [[NSUserDefaults standardUserDefaults] setValue:textField.text forKey:@"vehicleHours"];
        
    }else if(textField.tag==4){
        
         [[NSUserDefaults standardUserDefaults] setValue:textField.text forKey:@"trailernum"];
        
    }
    [textField resignFirstResponder];
   // BFLog(@"entered return key");
    return YES;
    
}

/*- (void)backgroundTap {
    [self.view endEditing:YES];
    BFLog(@"Touched Inside");}*/

//-(void)textFieldEndBeginEditing:(UITextField *)textField{
//}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    int rows;
    if (section==1) {
        rows=6;
    }else{
        rows=11;
    }
    return rows;
}


-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    NSString *title=@"";
    switch (section) {
        case 0:
            title=@"Vehicle Details";
            break;
        case 1:
            title=@"Trailer Details";
            
        default:
            break;
    }
    return title;
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    CGFloat height = 50.00;
    switch (indexPath.section) {
        case 0:{
            
            if(indexPath.row==9){
                
                if ([vehicle_defects count]>2) {
                    height = 25*[vehicle_defects count];
                }
                
            }
            
            
            break;
        }
            
        case 1:
        {
            
            if(indexPath.row==3){
                
                if ([trailer_defects1 count]>2) {
                    height = 30*[trailer_defects1 count];
                }
                
            }
            
            
            break;
            
        }
            
        default:
            break;
    }
    
    
    return height;
    
}

-(void)clearcell:(UITableViewCell *)cell{
    
    for (UIView * view in[cell.contentView subviews]){
        if (view==vehicle_num) {
            [vehicle_num removeFromSuperview];
            
        }
        if([view isKindOfClass:[UIButton class] ] && view.tag==1){
            [view removeFromSuperview];
            
        }
        [view removeFromSuperview];
    }
    cell.textLabel.text=nil;
    cell.imageView.image=nil;

    
    
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
   
    UITableViewCell *cell ;
    NSString *cellIdentifier=[NSString stringWithFormat:@"cell"];
     //cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if(cell==nil){
        
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    switch (indexPath.section) {
        case 0:
        {
            
            
            
            
            
            
            switch (indexPath.row) {
                    
                case 0:{
                    NSString *label = @"Truck/Tractor Number:*";
                    
                    NSMutableAttributedString *text =
                    [[NSMutableAttributedString alloc]
                     initWithString:@"Truck/Tractor Number:*"];
                    [text addAttribute:NSForegroundColorAttributeName
                                 value:[UIColor colorWithRed:0.36 green:0.67 blue:0.20 alpha:1.0]
                                 range:NSMakeRange(0,[label length]-1)];

                    
                    [text addAttribute:NSForegroundColorAttributeName
                                 value:[UIColor redColor]
                                 range:NSMakeRange([label length]-1, 1)];
                    

                    [cell.textLabel setAttributedText:text];
                    break;
                    
                }
                    
                case 1:
                {
                   vehicle_num = [[UITextField alloc] initWithFrame:CGRectMake(20, 10, 500, 30)];
                    vehicle_num.placeholder=@"Truck/Tractor Number";
                    vehicle_num.delegate=self;
                    vehicle_num.tag=1;
                    if (action==1) {
                        if ([selectedDVIR count]!=0) {
                            //NSDictionary *dummy = [selectedDVIR objectForKey:@"vehicle"];
                            NSData *data=[[selectedDVIR objectForKey:@"vehicle"] dataUsingEncoding:NSUTF8StringEncoding];
                            NSDictionary *dummy = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                           // BFLog(@"num:%@",dummy[@"defects"][0]);
                            //[vehicle_defects removeAllObjects];
                            //vehicle_defects=[dummy[@"defects"] mutableCopy];
                            vehicle_num.text=dummy[@"number"];
                            [[NSUserDefaults standardUserDefaults] setValue:dummy[@"number"] forKey:@"trucknum"];
                            
                        }
                    }else{
                    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"trucknum"]!=nil) {
                        vehicle_num.text=[[NSUserDefaults standardUserDefaults] objectForKey:@"trucknum"];
                    }
                    }
                    [cell.contentView addSubview:vehicle_num];
                    break;
                }
                    
                case 2:{
                    
                    NSString *label = @"Miles:*";
                    
                    NSMutableAttributedString *text =
                    [[NSMutableAttributedString alloc]
                     initWithString:@"Miles:*"];
                    [text addAttribute:NSForegroundColorAttributeName
                                 value:[UIColor colorWithRed:0.36 green:0.67 blue:0.20 alpha:1.0]
                                 range:NSMakeRange(0,[label length]-1)];
                    
                    
                    [text addAttribute:NSForegroundColorAttributeName
                                 value:[UIColor redColor]
                                 range:NSMakeRange([label length]-1, 1)];
                    
                    
                    [cell.textLabel setAttributedText:text];
                   // cell.textLabel.textColor = [UIColor colorWithRed:0.36 green:0.67 blue:0.20 alpha:1.0];
                    break;
                    
                    
                }
                    
                case 3:{
                    vehicleMiles = [[UITextField alloc] initWithFrame:CGRectMake(20, 10, 500, 30)];
                    vehicleMiles.placeholder = @"Miles";
                    vehicleMiles.tag = 2;
                    //vehicleMiles.keyboardType=UIKeyboardTypeNumberPad;
                    if (action==1) {
                        if ([selectedDVIR count]!=0) {
                            //NSDictionary *dummy = [selectedDVIR objectForKey:@"vehicle"];
                            NSData *data=[[selectedDVIR objectForKey:@"vehicle"] dataUsingEncoding:NSUTF8StringEncoding];
                            NSDictionary *dummy = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                            // BFLog(@"num:%@",dummy[@"defects"][0]);
                            //[vehicle_defects removeAllObjects];
                            //vehicle_defects=[dummy[@"defects"] mutableCopy];
                            vehicleMiles.text=dummy[@"miles"];
                            [[NSUserDefaults standardUserDefaults] setValue:dummy[@"miles"] forKey:@"vehicleMiles"];
                            
                        }
                    }else {if([[NSUserDefaults standardUserDefaults] objectForKey:@"vehicleMiles"]!=nil) {
                        vehicleMiles.text=[[NSUserDefaults standardUserDefaults] objectForKey:@"vehicleMiles"];
                        }}
                    vehicleMiles.delegate = self;
                    [cell.contentView addSubview:vehicleMiles];
                    break;
                    
                    
                    
                }
                case 4:{
                    
                    NSString *label = @"Hours:*";
                    
                    NSMutableAttributedString *text =
                    [[NSMutableAttributedString alloc]
                     initWithString:@"Hours:*"];
                    [text addAttribute:NSForegroundColorAttributeName
                                 value:[UIColor colorWithRed:0.36 green:0.67 blue:0.20 alpha:1.0]
                                 range:NSMakeRange(0,[label length]-1)];
                    
                    
                    [text addAttribute:NSForegroundColorAttributeName
                                 value:[UIColor redColor]
                                 range:NSMakeRange([label length]-1, 1)];
                    
                    
                    [cell.textLabel setAttributedText:text];
                    //cell.textLabel.textColor = [UIColor colorWithRed:0.36 green:0.67 blue:0.20 alpha:1.0];
                    break;
                    
                    
                }
                case 5:{
                    vehicleHours = [[UITextField alloc] initWithFrame:CGRectMake(20, 10, 500, 30)];
                    vehicleHours.placeholder = @"Hours";
                    vehicleHours.delegate = self;
                    if (action==1) {
                        if ([selectedDVIR count]!=0) {
                            //NSDictionary *dummy = [selectedDVIR objectForKey:@"vehicle"];
                            NSData *data=[[selectedDVIR objectForKey:@"vehicle"] dataUsingEncoding:NSUTF8StringEncoding];
                            NSDictionary *dummy = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                            // BFLog(@"num:%@",dummy[@"defects"][0]);
                            //[vehicle_defects removeAllObjects];
                            //vehicle_defects=[dummy[@"defects"] mutableCopy];
                            vehicleHours.text=dummy[@"hours"];
                            [[NSUserDefaults standardUserDefaults] setValue:dummy[@"miles"] forKey:@"vehicleHours"];
                            
                        }
                    }else{
                    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"vehicleHours"]!=nil) {
                        vehicleHours.text=[[NSUserDefaults standardUserDefaults] objectForKey:@"vehicleHours"];
                    }
                        }
                    vehicleHours.tag =3;
                    [cell.contentView addSubview:vehicleHours];
                    break;
                    
                    
                    
                }
                    
                case 6:
                {
                    
                    cell.textLabel.text = @"Inspection:";
                    cell.textLabel.textColor = [UIColor colorWithRed:0.36 green:0.67 blue:0.20 alpha:1.0];
                    break;
                    
                    
                }
                    
                case 7:{
                    
                    preButton  = [UIButton buttonWithType:UIButtonTypeCustom];
                    preButton.frame = CGRectMake(20, 0, 20, 40);
                    [preButton setImage:[UIImage imageNamed:@"icons/radioButtonOFF.png"] forState:UIControlStateNormal];
                    [preButton addTarget:self action:@selector(activatePreInspection:) forControlEvents:UIControlEventTouchUpInside];
                    preButton.tag = indexPath.row;
                    [cell.contentView addSubview:preButton];
                    
                    UILabel *preLabel = [[UILabel alloc] initWithFrame:CGRectMake(preButton.bounds.origin.x+preButton.bounds.size.width+20, 0, (tableView.bounds.size.width/2)-30, 40)];
                    preLabel.text = @"Pre Inspection";
                    [cell.contentView addSubview:preLabel];
                    
                    
                    postButton = [UIButton buttonWithType:UIButtonTypeCustom];
                    postButton.frame = CGRectMake((tableView.bounds.size.width/2)+20, 0, 20, 40);
                    [postButton setImage:[UIImage imageNamed:@"icons/radioButtonOFF.png"] forState:UIControlStateNormal];
                    [postButton addTarget:self action:@selector(activatePostInspection:) forControlEvents:UIControlEventTouchUpInside];
                    postButton.tag = indexPath.row;
                    [cell.contentView addSubview:postButton];
                    
                    
                    
                    UILabel *postLabel = [[UILabel alloc] initWithFrame:CGRectMake((tableView.bounds.size.width/2)+40, 0, (tableView.bounds.size.width/2)-40, 40)];
                    postLabel.text = @"Post Inspection";
                    [cell.contentView addSubview:postLabel];
                    if (action==1) {
                        if ([selectedDVIR count]!=0) {
                            //NSDictionary *dummy = [selectedDVIR objectForKey:@"vehicle"];
                            NSData *data=[[selectedDVIR objectForKey:@"vehicle"] dataUsingEncoding:NSUTF8StringEncoding];
                            NSDictionary *dummy = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                            // BFLog(@"num:%@",dummy[@"defects"][0]);
                            //[vehicle_defects removeAllObjects];
                            //vehicle_defects=[dummy[@"defects"] mutableCopy];
                            if([[dummy[@"inspectionType"] stringValue] isEqualToString:@""]){
                                
                            
                            }else{
                            NSInteger type = [dummy[@"inspectionType"] integerValue];
                                if(type==0){
                                     [preButton setImage:[UIImage imageNamed:@"icons/radioButtonON.png"] forState:UIControlStateNormal];
                                     [postButton setImage:[UIImage imageNamed:@"icons/radioButtonOFF.png"] forState:UIControlStateNormal];
                                    inspectionType=0;
                                    
                                }else{
                                    [preButton setImage:[UIImage imageNamed:@"icons/radioButtonOFF.png"] forState:UIControlStateNormal];
                                    [postButton setImage:[UIImage imageNamed:@"icons/radioButtonON.png"] forState:UIControlStateNormal];
                                    inspectionType=1;
                                    
                                }
                            [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithInteger:inspectionType] forKey:@"inspectionType"];
                            }
                            
                        }
                    }else{
                        
                        
                        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"inspectionType"]!=nil) {
                            NSInteger ins = [[[NSUserDefaults standardUserDefaults] objectForKey:@"inspectionType"] integerValue];
                            if(ins==0){
                                [preButton setImage:[UIImage imageNamed:@"icons/radioButtonON.png"] forState:UIControlStateNormal];
                                [postButton setImage:[UIImage imageNamed:@"icons/radioButtonOFF.png"] forState:UIControlStateNormal];
                                inspectionType=0;
                                
                            }else{
                                [preButton setImage:[UIImage imageNamed:@"icons/radioButtonOFF.png"] forState:UIControlStateNormal];
                                [postButton setImage:[UIImage imageNamed:@"icons/radioButtonON.png"] forState:UIControlStateNormal];
                                inspectionType=1;
                                
                            }
                            
                        }else{
                            
                            [preButton setImage:[UIImage imageNamed:@"icons/radioButtonOFF.png"] forState:UIControlStateNormal];
                            [postButton setImage:[UIImage imageNamed:@"icons/radioButtonOFF.png"] forState:UIControlStateNormal];
                            
                            
                        }
                        
                        
                        
                    }
                    
                    break;
                    
                    
                    
                }
                    
                    
                case 8:
                {
                    
                    cell.textLabel.text = @"Defects Details:";
                    cell.textLabel.textColor = [UIColor colorWithRed:0.36 green:0.67 blue:0.20 alpha:1.0];
                    break;
                    
                    
                }
                case 9:
                {
                    if (action==1 && editedVehicleDefects==0) {
                        if ([selectedDVIR count]!=0) {
                            //NSDictionary *dummy = [selectedDVIR objectForKey:@"vehicle"];
                            NSData *data=[[selectedDVIR objectForKey:@"vehicle"] dataUsingEncoding:NSUTF8StringEncoding];
                            NSDictionary *dummy = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                            //NSMutableArray *defects = dummy[@"defects"];
                            /*if([defects count]!=0){
                                for (int i=0; i<[defects count]; i++) {
                                    
                                    if (![vehicle_defects containsObject:defects[i]]) {
                                        
                                        
                                        [vehicle_defects addObject:defects[i]];
                                    }
                                    
                                }
                                
                                
                            }*/
                            //BFLog(@"num:%@",dummy[@"defects"][0]);
                            //[vehicle_defects removeAllObjects];
                            
                        }
                    }
                    if ([vehicle_defects count]==0) {
                        cell.textLabel.text=@"NO Defects Found";
                    }else{
                        cell.textLabel.numberOfLines = [vehicle_defects count];
                        NSString *defects=@"";
                        if([defectsDummy count]!=0){
                        if([[defectsDummy[0] valueForKey:@"info"] isEqualToString:@"No Notes"]){
                            defects=[NSString stringWithFormat:@"%@",[defectsDummy[0] valueForKey:@"defects"]];
                            
                        }else{
                        defects=[NSString stringWithFormat:@"%@(%@)",[defectsDummy[0] valueForKey:@"defects"],[defectsDummy[0] valueForKey:@"info"]];
                        }
                        for (int i=1; i<[defectsDummy count]; i++) {
                            defects=[NSString stringWithFormat:@"%@\r%@(%@)",defects,[defectsDummy[i] valueForKey:@"defects"],[defectsDummy[i] valueForKey:@"info"]];
                        }
                        }
                        cell.textLabel.text=defects;
                        
                        
                    }
                    break;
                    
                }
                case 10:
                {
                    /*CGRect buttonRect = CGRectMake(0, 10, 500, 30);
                    UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
                    button.frame = buttonRect;
                    // set the button title here if it will always be the same
                    [button setTitle:@"Add/Remove Vehicle Defects" forState:UIControlStateNormal];
                    button.tag = 1;
                    [button addTarget:self action:@selector(selectDefects) forControlEvents:UIControlEventTouchUpInside];
                   
                    //[button addTarget:self action:@selector(myAction:) forControlEvents:UIControlEventTouchUpInside];
                    [cell.contentView addSubview:button];*/
                    cell.textLabel.text=@"Add/Remove Vehicle Defects";
                    cell.textLabel.textColor=[UIColor blueColor];

                    break;
                    
                }
                    
                default:
                    break;
            }
            
            break;
        }
        
        case 1:{
            switch (indexPath.row) {
                    
                case 0:{
                    NSString *label = @"Trailer Number:*";
                    
                    NSMutableAttributedString *text =
                    [[NSMutableAttributedString alloc]
                     initWithString:@"Trailer Number:*"];
                    [text addAttribute:NSForegroundColorAttributeName
                                 value:[UIColor colorWithRed:0.36 green:0.67 blue:0.20 alpha:1.0]
                                 range:NSMakeRange(0,[label length]-1)];
                    
                    
                    [text addAttribute:NSForegroundColorAttributeName
                                 value:[UIColor redColor]
                                 range:NSMakeRange([label length]-1, 1)];
                    
                    
                    [cell.textLabel setAttributedText:text];
                    break;
                    
                }
    
                case 1:
                {
                    trailer_num = [[UITextField alloc] initWithFrame:CGRectMake(20, 10, 500, 30)];
                    trailer_num.placeholder=@"Trailer Number";
                    trailer_num.tag=4;
                    if (action==1) {
                        if ([selectedDVIR count]!=0) {
                            //NSDictionary *dummy = [selectedDVIR objectForKey:@"vehicle"];
                            NSData *data=[[selectedDVIR objectForKey:@"trailer"] dataUsingEncoding:NSUTF8StringEncoding];
                            NSDictionary *dummy = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                           
                            trailer_num.text=dummy[@"number"];
                            [[NSUserDefaults standardUserDefaults] setValue:dummy[@"number"] forKey:@"trailernum"];
                            
                        }
                    }else{
                    //BFLog(@"triler:%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"trailernum"]);
                    if (![[[NSUserDefaults standardUserDefaults] objectForKey:@"trailernum"] isEqual:@""]) {
                        trailer_num.text=[[NSUserDefaults standardUserDefaults] objectForKey:@"trailernum"];
                    }
                    }
                    trailer_num.delegate=self;
                    [cell.contentView addSubview:trailer_num];
                    break;
                }
                case 2:
                {
                    
                    cell.textLabel.text = @"Defects Details:";
                    cell.textLabel.textColor = [UIColor colorWithRed:0.36 green:0.67 blue:0.20 alpha:1.0];
                    break;
                    
                    
                }
                case 3:
                {
                    
                    if (action==1 && editedTrailerDefects==0) {
                        if ([selectedDVIR count]!=0) {
                            //NSDictionary *dummy = [selectedDVIR objectForKey:@"vehicle"];
                            NSData *data=[[selectedDVIR objectForKey:@"trailer"] dataUsingEncoding:NSUTF8StringEncoding];
                            NSDictionary *dummy = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                            NSMutableArray *defects = dummy[@"defects"];
                            /*if([defects count]!=0){
                                for (int i=0; i<[defects count]; i++) {
                                    
                                    if (![trailer_defects1 containsObject:defects[i]]) {
                                        
                                        
                                        [trailer_defects1 addObject:defects[i]];
                                    }
                                    
                                }
                                
                                
                            }*/

                        }
                    }

                    if ([trailer_defects1 count]==0) {
                        cell.textLabel.text=@"NO Defects Found";
                    }else{
                        cell.textLabel.numberOfLines = [trailer_defects1 count];
                        NSString *defects=@"";
                        if([defectsDummyTrailers count]!=0){
                        if([[defectsDummyTrailers[0] valueForKey:@"info"] isEqualToString:@"No Notes"]){
                            defects=[NSString stringWithFormat:@"%@",[defectsDummyTrailers[0] valueForKey:@"defects"]];
                            
                        }else{
                            defects=[NSString stringWithFormat:@"%@(%@)",[defectsDummyTrailers[0] valueForKey:@"defects"],[defectsDummyTrailers[0] valueForKey:@"info"]];
                        }
                        for (int i=1; i<[defectsDummyTrailers count]; i++) {
                            defects=[NSString stringWithFormat:@"%@\r%@(%@)",defects,[defectsDummyTrailers[i] valueForKey:@"defects"],[defectsDummyTrailers[i] valueForKey:@"info"]];
                        }
                        }
                        cell.textLabel.text=defects;
                        
                        
                    }
                    break;
                    
                }
                case 4:
                {
                   /* CGRect buttonRect = CGRectMake(0, 10, 500, 30);
                    UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
                    button.frame = buttonRect;
                    // set the button title here if it will always be the same
                    [button setTitle:@"Add/Remove Vehicle Defects" forState:UIControlStateNormal];
                    button.tag = 1;
                    [button addTarget:self action:@selector(selectDefects) forControlEvents:UIControlEventTouchUpInside];
                    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                   // [button setImage:[UIImage imageNamed:@"plus.png"] forState:UIControlStateNormal];
                    //[button addTarget:self action:@selector(myAction:) forControlEvents:UIControlEventTouchUpInside];
                    [cell.contentView addSubview:button];*/
                    cell.textLabel.text=@"Add/Remove Vehicle Defects";
                    cell.textLabel.textColor=[UIColor blueColor];
                    break;
                    
                }
                case 5:{
                    CGRect buttonRect = CGRectMake(tableView.frame.size.width/2-50, 10, 100, 30);
                    UIButton *save = [UIButton buttonWithType:UIButtonTypeRoundedRect];
                    save.frame = buttonRect;
                    
                    [save setTitle:@"Done" forState:UIControlStateNormal];
                    [save setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                    [save setBackgroundColor:[UIColor greenColor]];
                    [save addTarget:self action:@selector(saveToDb) forControlEvents:UIControlEventTouchUpInside];
                    [save viewWithTag:1];
                    [cell.contentView addSubview:save];
                    
                    
                    break;
                }
                    
                default:
                    break;
            }
            
            break;
        }

               default:
            break;
    }
    return cell;
}
-(void)saveToDb{
    
    vehicle_done=1;
    //BFLog(@"time:%@",trailer_defects1);
    if([vehicle_num.text isEqualToString:@""] || [trailer_num.text isEqualToString:@""] || [trailer_num.text isEqualToString:@""] || [trailer_num.text isEqualToString:@""]){
        
        UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@""
                                                         message:@"Required fields cannot be empty!"
                                                        delegate:self
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles:nil, nil];
        
        [alert1 show];
        
        
    }
    
    /*if ([vehicle_num.text isEqualToString:@""]) {
        UIAlertView *alert2 = [[UIAlertView alloc] initWithTitle:@""
                                                         message:@"Please fill Truck/Tractor Number!!!"
                                                        delegate:self
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles:nil, nil];
        
        [alert2 show];
        
    }
    else if ([trailer_num.text isEqualToString:@""]){
        
        UIAlertView *alert2 = [[UIAlertView alloc] initWithTitle:@""
                                                         message:@"Please fill Trailer Number!!!"
                                                        delegate:self
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles:nil, nil];
        
        [alert2 show];
        
        
    }*/
    else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Success!!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];

        vehicleNumber=vehicle_num.text;
        trailerNumber=trailer_num.text;
        miles = vehicleMiles.text;
        hours = vehicleHours.text;
        
    }
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.section) {
        case 0:
            if (indexPath.row==10) {
                [[NSUserDefaults standardUserDefaults] setValue:trailer_num.text forKey:@"trailernum"];
                [[NSUserDefaults standardUserDefaults] setValue:vehicle_num.text  forKey:@"trucknum"];
          
                UIStoryboard *storyBoard = self.storyboard;
                UIViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"vehicle_defects"];
                [self presentViewController:vc animated:YES completion:nil];
 
            }
            break;
        case 1:
            if (indexPath.row==4) {
                [[NSUserDefaults standardUserDefaults] setValue:trailer_num.text forKey:@"trailernum"];
                [[NSUserDefaults standardUserDefaults] setValue:vehicle_num.text forKey:@"trucknum"];

                UIStoryboard *storyBoard = self.storyboard;
                UIViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"trailer_defects"];
                [self presentViewController:vc animated:YES completion:nil];
                
            }
            break;
            
        default:
            break;
    }
    
    
    
    
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)activatePreInspection:(id)sender{
    
    preInspection = true;
    postInspection = false;
    inspectionType=0;
    [preButton setImage:[UIImage imageNamed:@"icons/radioButtonON.png"] forState:UIControlStateNormal];
    [postButton setImage:[UIImage imageNamed:@"icons/radioButtonOFF.png"] forState:UIControlStateNormal];
     [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithInteger:inspectionType] forKey:@"inspectionType"];
    
}
-(void)activatePostInspection:(id)sender{
    preInspection = false;
    postInspection = true;
    inspectionType=1;
    [preButton setImage:[UIImage imageNamed:@"icons/radioButtonOFF.png"] forState:UIControlStateNormal];
    [postButton setImage:[UIImage imageNamed:@"icons/radioButtonON.png"] forState:UIControlStateNormal];
     [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithInteger:inspectionType] forKey:@"inspectionType"];

    
}
-(void)viewWillAppear:(BOOL)animated{
    
    [defects_tables reloadData];
}
-(void)saveEverything{
    
    
    //BFLog(@"called from general");
    
    
}
-(void)selectDefects{
    
}

@end
