//
//  EditForm.h
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 7/29/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "logForm.h"
#import "AppDelegate.h"
#import "generalForm.h"
#import "carrierform2.h"
#import "others.h"
#import "DBManager1.h"
#import "menuScreenViewController.h"

extern int edit_called;
@interface EditForm : UIViewController<UIAlertViewDelegate>
@property (weak, nonatomic) IBOutlet UINavigationBar *navigation;
@property (nonatomic, strong) DBManager1 *dbManager;
@property (weak, nonatomic) IBOutlet UIView *general;
@property (weak, nonatomic) IBOutlet UIView *carrier;
@property (weak, nonatomic) IBOutlet UIView *others;
@property (weak, nonatomic) IBOutlet UISegmentedControl *switches;
- (IBAction)chang_screen:(id)sender;
- (IBAction)SaveSettings:(id)sender;
@end
