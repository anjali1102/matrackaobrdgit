//
//  EditForm.m
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 7/29/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//

#import "EditForm.h"
#import "StatusViewController.h"
#import "NotificationHandler.h"
int edit_called=0;
@interface EditForm ()
{
    UIAlertView *  alert;

}
@end

@implementation EditForm
@synthesize dbManager;

- (void)viewDidLoad {
    [super viewDidLoad];
    edit_called=1;
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(handleUpdatedData:)
//                                                 name:@"DataUpdated"
//                                               object:nil];
    dbManager = [[DBManager1 alloc] initWithDatabaseFilename:@"HOSDatabase.sql"];

       form_id=1;
    _switches.selectedSegmentIndex=section_num;
    if (_switches.selectedSegmentIndex == 0) {
        [UIView animateWithDuration:(0.5) animations:^{
            self.general.alpha=1;
            self.carrier.alpha=0;
            self.others.alpha=0;
            
            
        }];
        
    }
    else if(_switches.selectedSegmentIndex == 1){
        
        [UIView animateWithDuration:(0.5) animations:^{
            self.general.alpha=0;
            self.carrier.alpha=1;
            self.others.alpha=0;
            
        }];
        
        
    }
    else if(_switches.selectedSegmentIndex == 2){
        
        [UIView animateWithDuration:(0.5) animations:^{
            self.general.alpha=0;
            self.carrier.alpha=0;
            self.others.alpha=1;
        }];
        
        
    }
    // Do any additional setup after loading the view.
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"DataUpdated" object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleUpdatedData:)
                                                 name:@"DataUpdated"
                                               object:nil];
}
-(void)handleUpdatedData:(NSNotification *)notification {
    NSDictionary* userInfo = notification.userInfo;
    driver_status drvStat;
    int driverStat;
    driverStat = [[userInfo valueForKey:@"driverStatus"] intValue];
    if(driverStat == 1)
        drvStat = driver_status_driving;
    else if (driverStat == 2)
        drvStat = driver_status_onDuty;
    else if (driverStat == 3)
        drvStat = driver_status_offDuty;
    else if (driverStat == 4)
        drvStat = driver_status_none;
    else if (driverStat == 6)
        drvStat = driver_status_onDuty_immediate;
    else
        drvStat = driver_status_connect_back;
//    if(driverStat == 2){
//        dispatch_async(dispatch_get_main_queue(), ^{
//            alert = [[UIAlertView alloc]initWithTitle:@"Status Change" message:@"Do you want to continue driving or change your duty status" delegate:self cancelButtonTitle:@"Continue Driving" otherButtonTitles:@"Change Status", nil];
//            alert.tag=1005;
//            alert.delegate=self;
//            [alert show];
//            [self performSelector:@selector(dismissAlertView) withObject:nil afterDelay:60.0];
//        });
//    }else{
        NotificationHandler *objHelper = [NotificationHandler new];
        [objHelper recievePushNotification:self withDriverStatus:drvStat];
   // }
}
- (void)dismissAlertView {
    NotificationHandler *objHelper = [NotificationHandler new];
    [objHelper recievePushNotification:self withDriverStatus:driver_status_onDuty_immediate];
    [alert dismissWithClickedButtonIndex:-1 animated:YES];
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if(alertView.tag == 1005){
        if(buttonIndex==1){
            
            UIStoryboard *storyBoard = self.storyboard;
            UIViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"changedutystatus"];
            [self presentViewController:vc animated:YES completion:nil];
        }
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/




- (IBAction)chang_screen:(id)sender {
    [self.view endEditing:YES];

    if (_switches.selectedSegmentIndex == 0) {
        
        [UIView animateWithDuration:(0.5) animations:^{
            self.general.alpha=1;
            self.carrier.alpha=0;
            self.others.alpha=0;

            
        }];
        
    }
    else if(_switches.selectedSegmentIndex == 1){
        
        [UIView animateWithDuration:(0.5) animations:^{
            self.general.alpha=0;
            self.carrier.alpha=1;
            self.others.alpha=0;

        }];
        
        
    }
    else if(_switches.selectedSegmentIndex == 2){
        
        [UIView animateWithDuration:(0.5) animations:^{
            self.general.alpha=0;
            self.carrier.alpha=0;
            self.others.alpha=1;
        }];
        
        
    }
        
}

- (IBAction)SaveSettings:(id)sender {
  
    

}
@end
