//
//  HOSRecap.h
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 8/2/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "logScreenViewController.h"
#import "DBManager1.h"

@interface HOSRecap : UIViewController<UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UIScrollView *scroller_recap;
@property (strong, nonatomic) IBOutlet UITableView *recap_table;
@property (nonatomic, strong) DBManager1 *dbManager;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *helpAlert;
- (IBAction)helpAlert1:(id)sender;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *menu_hosrecap;
-(NSInteger)calculateRestart:(NSArray*)datesArray;
-(void)getRecap:(NSString *)dateNew;
-(NSMutableDictionary *)returnRecapData:(NSString *)dateNew;
@end
