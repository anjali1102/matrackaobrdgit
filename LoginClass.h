//
//  LoginClass.h
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 10/23/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LoginClass : NSObject
@property(nonatomic,strong) NSString *operation;
@property(nonatomic,strong) NSString *username;
@property(nonatomic,strong) NSString *password;
@property(nonatomic,strong) NSString *mobileidentifier;
@property(nonatomic,strong) NSString *userAgent;
@property(nonatomic,strong) NSString *pushidentifier;
-(NSString*)constructInput:(NSMutableDictionary*)listValues;
@end
