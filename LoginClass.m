
//
//  LoginClass.m
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 10/23/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import "LoginClass.h"

@implementation LoginClass
@synthesize operation,username,password,mobileidentifier,pushidentifier,userAgent;

-(NSString*)constructInput:(NSMutableDictionary*)listValues{
    NSString *str = @"";
    if(listValues !=nil && [listValues count]!=0){
        for (int i=0; i<[listValues count]; i++) {
            if(i!=0){
                
                str = [str stringByAppendingString:@"###"];
                
            }
            NSString *currentKey = [listValues allKeys][i];
            NSString *currentValue = [listValues allValues][i];
            str = [str stringByAppendingString:currentKey];
            str = [str stringByAppendingString:@"::"];
            str = [str stringByAppendingString:currentValue];
        }

    }
    
     return str;
    
}

@end
