//
//  LoginResponse.h
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 10/23/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LoginResponse : NSObject
@property(nonatomic,strong) NSString *result;
@property(nonatomic,strong) NSString *authCode;
@property(nonatomic,strong) NSString *reason;
-(NSMutableDictionary*)parseResponse:(NSString*)responseString;
@end
