//
//  AppDelegate.h
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 7/25/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DBManager1.h"
#import "Firebase.h"
#import <UserNotifications/UserNotifications.h>
#import <CoreData/CoreData.h>
#import "loginViewController.h"
#import "KeychainItemWrapper.h"
#import "DBManager1.h"
extern int popup;
extern int cycle_beg;
extern int y_origin3;
extern int y_origin1;
extern int sign_id;
extern int form_id;
extern int DVIR_selected;
extern int gform_id;
extern int carrier_id;
extern int others_id;
extern NSString *databasePath1;
extern NSMutableArray *dataarray3;
extern NSMutableArray *dataarray4;
extern NSMutableArray *dataarray5;
extern NSMutableArray *newline;
//minnu
extern NSMutableArray *oldline;
//minnu
extern KeychainItemWrapper *credentials;
extern NSMutableArray *vehicle_defects;
extern NSMutableArray *trailer_defects1;
extern NSMutableArray *dataarray1;
extern NSMutableArray *dataarray;
extern NSMutableArray *dataDictArray;
extern NSMutableArray *location_array;
extern int load_count;
extern NSMutableArray *dataarray2;
@interface AppDelegate : UIResponder <UIApplicationDelegate,NSObject,FIRMessagingDelegate,UNUserNotificationCenterDelegate>{
    NSManagedObjectModel *managedObjectModel;
    NSManagedObjectContext *managedObjectContext;
    NSPersistentStoreCoordinator *persistentStoreCoordinator;
    NSString *vehicleString;
    NSMutableDictionary *distDict;
    NSMutableDictionary *distDictLastEntry;

}
@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, retain, readonly) NSManagedObjectModel *managedObjectModel;
@property (nonatomic, retain, readonly) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, retain, readonly) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (strong, readonly) NSPersistentContainer *persistentContainer;
- (NSString *)applicationDocumentsDirectory;
+ (AppDelegate *)sharedAppDelegate;
@end

