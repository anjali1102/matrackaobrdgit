//
//  AppDelegate.m
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 7/25/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//
#import "KeychainItemWrapper.h"
#import "AppDelegate.h"
#import <Foundation/Foundation.h>
#import "Reachability.h"
#import <CoreData/CoreData.h>
#import <BugfenderSDK/BugfenderSDK.h>
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import "TIMERUIApplication.h"
#import "StatusViewController.h"
#import "NotificationHandler.h"
#import "Header.h"
#import "MobileCheckIn.h"
#import "commonDataHandler.h"
#import "Suggestion.h"
/* Push notification*/

#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
@import UserNotifications;
#endif



/* Push notification*/

int y_origin3;
int y_origin1;
int cycle_beg=0;
int sign_id=0;
int form_id=0;
int DVIR_selected=0;
int gform_id=0;
int carrier_id=0;
int others_id=0;
int popup=0;
NSString *databasePath1=@"";
NSMutableArray *dataarray3;
NSMutableArray *dataarray4;
NSMutableArray *dataarray5;
NSMutableArray *newline;
NSMutableArray *vehicle_defects;
NSMutableArray *trailer_defects1;
NSMutableArray *dataarray1;
NSMutableArray *dataarray2;
NSMutableArray *dataarray;
NSMutableArray *dataDictArray;
NSMutableArray *location_array;
KeychainItemWrapper *credentials ;
int load_count=0;
//dispatch_queue_t serialQueue;
NSOperationQueue *serverUpdates;
NSInvocationOperation *schedulerThread;
NSInvocationOperation *oneTimeThread;
//Implement UNUserNotificationCenterDelegate to receive display notification via APNS for devices
// running iOS 10 and above.
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0

@interface AppDelegate ()<UNUserNotificationCenterDelegate>
{
    BOOL isReAuth;
    BOOL isReAuthAck;
    NSMutableArray *arrayForSuggestionCommon;
    NSMutableArray *arrayForCheckInCommon;
}
@end
#endif
// Copied from Apple's header in case it is missing in some cases (e.g. pre-Xcode 8 builds).
#ifndef NSFoundationVersionNumber_iOS_9_x_Max
#define NSFoundationVersionNumber_iOS_9_x_Max 1299
#endif

@implementation AppDelegate

NSString *const kGCMMessageIDKey = @"gcm.message_id";

NSMutableDictionary *distDictLastEntry;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
   // serialQueue = dispatch_queue_create("com.MaTrackHos.serverQueue", DISPATCH_QUEUE_SERIAL);
   credentials = [[KeychainItemWrapper alloc] initWithIdentifier:@"credentials" accessGroup:nil];
    serverUpdates = [[NSOperationQueue alloc]init];
    /* Push notification*/
    
    [application setStatusBarHidden:YES];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
    if([statusBar respondsToSelector:@selector(setBackgroundColor:)]){
        
        statusBar.backgroundColor = [UIColor colorWithRed:0.79 green:0.92 blue:0.24 alpha:1.0];
        
    }
    [Fabric with:@[[Crashlytics class]]];
    [Bugfender activateLogger:@"LYldcji8RYf3SeG3EcxnOjFh5TyHWipn"];
 //   [Bugfender enableUIEventLogging];  // optional, log user interactions automatically
    //BFLog(@"Init Logs!"); // use //BFLog as you would use //BFLog*/
    NSString *bugfenderDeviceIdentifier = [Bugfender deviceIdentifier];
    [Bugfender setForceEnabled:NO];
    [CrashlyticsKit setUserIdentifier: bugfenderDeviceIdentifier];
    
    UIPageControl *pageControl = [UIPageControl appearance];
    pageControl.pageIndicatorTintColor = [UIColor lightGrayColor];
    pageControl.currentPageIndicatorTintColor = [UIColor blackColor];
    pageControl.backgroundColor = [UIColor whiteColor];
    vehicle_defects=[[NSMutableArray alloc]init];
    trailer_defects1=[[NSMutableArray alloc]init];
    
    dataarray3 = [[NSMutableArray alloc]init];
    dataarray4 = [[NSMutableArray alloc]init];
    dataarray5 = [[NSMutableArray alloc]init];
    newline=[[NSMutableArray alloc]init];
    dataarray1 = [[NSMutableArray alloc]init];
    dataarray2 = [[NSMutableArray alloc]init];
    dataarray = [[NSMutableArray alloc]init];
    dataDictArray=[[NSMutableArray alloc]init];
    location_array = [[NSMutableArray alloc]init];
    //distDictArray
    distDict=[[NSMutableDictionary alloc]init];
    distDictLastEntry=[[NSMutableDictionary alloc]init];
    //distDictArray
    NSUserDefaults *deafults = [NSUserDefaults standardUserDefaults];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidTimeout:) name:kApplicationDidTimeoutNotification object:nil];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    if ([deafults objectForKey:@"decision_to_db_load"]!=nil) {
        if ([[deafults objectForKey:@"decision_to_db_load"] integerValue]==1) {
            cycle_beg=1;
            [deafults setValue:@"1" forKey:@"decision_to_db_load"];
            
            
        }
        
        
        
    }else{
        
        cycle_beg=0;
        
        [deafults setValue:@"0" forKey:@"decision_to_db_load"];
        
    }
    
    
    
    //  NSDate * now_app = [NSDate date];
    NSDateFormatter *outputFormatter_app = [[NSDateFormatter alloc] init];
    [outputFormatter_app setDateFormat:@"MM/dd/YYYY"];
    // NSString *today_app = [outputFormatter_app stringFromDate:now_app];
    //checking existence of database
    NSString *docsDir;
    NSArray *dirPaths;
    sqlite3 *db=NULL;
    // Get the documents directory
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = [dirPaths objectAtIndex:0];
    //dbManager = [[DBManager1 alloc] initWithDatabaseFilename:@"HOSDatabase.sql"];    // Build the path to the database file
    NSString *databasePath1 = [[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent: @"HOSDatabase.sql"]];
    
    NSFileManager *filemgr = [NSFileManager defaultManager];
    
    if ([filemgr fileExistsAtPath: databasePath1 ] == NO) {
        ////BFLog(@"Database not exists");
        const char *dbpath = [databasePath1 UTF8String];
        
        if (sqlite3_open(dbpath, &db) == SQLITE_OK) {
            char *errMsg;
            const char *sql_stmt0="CREATE TABLE SamTable (id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL , date DOUBLE, date_date date)";
            const char *sql_stmt = "CREATE TABLE TempTable(id integer primary key,date text,data varchar(30),last_status text,flag integer,\"24hr_off\" integer,dateInSecs DOUBLE)";
            const char *sql_stmt1 = "CREATE TABLE Docs (id integer primary key ,documents varchar(100),date text)";
            const char *sql_stmt2 = "CREATE TABLE FinalTable(id integer primary key,date text,data varchar(30),last_status text,flag integer,server_sync integer)";
            const char *sql_stmt3 = "CREATE TABLE Carrier_Sett(id integer primary key,carrier_name text,main_addr varchar(5000),home_addr varchar(5000))";
            
            const char *sql_stmt4 = "CREATE TABLE DVIR_Report(id integer primary key,date text,time text,location varchar(30), vehicle_defects VARCHAR(200), trailer_defects TEXT,dvirId integer, driverSigned integer, mechanicSigned integer,defectsCorrected integer,defectsNotCorrected integer,odoValue integer,carrierValue VARCHAR(100),mechNotes varchar(1000))";
            
            const char *sql_stmt5 = "CREATE TABLE GeneralSettings(id integer primary key,vehicles text,trailers text,distance text,shipping_docu text,driver text,driverId varchar(1000))";
            const char *sql_stmt6 = "CREATE TABLE OtherSettings(id integer primary key,codriver text,origin text,destination text,notes text)";
            const char *sql_stmt7 = "CREATE TABLE AccountSettings(id integer primary key,name text,lastname text,driverid text,mobile_num text,email text,username text)";
            const char *sql_stmt8 = "CREATE TABLE LogSettings(id integer primary key,timezone text,cycle text,cargo_type text,odometer text,restart text,restBreak text)";
            const char *sql_stmt9 = "CREATE TABLE preferenceDetails(id integer primary key,fileLocation text,orientation text)";
            const char *sql_stmt10 = "CREATE TABLE authDetails(id integer primary key,authCode text)";
            
            if (sqlite3_exec(db, sql_stmt0, NULL, NULL, &errMsg) != SQLITE_OK) {
                ////BFLog(@"Failed to create a table");
            }
            if (sqlite3_exec(db, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK) {
                // //BFLog(@"Failed to create a table");
            }
            if (sqlite3_exec(db, sql_stmt1, NULL, NULL, &errMsg) != SQLITE_OK) {
                //BFLog(@"Failed to create a table");
            }
            if (sqlite3_exec(db, sql_stmt2, NULL, NULL, &errMsg) != SQLITE_OK) {
                //BFLog(@"Failed to create a table");
            }
            if (sqlite3_exec(db, sql_stmt3, NULL, NULL, &errMsg) != SQLITE_OK) {
                //BFLog(@"Failed to create a table");
            }
            if (sqlite3_exec(db, sql_stmt4, NULL, NULL, &errMsg) != SQLITE_OK) {
                //BFLog(@"Failed to create a table");
            }
            if (sqlite3_exec(db, sql_stmt5, NULL, NULL, &errMsg) != SQLITE_OK) {
                //BFLog(@"Failed to create a table");
            }
            if (sqlite3_exec(db, sql_stmt6, NULL, NULL, &errMsg) != SQLITE_OK) {
                //BFLog(@"Failed to create a table");
            }
            if (sqlite3_exec(db, sql_stmt7, NULL, NULL, &errMsg) != SQLITE_OK) {
                //BFLog(@"Failed to create a table");
            }
            if (sqlite3_exec(db, sql_stmt8, NULL, NULL, &errMsg) != SQLITE_OK) {
                //BFLog(@"Failed to create a table");
            }
            if (sqlite3_exec(db, sql_stmt9, NULL, NULL, &errMsg) != SQLITE_OK) {
                //BFLog(@"Failed to create a table");
            }
            if (sqlite3_exec(db, sql_stmt10, NULL, NULL, &errMsg) != SQLITE_OK) {
                //BFLog(@"Failed to create a table");
            }
            sqlite3_close(db);
        }
        else {
            //BFLog(@"Failed to open/create database");
            
        }
    }
    else{
        //BFLog(@"Database exists:%@",databasePath1);
    }
    NSString *usr =@"";
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"username"]!=nil) {
        usr = [[NSUserDefaults standardUserDefaults] valueForKey:@"username"];
        
    }
    if([[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"savePassword_%@",usr]]!=nil){
        if([[NSUserDefaults standardUserDefaults] boolForKey:[NSString stringWithFormat:@"savePassword_%@",usr]] == YES){
            
            
            
            
            // NSString *username1 = [deafults stringForKey:@"username"];
            //if (([deafults objectForKey:@"username"]!=nil)&&([deafults objectForKey:@"password"]!=nil)){
            if([credentials objectForKey:(id)kSecAttrAccount]!=nil && [credentials objectForKey:(id)kSecValueData]!=nil){
//                NSLog(@"%@",[credentials objectForKey:(id)kSecAttrAccount]);
                //BFLog(@"Already Loggedin");
                popup=0;
                UIViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"menuscreen"];
                self.window.rootViewController = vc;
            }
            else{
                
                //BFLog(@"Not Loggedin");
                if([[NSUserDefaults standardUserDefaults] objectForKey:@"USER_SIGNATURE_PATH"]!=nil){
                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"USER_SIGNATURE_PATH"];
                }
                if([[NSUserDefaults standardUserDefaults] objectForKey:@"sign"]!=nil){
                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"sign"];
                }
                if([[NSUserDefaults standardUserDefaults] objectForKey:@"USER_SIGNATURE_PATH_MECH"]!=nil){
                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"USER_SIGNATURE_PATH_MECH"];
                }
                UIViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"main1"];
                self.window.rootViewController = vc;
                
            }
            
            
            
            
        }else{
            
            //BFLog(@"Not Loggedin");
            
            UIViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"main1"];
            self.window.rootViewController = vc;
            
            
        }
        
        
    }else{
        
        //BFLog(@"Not Loggedin");
       /// [[NSUserDefaults standardUserDefaults] setBool:YES forKey:[NSString stringWithFormat:@"savePassword_%@",usr]];
        UIViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"main1"];
        self.window.rootViewController = vc;
    }
    /*KeychainItemWrapper *credentials = [[KeychainItemWrapper alloc] initWithIdentifier:@"credentials" accessGroup:nil];
     
     
     // NSString *username1 = [deafults stringForKey:@"username"];
     //if (([deafults objectForKey:@"username"]!=nil)&&([deafults objectForKey:@"password"]!=nil)){
     if([credentials objectForKey:@"username"]!=nil && [credentials objectForKey:@"password"]!=nil){
     //BFLog(@"Already Loggedin");
     popup=0;
     UIViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"menuscreen"];
     self.window.rootViewController = vc;
     }
     else{
     
     //BFLog(@"Not Loggedin");
     UIViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"main1"];
     self.window.rootViewController = vc;
     
     }*/
    // Override point for customization after application launch.
    
    /* Push notification*/
    // [START configure_firebase]
    [FIRApp configure];
    // [END configure_firebase]
    float ver = [[[UIDevice currentDevice] systemVersion] floatValue];
    // [START set_messaging_delegate]
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_7_1) {
        // iOS 7.1 or earlier. Disable the deprecation warnings.
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
        UIRemoteNotificationType allNotificationTypes =
        (UIRemoteNotificationTypeSound |
         UIRemoteNotificationTypeAlert |
         UIRemoteNotificationTypeBadge);
        [application registerForRemoteNotificationTypes:allNotificationTypes];
#pragma clang diagnostic pop
    } else {
        // iOS 8 or later
        // [START register_for_notifications]
        if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_9_x_Max) {
            UIUserNotificationType allNotificationTypes =
            (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
            UIUserNotificationSettings *settings =
            [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
            [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
        } else {
            // iOS 10 or later
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
            // For iOS 10 display notification (sent via APNS)
            [UNUserNotificationCenter currentNotificationCenter].delegate = self;
            [FIRMessaging messaging].delegate = self;
            [FIRMessaging messaging].shouldEstablishDirectChannel=true;
            UNAuthorizationOptions authOptions =
            UNAuthorizationOptionAlert
            | UNAuthorizationOptionSound
            | UNAuthorizationOptionBadge;
            [[UNUserNotificationCenter currentNotificationCenter] requestAuthorizationWithOptions:authOptions completionHandler:^(BOOL granted, NSError * _Nullable error) {
            }];
#endif
        }
        
        [[UIApplication sharedApplication] registerForRemoteNotifications];
        
        // [END register_for_notifications]
    }
    
    //scheduler ---> contact the server in every 15 mintues (15*60=900sec)
    [NSTimer scheduledTimerWithTimeInterval:900.0f repeats:YES block:^(NSTimer * _Nonnull timer) {
        isReAuth = YES;
      //  schedulerThread = [[NSInvocationOperation alloc]initWithTarget:self selector:@selector(dummyNotificationCall) object:nil];
        schedulerThread = [[NSInvocationOperation alloc]initWithTarget:self selector:@selector(mobileCheckInAPI) object:nil];
        [schedulerThread setQueuePriority:NSOperationQueuePriorityNormal];
        [serverUpdates addOperation:schedulerThread];
        
    }];
    
    
    
    return YES;
}

-(void)applicationDidTimeout:(NSNotification *) notif{
    
    //BFLog(@"Time Exceeded");
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    UIViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"menuscreen"];
    self.window.rootViewController = vc;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    
    NSURL *strUrl = [NSURL URLWithString:@"http://www.gpstracking-server1.com/gpstracking/blackbox/client_login.php"];
    NSData *urlData = [NSData dataWithContentsOfURL:strUrl];
    if (!urlData) {
        
        UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                         message:@"Please check your internet connectivity!!!"
                                                        delegate:self
                                               cancelButtonTitle:@"Cancel"
                                               otherButtonTitles:@"OK", nil];
        
        [alert1 show];
        
        
    }
    
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    [self saveContext];
    
}


- (void)saveContext {
    NSManagedObjectContext *context = self.persistentContainer.viewContext;
    NSError *error = nil;
    if ([context hasChanges] && ![context save:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
//        NSLog(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
    }
}

- (NSManagedObjectContext *) managedObjectContext {
    if (managedObjectContext != nil) {
        return managedObjectContext;
    }
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        managedObjectContext = [[NSManagedObjectContext alloc] init];
        [managedObjectContext setPersistentStoreCoordinator: coordinator];
    }
    
    return managedObjectContext;
}

- (NSManagedObjectModel *)managedObjectModel {
    if (managedObjectModel != nil) {
        return managedObjectModel;
    }
    managedObjectModel = [NSManagedObjectModel mergedModelFromBundles:nil] ;
    
    return managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator

{
    
    if (persistentStoreCoordinator != nil) {
        return persistentStoreCoordinator;
    }
    NSURL *storeUrl = [NSURL fileURLWithPath: [[self  applicationDocumentsDirectory]
                                               stringByAppendingPathComponent: @"<Project Name>.sqlite"]];
    NSError *error = nil;
    persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc]
                                  initWithManagedObjectModel:[self managedObjectModel]];
    if(![persistentStoreCoordinator      addPersistentStoreWithType:NSSQLiteStoreType configuration:nil   URL:storeUrl options:nil error:&error])
    {
        /*Error for store creation should be handled in here*/
    }
    
    return persistentStoreCoordinator;
}
- (NSString *)applicationDocumentsDirectory {
    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    // If you are receiving a notification message while your app is in the background,
    // this callback will not be fired till the user taps on the notification launching the application.
    // TODO: Handle data of notification
    
    // With swizzling disabled you must let Messaging know about the message, for Analytics
    [[FIRMessaging messaging] appDidReceiveMessage:userInfo];
    
    // Print message ID.
    if (userInfo[kGCMMessageIDKey]) {
//        NSLog(@"Message ID: %@", userInfo[kGCMMessageIDKey]);
    }
    
    // Print full message.
//    NSLog(@"%@", userInfo);
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    // If you are receiving a notification message while your app is in the background,
    // this callback will not be fired till the user taps on the notification launching the application.
    // TODO: Handle data of notification
    
    // With swizzling disabled you must let Messaging know about the message, for Analytics
    [[FIRMessaging messaging] appDidReceiveMessage:userInfo];
    
    // Print message ID.
    if (userInfo[kGCMMessageIDKey]) {
//        NSLog(@"Message ID: %@", userInfo[kGCMMessageIDKey]);
    }
    
    // Print full message.
//    NSLog(@"%@", userInfo);
    NSString *strAlert = [[userInfo valueForKey:@"aps"] valueForKey:@"alert"];
    if([strAlert containsString:@"actioncode"]){
        NSString *strActionCode = [strAlert stringByReplacingOccurrencesOfString:@"actioncode" withString:@""];
        switch ([strActionCode integerValue]) {
            case 1:
                //Change to driving
            {
//                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//                StatusViewController *obj= (StatusViewController *)[storyboard instantiateViewControllerWithIdentifier:@"drivingProgress"];
//                [self.window.rootViewController.navigationController pushViewController:obj animated:YES];
                
            }
                break;
            case 2:
                //Change to on duty
                break;
            case 3:
                //Change to off duty
                break;
            case 4:
                //change to none
                break;
            case 5:
                // Connect back to server
                break;
            default:
                break;
        }
    }
    
    completionHandler(UIBackgroundFetchResultNewData);
}

// [END receive_message]

// [START ios_10_message_handling]
// Receive displayed notifications for iOS 10 devices.
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
// Handle incoming notification messages while app is in the foreground.
- (void)userNotificationCenter:(UNUserNotificationCenter *)center
       willPresentNotification:(UNNotification *)notification
         withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler {
    
    
    NSDictionary *userInfo = notification.request.content.userInfo;
    
    // With swizzling disabled you must let Messaging know about the message, for Analytics
    [[FIRMessaging messaging] appDidReceiveMessage:userInfo];
    
    // Print message ID.
    if (userInfo[kGCMMessageIDKey]) {
//        NSLog(@"Message ID: %@", userInfo[kGCMMessageIDKey]);
    }
    
    // Print full message.
//    NSLog(@"%@", userInfo);
    
    NSString *strAlert = [[userInfo valueForKey:@"aps"] valueForKey:@"alert"];
    if([strAlert containsString:@"actioncode"]){
        NSString *strActionCode = [strAlert stringByReplacingOccurrencesOfString:@"actioncode=" withString:@""];
        NSDictionary* actioncodeInfo;
        switch ([strActionCode intValue]) {
            case 1:
                //Change to driving
                actioncodeInfo = @{@"driverStatus": @"1"};
                break;
            case 2:
                //Change to on duty
                actioncodeInfo = @{@"driverStatus": @"2"};
                break;
            case 3:
                //Change to off duty
                actioncodeInfo = @{@"driverStatus": @"3"};
                break;
            case 4:
                //change to none
                actioncodeInfo = @{@"driverStatus": @"4"};
                break;
            case 5:
                // Connect back to server
                actioncodeInfo = @{@"driverStatus": @"5"};
                break;
            case 6:
                actioncodeInfo = @{@"driverStatus": @"6"};
            default:
                break;
        }
        if([strActionCode intValue] == 5){
            [self mobileCheckInAPI];
        }else{
            [[NSNotificationCenter defaultCenter] postNotificationName:@"DataUpdated"
                                                                object:self userInfo:actioncodeInfo];
        }
    }
    //[NSTimer scheduledTimerWithTimeInterval:5.0f repeats:YES block:^(NSTimer * _Nonnull timer) {
    
   /* dispatch_async(serialQueue, ^{
        [self fetchFromServer];
    });*/
    //once push notification receives
    oneTimeThread = [[NSInvocationOperation alloc]initWithTarget:self selector:@selector(fetchFromServer) object:nil];
    if([schedulerThread isExecuting]){
        [schedulerThread setQueuePriority:NSOperationQueuePriorityLow];
        [oneTimeThread setQueuePriority:NSOperationQueuePriorityHigh];
    }else{
        
        [oneTimeThread setQueuePriority:NSOperationQueuePriorityNormal];
    }
    
    [serverUpdates addOperation:oneTimeThread];
    
    
    // }];
    
    
    
    
    
    // Change this to your preferred presentation option
    completionHandler(UNNotificationPresentationOptionAlert);
    
    
    
}


-(void)reachabilityDidChange:(NSNotification*)notification{
    Reachability *reachability = (Reachability*)[notification object];
//    NSLog(@"%d",[reachability isReachable]);
    if([reachability isReachable]){
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat: @"http://23.239.28.100/mydealership/iOS_APN/changedData.php"]];
            NSURLRequest *request = [NSURLRequest requestWithURL:url];
            NSOperationQueue *queue = [[NSOperationQueue alloc] init];
            [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse * _Nullable response, NSData * _Nullable data, NSError * _Nullable connectionError) {
                
                if(connectionError==nil){
                    
//                    NSLog(@"got changes");
                    NSError * error;
                    NSMutableDictionary *newData = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
//                    NSLog(@"Changed Data:%@",newData);
                    
                }
                
            }];
            
            
        });
        
    }else{
        
//        NSLog(@"No Internet");
        
    }
    
    
}

-(void)fetchFromServer{

    Reachability *internetReachable = [Reachability reachabilityWithHostName:@"www.google.com"];
    NetworkStatus currentStatus = [internetReachable currentReachabilityStatus];
    [internetReachable startNotifier];
    // [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityDidChange:) name:kReachabilityChangedNotification object:nil];
    /*internetReachable.reachableBlock = ^(Reachability *internetReachable) {*/
    
    if(currentStatus != NotReachable){
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat: @"http://23.239.28.100/mydealership/iOS_APN/changedData.php"]];
            NSURLRequest *request = [NSURLRequest requestWithURL:url];
            NSOperationQueue *queue = [[NSOperationQueue alloc] init];
            [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse * _Nullable response, NSData * _Nullable data, NSError * _Nullable connectionError) {
                
                if(connectionError==nil){
                    
//                    NSLog(@"got changes");
                    NSError * error;
                    NSMutableDictionary *newData = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
//                    NSLog(@"Changed Data:%@",newData);
                    
                }
                
            }];
            
            
        });
        
        
    }else{
        //  internetReachable.unreachableBlock = ^(Reachability *internetReachable) {*/
//        NSLog(@"No Internet");
    }
    
    
    
    
}
// Handle notification messages after display notification is tapped by the user.
- (void)userNotificationCenter:(UNUserNotificationCenter *)center
didReceiveNotificationResponse:(UNNotificationResponse *)response
         withCompletionHandler:(void(^)())completionHandler {
    NSDictionary *userInfo = response.notification.request.content.userInfo;
    if (userInfo[kGCMMessageIDKey]) {
//        NSLog(@"Message ID: %@", userInfo[kGCMMessageIDKey]);
    }
    
    // Print full message.
//    NSLog(@"%@", userInfo);
    
    completionHandler();
}
#endif
// [END ios_10_message_handling]

// [START refresh_token]
- (void)messaging:(nonnull FIRMessaging *)messaging didRefreshRegistrationToken:(nonnull NSString *)fcmToken {
    // Note that this callback will be fired everytime a new token is generated, including the first
    // time. So if you need to retrieve the token as soon as it is available this is where that
    // should be done.
//    NSLog(@"FCM registration token: %@", fcmToken);
    
    // TODO: If necessary send token to application server.
}
// [END refresh_token]

// [START ios_10_data_message]
// Receive data messages on iOS 10+ directly from FCM (bypassing APNs) when the app is in the foreground.
// To enable direct data messages, you can set [Messaging messaging].shouldEstablishDirectChannel to YES.
- (void)messaging:(FIRMessaging *)messaging didReceiveMessage:(FIRMessagingRemoteMessage *)remoteMessage {
//    NSLog(@"Received data message: %@", remoteMessage.appData);
    //NSDictionary *responseData = remoteMessage.appData;
    /* if([responseData count]!=0){
     
     NSData *string = [responseData[@"data"] dataUsingEncoding:NSUTF8StringEncoding];
     NSDictionary *data = [NSJSONSerialization JSONObjectWithData:string options:0 error:nil];
     NSLog(@"%@",data);
     NSString *query_del10 =[NSString stringWithFormat:@"delete from messages;"];
     // [dbManager executeQuery:query_del10];
     NSString *query_insert1 = [NSString stringWithFormat:@"insert into messages values('%@', '%@')", data[@"message"],data[@"message"]];
     
     //    [dbManager executeQuery:query_insert1];
     
     // If the query was successfully executed then pop the view controller.
     //   if (dbManager.affectedRows != 0) {
     //       NSLog(@"Query was executed successfully for empty data. Affected rows = %d", dbManager.affectedRows);
     //     ViewController *s = [[ViewController alloc]init];
     //       [s viewDidAppear:YES];
     // Pop the view controller.
     //[self.navigationController popViewControllerAnimated:YES];
     // }
     // else{
     //   NSLog(@"Could not execute the query");
     //}
     //  NSString *query0 = [NSString stringWithFormat:@"select * from messages;"];
     //NSArray *authData = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query0]];
     //NSLog(@"%@",authData);
     
     
     
     }*/
    
    
}
// [END ios_10_data_message]

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
//    NSLog(@"Unable to register for remote notifications: %@", error);
}
-(void)sendDeviceToken:(NSString*)token{
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat: @"http://23.239.28.100/mydealership/iOS_APN/composeMessage.php?token=%@",token]];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse * _Nullable response, NSData * _Nullable data, NSError * _Nullable connectionError) {
        
        if(connectionError==nil){
            
//            NSLog(@"Token is sent successfully");
            
        }
        
    }];
    
    
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    NSLog(@"APNs device token retrieved: %@", deviceToken);
    NSString *deviceTokenString = [[[[deviceToken description] stringByReplacingOccurrencesOfString:@"<" withString:@""] stringByReplacingOccurrencesOfString:@">" withString:@""] stringByReplacingOccurrencesOfString:@" " withString:@""];
    [self sendDeviceToken:deviceTokenString];
    [[NSUserDefaults standardUserDefaults] setValue:deviceTokenString forKey:@"pushIdentifier" ];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [FIRMessaging messaging].APNSToken=deviceToken;
//    NSLog(@"FCM token:%@",[[FIRMessaging messaging] FCMToken]);
    
    
    // With swizzling disabled you must set the APNs device token here.
    // [FIRMessaging messaging].APNSToken = deviceToken;
}
- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings{
//     NSLog(@"Successfully registered!");
}
#pragma mark    - <SharedInstance>
+ (AppDelegate *)sharedAppDelegate{
    return (AppDelegate *)[UIApplication sharedApplication].delegate;
}
#pragma mark    - <dummy notification>
-(void)dummyNotificationCall{
    NSDictionary* actioncodeInfo;
    actioncodeInfo = @{@"driverStatus": @"2"};
    
    NSString *strActionCode = [actioncodeInfo valueForKey:@"driverStatus"];
    if([strActionCode intValue] == 5){
        [self mobileCheckInAPI];
    }else{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"DataUpdated"
                                                            object:self userInfo:actioncodeInfo];
    }
}
#pragma mark - <MobileCheckIn>
-(void)mobileCheckInAPI{
    arrayForCheckInCommon = [NSMutableArray new];
    arrayForSuggestionCommon = [NSMutableArray new];
    if([commonDataHandler sharedInstance].strUserName.length <= 0 || [commonDataHandler sharedInstance].strAuthCode.length <= 0){
        DBManager *objDB= [[DBManager alloc] initWithDatabaseFilename:@"hosDB.sql"];
        NSArray *userInfoArray=[[NSArray alloc]initWithArray: [objDB getUserId]];
        NSArray *detailsArray = [userInfoArray firstObject];
        [commonDataHandler sharedInstance].strUserId = detailsArray[0];
        if([commonDataHandler sharedInstance].strUserName.length <= 0)
            [commonDataHandler sharedInstance].strUserName = detailsArray[1];
        if([commonDataHandler sharedInstance].strAuthCode.length <= 0)
            [commonDataHandler sharedInstance].strAuthCode = detailsArray[[detailsArray count]-2];
    }
    if([commonDataHandler sharedInstance].strUserName.length > 0){
        NSMutableDictionary *keyList = [[NSMutableDictionary alloc] init];
        [keyList setValue:@"checkin" forKey:OPERATION];
        [keyList setValue:[commonDataHandler sharedInstance].strUserName forKey:USER_NAME];
        [keyList setValue:[Bugfender deviceIdentifier] forKey:DEVICE_ID];
        [keyList setValue:[commonDataHandler sharedInstance].strAuthCode forKey:AUTH_CODE];
        NSString *stringParams = [[commonDataHandler sharedInstance] constructInput:keyList];
        [self getConnection:[NSString stringWithFormat:@"%@%@", WEB_ROOT, MOBILE_CHECK_IN_URL] withParams:stringParams];

    }

}
-(void)getConnection:(NSString *)urlString withParams:(NSString*)paramString{
    __block NSString *responseString = @"";
    NSString *urlStringWithParam = [NSString stringWithFormat:@"payload=%@",paramString];
//    NSLog(@"_________________________%@", urlStringWithParam);
    [arrayForSuggestionCommon removeAllObjects];
    [arrayForCheckInCommon removeAllObjects];
    
    NSString *enodedURl = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [NSURL URLWithString:enodedURl] ;
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    
    [request setHTTPBody:[urlStringWithParam dataUsingEncoding:NSUTF8StringEncoding]];
    NSOperationQueue *queue1 = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:request queue:queue1 completionHandler:^(NSURLResponse * _Nullable response, NSData * _Nullable data, NSError * _Nullable connectionError) {
        responseString = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
    //responseString = @"operation::updateloggraph###date::11/16/2017###vehiclestatus::1###associatedvehicle::vehicle1###odo::12###username::prabootest11###deviceidentifier::78AEDD7AA01CFFF409B4###slot::28###ack::1===operation::updateloggraph###date::11/16/2017###vehiclestatus::2###associatedvehicle::vehicle1###odo::15###username::prabootest11###deviceidentifier::78AEDD7AA01CFFF409B4###slot::29###ack::3===operation::updateloggraph###date::11/16/2017###vehiclestatus::3###associatedvehicle::vehicle1###odo::20###username::prabootest11###deviceidentifier::78AEDD7AA01CFFF409B4###slot::30###ack::3===operation::suggestion###username::praboomtk###deviceidentifier::78AEDD7AA01CFFF409B4###date::10/28/2017###slot::2###repeat::2###status::3###suggestedby::prabootest11###ack::2";

        if(![responseString isEqualToString:@""]){
            NSMutableArray *arrayResponse = [[NSMutableArray alloc] init];
            NSMutableArray *arrayMobileCheckIn   = [NSMutableArray new];
            NSMutableArray *arraySuggestion = [NSMutableArray new];
            
            arrayResponse =[[commonDataHandler sharedInstance] parseResponse:responseString];
            
            if(arrayResponse!=nil && [arrayResponse count]!=0){
                
                for (NSMutableDictionary *objDict in arrayResponse) {
                    MobileCheckIn *objMobileCheckIn = [MobileCheckIn new];
                    [objMobileCheckIn getDataFromAPI:objDict];
                    if([objMobileCheckIn.operation isEqualToString:OPERATION_LOG] ||[objMobileCheckIn.operation isEqualToString:OPERATION_CHECKIN] ){
                        objMobileCheckIn.userId = [commonDataHandler sharedInstance].strUserId;
                        [arrayMobileCheckIn addObject:objMobileCheckIn];
                    }else if([objMobileCheckIn.operation isEqualToString:OPERATION_SUGGESTION]){
                        Suggestion *objSuggestion = [Suggestion new];
                        [objSuggestion getDataFromAPI:objDict];
                        [arraySuggestion addObject:objSuggestion];
                    }
                }
                arrayForSuggestionCommon = arraySuggestion;
                arrayForCheckInCommon = arrayMobileCheckIn;
                
                    if(arrayMobileCheckIn.count > 0){
                        MobileCheckIn *objMobileCheckIn = [arrayMobileCheckIn firstObject];
                        if([objMobileCheckIn.result isEqualToString:@"failed"]){
                            if([objMobileCheckIn.reason isEqualToString:AUTH_CODE_INVALID]){
                                //Re-auth
                                if(isReAuth){
                                    isReAuth = NO;

//                                    [[commonDataHandler sharedInstance] reAuthenticateUser:^(BOOL finished) {
//                                        [self mobileCheckInAPI];
//                                    }];
                                }else{
                                    //BFLogErr(@"%@", objMobileCheckIn.reason);
                                }
                            }
                        }else{
                            isReAuthAck = YES;
                            [self acknowledgeAPICall:arrayMobileCheckIn andSuggestions:arraySuggestion];
                            DBManager *objDB= [[DBManager alloc] initWithDatabaseFilename:@"hosDB.sql"];
                            for (Suggestion *objSugg in arraySuggestion) {
                                [objDB insertIntoTable:objSugg];
                            }
                            for (MobileCheckIn *objCheckIn in arrayMobileCheckIn) {
                               NSArray *previouStatus=[[NSArray alloc] initWithArray:[objDB getPreviousStatus:objCheckIn]];
                                NSString* driverStatus=@"1";
                                if([previouStatus count]!=0){
                                    driverStatus=previouStatus[0][4];
                                    if([driverStatus isEqualToString:@""])
                                        driverStatus=@"1";
                                    }
                                else{
                                    driverStatus=@"1";
                                     }
                                    commonDataHandler *common=[commonDataHandler new];
                                    NSString * calculatedStatus=[common calculateStatus:@"1" vehicleStatus:objCheckIn.vehiclestatus driverStaus:driverStatus associatedVehicle:objCheckIn.associatedvehicle];
                                objCheckIn.calculatedstatus=calculatedStatus;
                                objCheckIn.driverstatus=driverStatus;
                                [objDB mobileCheckinUpdation:objCheckIn];
                               if(objDB.affectedRows!=0){
                                //   [objDB saveStatus:objCheckIn];
                                   vehicleString=[vehicleString stringByAppendingFormat:@"%@|",objCheckIn.associatedvehicle];
                                   if([objCheckIn.slot isEqualToString:@"1"]){
                                       distDict=[[NSMutableDictionary alloc]init];
                                       distDictLastEntry=[[NSMutableDictionary alloc]init];
                                   }
                                    //[objDB convertDateToLocal:objCheckIn.date slotFromServer:[objCheckIn.slot intValue]];
                                   if([distDict count]==0){
                                    [distDict setValue:objCheckIn.odo forKey:objCheckIn.associatedvehicle];
                                   }
                                    else
                                    {
                                    
                                     [distDictLastEntry setValue:objCheckIn.odo forKey:objCheckIn.associatedvehicle];
                                    }
                                   if([objCheckIn.slot isEqualToString:@"96"]){
                                       NSMutableArray *vehicleArr = [NSMutableArray new];
                                       vehicleArr=[[vehicleString componentsSeparatedByString:@"|"] mutableCopy];
                                       [vehicleArr removeLastObject];
                                       int dist=0;
                                       for(int i=0;i<[vehicleArr count];i++){
                                           if([distDict count]==0)
                                                dist+=([[distDictLastEntry valueForKey:[vehicleArr objectAtIndex:i]] intValue]-0);
                                           else if([distDictLastEntry count]==0)
                                                dist+=([[distDict valueForKey:[vehicleArr objectAtIndex:i]] intValue]);
                                           else
                                               dist+=([[distDictLastEntry valueForKey:[vehicleArr objectAtIndex:i]] intValue]-[[distDict valueForKey:[vehicleArr objectAtIndex:i]] intValue]);
                                       }
                                       
                                       [objDB updateDistance:objCheckIn dist:dist];
                                       
                                   }
                                  
                                }
                            }
                            
                        }
                    }
                
            }
        }

        
    }];
    
}


-(void)acknowledgeAPICall:(NSMutableArray *)arrayLogs andSuggestions:(NSMutableArray *)arraySuggestions{
    
    NSMutableArray *arrayAck = [NSMutableArray new];
    [arrayAck addObjectsFromArray:[arrayLogs mutableCopy]];
    [arrayAck addObjectsFromArray:[arraySuggestions mutableCopy]];
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0ul);
    dispatch_async(queue, ^{
        for (int i = 0; i<arrayAck.count; i++) {
                NSMutableDictionary *keyList = [[NSMutableDictionary alloc] init];

            if([[arrayAck objectAtIndex:i] isKindOfClass:[MobileCheckIn class]]){
                MobileCheckIn *objLog = [arrayAck objectAtIndex:i];
                [keyList setValue:OPERATION_ACK forKey:OPERATION];
                [keyList setValue:objLog.userName forKey:USER_NAME];
                [keyList setValue:objLog.deviceidentifier forKey:DEVICE_ID];
                [keyList setValue:[commonDataHandler sharedInstance].strAuthCode forKey:AUTH_CODE];
                [keyList setValue:objLog.ack forKey:ACK];
            }else if ([[arrayAck objectAtIndex:i] isKindOfClass:[Suggestion class]]){
                Suggestion *objSug = [arrayAck objectAtIndex:i];
                [keyList setValue:OPERATION_ACK forKey:OPERATION];
                [keyList setValue:objSug.username forKey:USER_NAME];
                [keyList setValue:objSug.deviceidentifier forKey:DEVICE_ID];
                [keyList setValue:[commonDataHandler sharedInstance].strAuthCode forKey:AUTH_CODE];
                [keyList setValue:objSug.ack forKey:ACK];
            }
                NSString *stringParams = [[commonDataHandler sharedInstance] constructInput:keyList];
                NSString *urlString = [NSString stringWithFormat:@"%@%@", WEB_ROOT, MOBILE_CHECK_IN_ACK_URL];
                __block NSString *responseString = @"";
                NSString *urlStringWithParam = [NSString stringWithFormat:@"payload=%@",stringParams];
//                NSLog(@"_________________________%@", urlStringWithParam);
                NSString *enodedURl = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                NSURL *url = [NSURL URLWithString:enodedURl] ;
                NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
                [request setURL:url];
                [request setHTTPMethod:@"POST"];
            
                [request setHTTPBody:[urlStringWithParam dataUsingEncoding:NSUTF8StringEncoding]];
                NSOperationQueue *queue1 = [[NSOperationQueue alloc] init];
                [NSURLConnection sendAsynchronousRequest:request queue:queue1 completionHandler:^(NSURLResponse * _Nullable response, NSData * _Nullable data, NSError * _Nullable connectionError) {
                    responseString = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
            
                    if(![responseString isEqualToString:@""]){

                    NSMutableArray *arrayAckResponse = [[NSMutableArray alloc] init];
                        NSMutableArray *arrayAckResult = [NSMutableArray new];
                    arrayAckResponse =[[commonDataHandler sharedInstance] parseResponse:responseString];
                        if(arrayAckResponse!=nil && [arrayAckResponse count]!=0){
                            
                            for (NSMutableDictionary *objDict in arrayAckResponse) {
                                MobileCheckIn *objMobileCheckIn = [MobileCheckIn new];
                                [objMobileCheckIn getDataFromAPI:objDict];
                                [arrayAckResult addObject:objMobileCheckIn];
                            }
                            if(arrayAckResult.count > 0){
                                MobileCheckIn *obj = [arrayAckResult firstObject];
                                if([obj.result isEqualToString:@"failed"]){
                                    if([obj.reason isEqualToString:AUTH_CODE_INVALID]){
                                        //Re-auth
                                        if(isReAuthAck){
                                            isReAuthAck = NO;
//                                            [[commonDataHandler sharedInstance] reAuthenticateUser:^(BOOL finished) {
//                                                [self acknowledgeAPICall:arrayForCheckInCommon andSuggestions:arrayForSuggestionCommon];
//                                            }];
                                        }else{
                                            //BFLogErr(@"%@", obj.reason);
                                        }
                                    }
                                }
                            }
                            
                        }
                    }
            
            
                }];

        }
        // Perform async operation
        dispatch_sync(dispatch_get_main_queue(), ^{
            // Update UI
        });
    });


}




@end

