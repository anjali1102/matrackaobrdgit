//
//  CarrierSettings.h
//  MaTrackHOS
//
//  Created by Raghee Chandran M on 7/12/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "settingsMenu.h"
#import "logScreenViewController.h"
#import "menuScreenViewController.h"
#import "DBManager1.h"
#import "DBManager.h"
#import "Carrier.h"
#import "settingsMenu.h"
#import "AppDelegate.h"
@interface CarrierSettings : UIViewController<UITextFieldDelegate, UIPickerViewDataSource, UIPickerViewDelegate>{
    
    UIPickerView *mainPicker1;
    UIPickerView *homePicker;
    
    
    
}
@property (weak, nonatomic) IBOutlet UITextField *main_state1;
@property (weak, nonatomic) IBOutlet UITextField *home_state;
@property (strong, nonatomic) NSArray *startes;
@property (strong, nonatomic) NSArray *statecodes;
@property (nonatomic, strong) DBManager1 *dbManager;
@property (strong, nonatomic) IBOutlet UIScrollView *scroller05;
@property (weak, nonatomic) IBOutlet UILabel *mainLabel;
@property (weak, nonatomic) IBOutlet UILabel *homeLabel;

@property (weak, nonatomic) IBOutlet UILabel *carrierLabel;
@property (weak, nonatomic) IBOutlet UITextField *carrier_txtfield;
- (IBAction)Savebtn:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *officeaddress1;
@property (weak, nonatomic) IBOutlet UITextField *city1;
@property (weak, nonatomic) IBOutlet UITextField *zip1;
@property (weak, nonatomic) IBOutlet UITextField *home1;
@property (weak, nonatomic) IBOutlet UITextField *city2;
@property (weak, nonatomic) IBOutlet UITextField *zip2;
@property (nonatomic, strong) Carrier *carrierData;
@end
