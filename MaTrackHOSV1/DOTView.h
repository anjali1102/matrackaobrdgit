//
//  DOTView.h
//  MaTrackHOS
//
//  Created by Raghee Chandran M on 7/6/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"


extern CGRect current_rect1;
extern int flag1;
#define kkGraphHeight 75
#define kkDefaultGraphWidth 285.5
#define kkOffsetX 43
//#define kkStepX 15.21
#define kkGraphBottom 0
#define kkGraphTop 117
#define kkStepY 15
#define kkOffsetY 0
#define kkCircleRadius 1
@interface DOTView : UIView

@end
