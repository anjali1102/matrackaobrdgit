//
//  DOTView.m
//  MaTrackHOS
//
//  Created by Raghee Chandran M on 7/6/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//

#import "DOTView.h"
CGRect current_rect1;
CGContextRef context1;
int flag1=0;
float kkStepX=0;
@implementation DOTView
- (void)drawLineGraphWithContext:(CGContextRef)ctx
{
    //BFLog(@"%@,%@",start_time,end_time);
    BFLog(@"graph loading......");
    CGContextSetLineWidth(ctx, 2.0);
    CGContextSetStrokeColorWithColor(ctx, [[UIColor colorWithRed:1.0 green:0.0 blue:0 alpha:2.0] CGColor]);
    
    //  if (flag==0) {
    
    // int maxGraphHeight = kGraphHeight - kOffsetY;
    //int y_origin=0;
    int y_offset=0;
    CGContextBeginPath(ctx);
    NSString *x1=dataarray1[0][1];
    if([x1  isEqual: @"ON"]){
        y_origin1 = 4;
        CGContextMoveToPoint(ctx, kkOffsetX, ((y_origin1) * kkStepY));
        float x = kkOffsetX;
        float y = ((y_origin1) * kkStepY);
        CGRect rect = CGRectMake(x - kkCircleRadius, y - kkCircleRadius, 2 * kkCircleRadius, 2 * kkCircleRadius);
        CGContextAddEllipseInRect(ctx, rect);
    }else  if([x1  isEqual: @"D"]){
        y_origin1 = 3;
        CGContextMoveToPoint(ctx, kkOffsetX, ((y_origin1) * kkStepY));
        float x = kkOffsetX;
        float y = ((y_origin1) * kkStepY);
        CGRect rect = CGRectMake(x - kkCircleRadius, y - kkCircleRadius, 2 * kkCircleRadius, 2 * kkCircleRadius);
        CGContextAddEllipseInRect(ctx, rect);
    }else  if([x1  isEqual: @"SB"]){
        y_origin1 =2;
        y_offset = 23;
        float x = kkOffsetX;
        float y = ((y_origin1) * kkStepY);
        CGRect rect = CGRectMake(x - kkCircleRadius, y - kkCircleRadius, 2 * kkCircleRadius, 2 * kkCircleRadius);
       CGContextAddEllipseInRect(ctx, rect);
        CGContextMoveToPoint(ctx, kkOffsetX, ((y_origin1) * kkStepY));
        
    }else  if([x1  isEqual: @"OFF"]){
        y_origin1 = 1;
        float x = kkOffsetX;
        float y = ((y_origin1) * kkStepY);
        CGRect rect = CGRectMake(x - kkCircleRadius, y - kkCircleRadius, 2 * kkCircleRadius, 2 * kkCircleRadius);
        CGContextAddEllipseInRect(ctx, rect);
        CGContextMoveToPoint(ctx, kkOffsetX, ((y_origin1) * kkStepY));
    }
    BFLog(@"y origin:%d",y_origin1);
    int data_size =(int) [dataarray1 count];
    BFLog(@"array size:%d",data_size);
    for (int i = 0; i < data_size; i++)
    {
        int y_pos=0;
        NSArray *time = [dataarray1[i][0] componentsSeparatedByString:@"."];
        NSString *starttime = [NSString stringWithFormat:@"%@.%@",time[0],time[1]];
        float x_one = [starttime floatValue];
                int y=0;
        NSString *x2 = dataarray1[i][1];
        if([x2  isEqual: @"ON"]){
            y_pos = 4;
            y = ((y_pos) * kkStepY);
            //NSString *posy = [NSString stringWithFormat:@"%d",y];
            //dataarray[i][1]=posy;
            
        }else  if([x2  isEqual: @"D"]){
            y_pos = 3;
            y = ((y_pos) * kkStepY);
            // NSString *posy = [NSString stringWithFormat:@"%d",y];
            //dataarray[i][1]=posy;             ;
        }else  if([x2  isEqual: @"SB"]){
            y_pos = 2;
            y_offset = 23;
            y = ((y_pos) * kkStepY);
            //NSString *posy = [NSString stringWithFormat:@"%d",y];
            //dataarray[i][1]=posy;
            
        }else  if([x2  isEqual: @"OFF"]){
            y_pos = 1;
            y = ((y_pos) * kkStepY);
            //NSString *posy = [NSString stringWithFormat:@"%d",y];
            //dataarray[i][1]=posy;
        }
        BFLog(@"x,y:%f,%d",x_one,y);
        CGContextAddLineToPoint(ctx, kkOffsetX+(kkStepX*(x_one)),y);
        
        //CGContextAddLineToPoint(ctx, kStepX*(0.45)+9,((1 + 3) * 22));
        
        // CGContextAddLineToPoint(ctx, kStepX*(15.45)+9,((1 + 3) * 22));
        
        //CGContextAddLineToPoint(ctx, kStepX*(15.45)+9,((1 + 2) * 23));
        
        // CGContextAddLineToPoint(ctx, kStepX*(15.45)+9,((1 + 2) * 23));
        //  double x1 = [dataarray[0][0] doubleValue];
        //CGContextAddLineToPoint(ctx, x1, ((1 + 2) * 23));
        
        //}
    }
    BFLog(@"new array with new y pos%@",dataarray1);
    CGContextDrawPath(ctx, kCGPathStroke);
    CGContextSetFillColorWithColor(ctx, [[UIColor colorWithRed:1.0 green:0.5 blue:0 alpha:2.0] CGColor]);
    
    
   //BFLog(@"dta size:%lu",sizeof(data)/sizeof(float));
    //sample_data = self.data;
    BFLog(@"data:%@",dataarray1);
    
    CGContextDrawPath(ctx, kCGPathFillStroke);
    
    
    
    
    //CGContextRetain(ctx);
    UIGraphicsPushContext(ctx);
    CGContextStrokePath(ctx);
    /*}
     else{
     BFLog(@"Plottinggg updated values");
     NSArray *start = [start_time componentsSeparatedByString:@":"];
     NSArray *end = [end_time componentsSeparatedByString:@":"];
     NSString *start_val =[NSString stringWithFormat:@"%@.%@",start[0],start[1]];
     NSString *end_val = [NSString stringWithFormat:@"%@.%@",end[0],end[1]];
     float start1 = [start_val floatValue];
     float end1 = [end_val floatValue];
     BFLog(@"start n end time:%f,%f",start1,end1);
     UIGraphicsPopContext();
     CGContextBeginPath(ctx);
     //CGContextMoveToPoint(ctx, kOffsetX, ((1 + 1) * 22));
     
     // for (int i = 1; i <= 4; i++@
     //{
     BFLog(@"%f,%d",kStepX*(0.45)+9,((1 + 1) * 24));
     
     
     CGContextMoveToPoint(ctx,kStepX*(16.45)+9,((1 + 1) * 24));
     
     //CGContextAddLineToPoint(ctx, kStepX*(0.45)+9,((1 + 1) * 24));
     //CGContextAddLineToPoint(ctx, kStepX*(0.45)+9,((1 + 3) * 22));
     
     CGContextAddLineToPoint(ctx, kStepX*(19.15)+9,((1 + 3) * 22));
     
     CGContextAddLineToPoint(ctx, kStepX*(19.15)+9,((1 + 2) * 23));
     
     CGContextAddLineToPoint(ctx, kStepX*(19.45)+9,((1 + 2) * 23));
     // }
     CGContextDrawPath(ctx, kCGPathStroke);
     CGContextSetFillColorWithColor(ctx, [[UIColor colorWithRed:1.0 green:0.5 blue:0 alpha:2.0] CGColor]);
     BFLog(@"dta size:%lu",sizeof(data)/sizeof(float));
     // BFLog(@"dta size:%f,%f",data[0],data[2]);
     //for (int i = 1; i <= sizeof(data)/sizeof(float); i++)
     //{
     float x = kStepX*(0.45)+9;
     float y = ((1 + 1) * kStepY)+10.0;
     CGRect rect = CGRectMake(x - kCircleRadius, y - kCircleRadius, 2 * kCircleRadius, 2 * kCircleRadius);
     CGContextAddEllipseInRect(ctx, rect);
     //}
     CGContextDrawPath(ctx, kCGPathFillStroke);
     
     CGContextStrokePath(ctx);
     }*/
    
    
    /* for (int i = 0; i <= 24; i++)
     {
     NSString *theText=[[NSString alloc]init];
     if (i == 0 || i == 24){
     theText = @"M";
     } else if(i == 12){
     theText = @"N";
     
     }else {
     theText = [NSString stringWithFormat:@"%d", i];
     }
     BFLog(@"%f,%f",kOffsetX+i*kStepX, kGraphBottom-12.0);
     [theText drawAtPoint:CGPointMake(kOffsetX+i*kStepX,0.0) withAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"Helvetica" size:5]}];
     
     }*/
    //NSArray *events = [[NSArray alloc]initWithObjects:@"OFF",@"SB",@"D",@"ON", nil];
    /* for (int i = 0; i < 4; i++)
     {
     //NSString *theText = [NSString stringWithFormat:@"%@", events[i]];
     BFLog(@"%d,%d",0, i*5);
     // [theText drawAtPoint:CGPointMake(0, kGraphBottom - kOffsetY - (i+1) * kStepY) withAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"Helvetica" size:8]}];
     
     }*/
    
    
    
    
}

- (void)drawRect:(CGRect)rect {
 BFLog(@"view refreshingg....:%@",NSStringFromCGRect(rect));
 current_rect1 = CGRectFromString(NSStringFromCGRect(rect));
 //flag=flag1;
 context1 = UIGraphicsGetCurrentContext();
 //current_context = UIGraphicsGetCurrentContext();
 BFLog(@"current context:%@",context1);
 CGContextSetLineWidth(context1, 0.6);
 CGContextSetStrokeColorWithColor(context1, [[UIColor lightGrayColor] CGColor]);
 //CGFloat dash[] = {2.0, 2.0};
 //CGContextSetLineDash(context, 0.0, dash, 2);    // How many lines?
 //int howManyk = (kkDefaultGraphWidth - kkOffsetX) / kkStepX;
 int howManyk=24;
    kkStepX=(kkDefaultGraphWidth)/(howManyk);
 BFLog(@"vertical lines:%d",howManyk);
 // Here the lines go
 for (int i = 0; i <= howManyk; i++)
 {
 CGContextMoveToPoint(context1, kkOffsetX + i * kkStepX, kkGraphTop);
 CGContextAddLineToPoint(context1, kkOffsetX + i * kkStepX, kkGraphBottom);
 }
 //int howManyHorizontal = (kGraphBottom - kGraphTop - kOffsetY) / kStepY;
 int howManyHorizontalk = 4;
 BFLog(@"horizontal lines:%d",howManyHorizontalk);
 for (int i = 0; i <howManyHorizontalk; i++)
 {
 BFLog(@"%d,%d",kkOffsetX, kkGraphBottom - kkOffsetY - i * kkStepY);
 CGContextMoveToPoint(context1, kkOffsetX, kkGraphBottom - kkOffsetY - i * kkStepY);
 //BFLog(@"%d,%f",kDefaultGraphWidth, kGraphBottom - kOffsetY - i * kStepY);
 //BFLog(@"------------------");
 CGContextAddLineToPoint(context1, kkDefaultGraphWidth, kkGraphBottom - kkOffsetY - i * kkStepY);
 }
 CGContextStrokePath(context1);
 CGContextSetLineDash(context1, 0, NULL, 0); // Remove the dash
 
 
 UIImage *image = [UIImage imageNamed:@"graph_full2.png"];
 CGRect imageRect = CGRectMake(0, 0, 367,100);
 CGContextDrawImage(context1, imageRect, image.CGImage);
    
 if (flag1 != 0) {
     BFLog(@"updating the view....");
    [self drawLineGraphWithContext:context1];
   flag1 = 0;
    
}
}
@end
