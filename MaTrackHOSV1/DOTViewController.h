//
//  DOTViewController.h
//  MaTrackHOS
//
//  Created by Raghee Chandran M on 7/6/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "DOTView.h"
#import "ViewController.h"
#import "customCell.h"
#import "logScreenViewController.h"
#import "dotmenuViewController.h"
#import "DBManager1.h"


extern NSString *date;
@interface DOTViewController : UIViewController <UITextFieldDelegate,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UIAlertViewDelegate> {
    
    
     UIDatePicker *datepicker;
}
@property (nonatomic, strong) DBManager1 *dbManager;
@property (weak, nonatomic) IBOutlet UIWebView *dotweb;
/*@property (weak, nonatomic) IBOutlet UIBarButtonItem *barButton;
@property (weak, nonatomic) IBOutlet UILabel *date;
@property (weak, nonatomic) IBOutlet UILabel *titles;
- (IBAction)next:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *nextbtn;

@property (weak, nonatomic) IBOutlet UILabel *timezone;
- (IBAction)previous:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *rule;*/
@property (weak, nonatomic) IBOutlet UICollectionView *settingsdata;
@property (weak, nonatomic) IBOutlet UINavigationItem *navigation;
@property (weak, nonatomic) IBOutlet UIScrollView *scroller_dot;
@end
