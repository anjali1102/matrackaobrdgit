//
//  ELDInfoCell.h
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 9/25/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ELDInfoCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *currentStatusView;
@property (weak, nonatomic) IBOutlet UIImageView *icon;
@property (weak, nonatomic) IBOutlet UILabel *status;
@property (weak, nonatomic) IBOutlet UIView *currentVehicleView;
@property (weak, nonatomic) IBOutlet UILabel *currentVehicle;
@property (nonatomic, strong) IBOutlet UILabel *lblCurrentVehicle;
@end
