//
//  LogSettings.h
//  MaTrackHOS
//
//  Created by Raghee Chandran M on 7/12/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "settingsMenu.h"
#import "logScreenViewController.h"
#import "menuScreenViewController.h"
#import "DBManager1.h"
#import "DBManager.h"
#import "rules.h"
#import "User.h"

@interface LogSettings : UIViewController <UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate,UIAlertViewDelegate>{
    UIPickerView *timezonePicker;
    UIPickerView *cyclePicker;
    UIPickerView *cargoPicker;
    UIPickerView *odometerPicker;
    UIPickerView *logincremenrPicker;
    UIPickerView *restartPicker;

    
    
    
    
    
}
-(float)calculatePosx:(NSString *)time;
@property (weak, nonatomic) IBOutlet UITextField *timeZone;
@property (weak, nonatomic) IBOutlet UITextField *cycle;
@property (weak, nonatomic) IBOutlet UITextField *cargoType;
- (IBAction)save:(id)sender;
@property (strong, nonatomic) IBOutlet UIScrollView *scroller06;
@property (weak, nonatomic) IBOutlet UILabel *cycleLabel;
@property (weak, nonatomic) IBOutlet UILabel *odometerLabel;
@property (weak, nonatomic) IBOutlet UILabel *restartLabel;
@property (weak, nonatomic) IBOutlet UILabel *cargoLabel;
@property (weak, nonatomic) IBOutlet UITextField *restBreak;

@property (weak, nonatomic) IBOutlet UILabel *homeLabel;
@property (weak, nonatomic) IBOutlet UITextField *odometer;
@property (weak, nonatomic) IBOutlet UITextField *restart;
@property (nonatomic, strong) DBManager1 *dbManager;
@property (nonatomic, strong) rules *logSettingsData;

@property (weak, nonatomic) IBOutlet UILabel *restBreakLabel;

@property (strong, nonatomic) NSArray *timeZones;
@property (strong, nonatomic) NSArray *cycles;
@property (strong, nonatomic) NSArray *cargotypes;
@property (strong, nonatomic) NSArray *odometervalues;
@property (strong, nonatomic) NSArray *locincvalues;
@property (strong, nonatomic) NSArray *restartvalues;


@end
