//
//  SignaturePad.m
//  MaTrackHOS
//
//  Created by Raghee Chandran M on 7/7/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//

#import "SignaturePad.h"
CGRect rect1;

@implementation SignaturePad
{
    UIBezierPath *path; // (3)
}

- (id)initWithCoder:(NSCoder *)aDecoder // (1)
{
    if (self = [super initWithCoder:aDecoder])
    {
        [self setMultipleTouchEnabled:NO]; // (2)
        [self setBackgroundColor:[UIColor cyanColor]];
        path = [UIBezierPath bezierPath];
        [path setLineWidth:1.5];
    }
    return self;
}

- (void)drawRect:(CGRect)rect // (5)
{
    [self setNeedsDisplay];
    [[UIColor blackColor] setStroke];
    [path stroke];
    rect1=rect;
}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    CGPoint p = [touch locationInView:self];
    [path moveToPoint:p];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    CGPoint p = [touch locationInView:self];
    [path addLineToPoint:p]; // (4)
    [self setNeedsDisplay];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self touchesMoved:touches withEvent:event];
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self touchesEnded:touches withEvent:event];
}

- (void)erase{
    BFLog(@"Erasing");
    path = nil;
    path = [UIBezierPath bezierPath];
    [self setNeedsDisplay];
       
    
}

@end
