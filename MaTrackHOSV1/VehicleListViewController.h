//
//  VehicleListViewController.h
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 9/25/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DBManager.h"
#import "user2vehicle.h"

@protocol VehicleListViewControllerDelegate <NSObject>
-(void)onSelectVehicleName:(NSString *)strVehicleName;
@end

@interface VehicleListViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate>
@property (weak, nonatomic) IBOutlet UISearchBar *searchVehicle;
@property (strong, nonatomic) IBOutlet UITableView *vehicleList;
@property (nonatomic, weak) id<VehicleListViewControllerDelegate> delegate;
@end
