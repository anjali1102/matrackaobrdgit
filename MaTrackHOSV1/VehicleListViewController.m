//
//  VehicleListViewController.m
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 9/25/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import "VehicleListViewController.h"
#import "commonDataHandler.h"
#import "Header.h"
#import "MobileCheckIn.h"
#import "DBManager.h"
#import "VehicleSelection.h"
#import "MBProgressHUD.h"
#import "NotificationHandler.h"
#import "StatusViewController.h"
@interface VehicleListViewController ()<UIAlertViewDelegate>
{
    NSString *strVehicleName;
    BOOL isReAuth;
    BOOL isReAuthPossible;
    UIAlertView *  alert;

}
@end
NSArray *vehicles;
NSMutableArray *vehListTable;
@implementation VehicleListViewController
@synthesize vehicleList;
@synthesize searchVehicle;
- (void)viewDidLoad {
    [super viewDidLoad];
   
    vehicleList.delegate = self;
    vehicleList.dataSource = self;
    vehicleList.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    isReAuth = YES;
    isReAuthPossible = YES;
    [self possibleVehicleAPI];
    // Do any additional setup after loading the view.
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"DataUpdated" object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleUpdatedData:)
                                                 name:@"DataUpdated"
                                               object:nil];
}
-(void)handleUpdatedData:(NSNotification *)notification {
    NSDictionary* userInfo = notification.userInfo;
    driver_status drvStat;
    int driverStat;
    driverStat = [[userInfo valueForKey:@"driverStatus"] intValue];
    if(driverStat == 1)
        drvStat = driver_status_driving;
    else if (driverStat == 2)
        drvStat = driver_status_onDuty;
    else if (driverStat == 3)
        drvStat = driver_status_offDuty;
    else if (driverStat == 4)
        drvStat = driver_status_none;
    else if (driverStat == 6)
        drvStat = driver_status_onDuty_immediate;
    else
        drvStat = driver_status_connect_back;
    //    if(driverStat == 2){
    //        dispatch_async(dispatch_get_main_queue(), ^{
    //            alert = [[UIAlertView alloc]initWithTitle:@"Status Change" message:@"Do you want to continue driving or change your duty status" delegate:self cancelButtonTitle:@"Continue Driving" otherButtonTitles:@"Change Status", nil];
    //            alert.tag=1005;
    //            alert.delegate=self;
    //            [alert show];
    //            [self performSelector:@selector(dismissAlertView) withObject:nil afterDelay:60.0];
    //        });
    //    }else{
    NotificationHandler *objHelper = [NotificationHandler new];
    [objHelper recievePushNotification:self withDriverStatus:drvStat];
    //  }
}
- (void)dismissAlertView {
    NotificationHandler *objHelper = [NotificationHandler new];
    [objHelper recievePushNotification:self withDriverStatus:driver_status_onDuty_immediate];
    [alert dismissWithClickedButtonIndex:-1 animated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark    - <UITableView Delegates & Datasource>
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{

    return @"All Vehicles";
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    return [vehListTable count];
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if(cell==nil){
        cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"cell"];
    }
    cell.textLabel.text=vehListTable[indexPath.row];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    strVehicleName = vehListTable[indexPath.row];
    [self dismissViewControllerAnimated:YES completion:^{
        isReAuth = YES;
        [self callVehicleSelectAPI:strVehicleName];
    }];
}

#pragma mark    - <SearchBar Delegate>
-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    NSString *text;
    if([searchText length]>0){
        [vehListTable removeAllObjects ];
        for(int i=0;i<[vehicles count];i++){
            text=[vehicles objectAtIndex:i];
            if(text.length>= searchText.length){
                NSRange result=[text rangeOfString:searchText options:NSCaseInsensitiveSearch];
                if(result.length>0){
                    [vehListTable addObject:[vehicles objectAtIndex:i]];
                }
            }
        }
    }
    else{
        [vehListTable addObjectsFromArray:vehicles];
    }
    [vehicleList reloadData];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark    - <API call - Vehicle Selection>
-(void)callVehicleSelectAPI:(NSString *)strSelectedVeh{

    NSMutableDictionary *requestDict = [[NSMutableDictionary alloc] init];
    [requestDict setValue:OPERATION_VEHICLE forKey:OPERATION];
    [requestDict setValue:[commonDataHandler sharedInstance].strUserName forKey:USER_NAME];
    [requestDict setValue:[Bugfender deviceIdentifier] forKey:DEVICE_ID];
    [requestDict setValue:[commonDataHandler sharedInstance].strAuthCode forKey:AUTH_CODE];
    [requestDict setValue:strSelectedVeh forKey:SELECTED_VEHICLE];
    NSString *stringParams = [[commonDataHandler sharedInstance] constructInput:requestDict];
    [self getConnection:[NSString stringWithFormat:@"%@%@", WEB_ROOT, VEHICLE_SELECTION_URL] withParams:stringParams];
    
}
-(void)getConnection:(NSString *)urlString withParams:(NSString*)paramString{
    __block NSString *responseString = @"";
     [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSString *urlStringWithParam = [NSString stringWithFormat:@"payload=%@",paramString];
    NSString *enodedURl = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [NSURL URLWithString:enodedURl] ;
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    
    [request setHTTPBody:[urlStringWithParam dataUsingEncoding:NSUTF8StringEncoding]];
    NSOperationQueue *queue1 = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:request queue:queue1 completionHandler:^(NSURLResponse * _Nullable response, NSData * _Nullable data, NSError * _Nullable connectionError) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        });
        responseString = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
        if(![responseString isEqualToString:@""]){
            NSMutableArray *arrayResponse = [[NSMutableArray alloc] init];
            NSMutableArray *arrayMobileCheckIn   = [NSMutableArray new];
            [arrayMobileCheckIn removeAllObjects];
            [arrayResponse removeAllObjects];
            arrayResponse =[[commonDataHandler sharedInstance] parseResponse:responseString];
            
            if(arrayResponse!=nil && [arrayResponse count]!=0){
                
                for (NSMutableDictionary *objDict in arrayResponse) {
                    MobileCheckIn *objMobileCheckIn = [MobileCheckIn new];
                    [objMobileCheckIn getDataFromAPI:objDict];
                    [arrayMobileCheckIn addObject:objMobileCheckIn];
                }

                if(arrayMobileCheckIn.count > 0){
                    MobileCheckIn *objMobileCheckIn = [arrayMobileCheckIn firstObject];
                    if([objMobileCheckIn.result isEqualToString:@"failed"]){
                        if([objMobileCheckIn.reason isEqualToString:AUTH_CODE_INVALID]){
                            //Re-auth
                            if(isReAuth){
                                isReAuth = FALSE;
//                                [[commonDataHandler sharedInstance] reAuthenticateUser:^(BOOL finished) {
//                                    [self callVehicleSelectAPI:strVehicleName];
//                                }];
                               
                            }
                            
                        }else{
                            dispatch_async(dispatch_get_main_queue(), ^{
                                UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                                 message:objMobileCheckIn.reason
                                                                                delegate:nil
                                                                       cancelButtonTitle:@"OK"
                                                                       otherButtonTitles:nil, nil];
                                [alert1 show];
                            });
                        }
                    }else{
                        [[NSUserDefaults standardUserDefaults] setObject:strVehicleName forKey:SELECTED_VEHICLE];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        [self.delegate onSelectVehicleName:strVehicleName];
                        [self saveToDB:strVehicleName];
                        
                    }
                }
            }else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Select Vehicle"
                                                                     message:@"Something wrong happened!!"
                                                                    delegate:nil
                                                           cancelButtonTitle:@"OK"
                                                           otherButtonTitles:nil, nil];
                    [alert1 show];
                });
            }
        }
        
        
    }];
    
}
-(void)saveToDB:(NSString *)strVehicleName{
    DBManager *objDB= [[DBManager alloc] initWithDatabaseFilename:@"hosDB.sql"];
    NSArray *userInfoArray=[[NSArray alloc]initWithArray: [objDB getUserId]];
    if([userInfoArray count]!=0){
        int userId = [userInfoArray[0][0] intValue];
        NSDictionary *token=@{@"columnName":@"userid",@"entry":[NSString stringWithFormat:@"%d",userId],};
        NSMutableDictionary *whereToken=[[NSMutableDictionary alloc] initWithDictionary:token];
        user2vehicle *objUsrVeh=[user2vehicle new];
        objUsrVeh.userid=[NSString stringWithFormat:@"%d", userId];
        objUsrVeh.vehicleid=strVehicleName;
        [objDB userToVehicle:objUsrVeh];
    }
}
-(void)possibleVehicleAPI{
    NSMutableDictionary *keyList = [[NSMutableDictionary alloc] init];
        [keyList setValue:@"possiblevehicle" forKey:OPERATION];
        [keyList setValue:[commonDataHandler sharedInstance].strUserName forKey:USER_NAME];
        [keyList setValue:[Bugfender deviceIdentifier] forKey:DEVICE_ID];
        [keyList setValue:[commonDataHandler sharedInstance].strAuthCode forKey:AUTH_CODE];
    NSString *stringParams = [[commonDataHandler sharedInstance] constructInput:keyList];
    __block NSString *responseString = @"";
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSString *urlString = [NSString stringWithFormat:@"%@%@", WEB_ROOT, POSSIBLE_VEHICLE_LIST_URL];
    NSString *urlStringWithParam = [NSString stringWithFormat:@"payload=%@",stringParams];
    NSString *enodedURl = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [NSURL URLWithString:enodedURl] ;
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    
    [request setHTTPBody:[urlStringWithParam dataUsingEncoding:NSUTF8StringEncoding]];
    NSOperationQueue *queue1 = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:request queue:queue1 completionHandler:^(NSURLResponse * _Nullable response, NSData * _Nullable data, NSError * _Nullable connectionError) {
        responseString = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        });
        if(![responseString isEqualToString:@""]){
            NSMutableArray *arrayResponse = [[NSMutableArray alloc] init];
            NSMutableArray *arrayVehicleList   = [NSMutableArray new];
            [arrayVehicleList removeAllObjects];
            [arrayResponse removeAllObjects];
            arrayResponse =[[commonDataHandler sharedInstance] parseResponse:responseString];
            
            if(arrayResponse!=nil && [arrayResponse count]!=0){
                
                for (NSMutableDictionary *objDict in arrayResponse) {
                    VehicleSelection *objVehicle= [VehicleSelection new];
                    [objVehicle getDataFromAPI:objDict];
                    [arrayVehicleList addObject:objVehicle];
                }
                
                if(arrayVehicleList.count > 0){
                    VehicleSelection *objMobileCheckIn = [arrayVehicleList firstObject];
                    if([objMobileCheckIn.strResult isEqualToString:@"failed"]){
                        if([objMobileCheckIn.strReason isEqualToString:AUTH_CODE_INVALID]){
                            //Re-auth
                            if(isReAuthPossible){
                                isReAuthPossible = FALSE;
//                                [[commonDataHandler sharedInstance] reAuthenticateUser:^(BOOL finished) {
//                                    [self possibleVehicleAPI];
//                                }];
                                
                            }
                            
                        }else{
                            dispatch_async(dispatch_get_main_queue(), ^{
                                UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                                 message:objMobileCheckIn.strReason
                                                                                delegate:nil
                                                                       cancelButtonTitle:@"OK"
                                                                       otherButtonTitles:nil, nil];
                                [alert1 show];
                            });
                        }
                    }else{
                        
                        VehicleSelection *objVeh = [arrayVehicleList firstObject];
                                                        vehicles = objVeh.arrayPossibleVehicles;
                                                        vehListTable=[vehicles mutableCopy];
                    }
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [vehicleList reloadData];
                    });
                }
            }
        }
        
        
    }];
    
}
@end
