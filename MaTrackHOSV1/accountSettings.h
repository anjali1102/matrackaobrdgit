//
//  accountSettings.h
//  MaTrackHOS
//
//  Created by Raghee Chandran M on 7/15/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "settingsMenu.h"
#import "ViewController.h"
#import "logScreenViewController.h"
#import "menuScreenViewController.h"
#import "DBManager1.h"
#import "User.h"
#import "DBManager.h"
#import "settingsMenu.h"
#import "AppDelegate.h"
@interface accountSettings : UIViewController<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *name;
@property (weak, nonatomic) IBOutlet UITextField *driverid;
@property (weak, nonatomic) IBOutlet UITextField *email;
@property (weak, nonatomic) IBOutlet UITextField *mobile;
@property (nonatomic, strong) DBManager1 *dbManager;
@property (strong, nonatomic) IBOutlet UIScrollView *scroller04;
@property (weak, nonatomic) IBOutlet UITextField *DOT;
@property (weak, nonatomic) IBOutlet UILabel *driverIdLabel;
@property (weak, nonatomic) IBOutlet UILabel *mobileLabel;
@property (weak, nonatomic) IBOutlet UILabel *dotLabel;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UITextField *lastnameLabel;
@property (weak, nonatomic) IBOutlet UILabel *lastName;
@property (nonatomic,strong) User *accountData;
@property (weak, nonatomic) IBOutlet UILabel *emailLabel;
- (IBAction)saveSettings:(id)sender;
-(void)addRequired:(UILabel*)label;
@end
