//
//  dotmenuViewController.h
//  MaTrackHOS
//
//  Created by Raghee Chandran M on 7/25/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "logScreenViewController.h"

@interface dotmenuViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *dotTable;
- (IBAction)helpAlert:(id)sender;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *menu_dot;

@end
