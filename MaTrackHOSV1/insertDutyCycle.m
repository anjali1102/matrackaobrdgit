//
//  insertDutyCycle.m
//  MaTrackHOS
//
//  Created by Raghee Chandran M on 7/19/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//

#import "insertDutyCycle.h"
#import "MBProgressHUD.h"
#import "accountSettings.h"
#import "KeychainItemWrapper.h"
BOOL returnString = NO;
int slotInsert;
int repeat;
int insertStatus=0;
NSString *address_full1=@"";
NSString *status_new=@"";
UITextField *start;
UITextField *End;
UIButton *offButton1;
UIButton *onButton1;
UIButton *sbButton1;
//UIButton *dButton1;
UIButton *address1;
UITextField *location1;
UITextField *notes1;
NSInteger insertedPos=0;
NSMutableArray *location1_list;
NSMutableArray *notes_array;
NSMutableArray *statusArr;
NSInteger autoLoginCount=0;
NSString *statusString=@"";
NSString *statusStrNew=@"";
insertView *i;

@interface insertDutyCycle (){
    
    CLGeocoder *geocoder;
    CLPlacemark *placemark;
}
@property (atomic, assign) BOOL canceled;
@end

@implementation insertDutyCycle
@synthesize dbManager;
@synthesize contentTable;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    insertedPos=0;
    UITapGestureRecognizer *backgroundTapped=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(backgroundTap)];
    [self.view addGestureRecognizer:backgroundTapped];
    contentTable.delegate=self;
    contentTable.dataSource=self;
    
    dbManager = [[DBManager1 alloc] initWithDatabaseFilename:@"HOSDatabase.sql"];
    locationManager = [[CLLocationManager alloc] init];
    geocoder = [[CLGeocoder alloc] init];
    locationManager.delegate = self;
    [locationManager requestWhenInUseAuthorization];
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager startUpdatingLocation];
    
    BFLog(@"date:%f",contentTable.frame.size.width/2);
    [self getthedata:current_date];
    start=[[UITextField alloc] initWithFrame:CGRectMake(30, 0, 100, 40)];
    start.placeholder=@"Start Time";
    start.textAlignment=NSTextAlignmentRight;
    start.delegate=self;
    startPicker=[[UIDatePicker alloc] init];
    startPicker.datePickerMode =  UIDatePickerModeTime;
    startPicker.minuteInterval=15.0;
    start.inputView = startPicker;
    [startPicker addTarget:self action:@selector(ShowSelectedDate1) forControlEvents:UIControlEventValueChanged];

    UIToolbar *toolBar=[[UIToolbar alloc]initWithFrame:CGRectMake(0,0,230,44)];
    [toolBar setTintColor:[UIColor blackColor]];
    UIBarButtonItem *doneBtn=[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(donePressed1)];
    UIBarButtonItem *space=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [toolBar setItems:[NSArray arrayWithObjects:space, doneBtn,nil]];
    [start setInputAccessoryView:toolBar];
    End=[[UITextField alloc] initWithFrame:CGRectMake(150, 0, 100, 40)];
    End.placeholder=@"End Time";
    End.textAlignment=NSTextAlignmentRight;
    
    End.delegate=self;
    endPicker=[[UIDatePicker alloc] init];
    endPicker.datePickerMode =  UIDatePickerModeTime;
    endPicker.minuteInterval=15.0;
    if (section_index==1) {
       commonDataHandler *common=[commonDataHandler new];
        NSDate *date=[common getCurrentTimeForPickerInLocalTimeZone];
        endPicker.maximumDate=[NSDate date];
        startPicker.maximumDate=[NSDate date];
    }
    End.inputView = endPicker;
    [endPicker addTarget:self action:@selector(ShowSelectedDate2) forControlEvents:UIControlEventValueChanged];

    
    UIToolbar *toolBar1=[[UIToolbar alloc]initWithFrame:CGRectMake(0,0,230,44)];
    [toolBar1 setTintColor:[UIColor blackColor]];
    UIBarButtonItem *doneBtn1=[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(donePressed2)];
    UIBarButtonItem *space1=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [toolBar1 setItems:[NSArray arrayWithObjects:space1, doneBtn1,nil]];
    [End setInputAccessoryView:toolBar1];
    // Do any additional setup after loading the view.
}
//minnu
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    if([status_new isEqualToString:@"OFF"]){
        [offButton1 sendActionsForControlEvents:UIControlEventTouchUpInside];
        offButton1.backgroundColor = [UIColor colorWithRed:0.0 green:0.99 blue:0.99 alpha:1.0];

    }else if([status_new isEqualToString:@"D"]){
        [dButton1 sendActionsForControlEvents:UIControlEventTouchUpInside];
        dButton1.backgroundColor = [UIColor colorWithRed:0.0 green:0.99 blue:0.99 alpha:1.0];
    }else if([status_new isEqualToString:@"ON"]){
        [onButton1 sendActionsForControlEvents:UIControlEventTouchUpInside];
        onButton1.backgroundColor = [UIColor colorWithRed:0.0 green:0.99 blue:0.99 alpha:1.0];

    }else if([status_new isEqualToString:@"SB"]){
        [sbButton1 sendActionsForControlEvents:UIControlEventTouchUpInside];
        sbButton1.backgroundColor = [UIColor colorWithRed:0.0 green:0.99 blue:0.99 alpha:1.0];
    }
//    BFLog(@"entered return key");
    return YES;
}
- (void)backgroundTap {
    [self.view endEditing:YES];
    BFLog(@"Touched Inside");}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    CGPoint pointInTable = [textField.superview convertPoint:textField.frame.origin toView:self.contentTable];
    CGPoint contentOffset = self.contentTable.contentOffset;
    
    contentOffset.y = (pointInTable.y - textField.inputAccessoryView.frame.size.height)-100;
    
    BFLog(@"contentOffset is: %@", NSStringFromCGPoint(contentOffset));
    
    [self.contentTable setContentOffset:contentOffset animated:YES];
    
    return YES;
}
-(void)clearcell:(UITableViewCell *)cell{
    
    for (UIView * view in[cell.contentView subviews]){
        [view removeFromSuperview];
    }
    cell.textLabel.text=nil;
    cell.imageView.image=nil;
    
    
    
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    if ([textField.superview.superview isKindOfClass:[UITableViewCell class]])
    {
        UITableViewCell *cell = (UITableViewCell*)textField.superview.superview;
        NSIndexPath *indexPath = [self.contentTable indexPathForCell:cell];
        
        [self.contentTable scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:TRUE];
    }
    
    return YES;
}

//minnu

-(void)getthedata:(NSString *)date{
    [dataarray4 removeAllObjects];
    DBManager *objDB = [[DBManager alloc] initWithDatabaseFilename:@"hosDB.sql"];
    NSDictionary *token;
    ;
    NSMutableDictionary *whereToken;
    NSArray *userInfoArray=[[NSArray alloc]initWithArray: [objDB getUserId]];
    if([userInfoArray count]!=0){
        int userId = [userInfoArray[0][0] intValue];
        token=@{@"columnName":@"id",@"entry":[NSString stringWithFormat:@"%d",userId]};
        ;
        whereToken=[[NSMutableDictionary alloc] initWithDictionary:token];
        
        //[objDB getLogEditData:whereToken];
        token=@{@"userid":[NSString stringWithFormat:@"%d",userId],@"date":[NSString stringWithFormat:@"%@",date]};
        ;
        whereToken=[[NSMutableDictionary alloc] initWithDictionary:token];
        NSArray* dataFromDB=[[NSArray alloc]initWithArray:[objDB getLogEditData:whereToken]];
        statusString=@"";noteStr=@"",locStr=@"";
        for(int j=0;j<[dataFromDB count];j++){
            if([dataFromDB[j][1] isEqualToString:@""])
                statusString= [statusString stringByAppendingString:[NSString stringWithFormat:@"1"]];
            else
                statusString= [statusString stringByAppendingString:[NSString stringWithFormat:@"%@",[[dataFromDB objectAtIndex:j] objectAtIndex:1]]];
            
            if([dataFromDB[j][2] isEqualToString:@""]||[dataFromDB[j][2] isEqualToString:@"(null)"])
                locStr= [locStr stringByAppendingString:[NSString stringWithFormat:@"-|"]];
            else
                locStr= [locStr stringByAppendingString:[NSString stringWithFormat:@"%@|",[[dataFromDB objectAtIndex:j] objectAtIndex:2]]];
            
            if([dataFromDB[j][3] isEqualToString:@""]||[dataFromDB[j][3] isEqualToString:@"(null)"])
                noteStr= [noteStr stringByAppendingString:[NSString stringWithFormat:@"-|"]];
            else
                noteStr= [noteStr stringByAppendingString:[NSString stringWithFormat:@"%@|",[[dataFromDB objectAtIndex:j] objectAtIndex:3]]];
        }
        
        NSString* charStr=[@"" stringByPaddingToLength:96 withString:@"1" startingAtIndex:0];
        if([dataFromDB count]==0)
            statusString=charStr;
        else
        {
            if([statusString isEqualToString:@""])
                statusString=charStr;
            
        }
        
        commonDataHandler *common=[commonDataHandler new];
        statusString=[common modifyData:statusString date:date];
        graphDataOperations *obj=[graphDataOperations new];
        NSMutableArray *dict=[obj generateGraphPoints:statusString];
        for (int i=0; i<[dict count]; i++) {
            
//            NSLog(@"posx:%@,%d",[dict[0] objectForKey:@"posx"],i);
            float posx=[[dict[i] objectForKey:@"posx"] floatValue];
            float x1 = posx - 40.0;
            int x2 = x1/12.25;
            int x3 = x2/4.0;
            int x4 = x2%4;
            
            NSString *minutes;
            if (15*x4 <15) {
                minutes=@"00";
            }
            else if(15*x4 >=15 && 15*x4 <30){
                minutes=@"15";
            }else if(15*x4 >=30 && 15*x4 <45){
                minutes=@"30";
            }else if(15*x4 >=45 && 15*x4 <60){
                minutes=@"45";
            }
            NSString *posx_1 = [NSString stringWithFormat:@"%d:%@",x3,minutes];
            
            // float time = [posx_1 floatValue];
            int event_no =(int)[[dict[i] objectForKey:@"posy"] integerValue];
            NSString * event;
            if (event_no == 245) {
                event=@"ON";
            }
            else if(event_no == 125){
                event=@"SB";
                
            }else if (event_no == 185){
                event = @"D";
            }else if (event_no == 65){
                event = @"OFF";
            }
            NSArray *final_data = [[NSArray alloc] initWithObjects:posx_1,event, nil];
            [dataarray4 addObject:final_data];
        }
        if (section_index==2) {
            
            
            NSString *last_status =dataarray4[[dataarray4 count]-1][1];
            [dataarray4 removeLastObject];
            
            NSArray *last = [[NSArray alloc] initWithObjects:@"24:00",last_status, nil];
            [dataarray4 addObject:last];
        }
        
    }
    BFLog(@"data array 4:%@",dataarray4);
    
    NSArray *subViews = [self.view subviews];
    for(UIView *view in subViews){
        if ([view isKindOfClass:[insertView class]]) {
            [view removeFromSuperview];
            flag_in = flag_in +1;
            
            insertView *i = [[insertView alloc] initWithFrame:(CGRectMake(0, 100, 600, 100))];
            y_origin1=0;
            [i setOpaque:NO];
            
            [self.view addSubview:i];
            [i setNeedsDisplay];
        }
    }
    
    
}

- (void)donePressed1{
    NSDateFormatter *formatter1 =[[NSDateFormatter alloc]init];
    [formatter1 setDateFormat:@"HH:mm"];
    start.text = [NSString stringWithFormat:@"%@",[formatter1 stringFromDate:startPicker.date]];
   /* NSDateFormatter *formatter1 =[[NSDateFormatter alloc]init];
    [formatter1 setDateFormat:@"HH:mm"];
    NSString *startTime = [NSString stringWithFormat:@"%@",[formatter1 stringFromDate:startPicker.date]];
    
    NSDate *currentTime = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm"];
    NSString *resultString = [dateFormatter stringFromDate: currentTime];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"HH:mm"];
    
    NSDate *date1= [formatter dateFromString:startTime];
    NSDate *date2 = [formatter dateFromString:resultString];
    
    NSComparisonResult result = [date1 compare:date2];
    if(result == NSOrderedDescending)
    {
        [[[UIAlertView alloc]initWithTitle:@"Invalid Time" message:@"Start time should be less than current time" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
    }
    else if(result == NSOrderedAscending)
    {
        int hour;
        int minuteOrg;
        hour = 0;
        NSString * minuteExp;
        minuteOrg = 0;
        minuteExp = @"00";
        NSArray *arrayStart = [startTime componentsSeparatedByString:@":"];
        if(arrayStart.count > 0){
            hour = [arrayStart[0] intValue];
            minuteOrg = [arrayStart[1] intValue];
            if(minuteOrg>0 && minuteOrg < 15)
                minuteExp = @"00";
            else if (minuteOrg>=15 && minuteOrg < 30)
                minuteExp = @"15";
            else if (minuteOrg>=30 && minuteOrg < 45)
                minuteExp = @"30";
            else if (minuteOrg>=45 && minuteOrg <= 59)
                minuteExp = @"45";
        }
        startTime = [NSString stringWithFormat:@"%d:%@", hour, minuteExp];
        start.text=startTime;
    }*/
    [start resignFirstResponder];
}
- (void)ShowSelectedDate1
{
    
    /*NSDateFormatter *formatter1 =[[NSDateFormatter alloc]init];
    [formatter1 setDateFormat:@"HH:mm"];
    NSString *startTime = [NSString stringWithFormat:@"%@",[formatter1 stringFromDate:startPicker.date]];
    
    NSDate *currentTime = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm"];
    NSString *resultString = [dateFormatter stringFromDate: currentTime];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"HH:mm"];
    
    NSDate *date1= [formatter dateFromString:startTime];
    NSDate *date2 = [formatter dateFromString:resultString];
    
    NSComparisonResult result = [date1 compare:date2];
    if(result == NSOrderedDescending)
    {
        [[[UIAlertView alloc]initWithTitle:@"Invalid Time" message:@"Start time should be less than current time" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
    }
    else if(result == NSOrderedAscending)
    {
        start.text=startTime;
    }
//    else
//    {
//        NSLog(@"date1 is equal to date2");
//    }*/
    NSDateFormatter *formatter1 =[[NSDateFormatter alloc]init];
    [formatter1 setDateFormat:@"HH:mm"];
    start.text = [NSString stringWithFormat:@"%@",[formatter1 stringFromDate:startPicker.date]];
    
}

- (void)donePressed2{
    NSDateFormatter *formatter2 =[[NSDateFormatter alloc]init];
    [formatter2 setDateFormat:@"HH:mm"];
    NSString *endTime = [NSString stringWithFormat:@"%@",[formatter2 stringFromDate:endPicker.date]];
    NSDate *st1=startPicker.date;
    NSDate *en = endPicker.date;
    NSDateFormatter *formatterNew =[[NSDateFormatter alloc]init];
    [formatterNew setDateFormat:@"YYYY-MM-dd HH:mm"];
    NSString *startStr=[[NSString alloc]initWithString:[formatterNew stringFromDate:st1]];
    NSString *endStr=[[NSString alloc]initWithString:[formatterNew stringFromDate:en]];
    // NSComparisonResult result = [now compare:en];
    NSComparisonResult res = [en compare:st1];
    //NSLog(@"%ld",(long)res);
    
    if ((res == -1|| [startStr isEqualToString:endStr]) && ![endTime isEqualToString:@"00:00"]){
        
        UIAlertView *alert1 = [[UIAlertView alloc]initWithTitle:@"Attention!" message:@"Invalid Time-End time should be larger than Start Time" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert1 show];
        
    }else{
        
        
        if ([endTime isEqualToString:@"00:00"]) {
            End.text=@"24:00";
            
        }else{
            End.text=endTime;
        }
    }

    
    /*NSDateFormatter *formatter1 =[[NSDateFormatter alloc]init];
    [formatter1 setDateFormat:@"HH:mm"];
    NSString *endTTime = [NSString stringWithFormat:@"%@",[formatter1 stringFromDate:endPicker.date]];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm"];
    NSString *resultString = [dateFormatter stringFromDate: startPicker.date];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"HH:mm"];
    
    NSDate *date1= [formatter dateFromString:endTTime];
    NSDate *date2 = [formatter dateFromString:resultString];
    
    NSComparisonResult result = [date1 compare:date2];
    
    NSDate *currentTime = [NSDate date];
    NSDateFormatter *dateFormatterEnd = [[NSDateFormatter alloc] init];
    [dateFormatterEnd setDateFormat:@"HH:mm"];
    NSString *resultStringCurrent = [dateFormatterEnd stringFromDate: currentTime];
    
    NSDateFormatter *formatterEnd = [[NSDateFormatter alloc] init];
    [formatterEnd setDateFormat:@"HH:mm"];
    
    NSDate *dateEnd1= [formatterEnd dateFromString:endTTime];
    NSDate *dateEnd2 = [formatterEnd dateFromString:resultStringCurrent];
    
    NSComparisonResult resultEnd = [dateEnd1 compare:dateEnd2];
    
    if(result == NSOrderedDescending && resultEnd == NSOrderedAscending)
    {
        int hour;
        int minuteOrg;
        hour = 0;
        NSString * minuteExp;
        minuteOrg = 0;
        minuteExp = @"00";
        NSArray *arrayEnd = [endTTime componentsSeparatedByString:@":"];
        if(arrayEnd.count > 0){
            hour = [arrayEnd[0] intValue];
            minuteOrg = [arrayEnd[1] intValue];
            if(minuteOrg>0 && minuteOrg <= 15)
                minuteExp = @"00";
            else if (minuteOrg>15 && minuteOrg <= 30)
                minuteExp = @"15";
            else if (minuteOrg>30 && minuteOrg <= 45)
                minuteExp = @"30";
            else if (minuteOrg>45 && minuteOrg <= 59)
                minuteExp = @"45";
        }
        endTTime = [NSString stringWithFormat:@"%d:%@", hour, minuteExp];
        
        End.text=endTTime;
        
    }
    else
    {
        [[[UIAlertView alloc]initWithTitle:@"Invalid Time" message:@"End time should be greater than start time and less than or equal to the current time" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
    }*/
    [End resignFirstResponder];
    
    
    
}
- (void)ShowSelectedDate2
{
    
    NSDateFormatter *formatter2 =[[NSDateFormatter alloc]init];
    [formatter2 setDateFormat:@"HH:mm"];
    NSString *endTime = [NSString stringWithFormat:@"%@",[formatter2 stringFromDate:endPicker.date]];
    NSDate *st1=startPicker.date;
    NSDate *en = endPicker.date;
    // NSComparisonResult result = [now compare:en];
    NSComparisonResult res = [en compare:st1];
    NSLog(@"%ld",(long)res);
    
    if ((res == -1|| res==0) && ![endTime isEqualToString:@"00:00"]){
        
        UIAlertView *alert1 = [[UIAlertView alloc]initWithTitle:@"Attention!" message:@"Invalid Time-End time should be larger than Start Time" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert1 show];
        
    }else{
        
        
        if ([endTime isEqualToString:@"00:00"]) {
            End.text=@"24:00";
            
        }else{
            End.text=endTime;
        }
    }
    /*NSDateFormatter *formatter1 =[[NSDateFormatter alloc]init];
    [formatter1 setDateFormat:@"HH:mm"];
    NSString *endTTime = [NSString stringWithFormat:@"%@",[formatter1 stringFromDate:endPicker.date]];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm"];
    NSString *resultString = [dateFormatter stringFromDate: startPicker.date];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"HH:mm"];
    DBManager *objDB=[DBManager new];
    
    NSDate *date1= [formatter dateFromString:endTTime];
    NSDate *date2 = [formatter dateFromString:resultString];
    
    NSComparisonResult result = [date1 compare:date2];
    
    NSDate *currentTime = [NSDate date];
    NSDateFormatter *dateFormatterEnd = [[NSDateFormatter alloc] init];
    [dateFormatterEnd setDateFormat:@"HH:mm"];
    NSString *resultStringCurrent = [dateFormatterEnd stringFromDate: currentTime];
    
    NSDateFormatter *formatterEnd = [[NSDateFormatter alloc] init];
    [formatterEnd setDateFormat:@"HH:mm"];
    
    NSDate *dateEnd1= [formatterEnd dateFromString:endTTime];
    NSDate *dateEnd2 = [formatterEnd dateFromString:resultStringCurrent];
    
    NSComparisonResult resultEnd = [dateEnd1 compare:dateEnd2];
    
    if(result == NSOrderedDescending && resultEnd == NSOrderedAscending)
    {
        
        End.text=endTTime;
        
    }
//    else if(result == NSOrderedAscending)
//    {
//        [[[UIAlertView alloc]initWithTitle:@"Invalid Time" message:@"End time should be less than start time" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
//    }
    else
    {
        [[[UIAlertView alloc]initWithTitle:@"Invalid Time" message:@"End time should be greater than start time and less than or equal to the current time" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
    }*/
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
    
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return 9;
    
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat height ;
    switch (indexPath.row) {
        case 0:
            height=100;
            break;
        case 1:
            height=40;
            break;
            
        default:
            height=40;
            break;
    }
    
    return height;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString *cellIdentifier = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    [self clearcell:cell];
    switch (indexPath.row) {
        case 0:
        {
            insertView *graph = [[insertView alloc]initWithFrame:CGRectMake(0, 0, cell.contentView.frame.size.width, 150)];
            [cell.contentView addSubview:graph];
            
            
            
        }
            break;
        case 1:
        {
            UILabel *events = [[UILabel alloc] initWithFrame:CGRectMake(50, 0, 500, 40)];
            NSString *textTitle = @"Start & End Time*";
            NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:textTitle];
            NSRange range = {[textTitle length]-1,1};
            UIColor *color = [UIColor redColor];
            UIColor *color1 = [UIColor colorWithRed:0.20 green:0.60 blue:0.07 alpha:1.0];
            [str addAttribute:NSForegroundColorAttributeName value:color range:range];
            [str addAttribute:NSForegroundColorAttributeName value:color1 range:NSMakeRange(0,[textTitle length]-1)];
            [events setAttributedText:str];
            [cell.contentView addSubview:events];
            
        }
            break;
            
        case 2:
        {
            
            [cell.contentView addSubview:start];
            
            
            [cell.contentView addSubview:End];
            
            
            
            
            
        }
            
            break;
            
            
        case 3:
        {
            
            UILabel *events = [[UILabel alloc] initWithFrame:CGRectMake(50, 0, 500, 40)];
            NSString *textTitle = @"Events*";
            NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:textTitle];
            NSRange range = {[textTitle length]-1,1};
            UIColor *color = [UIColor redColor];
            UIColor *color1 =[UIColor colorWithRed:0.20 green:0.60 blue:0.07 alpha:1.0];
            [str addAttribute:NSForegroundColorAttributeName value:color range:range];
            [str addAttribute:NSForegroundColorAttributeName value:color1 range:NSMakeRange(0,[textTitle length]-1)];
            [events setAttributedText:str];
            [cell.contentView addSubview:events];
            
        }
            break;
            
            
            
        case 4:
        {
            NSLog(@"____________________%@", status_new);
            CGRect buttonRect = CGRectMake(50, 10, 50, 30);
            offButton1 = [UIButton buttonWithType:UIButtonTypeCustom];
            offButton1.frame = buttonRect;
            // set the button title here if it will always be the same
            [offButton1 setTitle:@"OFF" forState:UIControlStateNormal];
            offButton1.tag = 1;
            [offButton1 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [offButton1 setSelected:YES];
            [offButton1 setHighlighted:YES];
            
            offButton1.backgroundColor=[UIColor colorWithRed:0.80 green:1.00 blue:0.00 alpha:1.0];
            [offButton1 addTarget:self action:@selector(highlight:) forControlEvents:UIControlEventTouchUpInside];
            //[button addTarget:self action:@selector(myAction:) forControlEvents:UIControlEventTouchUpInside];

            [cell.contentView addSubview:offButton1];
            
            CGRect buttonRect1 = CGRectMake(110, 10, 50, 30);
            sbButton1 = [UIButton buttonWithType:UIButtonTypeCustom];
            sbButton1.frame = buttonRect1;
            [sbButton1 setTitle:@"SB" forState:UIControlStateNormal];
            sbButton1.backgroundColor=[UIColor colorWithRed:0.80 green:1.00 blue:0.00 alpha:1.0];
            [sbButton1 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [sbButton1 setSelected:YES];
            [sbButton1 setHighlighted:YES];
            sbButton1.tag =2;
            [sbButton1 addTarget:self action:@selector(highlight:) forControlEvents:UIControlEventTouchUpInside];
            // set the button title here if it will always be the same
            
//            if([status_new isEqualToString:@"SB"]){
//                [sbButton1 sendActionsForControlEvents:UIControlEventTouchUpInside];
//            }
            //[button addTarget:self action:@selector(myAction:) forControlEvents:UIControlEventTouchUpInside];
            [cell.contentView addSubview:sbButton1];
            
            CGRect buttonRect2 = CGRectMake(170, 10, 50, 30);
            dButton1 = [UIButton buttonWithType:UIButtonTypeCustom];
            dButton1.frame = buttonRect2;
            [dButton1 setTitle:@"D" forState:UIControlStateNormal];
            dButton1.backgroundColor=[UIColor colorWithRed:0.80 green:1.00 blue:0.00 alpha:1.0];
            [dButton1 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [dButton1 setSelected:YES];
            [dButton1 setHighlighted:YES];
            dButton1.tag=3;
            [dButton1 addTarget:self action:@selector(highlight:) forControlEvents:UIControlEventTouchUpInside];
            
            
            // set the button title here if it will always be the same
//            if([status_new isEqualToString:@"D"]){
//                [dButton1 sendActionsForControlEvents:UIControlEventTouchUpInside];
//            }
            //[button addTarget:self action:@selector(myAction:) forControlEvents:UIControlEventTouchUpInside];
            [cell.contentView addSubview:dButton1];
            CGRect buttonRect3 = CGRectMake(230, 10, 50, 30);
            onButton1 = [UIButton buttonWithType:UIButtonTypeCustom];
            onButton1.frame = buttonRect3;
            [onButton1 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [onButton1 setTitle:@"ON" forState:UIControlStateNormal];
            [onButton1 setSelected:YES];
            [onButton1 setHighlighted:YES];
            onButton1.tag = 4;
            onButton1.backgroundColor=[UIColor colorWithRed:0.80 green:1.00 blue:0.00 alpha:1.0];
            [onButton1 addTarget:self action:@selector(highlight:) forControlEvents:UIControlEventTouchUpInside];
            // [onButton1 addTarget:self action:@selector(onDuty:) forControlEvents:UIControlEventTouchUpInside];
            // set the button title here if it will always be the same
//            if([status_new isEqualToString:@"ON"]){
//                [onButton1 sendActionsForControlEvents:UIControlEventTouchUpInside];
//            }
            //[button addTarget:self action:@selector(myAction:) forControlEvents:UIControlEventTouchUpInside];
            [cell.contentView addSubview:onButton1];
            
            
        }
            break;
            
            
            
        case 5:
        {
            UILabel *events = [[UILabel alloc] initWithFrame:CGRectMake(50, 0, 500, 40)];
            NSString *textTitle = @"Location*";
            NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:textTitle];
            NSRange range = {[textTitle length]-1,1};
            UIColor *color = [UIColor redColor];
            UIColor *color1 = [UIColor colorWithRed:0.20 green:0.60 blue:0.07 alpha:1.0];
            [str addAttribute:NSForegroundColorAttributeName value:color range:range];
            [str addAttribute:NSForegroundColorAttributeName value:color1 range:NSMakeRange(0,[textTitle length]-1)];
            [events setAttributedText:str];
            [cell.contentView addSubview:events];
            
            
        }
            break;
            
            
        case 6:{
            location1 = [[UITextField alloc] initWithFrame:CGRectMake(50, 0, 130, 40)];
            location1.placeholder=@"Location";
            location1.delegate=self;
            location1.textAlignment=NSTextAlignmentJustified;
            location1.text=address_full1;
            [cell.contentView addSubview:location1];
            
            /* CGRect address1Btn = CGRectMake(550, 0, 50, 40);
             address1 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
             address1.frame=address1Btn;
             [address1 addTarget:self action:@selector(address1) forControlEvents:UIControlEventTouchUpInside];
             [address1 setBackgroundImage:[UIImage imageNamed:@"icons/location1.png"] forState:UIControlStateNormal];
             
             [cell.contentView addSubview:address1];*/
            CGRect buttonRect3 = CGRectMake(230, 10, 30, 30);
            address1 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            address1.frame = buttonRect3;
            [address1 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [address1 addTarget:self action:@selector(address) forControlEvents:UIControlEventTouchUpInside];
            // set the button title here if it will always be the same
            //[onButton1 setTitle:@"ON" forState:UIControlStateNormal];
            [address1 setBackgroundImage:[UIImage imageNamed:@"location.png"] forState:UIControlStateNormal];
            //onButton1.backgroundColor=[UIColor colorWithRed:0.80 green:1.00 blue:0.00 alpha:1.0];
            //[button addTarget:self action:@selector(myAction:) forControlEvents:UIControlEventTouchUpInside];
            [cell.contentView addSubview:address1];
            
            
            
            
            
        }
            break;
            
        case 7:
        {
            UILabel *events = [[UILabel alloc] initWithFrame:CGRectMake(50, 0, 500, 40)];
            events.text = @"Notes";
            [events setFont:[UIFont fontWithName:@"Helvetica Neue Bold" size:15.0]];
            events.textColor = [UIColor colorWithRed:0.20 green:0.60 blue:0.07 alpha:1.0];
            [cell.contentView addSubview:events];
            
            
        }
            break;
            
        case 8:
        {
            notes1 = [[UITextField alloc] initWithFrame:CGRectMake(50, 0, 550, 40)];
            notes1.placeholder=@"notes";
            notes1.delegate=self;
            [cell.contentView addSubview: notes1];
            
            
            
        }
            break;
            
            
            
        default:
            break;
    }
    
    
    
    
    
    
    
    
    return cell;
    
    
    
}
-(void)highlight:(UIButton*)button{
    /* NSIndexPath *index = [NSIndexPath indexPathForRow:4 inSection:0];
     [contentTable beginUpdates];
     [contentTable reloadRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationNone];
     [contentTable endUpdates];*/
    button.backgroundColor = [UIColor cyanColor];
    for(int i=1;i<5;i++){
        
        if(i !=button.tag){
            
            [self.view viewWithTag:i].backgroundColor = [UIColor colorWithRed:0.80 green:1.00 blue:0.00 alpha:1.0];
        }
        
    }
    NSString *event =@"";
    switch (button.tag) {
        case 1:
            event = @"OFF";
            break;
        case 2:
            event = @"SB";
            break;
        case 3:
            event = @"D";
            break;
            
        case 4:
            event = @"ON";
            break;
        default:
            break;
    }
    status_new=event;
    is_editing1=1;
    // [self dataUpdate:event buttonTag:button.tag];
    
    
    newline=[dataarray4 mutableCopy];
    //minnu
    
    NSString *startTime=start.text;
    NSString *endTime = End.text;
    if (![startTime isEqualToString:@""]&&![endTime isEqualToString:@""]&& ![status_new isEqualToString:@""]) {
        
    //minnu
    NSString *init=@"0:0";
    NSArray *initTime=[init componentsSeparatedByString:@":"];
    NSArray *start=[startTime componentsSeparatedByString:@":"];
    NSArray *end=[endTime componentsSeparatedByString:@":"];
    int duration=([end[0]intValue]*60+[end[1] intValue])-([start[0]intValue]*60+[start[1] intValue]);
    repeat=(int)duration/15;
        
    duration=([start[0]intValue]*60+[start[1] intValue])-([initTime[0]intValue]*60+[initTime[1] intValue]);
    slotInsert=(int)duration/15;
    slotInsert=slotInsert+1;
    NSString *statusNew=[self getStatusCode:status_new];
    annotation *obj=[annotation new];
    NSString* charStr=statusString;
    NSMutableString *charStrWithSlash=[NSMutableString stringWithFormat:@""];
    for(int i=0;i<charStr.length;i++){
        NSUInteger len=1;
        [charStrWithSlash appendFormat:@"%@|",[charStr substringWithRange:NSMakeRange(i, len)]];
    }
        NSString *tempStr;
        if([statusNew isEqualToString:@"9"]){
            tempStr=[obj updateStr:charStrWithSlash index1:slotInsert index2:repeat notes:@"7"];
            NSRange range=[tempStr rangeOfString:@"7"];
            if(NSNotFound !=range.location){
                tempStr=[tempStr stringByReplacingCharactersInRange:range withString:@"9"];
            }
        }
        else
            tempStr=[obj updateStr:charStrWithSlash index1:slotInsert index2:repeat notes:statusNew];
    NSString* newCharStr=[tempStr stringByReplacingOccurrencesOfString:@"|" withString:@""];
        statusArr=[tempStr componentsSeparatedByString:@"|"];
        statusStrNew=newCharStr;
    //minnu
  /*  if ([endTime isEqualToString:@"00:00"]) {
        BFLog(@"");
        endTime=@"24:00";
    }
    NSMutableArray *datavalues = [[NSMutableArray alloc] init];
    int size_array = (int)[newline count];
    for (int i=0; i<size_array; i++) {
        float x1 = [self getposx:newline[i][0]];
        NSString *x2 = [NSString stringWithFormat:@"%.2f",x1];
        NSArray *contents = [[NSArray alloc] initWithObjects:x2,newline[i][1],nil];
        [datavalues addObject:contents];
        
    }
    BFLog(@"data:%@",datavalues);
    NSMutableArray *dummy = [[NSMutableArray alloc] init];
    if (![startTime isEqualToString:@""]&&![endTime isEqualToString:@""]&& ![status_new isEqualToString:@""]) {
        
        BFLog(@"start:%@,end:%@,status:%@",startTime,endTime,status_new);
        float start_new = [self getposx:startTime];
        float end_new  = [self getposx:endTime];
        float start_final = floorf((start_new*100)/100);
        float end_final = floorf((end_new*100)/100);
        //NSString *start = [NSString stringWithFormat:@"%.2f",start_final];
        BFLog(@"start:%f,end:%f",start_final,end_final);
        int size_values = (int)[datavalues count];
        NSArray *previous_element = [[NSArray alloc]init];
        //NSArray *next_element = [[NSArray alloc]init];
        NSString *previous_status ;
        int edit_change=0;
        NSMutableArray *new1=[[NSMutableArray alloc]init];
        [new1 insertObject:startTime atIndex:0];
        [new1 insertObject:status_new atIndex:1];
        NSMutableArray *new2=[[NSMutableArray alloc]init];
        [new2 insertObject:endTime atIndex:0];
        [new2 insertObject:status_new atIndex:1];
        NSInteger a = 0;
        a=[newline indexOfObject:new1];
        NSInteger b =[newline indexOfObject:new2];
        if(NSNotFound!=a && NSNotFound!=b){
            insertedPos=a;
            dummy=newline;
            BFLog(@"No Change");
            
            
            
            
            
        }else{
            for (int i=0; i<size_values; i++) {
                
                NSString *next_status;
                
                if (edit_change==0)
                {
                    
                    
                    if ([datavalues[i][0] floatValue]> start_new)
                    {
                        if ([datavalues[i][0] floatValue] < end_new)
                        {
                            BFLog(@"element %@ in between the ranges",newline[i]);
                            if (![previous_status isEqualToString:status_new])
                            {
                                NSArray *dummy1 = @[startTime,previous_status];
                                [dummy addObject:dummy1];
                                
                                
                                insertedPos=[dummy count];
                                
                                NSArray *dummy2 = @[startTime,status_new];
                                [dummy addObject:dummy2];
                                NSArray *dummy3 = @[endTime,status_new];
                                [dummy addObject:dummy3];
                                
                            }else{
                                
                                
                                insertedPos=[dummy count]-1;
                                
                                NSArray *dummy3 = @[endTime,status_new];
                                [dummy addObject:dummy3];
                                
                                
                                
                            }
                            next_status=[self nextStatus:i data:datavalues end:end_new];
                            if(![status_new isEqualToString:next_status]){
                                
                                NSArray *dummy4 = @[endTime,next_status];
                                [dummy addObject:dummy4];
                            }
                            edit_change=1;
                            
                            
                            
                        }else{
                            edit_change=1;
                            if (![previous_status isEqualToString:status_new]) {
                                
                                
                                NSArray *dummy1 = @[startTime,previous_status];
                                [dummy addObject:dummy1];
                                
                                
                                
                                insertedPos=[dummy count];
                                NSArray *dummy2 = @[startTime,status_new];
                                [dummy addObject:dummy2];
                                NSArray *dummy3 = @[endTime,status_new];
                                [dummy addObject:dummy3];
                                if ([newline[i][1] isEqualToString:status_new] && i==size_values-1)
                                {//removedi===size-1
                                    [dummy removeLastObject];
                                    NSArray *dummy3 = @[@"24:00",status_new];
                                    [dummy addObject:dummy3];
                                    
                                }else{
                                    
                                    NSArray *dummy3 = @[endTime,newline[i][1]];
                                    if (i+1 < [datavalues count])
                                    {
                                        if(![dummy[[dummy count]-1][0] isEqualToString:newline[i+1][0]])
                                        {
                                            
                                            [dummy addObject:dummy3];
                                            [dummy addObject:newline[i]];
                                            
                                        }
                                    }else{
                                        [dummy addObject:dummy3];
                                        [dummy addObject:newline[i]];
                                        
                                    }
                                }
                            }else{
                                
                                [dummy removeAllObjects];
                                dummy=[[NSMutableArray alloc]initWithArray:newline];
                                
                            }
                            
                            
                            
                        }
                        
                    }else if([datavalues[i][0] floatValue] == start_new){
                        if([datavalues[i+1][0] floatValue] == end_new){
                            
                            NSArray *dummy3 = @[startTime,status_new];
                            
                            NSArray *dummy4 = @[endTime,status_new];
                            [dummy addObject:dummy3];
                            [dummy addObject:dummy4];
                            
                            
                            
                        }else if ([datavalues[i][1] isEqualToString:status_new])
                        {
                            
                            NSArray *dummy3 = @[endTime,status_new];
                            [dummy addObject:dummy3];
                        }else{
                            
                            if ([datavalues[i][0] floatValue] < end_new) {
                                BFLog(@"element %@ in between the ranges",newline[i]);
                                if(i!=0 )
                                {
                                    NSArray *dummy1 = @[startTime,previous_status];
                                    [dummy addObject:dummy1];
                                }
                                
                                insertedPos=[dummy count];
                                
                                NSArray *dummy2 = @[startTime,status_new];
                                [dummy addObject:dummy2];
                                NSArray *dummy3 = @[endTime,status_new];
                                [dummy addObject:dummy3];
                                
                                next_status=[self nextStatus:i data:datavalues end:end_new];
                                
                                if(![status_new isEqualToString:next_status])
                                {
                                    NSArray *dummy4 = @[endTime,next_status];
                                    [dummy addObject:dummy4];
                                    
                                }
                            }
                        }
                        
                        
                        
                        edit_change=1;
                        
                        
                    }else{
                        
                        [dummy addObject:newline[i]];
                        BFLog(@"new array:%@",dummy);
                    }
                    
                }else{ //if edited==0
                    //if(i+1<size_values){
                    if ([datavalues[i][0] floatValue] >= end_new){
                        if ([datavalues[i][1] isEqualToString:dummy[[dummy count]-2][1]]) {
                            [dummy removeLastObject];
                            [dummy addObject:newline[i]];
                        }
                        //else  [dummy removeLastObject];
                        
                        else{
                            if(![dummy[[dummy count]-1][1] isEqualToString:newline[i][1]]){
                                
                                
                                NSMutableArray *dummy4 = [[NSMutableArray alloc] init];
                                dummy4=[dummy objectAtIndex:[dummy count]-1];
                                NSArray *dummy5 = @[dummy4[0],newline[i][1]];
                                
                                //   [dummy removeLastObject];
                                
                                
                                [dummy addObject:dummy5];
                            }else{
                                [dummy addObject:newline[i]];
                            }
                        }
                    }
                    
                    //}else{
                    
                    //   break;
                    // }
                }
                
                previous_element=newline[i];
                previous_status = previous_element[1];
                
                
                
            }
            
            BFLog(@"final Array:%@",dummy);
            NSString *last_time;
            NSString *secnd_lasttime;
            NSString *third_last;
            
            if ([dummy count]>=3) {
                last_time = dummy[[dummy count]-1][0];
                secnd_lasttime = dummy[[dummy count]-2][0];
                third_last = dummy[[dummy count]-3][0];
            }
            
            if ([last_time isEqualToString:@"24:00"]&&[secnd_lasttime isEqualToString:@"24:00"]&&[third_last isEqualToString:@"24:00"]) {
                [dummy removeLastObject];
                [dummy removeLastObject];
            }
            else if ([last_time isEqualToString:@"24:00"]&&[secnd_lasttime isEqualToString:@"24:00"]) {
                
                [dummy removeLastObject];
            }
            [newline removeAllObjects];
            newline=dummy;
            
            */
        
            [newline removeAllObjects];
            graphDataOperations *objGraph=[graphDataOperations new];
            NSMutableArray *dict=[[NSMutableArray alloc] initWithArray:[objGraph generateGraphPoints:newCharStr]];
            for (int i=0; i<[dict count]; i++) {
                
                NSLog(@"posx:%@,%d",[dict[i] objectForKey:@"posx"],i);
                float posx=[[dict[i] objectForKey:@"posx"] floatValue];
                float x1 = posx - 40.0;
                int x2 = x1/12.25;
                int x3 = x2/4.0;
                int x4 = x2%4;
                
                NSString *minutes;
                if (15*x4 <15) {
                    minutes=@"00";
                }
                else if(15*x4 >=15 && 15*x4 <30){
                    minutes=@"15";
                }else if(15*x4 >=30 && 15*x4 <45){
                    minutes=@"30";
                }else if(15*x4 >=45 && 15*x4 <60){
                    minutes=@"45";
                }
                NSString *posx_1 = [NSString stringWithFormat:@"%d:%@",x3,minutes];
                
                // float time = [posx_1 floatValue];
                int event_no =(int)[[dict[i] objectForKey:@"posy"] integerValue];
                NSString * event;
                if (event_no == 245) {
                    event=@"ON";
                }
                else if(event_no == 125){
                    event=@"SB";
                    
                }else if (event_no == 185){
                    event = @"D";
                }else if (event_no == 65){
                    event = @"OFF";
                }
                NSArray *final_data = [[NSArray alloc] initWithObjects:posx_1,event, nil];
                [newline addObject:final_data];
            }
            if(![[NSSet setWithArray:dataarray4] isEqualToSet:[NSSet setWithArray:newline]])
                insertStatus=1;
            else
                insertStatus=0;
            
            //minnu
            BFLog(@"DATAARRAY_NEW\n %@",newline);
            BFLog(@"DATAARRAY_NEW\n %@",newline);
            BFLog(@"Locations:%@",location1_list);
            ///minnu
            //dataarray4=[oldline mutableCopy];
            //minnu
            // [contentTable reloadData];
            NSIndexPath *index = [NSIndexPath indexPathForRow:0 inSection:0];
            [contentTable beginUpdates];
            [contentTable reloadRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationNone];
            [contentTable endUpdates];
            
            // button.backgroundColor = [UIColor cyanColor];
            
 //       }
   //     insertStatus=1;
    }else{
        /*  if([status isEqualToString:@"off"]){
         offButton1.backgroundColor=[UIColor colorWithRed:0.80 green:1.00 blue:0.00 alpha:1.0];
         
         
         }else if([status isEqualToString:@"D"]){
         
         dButton1.backgroundColor=[UIColor colorWithRed:0.80 green:1.00 blue:0.00 alpha:1.0];
         
         
         }else if([status isEqualToString:@"ON"]){
         
         onButton1.backgroundColor=[UIColor colorWithRed:0.80 green:1.00 blue:0.00 alpha:1.0];
         
         }else{
         
         sbButton1.backgroundColor=[UIColor colorWithRed:0.80 green:1.00 blue:0.00 alpha:1.0];
         
         }*/
        UIAlertController *alert1 = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Please select start and end time before selecting the event" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ok1= [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            [alert1 dismissViewControllerAnimated:YES completion:nil];
            button.backgroundColor = [UIColor colorWithRed:0 green:1.00 blue:0.00 alpha:1.0];
            /*NSIndexPath *index  = [NSIndexPath indexPathForRow:4 inSection:0];
             [contentTable beginUpdates];
             [contentTable reloadRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationNone];
             [contentTable endUpdates];*/
        }];
        
        
        [alert1 addAction:ok1];
        [self presentViewController:alert1 animated:YES completion:nil];
        
        
    }
    
    
    
}

/*
 
 - (void)didReceiveMemoryWarning {
 [super didReceiveMemoryWarning];
 // Dispose of any resources that can be recreated.
 }
 
 
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (void)onDuty:(UIButton*)button {
    button.backgroundColor = [UIColor redColor];
    /*button.backgroundColor=[UIColor colorWithRed:0.00 green:1.00 blue:0.82 alpha:1.0];
     dButton1.backgroundColor=[UIColor colorWithRed:0.80 green:1.00 blue:0.00 alpha:1.0];
     offButton1.backgroundColor=[UIColor colorWithRed:0.80 green:1.00 blue:0.00 alpha:1.0];
     sbButton1.backgroundColor=[UIColor colorWithRed:0.80 green:1.00 blue:0.00 alpha:1.0];*/
    status_new=@"ON";
    is_editing1=1;
    //[self dataUpdate:@"ON"];
    
}

- (void)sleeper :(UIButton*)button{
    button.backgroundColor = [UIColor redColor];
    // button.backgroundColor=[UIColor colorWithRed:0.00 green:1.00 blue:0.82 alpha:1.0];
    //dButton1.backgroundColor=[UIColor colorWithRed:0.80 green:1.00 blue:0.00 alpha:1.0];
    //offButton1.backgroundColor=[UIColor colorWithRed:0.80 green:1.00 blue:0.00 alpha:1.0];
    //onButton1.backgroundColor=[UIColor colorWithRed:0.80 green:1.00 blue:0.00 alpha:1.0];
    status_new=@"SB";
    is_editing1=1;
    //  [self dataUpdate:@"SB"];
    
}

- (void)driving:(UIButton*)button {
    button.backgroundColor = [UIColor colorWithRed:0.00 green:1.00 blue:0.82 alpha:1.0];
    //[dButton1 setBackgroundColor:[UIColor redColor]];
    // sbButton1.backgroundColor=[UIColor colorWithRed:0.80 green:1.00 blue:0.00 alpha:1.0];
    //offButton1.backgroundColor=[UIColor colorWithRed:0.80 green:1.00 blue:0.00 alpha:1.0];
    //onButton1.backgroundColor=[UIColor colorWithRed:0.80 green:1.00 blue:0.00 alpha:1.0];
    status_new=@"D";
    is_editing1=1;
    //  [self dataUpdate:@"D"];
    
}

- (void)offDuty:(UIButton*)button {
    button.backgroundColor=[UIColor colorWithRed:0.00 green:1.00 blue:0.82 alpha:1.0];
    // dButton1.backgroundColor=[UIColor colorWithRed:0.80 green:1.00 blue:0.00 alpha:1.0];
    //sbButton1.backgroundColor=[UIColor colorWithRed:0.80 green:1.00 blue:0.00 alpha:1.0];
    //onButton1.backgroundColor=[UIColor colorWithRed:0.80 green:1.00 blue:0.00 alpha:1.0];
    status_new=@"OFF";
    is_editing1=1;
    //    [self dataUpdate:@"off"];
    
}

- (IBAction)Back:(id)sender {
    
    UIStoryboard *storyBoard = self.storyboard;
    UIViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"logsmenu"];
    [self presentViewController:vc animated:YES completion:nil];
}

- (void)address {
    
    
    
    locationManager.delegate = self;
    [locationManager requestWhenInUseAuthorization];
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager startUpdatingLocation];
    if ([address_full1 isEqualToString:@""]) {
        MBProgressHUD *hud =  [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        // Set the determinate mode to show task progress.
        hud.mode = MBProgressHUDModeDeterminate;
        hud.label.text = NSLocalizedString(@"Fetching address...", @"HUD loading title");
        dispatch_async(dispatch_get_main_queue(), ^{
            // Do something useful in the background and update the HUD periodically.
            [self doSomeWorkWithProgress];
            dispatch_async(dispatch_get_main_queue(), ^{
                //[MBProgressHUD hideHUDForView:self.view animated:YES];
                
                
                [hud hideAnimated:YES];
            });
            location1.text=address_full1;
            
            
        });
    }else{
        location1.text=address_full1;
    }
    
    // [self CurrentLocationIdentifier];
    //_location.text=address_full;
    BFLog(@"address:%@",address_full1);
    
}
- (void)doSomeWorkWithProgress {
    self.canceled = NO;
    // This just increases the progress indicator in a loop.
    float progress = 0.0f;
    while ([address_full1 isEqualToString:@""]&&progress<1.0) {
        if (self.canceled) break;
        progress += 0.005f;
        dispatch_async(dispatch_get_main_queue(), ^{
            // Instead we could have also passed a reference to the HUD
            // to the HUD to myProgressTask as a method parameter.
            [MBProgressHUD HUDForView:self.navigationController.view].progress = progress;
        });
        
        usleep(50000);
        
    }
    progress=1.0f;
    /* if ([address_full1 isEqualToString:@""]) {
     UIAlertController *alert1 = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Some Error in Fetcing the address1" preferredStyle:UIAlertControllerStyleAlert];
     UIAlertAction *ok1= [UIAlertAction actionWithTitle:@"OK" style:UIAlertViewStyleDefault handler:^(UIAlertAction * action) {
     [alert1 dismissViewControllerAnimated:YES completion:nil];
     }];
     
     
     [alert1 addAction:ok1];
     [self presentViewController:alert1 animated:YES completion:nil];
     
     
     }else{
     location1.text=address_full1;
     }*/
}


-(void)dataUpdate1{
    [newline addObjectsFromArray:dataarray4];
    NSString *startTime=start.text;
    NSString *endTime = End.text;
    if ([endTime isEqualToString:@"00:00"]) {
        BFLog(@"");
        endTime=@"24:00";
    }
    NSMutableArray *datavalues = [[NSMutableArray alloc] init];
    int size_array = (int)[newline count];
    for (int i=0; i<size_array; i++) {
        float x1 = [self getposx:newline[i][0]];
        NSString *x2 = [NSString stringWithFormat:@"%.2f",x1];
        NSArray *contents = [[NSArray alloc] initWithObjects:x2,newline[i][1],nil];
        [datavalues addObject:contents];
        
    }
    BFLog(@"data:%@",datavalues);
    NSMutableArray *dummy = [[NSMutableArray alloc] init];
    if (![startTime isEqualToString:@""]&&![endTime isEqualToString:@""]&& ![status_new isEqualToString:@""]){
        
        BFLog(@"start:%@,end:%@,status:%@",startTime,endTime,status_new);
        float start_new = [self getposx:startTime];
        float end_new  = [self getposx:endTime];
        float start_final = floorf((start_new*100)/100);
        float end_final = floorf((end_new*100)/100);
        //NSString *start = [NSString stringWithFormat:@"%.2f",start_final];
        BFLog(@"start:%f,end:%f",start_final,end_final);
        int size_values = (int)[datavalues count];
        NSArray *previous_element = [[NSArray alloc]init];
        //NSArray *next_element = [[NSArray alloc]init];
        NSString *previous_status=newline[0][1]; ;
        int edit_change=0;
        NSMutableArray *new1=[[NSMutableArray alloc]init];
        [new1 insertObject:startTime atIndex:0];
        [new1 insertObject:status_new atIndex:1];
        NSMutableArray *new2=[[NSMutableArray alloc]init];
        [new2 insertObject:endTime atIndex:0];
        [new2 insertObject:status_new atIndex:1];
        NSInteger a =[newline indexOfObject:new1];
        NSInteger b =[newline indexOfObject:new2];
        if(NSNotFound!=a && NSNotFound!=b){
            dummy=newline;
            BFLog(@"No Change");
            
        }else{
            for (int i=0; i<size_values; i=i+2) {
                if (edit_change==0) {
                    
                    if([datavalues[i][0] floatValue] == start_new && [datavalues[i+1][0] floatValue]==end_new){
                        
                        newline[i][1]=status_new;
                        newline[i+1][1]=status_new;
                        dummy=newline;
                        edit_change=1;
                    }else if([datavalues[i][0] floatValue]<start_new){
                        
                        if([datavalues[i+1][0] floatValue]>end_new){
                            
                            BFLog(@"In Between");
                            if (![previous_status isEqualToString:status_new]) {
                                //if(i==0){
                                [dummy addObject:newline[i]];
                                NSArray *dummy1 = @[startTime,previous_status];
                                [dummy addObject:dummy1];
                                insertedPos=[dummy count];
                                NSArray *dummy2 = @[startTime,status_new];
                                [dummy addObject:dummy2];
                                NSArray *dummy3 = @[endTime,status_new];
                                [dummy addObject:dummy3];
                                NSArray *dummy4 = @[endTime,previous_status];
                                [dummy addObject:dummy4];
                                [dummy addObject:newline[i+1]];
                                
                                // }else{
                                
                                
                                
                                
                                
                                //}//end of i==0
                                
                            }else{
                                NSArray *dummy3 = @[endTime,status_new];
                                [dummy addObject:dummy3];
                                
                            }//end of ![previous_status isEqualToString:status_new]
                            
                            
                        }else{
                            
                            [dummy addObject:newline[i]];
                            [dummy addObject:newline[i+1]];
                            
                            
                            
                        }//end of datavalues[i+1][0] floatValue]>end_new
                        
                        
                        
                        
                    }else{
                        
                        
                        
                        
                    }// end of [datavalues[i][0] floatValue]>start_new)
                    
                    
                    
                    
                }else{
                    int n = (int)[dummy count];
                    NSArray *last = dummy[n-1];
                    float last_posx = [self getposx:last[0]];
                    if([datavalues[i][0] floatValue]>last_posx){
                        [dummy addObject:newline[i]];
                        
                    }
                    if([datavalues[i+1][0] floatValue]>last_posx){
                        [dummy addObject:newline[i+1]];
                        
                    }
                    
                    
                    
                    
                    
                    
                }//end of edit_change
                
                
                previous_element=newline[i];
                previous_status = previous_element[1];
                
                
                
            }//end of for loop
            [newline removeAllObjects];
            newline=dummy;
            [contentTable reloadData];
        }
        insertStatus=1;
    }else{
        UIAlertController *alert1 = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Please select start and end time first before selecting the event!!" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ok1= [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            [alert1 dismissViewControllerAnimated:YES completion:nil];
        }];
        
        
        [alert1 addAction:ok1];
        [self presentViewController:alert1 animated:YES completion:nil];
        
        
        
    }
    
    
}



-(void)dataUpdate:(NSString *)status buttonTag:(NSInteger *)tag{
    [[self.view viewWithTag:tag] setBackgroundColor:[UIColor cyanColor]];
    ///minnu
    newline=[dataarray4 mutableCopy];
    //minnu
    
    NSString *startTime=start.text;
    NSString *endTime = End.text;
    if ([endTime isEqualToString:@"00:00"]) {
        BFLog(@"");
        endTime=@"24:00";
    }
    NSMutableArray *datavalues = [[NSMutableArray alloc] init];
    int size_array = (int)[newline count];
    for (int i=0; i<size_array; i++) {
        float x1 = [self getposx:newline[i][0]];
        NSString *x2 = [NSString stringWithFormat:@"%.2f",x1];
        NSArray *contents = [[NSArray alloc] initWithObjects:x2,newline[i][1],nil];
        [datavalues addObject:contents];
        
    }
    BFLog(@"data:%@",datavalues);
    NSMutableArray *dummy = [[NSMutableArray alloc] init];
    if (![startTime isEqualToString:@""]&&![endTime isEqualToString:@""]&& ![status_new isEqualToString:@""]) {
        
        BFLog(@"start:%@,end:%@,status:%@",startTime,endTime,status_new);
        float start_new = [self getposx:startTime];
        float end_new  = [self getposx:endTime];
        float start_final = floorf((start_new*100)/100);
        float end_final = floorf((end_new*100)/100);
        //NSString *start = [NSString stringWithFormat:@"%.2f",start_final];
        BFLog(@"start:%f,end:%f",start_final,end_final);
        int size_values = (int)[datavalues count];
        NSArray *previous_element = [[NSArray alloc]init];
        //NSArray *next_element = [[NSArray alloc]init];
        NSString *previous_status ;
        int edit_change=0;
        NSMutableArray *new1=[[NSMutableArray alloc]init];
        [new1 insertObject:startTime atIndex:0];
        [new1 insertObject:status_new atIndex:1];
        NSMutableArray *new2=[[NSMutableArray alloc]init];
        [new2 insertObject:endTime atIndex:0];
        [new2 insertObject:status_new atIndex:1];
        NSInteger a = 0;
        a=[newline indexOfObject:new1];
        NSInteger b =[newline indexOfObject:new2];
        if(NSNotFound!=a && NSNotFound!=b){
            insertedPos=a;
            dummy=newline;
            BFLog(@"No Change");
            
            
            
            
            
        }else{
            for (int i=0; i<size_values; i++) {
                
                NSString *next_status;
                
                if (edit_change==0)
                {
                    
                    
                    if ([datavalues[i][0] floatValue]> start_new)
                    {
                        if ([datavalues[i][0] floatValue] < end_new)
                        {
                            BFLog(@"element %@ in between the ranges",newline[i]);
                            if (![previous_status isEqualToString:status_new])
                            {
                                NSArray *dummy1 = @[startTime,previous_status];
                                [dummy addObject:dummy1];
                                
                                
                                insertedPos=[dummy count];
                                
                                NSArray *dummy2 = @[startTime,status_new];
                                [dummy addObject:dummy2];
                                NSArray *dummy3 = @[endTime,status_new];
                                [dummy addObject:dummy3];
                                
                            }else{
                                
                                
                                insertedPos=[dummy count]-1;
                                
                                NSArray *dummy3 = @[endTime,status_new];
                                [dummy addObject:dummy3];
                                
                                
                                
                            }
                            next_status=[self nextStatus:i data:datavalues end:end_new];
                            if(![status_new isEqualToString:next_status]){
                                
                                NSArray *dummy4 = @[endTime,next_status];
                                [dummy addObject:dummy4];
                            }
                            edit_change=1;
                            
                            
                            
                        }else{
                            edit_change=1;
                            if (![previous_status isEqualToString:status_new]) {
                                
                                
                                NSArray *dummy1 = @[startTime,previous_status];
                                [dummy addObject:dummy1];
                                
                                
                                
                                insertedPos=[dummy count];
                                NSArray *dummy2 = @[startTime,status_new];
                                [dummy addObject:dummy2];
                                NSArray *dummy3 = @[endTime,status_new];
                                [dummy addObject:dummy3];
                                if ([newline[i][1] isEqualToString:status_new] && i==size_values-1)
                                {//removedi===size-1
                                    [dummy removeLastObject];
                                    NSArray *dummy3 = @[@"24:00",status_new];
                                    [dummy addObject:dummy3];
                                    
                                }else{
                                    
                                    NSArray *dummy3 = @[endTime,newline[i][1]];
                                    if (i+1 < [datavalues count])
                                    {
                                        if(![dummy[[dummy count]-1][0] isEqualToString:newline[i+1][0]])
                                        {
                                            
                                            [dummy addObject:dummy3];
                                            [dummy addObject:newline[i]];
                                            
                                        }
                                    }else{
                                        [dummy addObject:dummy3];
                                        [dummy addObject:newline[i]];
                                        
                                    }
                                }
                            }else{
                                
                                [dummy removeAllObjects];
                                dummy=[[NSMutableArray alloc]initWithArray:newline];
                                
                            }
                            
                            
                            
                        }
                        
                    }else if([datavalues[i][0] floatValue] == start_new){
                        if([datavalues[i+1][0] floatValue] == end_new){
                            
                            NSArray *dummy3 = @[startTime,status_new];
                            
                            NSArray *dummy4 = @[endTime,status_new];
                            [dummy addObject:dummy3];
                            [dummy addObject:dummy4];
                            
                            
                            
                        }else if ([datavalues[i][1] isEqualToString:status_new])
                        {
                            
                            NSArray *dummy3 = @[endTime,status_new];
                            [dummy addObject:dummy3];
                        }else{
                            
                            if ([datavalues[i][0] floatValue] < end_new) {
                                BFLog(@"element %@ in between the ranges",newline[i]);
                                if(i!=0 )
                                {
                                    NSArray *dummy1 = @[startTime,previous_status];
                                    [dummy addObject:dummy1];
                                }
                                
                                insertedPos=[dummy count];
                                
                                NSArray *dummy2 = @[startTime,status_new];
                                [dummy addObject:dummy2];
                                NSArray *dummy3 = @[endTime,status_new];
                                [dummy addObject:dummy3];
                                
                                next_status=[self nextStatus:i data:datavalues end:end_new];
                                
                                if(![status_new isEqualToString:next_status])
                                {
                                    NSArray *dummy4 = @[endTime,next_status];
                                    [dummy addObject:dummy4];
                                    
                                }
                            }
                        }
                        
                        
                        
                        edit_change=1;
                        
                        
                    }else{
                        
                        [dummy addObject:newline[i]];
                        BFLog(@"new array:%@",dummy);
                    }
                    
                }else{ //if edited==0
                    //if(i+1<size_values){
                    if ([datavalues[i][0] floatValue] >= end_new){
                        if ([datavalues[i][1] isEqualToString:dummy[[dummy count]-2][1]]) {
                            [dummy removeLastObject];
                            [dummy addObject:newline[i]];
                        }
                        //else  [dummy removeLastObject];
                        
                        else{
                            if(![dummy[[dummy count]-1][1] isEqualToString:newline[i][1]]){
                                
                                
                                NSMutableArray *dummy4 = [[NSMutableArray alloc] init];
                                dummy4=[dummy objectAtIndex:[dummy count]-1];
                                NSArray *dummy5 = @[dummy4[0],newline[i][1]];
                                
                                //   [dummy removeLastObject];
                                
                                
                                [dummy addObject:dummy5];
                            }else{
                                [dummy addObject:newline[i]];
                            }
                        }
                    }
                    
                    //}else{
                    
                    //   break;
                    // }
                }
                
                previous_element=newline[i];
                previous_status = previous_element[1];
                
                
                
            }
            
//            BFLog(@"final Array:%@",dummy);
            NSString *last_time;
            NSString *secnd_lasttime;
            NSString *third_last;
            
            if ([dummy count]>=3) {
                last_time = dummy[[dummy count]-1][0];
                secnd_lasttime = dummy[[dummy count]-2][0];
                third_last = dummy[[dummy count]-3][0];
            }
            
            if ([last_time isEqualToString:@"24:00"]&&[secnd_lasttime isEqualToString:@"24:00"]&&[third_last isEqualToString:@"24:00"]) {
                [dummy removeLastObject];
                [dummy removeLastObject];
            }
            else if ([last_time isEqualToString:@"24:00"]&&[secnd_lasttime isEqualToString:@"24:00"]) {
                
                [dummy removeLastObject];
            }
            [newline removeAllObjects];
            newline=dummy;
            //minnu
//            BFLog(@"DATAARRAY_NEW\n %@",newline);
//            BFLog(@"DATAARRAY_NEW\n %@",newline);
//            BFLog(@"Locations:%@",location1_list);
            ///minnu
            //dataarray4=[oldline mutableCopy];
            //minnu
            [contentTable reloadData];
            
        }
        insertStatus=1;
    }else{
        /*  if([status isEqualToString:@"off"]){
         offButton1.backgroundColor=[UIColor colorWithRed:0.80 green:1.00 blue:0.00 alpha:1.0];
         
         
         }else if([status isEqualToString:@"D"]){
         
         dButton1.backgroundColor=[UIColor colorWithRed:0.80 green:1.00 blue:0.00 alpha:1.0];
         
         
         }else if([status isEqualToString:@"ON"]){
         
         onButton1.backgroundColor=[UIColor colorWithRed:0.80 green:1.00 blue:0.00 alpha:1.0];
         
         }else{
         
         sbButton1.backgroundColor=[UIColor colorWithRed:0.80 green:1.00 blue:0.00 alpha:1.0];
         
         }*/
        UIAlertController *alert1 = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Please select start and end time before selecting the event" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ok1= [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            [alert1 dismissViewControllerAnimated:YES completion:nil];
            
        }];
        
        
        [alert1 addAction:ok1];
    
        [self presentViewController:alert1 animated:YES completion:nil];
        
        
    }
    
}

- (IBAction)saveBtn:(id)sender {
    if([newline count]!=0){
    if([location1.text isEqualToString:@""] || [start.text isEqualToString:@""] || [End.text isEqualToString:@""]){
        
        
        UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@""
                                                         message:@"Required fields cannot be empty!"
                                                        delegate:self
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles:nil, nil];
        
        [alert1 show];
        
        
    }else{
        
//        BFLog(@"new inserted pos :%ld",(long)insertedPos);
        NSInteger check_flag=0;
        NSInteger notes_flag=0;
        NSString *startTime=start.text;
        NSString *endTime = End.text;
        if([newline count]!=0){
            dataarray4=[newline mutableCopy];
        }
        //minnu
        NSString *init=@"0:0";
        NSArray *initTime=[init componentsSeparatedByString:@":"];
        NSArray *start=[startTime componentsSeparatedByString:@":"];
        NSArray *end=[endTime componentsSeparatedByString:@":"];
        int duration=([end[0]intValue]*60+[end[1] intValue])-([start[0]intValue]*60+[start[1] intValue]);
        
        int repeat=(int)duration/15;
        //if([start[0]intValue]==0 && [start[1] intValue]==0)
           // repeat=repeat+1;
        duration=([start[0]intValue]*60+[start[1] intValue])-([initTime[0]intValue]*60+[initTime[1] intValue]);
        int slot=(int)duration/15;
        slot=slot+1;
        annotation *obj=[annotation new];
        NSString* charStr=[@"" stringByPaddingToLength:96 withString:@"-" startingAtIndex:0];
        NSMutableString *charStrWithSlash=[NSMutableString stringWithFormat:@""];
        for(int i=0;i<charStr.length;i++){
            NSUInteger len=1;
            [charStrWithSlash appendFormat:@"%@|",[charStr substringWithRange:NSMakeRange(i, len)]];
        }
        if([locStr isEqualToString:@""])locStr=charStrWithSlash;
        NSString* newLocationStr=[obj updateStr:locStr index1:slot index2:repeat notes:location1.text];
        if([noteStr isEqualToString:@""])noteStr=charStrWithSlash;
        NSString* newNotesStr=[obj updateStr:noteStr index1:slot index2:repeat notes:notes1.text];
        NSArray *locationStrArrayTemp=[newLocationStr componentsSeparatedByString:@"|"];
        NSMutableArray *locationStrArray=[locationStrArrayTemp mutableCopy];
        NSArray *notesStrArrayTemp=[newNotesStr componentsSeparatedByString:@"|"];
        NSMutableArray *notesStrArray=[notesStrArrayTemp mutableCopy];
        DBManager *objDB= [[DBManager alloc] initWithDatabaseFilename:@"hosDB.sql"];
        NSArray *userInfoArray=[[NSArray alloc]initWithArray: [objDB getUserId]];
        int userId = 0;
        if([userInfoArray count]!=0)
            userId = [userInfoArray[0][0] intValue];
            
        loggraph *objLog=[loggraph new];
        for(int i=0;i<[statusArr count];i++){
            objLog.location=[locationStrArray objectAtIndex:i];
            objLog.slot=[NSString stringWithFormat:@"%d",(int)(1+i)];
            objLog.notes=[notesStrArray objectAtIndex:i];
            objLog.location=[locationStrArray objectAtIndex:i];
            objLog.driverStatus=[statusArr objectAtIndex:i];
            objLog.calculatedStatus=[statusArr objectAtIndex:i];
            objLog.date=dateValue;
            objLog.insert=[NSString stringWithFormat:@"%d",1];
            objLog.userId=[NSString stringWithFormat:@"%d",userId];
            if(i==0)
                objLog.approve=[NSString stringWithFormat:@"%d",0];
            [objDB insertPastStatus:objLog];
            
            
        }
        NSLog(@"stringsLocNote:%@ : %@",newLocationStr,newNotesStr);
        if (objDB.affectedRows != 0) {
            // BFLog(@"Table updated successfully. Affected rows = %d", dbManager.affectedRows);
            UIAlertController *alert1 = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Modification Saved" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *ok1= [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                [alert1 dismissViewControllerAnimated:YES completion:nil];
            }];
            
            
            [alert1 addAction:ok1];
            [self presentViewController:alert1 animated:YES completion:nil];
            statusString=statusStrNew;
           
        }
        else{
            //  BFLog(@"Could not execute the query");
        }
        //minnu
        BFLog(@"Location array:%@",location1.text);
        if (location1_list==nil) {
            location1_list=[[NSMutableArray alloc] init];
        }
        for (int k=0; k<[location1_list count]; k++) {
            if ([[location1_list[k] valueForKey:@"id"] integerValue]==insertedPos) {
                
                NSMutableDictionary *sam1 = [[NSMutableDictionary alloc] init];
                [sam1 setObject:[location1_list[k] valueForKey:@"id"] forKey:@"id"];
                [sam1 setObject:location1.text forKey:@"value"];
                [location1_list removeObjectAtIndex:k];
                [location1_list insertObject:sam1 atIndex:k];
                check_flag=1;
                break;
                
            }
        }
        if (check_flag==0) {
            NSMutableDictionary *sam=[[NSMutableDictionary alloc] init];
            [sam setValue:[NSString stringWithFormat:@"%ld",(long)insertedPos] forKey:@"id"];
            [sam setObject:location1.text forKey:@"value"];
            [location1_list addObject:sam];
            
        }
        if (notes_array==nil) {
            notes_array=[[NSMutableArray alloc] init];
        }
        for (int k=0; k<[notes_array count]; k++) {
            if ([[notes_array[k] valueForKey:@"id"] integerValue]==insertedPos) {
                NSMutableDictionary *sam=[[NSMutableDictionary alloc] init];
                [sam setValue:[NSString stringWithFormat:@"%ld",(long)insertedPos] forKey:@"id"];
                [sam setObject:notes1.text forKey:@"value"];
                [notes_array removeObjectAtIndex:k];
                [notes_array insertObject:sam atIndex:k];
                notes_flag=1;
                break;
                
            }
        }
        if (notes_flag==0) {
            NSMutableDictionary *sam=[[NSMutableDictionary alloc] init];
            [sam setValue:[NSString stringWithFormat:@"%ld",(long)insertedPos] forKey:@"id"];
            [sam setObject:notes1.text forKey:@"value"];
            [notes_array addObject:sam];
            
        }
        
        
        BFLog(@"DATAARRAY_NEW\n %@",dataarray4);
        if (![startTime isEqualToString:@""]&&![endTime isEqualToString:@""]&& ![status_new isEqualToString:@""]) {
            
            NSArray *subViews = [self.view subviews];
            for(UIView *view in subViews){
                if ([view isKindOfClass:[insertView class]]) {
                    [view removeFromSuperview];
                    flag_in = flag_in +1;
                    
                    insertView *i = [[insertView alloc] initWithFrame:(CGRectMake(0, 70, 600, 100))];
                    y_origin1=0;
                    [i setOpaque:NO];
                    
                    [self.view addSubview:i];
                    [i setNeedsDisplay];
                }
            }
            
            if(insertStatus==1){
                [self savetoDb:dataarray4];
            }else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    UIAlertController *alert1 = [UIAlertController alertControllerWithTitle:@"Attention" message:@"No Modifications Made. Enter required values!!" preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction *ok1= [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                        [alert1 dismissViewControllerAnimated:YES completion:nil];
                    }];
                    
                    
                    [alert1 addAction:ok1];
                    [self presentViewController:alert1 animated:YES completion:nil];
                });
                
            }
            
            
            
            
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                UIAlertController *alert1 = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Please fill all the fields" preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *ok1= [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                    [alert1 dismissViewControllerAnimated:YES completion:nil];
                }];
                
                
                [alert1 addAction:ok1];
                [self presentViewController:alert1 animated:YES completion:nil];
            });
            
        }
    }
    }
    else{
        UIAlertController *alert1 = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Please fill the event before save." preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ok1= [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            [alert1 dismissViewControllerAnimated:YES completion:nil];
        }];
        
        
        [alert1 addAction:ok1];
        [self presentViewController:alert1 animated:YES completion:nil];
        
    }
    
}
-(void)moveToLogin{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"loginscreen"];
        [self presentViewController:vc animated:YES completion:nil];
    });
    
}

-(void)saveToServer:(NSMutableDictionary *)data{
    
//    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
//    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration];
//    //getting authCode
//    NSString *query0 = [NSString stringWithFormat:@"select * from authDetails;"];
//    NSArray *authData = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query0]];
//    NSString *authCode = @"";
//    if([authData count]!=0){
//        authCode = authData[0][1];
//        
//    }
//    //---------------------------
//    NSString *urlString = [NSString stringWithFormat:@"http://www.gpstracking-server1.com/gpstracking/hosapp/saveHosGraphData.php?masterUser=%@&user=%@&date=%@&approve=0&authCode=%@",master_menu,user_menu,current_date,authCode];
//    // NSString *urlString = [NSString stringWithFormat:@"http://23.239.28.100/mydealership/saveHosGraphData_app.php?masterUser=%@&user=%@&date=%@&approve=0",master_menu,user_menu,current_date];
//    NSURL *url = [NSURL URLWithString:urlString];
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
//    [request setHTTPMethod:@"HEAD"];
//    NSURLResponse *response;
//    NSError *error;
//    NSData *mydata = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
//    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
//    if(httpResponse!=nil){
//        if ([httpResponse statusCode] ==200 ) {
//            BFLog(@"Device connected to the internet");
//            NSURL *url = [NSURL URLWithString:urlString];
//            
//            //NSError *error;
//            
//            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
//                                                                   cachePolicy:NSURLRequestUseProtocolCachePolicy
//                                                               timeoutInterval:60.0];
//            
//            [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
//            [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
//            
//            [request setHTTPMethod:@"POST"];
//            /* NSDictionary *mapData = [[NSDictionary alloc] initWithObjectsAndKeys: @"TEST IOS", @"name",
//             @"IOS TYPE", @"typemap",
//             nil];*/
//            
//            //NSMutableDictionary *currentStatus=[[NSMutableDictionary alloc]init];
//            //[currentStatus setValue:status forKey:@"Status"];*/
//            /*  NSMutableArray *inner = [[NSMutableArray alloc] initWithArray:@[@"00:00",@"OFF"]];
//             NSMutableDictionary *data_dict = [[NSMutableDictionary alloc] init];
//             [data_dict setObject:inner forKey:@"payload"];
//             NSError *error;*/
//            NSData *postData = [NSJSONSerialization dataWithJSONObject:data options:0 error:&error];
//            [request setHTTPBody:postData];
//            
//            
//            NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
//                
//                NSDictionary *responseData = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
//                
//                if(error==nil){
//                    dispatch_async(dispatch_get_main_queue(), ^{
//                        UIAlertController *alert1 = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Modification Saved" preferredStyle:UIAlertControllerStyleAlert];
//                        UIAlertAction *ok1= [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
//                            [alert1 dismissViewControllerAnimated:YES completion:nil];
//                        }];
//                        
//                        
//                        [alert1 addAction:ok1];
//                        [self presentViewController:alert1 animated:YES completion:nil];
//                        
//                    });
//                    
//                }
//                
//            }];
//            
//            [postDataTask resume];
//        }else if([httpResponse statusCode]==403){
//            if(autoLoginCount<=1){
//                BOOL success =  [self autoLogin];
//                
//                if(success){
//         //           [self saveToServer:data];
//                    
//                }else{
//                    KeychainItemWrapper *credentials = [[KeychainItemWrapper alloc] initWithIdentifier:@"credentials" accessGroup:nil];
//                    [credentials resetKeychainItem];
//                    UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Session Expired!!"
//                                                                     message:@"Please Login Again"
//                                                                    delegate:self
//                                                           cancelButtonTitle:@"OK"
//                                                           otherButtonTitles:nil, nil];
//                    alert1.tag = 1;
//                    alert1.delegate = self;
//                    
//                    [alert1 show];
//                    menuScreenViewController *obj = [menuScreenViewController new];
//                    [obj syncServerLater:data date:current_date];
//                    
//                }
//            }else{
//                KeychainItemWrapper *credentials = [[KeychainItemWrapper alloc] initWithIdentifier:@"credentials" accessGroup:nil];
//                [credentials resetKeychainItem];
//                UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Session Expired!!"
//                                                                 message:@"Please Login Again"
//                                                                delegate:self
//                                                       cancelButtonTitle:@"OK"
//                                                       otherButtonTitles:nil, nil];
//                alert1.tag = 2;
//                alert1.delegate = self;
//                
//                [alert1 show];
//                menuScreenViewController *obj = [menuScreenViewController new];
//                [obj syncServerLater:data date:current_date];
//                
//                
//            }
//            
//        }else if([httpResponse statusCode]==503){
//            BFLog(@"Device not connected to the internet");
//            dispatch_async(dispatch_get_main_queue(), ^{
//                UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Service Unavailable"
//                                                                 message:@"No connectivity with server at this time,it will sync automatically"
//                                                                delegate:self
//                                                       cancelButtonTitle:@"OK"
//                                                       otherButtonTitles:nil, nil];
//                
//                [alert1 show];
//                menuScreenViewController *obj = [menuScreenViewController new];
//                [obj syncServerLater:data date:current_date];
//            });
//            
//        }
//    }
//    
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if(alertView.tag ==1 || alertView.tag ==2){
        
        if(buttonIndex==[alertView cancelButtonIndex]){
            
            
            [self moveToLogin];
            
        }
        
        
    }
    
    
    
}

-(BOOL)autoLogin{
    
    autoLoginCount++;
    KeychainItemWrapper *credentials = [[KeychainItemWrapper alloc] initWithIdentifier:@"credentials" accessGroup:nil];
    NSString *username1 = [credentials objectForKey:(id)kSecAttrAccount];
    NSData *password1 = [credentials objectForKey:(id)kSecValueData];
    NSString *pass = [[NSString alloc] initWithData:password1 encoding:NSUTF8StringEncoding];
    // NSString *username1 = [[NSUserDefaults standardUserDefaults] objectForKey:@"username"];
    //NSString *password1 = [[NSUserDefaults standardUserDefaults] objectForKey:@"password"];
    NSString *urlString = [NSString stringWithFormat:@"http://www.gpstracking-server1.com/gpstracking/hosapp/EnableLogin_hos.php?username=%@&password=%@",username1,pass];
    
    NSURL *url = [NSURL URLWithString:urlString];
    NSData *data = [NSData dataWithContentsOfURL:url];
    if(data){
        
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:urlString] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            //  BFLog(@"Retrieved Data:%@,%@",data,response);
            
            if (error.code==0) {
                
                NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                
                if(json!=nil){
                    if([json count]==1){
                        returnString=NO;
                        
                    }else{
                        NSString *authCode = json[@"authCode"];
                        if(![authCode isEqualToString:@""]){
                            NSString *query_del10 =[NSString stringWithFormat:@"delete from authDetails;"];
                            [dbManager executeQuery:query_del10];
                            
                            NSString *query_insert2 = [NSString stringWithFormat:@"insert into authDetails values(null, '%@')",authCode];
                            
                            // Execute the query.
                            
                            //NSString *query_del = @"delete from TempTable";
                            [dbManager executeQuery:query_insert2];
                            
                            // If the query was successfully executed then pop the view controller.
                            if (dbManager.affectedRows != 0) {
                                BFLog(@"Query was inserted the data successfully. Affected rows = %d", dbManager.affectedRows);
                            }
                            else{
                                BFLog(@"Could not execute the query");
                            }
                            
                            returnString = YES;
                            
                            
                            
                        }else{
                            
                            returnString=NO;
                            
                        }
                    }
                }else{
                    
                    returnString=NO;
                }
                
            }else{
                
                UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                                 message:@"Could not connect to server.Please try again later!!!"
                                                                delegate:self
                                                       cancelButtonTitle:@"OK"
                                                       otherButtonTitles:nil, nil];
                
                //[alert1 show];
                returnString=NO;
                
            }
            
            
            
        }];
        [dataTask resume];
        
        
        
    }else{
        
        
        returnString=NO;
    }
    
    
    
    
    return returnString;
    
}
-(NSString*)nextStatus:(int)index data:(NSMutableArray *)dataaarray end:(float)end_new{
    NSString *status=@"";
    for (int i=index; i<[dataaarray count]; i++) {
        
        
        
        if ([dataaarray[i][0] floatValue]>=end_new) {
            
            if(i!=[dataaarray count]-1){
                NSArray *next_element1=dataarray4[i];
                status = next_element1[1];
                break;
            }
            else{
                
                NSArray *next_element1=dataarray4[i];
                status = next_element1[1];
                break;
                
            }
        }
        
        
    }
    return status;
    
    
}

-(NSMutableArray *)swaptheelements:(NSMutableArray *)data index:(int)i{
    
    NSArray *cont = data[[data count]-1];
    [data insertObject:data[i] atIndex:[data count]-1];
    [data insertObject:cont atIndex:i];
    return data;
    
    
    
    
}
- (void)savetoDb:(NSMutableArray*)data{
    autoLoginCount=0;
    BFLog(@"data:%@",dataarray);
    // NSString *location1 = _location1.text;
    //  int id1 =(int) [data count]-1;
    // if ([location1_array count]==0) {
    //  NSMutableDictionary *dict1 = [[NSMutableDictionary alloc] init];
    //[dict1 setValue:@(id1) forKey:@"id"];
    // [dict1 setValue:location1 forKey:@"location1"];
    //  [location1_array addObject:dict1];
    //}else{
    
    
    
    //}
    NSMutableArray *outer_array = [[NSMutableArray alloc] init];
    NSMutableDictionary *data_dict = [[NSMutableDictionary alloc] init];
    
    for (int i=0; i<[data count]; i++) {
        
        NSString *x12=data[i][0];
        NSArray *time = [x12 componentsSeparatedByString:@":"];
        int x4 =(int) [time[1] integerValue]/15;
        int x2 =(int) [time[0] integerValue]*4+x4;
        float x1 = 12.25*x2;
        float posx = x1+40.0;
        int posy = 65;
        BFLog(@"posx:%.2f",posx);
        if ([data[i][1] isEqualToString:@"OFF"]) {
            posy = 65;
        }else if([data[i][1] isEqualToString:@"ON"]){
            posy = 245;
        }else if([data[i][1] isEqualToString:@"D"]){
            posy=185;
        }else if ([data[i][1] isEqualToString:@"SB"]){
            posy=125;
        }
        
        NSMutableDictionary *cont1 = [[NSMutableDictionary alloc] init];
        [cont1 setValue:@(i) forKey:@"id"];
        [cont1 setValue:@(posx) forKey:@"posx"];
        [cont1 setValue:@(posy) forKey:@"posy"];
        
        [outer_array addObject:cont1];
        
        
        
    }
    
    [data_dict setObject:outer_array forKey:@"payload"];
    
    if (location1_list !=NULL && location1_list!=(id)[NSNull null]  && location1_list!=nil && [location1_list count]!=0) {
        
        [data_dict setObject:location1_list forKey:@"location"];
        
        
    }
    if (notes_array!=NULL && notes_array!=(id)[NSNull null]  && notes_array!=nil && [notes_array count]!=0) {
        [data_dict setObject:notes_array forKey:@"notes"];
    }
    //[data_dict setObject:location1_array forKey:@"location1"];
    
    //BFLog(@"dictionary:%@",data_dict);
    NSData *data_object = [NSJSONSerialization dataWithJSONObject:data_dict options:NSJSONWritingPrettyPrinted error:nil];
   // [self saveToServer:data_dict];
    NSString *jsonStr = [[NSString alloc] initWithData:data_object
                                              encoding:NSUTF8StringEncoding];
    NSString *status = data[[data count]-1][1];
    // NSArray *values = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query1]];
    NSString *query1 = [NSString stringWithFormat:@"select * from TempTable where date='%@'",current_date];
    
    // Get the results.
    
    NSArray *values = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query1]];
    
    if ([values count]!=0) {
        
        
        
        NSString *query_update = [NSString stringWithFormat:@"UPDATE TempTable SET data='%@',last_status='%@',flag='%d' WHERE date='%@'",jsonStr,status,0,current_date];
        
        // Execute the query.
        
        //NSString *query_del = @"delete from TempTable";
        [dbManager executeQuery:query_update];
        
        // If the query was successfully executed then pop the view controller.
        if (dbManager.affectedRows != 0) {
            // BFLog(@"Table updated successfully. Affected rows = %d", dbManager.affectedRows);
            UIAlertController *alert1 = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Modification Saved" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *ok1= [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                [alert1 dismissViewControllerAnimated:YES completion:nil];
            }];
            
            
            [alert1 addAction:ok1];
            //[self presentViewController:alert1 animated:YES completion:nil];
            
            
        }
        else{
            //BFLog(@"Could not execute the query");
        }}
    
    else{
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat=@"MM/dd/yyyy";
        [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
        NSDate *dateData=[formatter dateFromString:current_date];
        NSTimeInterval interval=[dateData timeIntervalSince1970]*1000;
        BFLog(@"Timeinterval:%f",interval);
        
        
        NSString *query_insert1 = [NSString stringWithFormat:@"insert into TempTable values(null, '%@', '%@', '%@' ,'%d','%d','%f')", current_date,jsonStr,status,0,1,interval];
        //  NSString *query_insert1 = [NSString stringWithFormat:@"insert into TempTable values(null, '%@', '%@', '%@' ,'%d')", current_date,jsonStr,status,0];
        
        // Execute the query.
        
        //NSString *query_del = @"delete from TempTable";
        [dbManager executeQuery:query_insert1];
        
        // If the query was successfully executed then pop the view controller.
        if (dbManager.affectedRows != 0) {
            //BFLog(@"Query was inserted the data successfully. Affected rows = %d", dbManager.affectedRows);
            UIAlertController *alert1 = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Modification Saved" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *ok1= [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                [alert1 dismissViewControllerAnimated:YES completion:nil];
            }];
            
            
            [alert1 addAction:ok1];
            [self presentViewController:alert1 animated:YES completion:nil];
            
            
        }
        else{
            //BFLog(@"Could not execute the query");
        }
        
        
        
    }
    
    
    
    
}

-(float)getposx:(NSString *)posx1{
    
    NSString *x12=posx1;
    NSArray *time = [x12 componentsSeparatedByString:@":"];
    int x4 =(int) [time[1] integerValue]/15;
    int x2 =(int) [time[0] integerValue]*4+x4;
    float x1 = 12.25*x2;
    float posx = x1+40.0;
    
    return posx;
    
}
-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    CLLocation *newlocation = [locations lastObject];
    [geocoder reverseGeocodeLocation:newlocation completionHandler:^(NSArray *placemarks, NSError *error) {
        if (error == nil&& [placemarks count] >0) {
            placemark = [placemarks lastObject];
            NSString *latitude, *longitude, *state, *country,*locality;
            latitude = [NSString stringWithFormat:@"%f",newlocation.coordinate.latitude];
            longitude = [NSString stringWithFormat:@"%f",newlocation.coordinate.longitude];
            state = placemark.administrativeArea;
            country = placemark.country;
            locality=placemark.locality;
            // BFLog(@"address1:%@,%@,%@",locality,state,country);
            address_full1=[NSString stringWithFormat:@"%@, %@",locality,state];
            location1.text=address_full1;
            //address1_full=@"";
        } else {
            // BFLog(@"%@", error.debugDescription);
        }
    }];
    // Turn off the location1 manager to save power.
    [manager stopUpdatingLocation];
}
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    // BFLog(@"Cannot find the location.");
}
-(NSString *)getStatusCode:(NSString *)status_new{
    NSString *statusNew;
    if([status_new isEqualToString:@"D"])
        statusNew=@"7";
    else if([status_new isEqualToString:@"SB"])
        statusNew=@"6";
    else if([status_new isEqualToString:@"OFF"])
        statusNew=@"5";
    else if([status_new isEqualToString:@"ON"])
        statusNew=@"8";
    DBManager *objDB = [[DBManager alloc] initWithDatabaseFilename:@"hosDB.sql"];
    NSDictionary *token;
    ;
    NSMutableDictionary *whereToken;
    NSArray *userInfo=[[NSArray alloc]initWithArray:[objDB getUserId]];
    NSString * userId=@"";
    if([userInfo count]!=0)
        userId  =userInfo[0][0];
    token=@{@"date":dateValue,@"slot":[NSString stringWithFormat:@"%d",slotInsert
                                       ],@"userid":userId};
    ;
    whereToken=[[NSMutableDictionary alloc] initWithDictionary:token];
    if([status_new isEqualToString:@"D"])
        statusNew=[objDB findRestart:whereToken];
    
    
    return statusNew;
}

@end
