//
//  logForm.h
//  MaTrackHOS
//
//  Created by Raghee Chandran M on 7/15/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DBManager1.h"
#import "EditForm.h"
#import "DBManager.h"
#import "Carrier.h"
#import "logsmenuscreen.h"
#import "AppDelegate.h"
extern NSArray *general ;
extern NSArray *carrier_details ;
extern NSArray *other;
extern int section_num;
extern int formTableHeight;

@interface logForm : UIViewController<UITableViewDataSource, UITableViewDelegate>{
    
    

}

@property (weak, nonatomic) IBOutlet UIScrollView *scroller3;
@property (weak, nonatomic) IBOutlet UITableView *form_log;
@property (nonatomic, strong) DBManager1 *dbManager;
@end
