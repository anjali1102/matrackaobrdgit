//
//  logForm.m
//  MaTrackHOS
//
//  Created by Raghee Chandran M on 7/15/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//

#import "logForm.h"
#import "accountSettings.h"

int section_num=0;
NSArray *general ;
NSArray *carrier_details ;
NSArray *other;
int formTableHeight=0;
@interface logForm ()

@end

@implementation logForm
@synthesize dbManager;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.form_log.delegate=self;
    self.form_log.dataSource=self;
    formTableHeight=self.form_log.frame.size.height;
    [_scroller3 setScrollEnabled:YES];
    [_scroller3 setContentSize:(CGSizeMake(600, 3500))];
    CGPoint bottomOffset = CGPointMake(0, 3500-_scroller3.bounds.size.height);
    [_scroller3 setContentOffset:bottomOffset animated:YES];
    
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated{
    
   /*  raghee dbManager = [[DBManager1 alloc] initWithDatabaseFilename:@"HOSDatabase.sql"];
    NSString *query0 = [NSString stringWithFormat:@"select * from GeneralSettings;"];
    
    // Get the results.
    
    general = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query0]];
    BFLog(@"Result:%@",general);*/
   /* NSString *query1 = [NSString stringWithFormat:@"select * from Carrier_Sett;"];
    
    // Get the results.
    
    carrier_details = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query1]];
    BFLog(@"Result:%@",carrier_details);*/
    /* raghee NSString *query2 = [NSString stringWithFormat:@"select * from OtherSettings;"];
    
    // Get the results.
    
    other = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query2]];
    BFLog(@"Result:%@",other);*/
    carrier_details=[[NSArray alloc]init];
    Carrier *objCarrier=[Carrier new];
    DBManager *objDB= [[DBManager alloc] initWithDatabaseFilename:@"hosDB.sql"];
        NSDictionary *token;
    ;
    NSMutableDictionary *whereToken;
    NSArray *userInfoArray=[[NSArray alloc]initWithArray: [objDB getUserId]];
    if([userInfoArray count]!=0){
        int userId = [userInfoArray[0][0] intValue];
    token=@{@"columnName":@"userid",@"entry":[NSString stringWithFormat:@"%d",userId]};
    ;
    whereToken=[[NSMutableDictionary alloc] initWithDictionary:token];
    NSArray * carrierIdInfo= [[NSArray alloc] initWithArray:[objDB getCarrierId:whereToken]];
        if([carrierIdInfo count]!=0){
    token=@{@"columnName":@"id",@"entry":[NSString stringWithFormat:@"%@", carrierIdInfo[0][0]]};
    ;
    whereToken=[[NSMutableDictionary alloc] initWithDictionary:token];
            
            carrier_details = [[NSArray alloc] initWithArray:[objDB selectFromTable:objCarrier whereToken:whereToken]];}
    token=@{@"columnName":@"userid",@"entry":[NSString stringWithFormat:@"%d",userId],@"date":[NSString stringWithFormat:@"%@",dateValue]};
    ;
    whereToken=[[NSMutableDictionary alloc] initWithDictionary:token];
        general=[[NSArray alloc]init];
        other=[[NSArray alloc]init];
        
    NSArray *values = [[NSArray alloc] initWithArray:[objDB getLogData:whereToken]];
        if([values count]!=0){
        
        other = [[NSArray alloc]initWithObjects:values[10],values[11],values[13],values[5],nil];
        general=[[NSArray alloc]initWithObjects:values[14],values[15],values[12],values[6],[NSString stringWithFormat:@"%@ %@",values[1],values[2]],values[0], nil];
        
    
            
            if(![values[7] isEqualToString:@""]){
                NSArray *carrierhomeForm=[values[8] componentsSeparatedByString:@","];
                NSArray *carriermainForm=[values[9] componentsSeparatedByString:@","];
                NSArray *carrier_detailsTemp=[[NSArray alloc]initWithObjects:@"",values[7],carriermainForm[0],carriermainForm[1],carriermainForm[2],carriermainForm[3],carrierhomeForm[0],carrierhomeForm[1],carrierhomeForm[2],carrierhomeForm[3], nil];
                NSMutableArray *carrierTemp=[[NSMutableArray alloc]initWithCapacity:1];
                for (NSInteger i=0;i<1;i++)
                     [carrierTemp addObject:[NSNull null]];
                [carrierTemp replaceObjectAtIndex:0 withObject:carrier_detailsTemp];
                carrier_details=[[NSArray alloc]initWithArray:carrierTemp];
            }
    
    }   /* if(![values[0][12] isEqualToString:@""]){
            NSString *name=values[0][12];
        }
        if(![values[0][13] isEqualToString:@""]){
            NSArray *homeAddress=[values[0][13] componentsSeparatedByString:@","];
        }
        if(![values[0][14] isEqualToString:@""]){
            NSArray *mainAddress=[values[0][14] componentsSeparatedByString:@","];
        }*/


    [_form_log reloadData];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSInteger rows1;
    if (section==0) {
        rows1=6;
    }else if (section ==1){
        rows1=3;
    }else if (section ==2){
        
        rows1=4;
    }
    
    return rows1;
    
}
-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    
    NSString *title;
    if (section == 0) {
        title = @"General";
    }else if(section ==1){
        
        title=@"Carrier";
    }else if (section ==2){
        
        title=@"Others";
    }
    return title;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString *cellIdentifier = @"cell";
    // NSString *data=@"";
   // UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    cell.textLabel.textColor=[UIColor colorWithRed:0.125 green:0.525 blue:0.125 alpha:1.0];
    cell.detailTextLabel.textColor=[UIColor grayColor];
    if (indexPath.section ==0) {
                switch (indexPath.row) {
            case 0:
                    {
                        cell.textLabel.text=@"Vehicles";
                        accountSettings *obj = [accountSettings new];
                       // [obj addRequired:cell.textLabel];
                        
                        if ([general count]==0) {
                             cell.detailTextLabel.text=@"None";
                        }else{
                            if([general[0] isEqualToString:@"(null)"] ||[general[0] isEqualToString:@"<null>"]||[general[0] isEqualToString:@""]){
                                cell.detailTextLabel.text=@"None";
                                
                            }else{
                             cell.detailTextLabel.text=general[0];
                            }
                        }
                  
                
                break;
                    }
                
            case 1:
                cell.textLabel.text=@"Trailers";
                        if ([general count]==0) {
                            cell.detailTextLabel.text=@"None";
                        }else{
                            if([general[1] isEqualToString:@"(null)"]  ||[general[1] isEqualToString:@"<null>"]||[general[1] isEqualToString:@""]){
                                cell.detailTextLabel.text=@"None";
                                
                            }else{
                            cell.detailTextLabel.text=general[1];
                            }
                        }
                        

                break;
                
            case 2:
                    {
                cell.textLabel.text=@"Distance";
                        accountSettings *obj = [accountSettings new];
                      //  [obj addRequired:cell.textLabel];
                        if ([general count]==0) {
                            cell.detailTextLabel.text=@"None";
                        }else{
                            if([general[2] isEqualToString:@"(null)"]  ||[general[2] isEqualToString:@"<null>"]||[general[2] isEqualToString:@""]){
                                cell.detailTextLabel.text=@"None";
                                
                            }else{
                            cell.detailTextLabel.text=general[2];
                            }
                        }
                        
                break;
                    }
                
            case 3:
                cell.textLabel.text=@"Shipping Documents";
                        if ([general count]==0) {
                            cell.detailTextLabel.text=@"None";
                        }else{
                            if([general[3] isEqualToString:@"(null)"]  ||[general[3] isEqualToString:@"<null>"]||[general[3] isEqualToString:@""]){
                                cell.detailTextLabel.text=@"None";
                                
                            }else{
                                
                            cell.detailTextLabel.text=general[3];
                            }
                        }
                        
                break;
                
            case 4:
                    {
                        
                    cell.textLabel.text=@"Driver*";
                        accountSettings *obj = [accountSettings new];
                        [obj addRequired:cell.textLabel];
                        if ([general count]==0) {
                            cell.detailTextLabel.text=@"None";
                        }else{
                            if([general[4] isEqualToString:@"(null) (null)"]  ||[general[4] isEqualToString:@"<null> <null>"]||[general[4] isEqualToString:@""]){
                                cell.detailTextLabel.text=@"None";
                                
                            }else{
                            cell.detailTextLabel.text=general[4];
                                if([cell.detailTextLabel.text containsString:@"(null)"]){
                                    cell.detailTextLabel.text=[cell.detailTextLabel.text stringByReplacingOccurrencesOfString:@"(null)" withString:@""];
                                }

                            }
                        }
                        
                break;
                    }
                        
                    case 5:
                    {
                        
                        cell.textLabel.text=@"Driver Id";
                       
                        if ([general count]==0) {
                            cell.detailTextLabel.text=@"None";
                        }else{
                            if([general[5] isEqualToString:@"(null)"]  ||[general[5] isEqualToString:@"<null>"]||[general[5] isEqualToString:@""]){
                                cell.detailTextLabel.text=@"None";
                                
                            }else{
                            cell.detailTextLabel.text=general[5];
                            }
                        }
                        
                        break;
                    }
            default:
                break;
        }
    }
        if (indexPath.section ==1) {
            
            switch (indexPath.row) {
                case 0:
                {
                    
                cell.textLabel.text=@"Carrier Name*";
                    accountSettings *obj = [accountSettings new];
                    [obj addRequired:cell.textLabel];
                    if ([carrier_details count]==0) {
                        cell.detailTextLabel.text=@"None";
                    }else{
                        if([carrier_details[[carrier_details count]-1][1] isEqualToString:@"(null)"] || [carrier_details[[carrier_details count]-1][1] isEqualToString:@"<null>"]){
                        cell.detailTextLabel.text=@"None";                        }else{
                             cell.detailTextLabel.text=carrier_details[[carrier_details count]-1][1];
                        }
                    }
                    
                    break;
                }
                    
                case 1:
                {
                    cell.textLabel.text=@"Main Office Address*";
                    accountSettings *obj = [accountSettings new];
                    [obj addRequired:cell.textLabel];
                    if ([carrier_details count]==0) {
                        cell.detailTextLabel.text=@"None";
                    }else{
                        if([carrier_details[[carrier_details count]-1][2] isEqualToString:@"(null),(null),(null),(null)"] || [carrier_details[[carrier_details count]-1][2] isEqualToString:@"<null>,<null>,<null>,<null>"]){
                            cell.detailTextLabel.text=@"None";                        }else{
                        cell.detailTextLabel.text=[NSString stringWithFormat:@"%@,%@,%@,%@.",carrier_details[[carrier_details count]-1][2],carrier_details[[carrier_details count]-1][3],carrier_details[[carrier_details count]-1][4],carrier_details[[carrier_details count]-1][5]];
                            }
                    }                    break;
                }
                    
                case 2:{
                    cell.textLabel.text=@"Home Terminal Address*";
                    accountSettings *obj = [accountSettings new];
                    [obj addRequired:cell.textLabel];
                    if ([carrier_details count]==0) {
                        cell.detailTextLabel.text=@"None";
                    }else{
                        
                        if([carrier_details[[carrier_details count]-1][6] isEqualToString:@"(null),(null),(null),(null)"] || [carrier_details[[carrier_details count]-1][6] isEqualToString:@"<null>,<null>,<null>,<null>"]){
                            cell.detailTextLabel.text=@"None";                        }else{
                        cell.detailTextLabel.text=[NSString stringWithFormat:@"%@,%@,%@,%@.",carrier_details[[carrier_details count]-1][6],carrier_details[[carrier_details count]-1][7],carrier_details[[carrier_details count]-1][8],carrier_details[[carrier_details count]-1][9]];
                            }
                    }
                    break;
                    
                }
                    
                default:
                    break;
            }
        
        
        
    }
    if (indexPath.section ==2) {
        
        switch (indexPath.row) {
            case 0:cell.textLabel.text=@"Co-Driver";
                if ([other count]==0) {
                    cell.detailTextLabel.text=@"None";
                }else{
                    if([other[0] isEqualToString:@"<null>"] || [other[0] isEqualToString:@"(null)"]||[other[0] isEqualToString:@""]){
                        cell.detailTextLabel.text=@"None";
                        
                    }else{
                    cell.detailTextLabel.text=other[0];
                    }
                }
                break;
                
            case 1:
                cell.textLabel.text=@"Origin";
                if ([other count]==0) {
                    cell.detailTextLabel.text=@"None";
                }else{
                    if([other[1] isEqualToString:@"<null>"] || [other[1] isEqualToString:@"(null)"]||[other[0] isEqualToString:@""]){
                       cell.detailTextLabel.text=@"None";
                        
                    }else{
                    cell.detailTextLabel.text=other[1];
                    }
                }                break;
                
            case 2:
                cell.textLabel.text=@"Destination";
                if ([other count]==0) {
                    cell.detailTextLabel.text=@"None";
                }else{
                    if([other[2] isEqualToString:@"<null>"] || [other[2] isEqualToString:@"(null)"]||[other[2] isEqualToString:@""]){
                        cell.detailTextLabel.text=@"None";
                        
                    }else{
                    cell.detailTextLabel.text=other[2];
                    }
                }                break;
                
            case 3:
                cell.textLabel.text=@"Notes";
                if ([other count]==0) {
                    cell.detailTextLabel.text=@"None";
                }else{
                    if([other[3] isEqualToString:@"<null>"] || [other[3] isEqualToString:@"(null)"]||[other[3] isEqualToString:@"(null)"]){
                        cell.detailTextLabel.text=@"None";
                        
                    }else{
                    cell.detailTextLabel.text=other[3];
                    }
                }                break;
                
                         
            default:
                break;
        }
    }
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    section_num=(int)indexPath.section;
    
        UIStoryboard *storyBoard = self.storyboard;
        UIViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"editform"];
        [self presentViewController:vc animated:YES completion:nil];
    
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
