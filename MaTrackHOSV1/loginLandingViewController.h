//
//  loginLandingViewController.h
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 2/2/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DBManager1.h"
#import "pageContentViewController.h"

extern NSString *masterLogin;
extern NSString *userLogin;

@interface loginLandingViewController : UIViewController<UIPageViewControllerDataSource>
@property (weak, nonatomic) IBOutlet UIImageView *loaderImage;
@property (nonatomic, strong) DBManager1 *dbManager;
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@property (strong, nonatomic) UIPageViewController *pageViewController;
@property (strong, nonatomic) NSArray *pageTitles;
@property (strong, nonatomic) NSArray *pageImages;
@end
