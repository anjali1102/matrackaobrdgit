//
//  logsmenuscreen.h
//  MaTrackHOS
//
//  Created by Raghee Chandran M on 7/15/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "logScreenViewController.h"
#import "DVIR_Capture.h"
#import "Trailer_Defects.h"
#import "Vehicle_Defects.h"
#import "DVIRMenu.h"
#import "DBManager1.h"
#import "DBManager.h"
extern NSString *dateValue;
@interface logsmenuscreen : UIViewController
- (IBAction)showComponent:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *containerLog;
@property (weak, nonatomic) IBOutlet UIView *containerForm;
@property (weak, nonatomic) IBOutlet UISegmentedControl *options;
@property (weak, nonatomic) IBOutlet UIView *containerSign;
@property (weak, nonatomic) IBOutlet UINavigationBar *navigation;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *menu1;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *info1;
@property (nonatomic, strong) DBManager1 *dbManager;
@property (weak, nonatomic) IBOutlet UIView *containerDVIR;
- (IBAction)helpAlert:(id)sender;
@end
