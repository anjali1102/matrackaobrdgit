//
//  previousDayViewController.h
//  MaTrackHOS
//
//  Created by Raghee Chandran M on 7/16/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"
#import "graphPerDay.h"
#import "AppDelegate.h"
#import "logScreenViewController.h"
#import "DBManager1.h"
#import "headerCell.h"
#import "headerCell1.h"
#import "commonDataHandler.h"
#import "LogEventEditCollectionViewCell.h"
#import "LogEventInfoCollectionViewCell.h"

extern NSString *current_date;
extern NSInteger selected_section;
extern NSInteger selected_row;
extern int tableHeight;
extern int numofViolations;
extern int cellHeight;
extern int graphHeight;
extern int graphY;
extern int tableY;
extern int sectionHeight1;
extern NSMutableArray *deviceData1;

@interface previousDayViewController : UIViewController<UIAlertViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource, LogEventEditCollectionViewCellDelegate>
{
    IBOutlet UIButton  *vwAddEvent;
    IBOutlet UIButton *vwClearEvent;
    NSString *statusString;
    NSString *noteStr;
    NSString *locStr;
    NSString *newCharStr;
}
@property (weak, nonatomic) IBOutlet UITableView *perdayList;
@property (nonatomic, strong) IBOutlet UICollectionView *collecionViewLogEventInfo;
@property (nonatomic, strong) DBManager1 *dbManager;
@property (weak, nonatomic) IBOutlet graphPerDay *graph;
-(IBAction)AddEventAction:(id)sender;
-(IBAction)ClearAllEventAction:(id)sender;
@end
