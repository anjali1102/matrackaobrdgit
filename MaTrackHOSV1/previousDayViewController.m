//
//  previousDayViewController.m
//  MaTrackHOS
//
//  Created by Raghee Chandran M on 7/16/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//
#import <QuartzCore/QuartzCore.h>
#import "previousDayViewController.h"
#import "LogSettings.h" 
#import "loginViewController.h"
#import "KeychainItemWrapper.h"
#import "insertDutyCycle.h"
#import "Suggestion.h"
#import "SectionRows.h"
#import "editStatus.h"
#import "NSString+NullCheck.h"
NSInteger deleteRow=0;
NSString *dateGlobal;
int tableHeight=0;
int numofViolations=0;
int cellHeight=0;
NSDictionary *json2;
NSMutableArray *statusarray;
NSArray  *statusarray1;
NSMutableArray *starting_time;
NSMutableArray *duration;
NSMutableArray *location_prev;
NSMutableArray *notes_prev;
NSString *current_time;
NSString *current_date;
NSMutableArray *deviceData1;
NSInteger selected_section=0;
NSInteger selected_row=0;
int sections;
 int graphY=0;
int called=0;
int graphHeight=0;
int sectionHeight1=0;
int tableY=0;
int numCycles_prev=0;
float cycle_tym_prev=0.0;
float brk_hr_prev=0.0;
float restart_hr_prev=0.0;
float driving_hr_prev=0.0;
float duty_window_prev=0.0;
float break_tym_prev=0.0;
float driving_total=0.0;
float driving_cons_prev=0.0;
float duty_hr_prev=0.0;
float break_total_prev=0.0;
int numCycleStop_prev=0;
BOOL restart_found_prev=false;
NSMutableArray *violationArray;
@interface previousDayViewController ()
{
    NSMutableArray *arrayOfSuggestionDB;
    NSMutableArray *arrayOfTableData;
    NSMutableArray *arrayTemp;
}
@end

@implementation previousDayViewController
@synthesize dbManager;
@synthesize graph;
- (void)viewDidLoad {
    [super viewDidLoad];
    arrayOfSuggestionDB = [NSMutableArray new];


    vwAddEvent.layer.borderColor = [UIColor colorWithRed:0.125 green:0.525 blue:0.125 alpha:1.0].CGColor;
    vwAddEvent.layer.borderWidth = 1.0;
    vwAddEvent.layer.cornerRadius = 7;
    
    vwClearEvent.layer.borderColor = [UIColor colorWithRed:0.125 green:0.525 blue:0.125 alpha:1.0].CGColor;
    vwClearEvent.layer.borderWidth = 1.0;
    vwClearEvent.layer.cornerRadius = 7;
    
    violationArray=[[NSMutableArray alloc]init];
    
    restart_hr_prev=1706.00;//posx of 34hr
    if([cycleinfo isEqualToString:@"USA70hours"]||[cycleinfo isEqualToString:@"California80hours"]){
        numCycles_prev=8;
        cycle_tym_prev=3470.00;//posx value of 70hr
        
    }else{
        numCycles_prev=7;
        cycle_tym_prev=2980.00;//posx value for 60hr
    }
    
    if([cargoinfo isEqualToString:@"Passenger"]){
        driving_hr_prev=530.00;//posx of 10:00
        duty_window_prev=775.00;//posx of 15:00
        break_tym_prev=[self calculatePosx:@"08:00"];
        
        
    }else{
        driving_hr_prev=579.00;//posx of 11:00
        duty_window_prev=726.00;//posx of 14:00
        break_tym_prev=[self calculatePosx:@"10:00"];
        
        
    }
    called =  1;
    dbManager = [[DBManager1 alloc] initWithDatabaseFilename:@"HOSDatabase.sql"];
    
    //LGPlusButtonsView * view1 = [[LGPlusButtonsView alloc] initWithNumberOfButtons:1 firstButtonIsPlusButton:YES showAfterInit:YES];
    //[self.view addSubview:view1];


    statusarray = [[NSMutableArray alloc]init];
    starting_time = [[NSMutableArray alloc]init];
    duration= [[NSMutableArray alloc]init];
    NSDate *today = [NSDate date];
    NSDateFormatter *outputFormatter2 = [[NSDateFormatter alloc] init];
    [outputFormatter2 setDateFormat:@"MM/dd/YYYY"];
   // NSString *todaysdate = [outputFormatter2 stringFromDate:today];
    NSDateFormatter *outputFormatter3 = [[NSDateFormatter alloc] init];
    [outputFormatter3 setDateFormat:@"HH:mm"];
    current_time = [outputFormatter3 stringFromDate:today];
    /*if (section_index==2) {
     sections=2;
     }else if(section_index==1){
     
     sections=1;
     }
     BFLog(@"sections:%d",sections);*/
    commonDataHandler * common=[commonDataHandler new];
    if (section_index == 1 || ([selected_date isEqualToString:[common getCurrentTimeInLocalTimeZone]])) {
        dateGlobal=selected_date;
        [self getData:selected_date];
         [self checkViolation:selected_date];
        [self getDeviceData:selected_date];

        //[self getDVIRData:selected_date];
        
    }else{
        dateGlobal=selected_previous;
        [self getDeviceData:selected_previous];

        [self getData:selected_previous];
        NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
        [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
        [formatter setDateFormat:@"MM/dd/yyyy"];
        
        [self checkViolation:selected_previous];
        
    }
   
  
    sectionHeight1=[self.perdayList sectionHeaderHeight];
    [self getSuggestionTableData:dateGlobal];


    [_collecionViewLogEventInfo registerNib:[UINib nibWithNibName:@"LogEventInfoCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"LogEventInfoCollectionViewCell"];
    [_collecionViewLogEventInfo registerNib:[UINib nibWithNibName:@"LogEventEditCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"LogEventEditCollectionViewCell"];
    [self createTableData];
}

-(void)checkViolation:(NSString *)date{
    [violationArray removeAllObjects];
    driving_cons_prev=0.0;
    driving_total=0.0;
    break_total_prev=0.0;
    duty_hr_prev=0.0;
    
   /* NSString *query1 = [NSString stringWithFormat:@"select data,flag from TempTable where date='%@'",date];
    
    // Get the results.
    
    NSArray *datafortheDay = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query1]];
    BFLog(@"Result:%@",datafortheDay);
    if ([datafortheDay count]!=0) {
        NSString *dbresult = datafortheDay[[datafortheDay count]-1][0];
        NSData *data2= [dbresult dataUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *json_log1 = [NSJSONSerialization JSONObjectWithData:data2 options:0 error:nil];
        BFLog(@"Dictionary of db data:%@",json_log1);
        if ([json_log1 count]==0) {
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@" " withString:@""];
            dbresult = [NSString stringWithFormat:@"[%@]",dbresult];
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@"\";" withString:@";"];
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@"=\"" withString:@"="];
            
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@"location=(" withString:@"\"location\":["];
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@"payload=(" withString:@"\"payload\":["];
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@");" withString:@"],"];
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@";" withString:@"\","];
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@"=" withString:@":\""];
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@"id" withString:@"\"id\""];
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@"value" withString:@"\"value\""];
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@"posx" withString:@"\"posx\""];
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@"posy" withString:@"\"posy\""];
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@"timezone" withString:@"\"timezone\""];
            NSData *data2= [dbresult dataUsingEncoding:NSUTF8StringEncoding];
            json_log1 = [NSJSONSerialization JSONObjectWithData:data2 options:0 error:nil];
            BFLog(@"Dictionary of db data2:%@",json_log1);
            
        }
        
        
     
        NSMutableArray *dict = [[json_log1 objectForKey:@"payload"] mutableCopy];
        NSDate * now = [NSDate date];
        NSDateFormatter *outputFormatter_tody = [[NSDateFormatter alloc] init];
        [outputFormatter_tody setDateFormat:@"MM/dd/YYYY"];
        NSString *newDateString2 = [outputFormatter_tody stringFromDate:now];
        if([date isEqualToString:newDateString2]){
            if([dict count]%2!=0){
                
                NSMutableDictionary *last = dict[[dict count]-1];
                [dict removeLastObject];
                [dict removeLastObject];
                [dict addObject:last];
                
                
                
                
                
            }
            
            
            
            
        }*/
   //minnu
 /*  DBManager *objDB = [[DBManager alloc] initWithDatabaseFilename:@"hosDB.sql"];
    NSDictionary *token;
    ;
    NSMutableDictionary *whereToken;
    NSArray *userInfoArray=[[NSArray alloc]initWithArray: [objDB getUserId]];
    if([userInfoArray count]!=0){
        int userId = [userInfoArray[0][0] intValue];
        token=@{@"columnName":@"id",@"entry":[NSString stringWithFormat:@"%d",userId]};
        ;
        whereToken=[[NSMutableDictionary alloc] initWithDictionary:token];
        
        //[objDB getLogEditData:whereToken];
        token=@{@"userid":[NSString stringWithFormat:@"%d",userId],@"date":[NSString stringWithFormat:@"%@",date]};
        ;
        whereToken=[[NSMutableDictionary alloc] initWithDictionary:token];
        NSArray* dataFromDB=[[NSArray alloc]initWithArray:[objDB getLogEditData:whereToken]];
        statusString=@"";noteStr=@"",locStr=@"";
        for(int j=0;j<[dataFromDB count];j++){
            if([dataFromDB[j][1] isEqualToString:@""])
                statusString= [statusString stringByAppendingString:[NSString stringWithFormat:@"1"]];
            else
                statusString= [statusString stringByAppendingString:[NSString stringWithFormat:@"%@",[[dataFromDB objectAtIndex:j] objectAtIndex:1]]];
            
            if([dataFromDB[j][2] isEqualToString:@""]||[dataFromDB[j][2] isEqualToString:@"(null)"])
                locStr= [locStr stringByAppendingString:[NSString stringWithFormat:@"-|"]];
            else
                locStr= [locStr stringByAppendingString:[NSString stringWithFormat:@"%@|",[[dataFromDB objectAtIndex:j] objectAtIndex:2]]];
            
            if([dataFromDB[j][3] isEqualToString:@""]||[dataFromDB[j][3] isEqualToString:@"(null)"])
                noteStr= [noteStr stringByAppendingString:[NSString stringWithFormat:@"-|"]];
            else
                noteStr= [noteStr stringByAppendingString:[NSString stringWithFormat:@"%@|",[[dataFromDB objectAtIndex:j] objectAtIndex:3]]];
        }

        //if(dataFromDB[0])
        NSString* charStr=[@"" stringByPaddingToLength:96 withString:@"1" startingAtIndex:0];
        if([dataFromDB count]==0)
            statusString=charStr;
        else
        {
            if([statusString isEqualToString:@""])
                statusString=charStr;
            
        }
        
        commonDataHandler *common=[commonDataHandler new];
        statusString=[common modifyData:statusString date:date];*/
        graphDataOperations *obj=[graphDataOperations new];
        NSMutableArray *dict=[obj generateGraphPoints:statusString];
    //minnu
        int json_size = (int)[dict count];
        // int event_last =(int)[[dict[json_size-1] objectForKey:@"posy"] integerValue];
        for (int k=0; k<json_size; k=k+2) {
            int i=k;
            
            BFLog(@"posx:%@",[dict[0] objectForKey:@"posx"]);
            int event_no =(int)[[dict[i] objectForKey:@"posy"] integerValue];
            float posx1=[[dict[i] objectForKey:@"posx"] floatValue];
            float posx2=[[dict[i+1] objectForKey:@"posx"] floatValue];
            float duration=posx2-posx1;
            if(event_no==185){
                driving_total=driving_total+duration;
                duty_hr_prev=duty_hr_prev+duration;
                driving_cons_prev=driving_cons_prev+duration;
                
                
            }
            if (event_no==245) {
                driving_cons_prev=0.0;
                  duty_hr_prev=duty_hr_prev+duration;
            }
            if (event_no==65||event_no==125) {
                driving_cons_prev=0.0;
                break_total_prev=break_total_prev+duration;
            }
            
            
        }//end of for
  //  }//end of first if
    if(cycle_tym_prev!=3470.00){
    if(driving_cons_prev>[self calculatePosx:@"08:00"]){
        NSString *violation = [NSString stringWithFormat:@"Daily 8 Hour consecutive driving limit!!!"];
        NSString *violation1 = [NSString stringWithFormat:@"30 Minutes break required!!!"];
        if (![violationArray containsObject:violation]) {
            [violationArray addObject:violation];
            [violationArray addObject:violation1];

        }
        
        
    }
    }
   
  /*  if (break_total_prev<break_tym_prev) {
        if((duty_window_prev=775.00)){
            
            NSString *violation = [NSString stringWithFormat:@"Daily 8 Hour off duty required!!!"];
            if (![violationArray containsObject:violation]) {
                [violationArray addObject:violation];}
        }else{
            
            BFLog(@"Maximum 8 hour driving is allowed");
            NSString *violation = [NSString stringWithFormat:@"Daily 10 Hour off duty required!!!"];
            if (![violationArray containsObject:violation]) {
                [violationArray addObject:violation];
            }
            
            
        }
        
   
    }*/
    
    
    if (driving_total>driving_hr_prev) {
        
        if (driving_hr_prev==579.00) {
            BFLog(@"11 hour driving rule");
            NSString *violation = [NSString stringWithFormat:@"Daily 11 Hour driving limit!!!"];
            if (![violationArray containsObject:violation]) {
                [violationArray addObject:violation];
            }
            /*  if (![messages containsObject:violation]) {
             NSString *sub=@"Maximum 11 hours driving allowed in 14 hours duty cycle!!!!";
             [submessages addObject:sub];
             
             [messages addObject:violation];
             NSArray *sam=@[date,@"11hour"];
             [violationInfo addObject:sam];
             }*/
        }else{
            BFLog(@"10 hour driving rule");
            NSString *violation = [NSString stringWithFormat:@"Daily 10 Hour driving limit!!!"];
            if (![violationArray containsObject:violation]) {
                [violationArray addObject:violation];
            }
            /*if (![messages containsObject:violation]) {
             NSString *sub=@"Maximum 10 hours driving allowed in 14 hours duty cycle!!!!";
             [submessages addObject:sub];
             
             [messages addObject:violation];
             NSArray *sam=@[date,@"10hour"];
             [violationInfo addObject:sam];
             
             }*/
            
        }
    }
    
    if (duty_hr_prev>duty_window_prev) {
        if (duty_window_prev==[self calculatePosx:@"14:00"]) {
            NSString *violation = [NSString stringWithFormat:@"Daily 14 Hour duty limit!!!"];
            if (![violationArray containsObject:violation]) {
                [violationArray addObject:violation];
            }
          
            
        }else{
            NSString *violation = [NSString stringWithFormat:@"Daily 15 Hour duty limit!!!"];
            if (![violationArray containsObject:violation]) {
                [violationArray addObject:violation];
            }
            
            
        }
        
    }

    
        
        

        
    
   
    
    BFLog(@"Violations:%@",violationArray);
    numofViolations=(int)[violationArray count];
   
}


-(void)viewWillAppear:(BOOL)animated{
    //  if (called == 1) {
    [self viewDidLoad];
    //    called = 0;
    //
    //}
    
    
}

-(NSString*)calculateTime:(float)posx{
    
    int end=(posx-40)/12.25;
    int x1=end/4;
    int x2=end%4;
    NSString *decimalpart,*time;
    if (x2*15==0) {
        decimalpart=@"00";
    }else{
        
        decimalpart=[NSString stringWithFormat:@"%d",x2*15];
    }
    
    if (x1<10) {
        time=[NSString stringWithFormat:@"0%d:%@",x1,decimalpart];
    }else{
        
        time=[NSString stringWithFormat:@"%d:%@",x1,decimalpart];
    }
    
    return time;
    
}
-(float)calculatePosx:(NSString *)time{
    NSArray *parts=[time componentsSeparatedByString:@":"];
    
    BFLog(@"%d:",(int)4*(int)parts[0]);
    float x1=4*[parts[0] integerValue]+([parts[1] integerValue]/15);
    float posx=x1*12.25+40;
    
    return posx;
    
}

-(void)getData:(NSString *)date{
    [dataarray2 removeAllObjects];
    current_date=date;
//    NSLog(@"Current date:%@",current_date);
    /* raghee
     NSString *query1 = [NSString stringWithFormat:@"select * from TempTable where date='%@'",date];
    
    // Get the results.
    
    NSArray *datafortheDay = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query1]];
    BFLog(@"Result:%@",datafortheDay);
    if ([datafortheDay count]!=0) {
        NSString *dbresult = datafortheDay[[datafortheDay count]-1][2];
        
        
        
        BFLog(@"string:%@",dbresult);
        
        
        //BFLog(@"timezone:%lu",(unsigned long)[dbresult count]);
        NSData *data2= [dbresult dataUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *json_log1 = [NSJSONSerialization JSONObjectWithData:data2 options:0 error:nil];
        BFLog(@"Dictionary of db data:%@",json_log1);
        if ([json_log1 count]==0) {
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@" " withString:@""];
            dbresult = [NSString stringWithFormat:@"[%@]",dbresult];
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@"\";" withString:@";"];
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@"=\"" withString:@"="];
            
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@"location=(" withString:@"\"location\":["];
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@"payload=(" withString:@"\"payload\":["];
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@");" withString:@"],"];
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@";" withString:@"\","];
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@"=" withString:@":\""];
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@"id" withString:@"\"id\""];
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@"value" withString:@"\"value\""];
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@"posx" withString:@"\"posx\""];
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@"posy" withString:@"\"posy\""];
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@"timezone" withString:@"\"timezone\""];
            NSData *data2= [dbresult dataUsingEncoding:NSUTF8StringEncoding];
            json_log1 = [NSJSONSerialization JSONObjectWithData:data2 options:0 error:nil];
            BFLog(@"Dictionary of db data2:%@",json_log1);
            
        }
        
        
        
        NSMutableArray *dict = [json_log1 objectForKey:@"payload"];
         location_prev=[json_log1 objectForKey:@"location"];
        notes_prev=[json_log1 objectForKey:@"notes"];
        
        int json_size = (int)[dict count];
        BFLog(@"inner json count:%d",json_size);
        [dataarray2 removeAllObjects];
        for (int i=0; i<json_size; i++) {
            
            BFLog(@"posx:%@,%d",[dict[0] objectForKey:@"posx"],i);
            float posx=[[dict[i] objectForKey:@"posx"] floatValue];
            float x1 = posx - 40.0;
            int x2 = x1/12.25;
            int x3 = x2/4.0;
            int x4 = x2%4;
            //minnu
            NSString *minutes;
             if (15*x4 <15) {
             minutes=@"00";
             }
             else if(15*x4 >=15 && 15*x4 <30){
             minutes=@"15";
             }else if(15*x4 >=30 && 15*x4 <45){
             minutes=@"30";
             }else if(15*x4 >=45 && 15*x4 <60){
             minutes=@"45";
             }
            //BFLog(@"minutes---------")
            NSString *posx_1 = [NSString stringWithFormat:@"%d:%@",x3,minutes];
            //minnu
            // float time = [posx_1 floatValue];
            int event_no =(int)[[dict[i] objectForKey:@"posy"] integerValue];
            NSString * event;
            if (event_no == 245) {
                event=@"ON";
            }
            else if(event_no == 125){
                event=@"SB";
                
            }else if (event_no == 185){
                event = @"D";
            }else if (event_no == 65){
                event = @"OFF";
            }
            NSArray *final_data = [[NSArray alloc] initWithObjects:posx_1,event, nil];
            [dataarray2 addObject:final_data];
            
        }*/
    DBManager *objDB = [[DBManager alloc] initWithDatabaseFilename:@"hosDB.sql"];
    NSDictionary *token;
    ;
    NSMutableDictionary *whereToken;
    NSArray *userInfoArray=[[NSArray alloc]initWithArray: [objDB getUserId]];
    if([userInfoArray count]!=0){
        int userId = [userInfoArray[0][0] intValue];
        token=@{@"columnName":@"id",@"entry":[NSString stringWithFormat:@"%d",userId]};
        ;
        whereToken=[[NSMutableDictionary alloc] initWithDictionary:token];
        
        //[objDB getLogEditData:whereToken];
        token=@{@"userid":[NSString stringWithFormat:@"%d",userId],@"date":[NSString stringWithFormat:@"%@",date]};
            ;
            whereToken=[[NSMutableDictionary alloc] initWithDictionary:token];
            NSArray* dataFromDB=[[NSArray alloc]initWithArray:[objDB getLogEditData:whereToken]];
            statusString=@"";
                for(int j=0;j<[dataFromDB count];j++){
                    if([dataFromDB[j][1] isEqualToString:@""])
                        statusString= [statusString stringByAppendingString:[NSString stringWithFormat:@"1"]];
                    else
                    statusString= [statusString stringByAppendingString:[NSString stringWithFormat:@"%@",[[dataFromDB objectAtIndex:j] objectAtIndex:1]]];
                }
        
                NSString* charStr=[@"" stringByPaddingToLength:96 withString:@"1" startingAtIndex:0];
                if([dataFromDB count]==0)
                    statusString=charStr;
                else
                {
                    if([statusString isEqualToString:@""])
                        statusString=charStr;
                    
                }
        
        commonDataHandler *common=[commonDataHandler new];
        statusString=[common modifyData:statusString date:date];
        graphDataOperations *obj=[graphDataOperations new];
        [dataarray2 removeAllObjects];
        NSMutableArray *dict=[obj generateGraphPoints:statusString];
            for (int i=0; i<[dict count]; i++) {
                
//                NSLog(@"posx:%@,%d",[dict[0] objectForKey:@"posx"],i);
                float posx=[[dict[i] objectForKey:@"posx"] floatValue];
                float x1 = posx - 40.0;
                int x2 = x1/12.25;
                int x3 = x2/4.0;
                int x4 = x2%4;
                
                NSString *minutes;
                if (15*x4 <15) {
                    minutes=@"00";
                }
                else if(15*x4 >=15 && 15*x4 <30){
                    minutes=@"15";
                }else if(15*x4 >=30 && 15*x4 <45){
                    minutes=@"30";
                }else if(15*x4 >=45 && 15*x4 <60){
                    minutes=@"45";
                }
                NSString *posx_1 = [NSString stringWithFormat:@"%d:%@",x3,minutes];
                
                // float time = [posx_1 floatValue];
                int event_no =(int)[[dict[i] objectForKey:@"posy"] integerValue];
                NSString * event;
                if (event_no == 245) {
                    event=@"ON";
                }
                else if(event_no == 125){
                    event=@"SB";
                    
                }else if (event_no == 185){
                    event = @"D";
                }else if (event_no == 65){
                    event = @"OFF";
                }
                NSArray *final_data = [[NSArray alloc] initWithObjects:posx_1,event, nil];
                [dataarray2 addObject:final_data];
            }
            
        if (date==selected_previous) {
            
            
            float last_posx=[[dict[[dict count]-1] objectForKey:@"posx"] floatValue];
            NSString *lastStatus = dataarray2[[dataarray2 count]-1][1];
            if (last_posx!=1216) {
                if ([dict count]%2==0) {
                    
                
                [dataarray2 removeLastObject];
                NSArray *sam = @[@"24:00",lastStatus];
                [dataarray2 addObject:sam];
                }else{
                    
                    NSArray *sam = @[@"24:00",lastStatus];
                    [dataarray2 addObject:sam];
                }
                [self saveToDB:dataarray2 status:lastStatus date:dateGlobal];
            }
        }
        
    }else{
        
        NSArray *sam = @[@"00:00",@"OFF"];
        [dataarray2 addObject:sam];
        NSArray *sam1 = @[@"24:00",@"OFF"];
        [dataarray2 addObject:sam1];
        //minnu
        //[dataarray2 removeAllObjects];
        //dataarray2= [dataDictArray[selected_index] mutableCopy];
        //dataarray4=[dataarray2 mutableCopy];
        //dataarray5=[dataarray5 mutableCopy];
        //minnu
        [self saveToDB:dataarray2 status:@"OFF" date:dateGlobal];
        
    }
    
    
    BFLog(@"Final array for loading in previoud day:%@",dataarray2);
    
    
//     NSArray *subViews = [self.view subviews];
//     for(UIView *view in subViews){
//     if ([view isKindOfClass:[graphPerDay class]]) {
//     [view removeFromSuperview];
     flag4 = flag4 +1;
     
//     graphPerDay *f = [[graphPerDay alloc] initWithFrame:(CGRectMake(0, 80, 600,100))];
     y_origin1=0;
     [graph setOpaque:NO];
     
//     [self.view addSubview:f];
     [graph setNeedsDisplay];
    // }
   //  }
     
 
   
    /*  for (int i=0; i<[dataarray2 count]; i=i+2) {
     int k=i;
     //if (![dataarray2[i][1] isEqualToString:dataarray2[k][1]]) {
     
     [starting_time addObject:dataarray2[k][0]];
     //}
     }
     for(int j=0;j<[dataarray2 count];j++){
     
     [statusarray addObject:dataarray2[j][1]];
     
     }
     statusarray1 = [statusarray valueForKeyPath:@"@distinctUnionOfObjects.self"];
     BFLog(@"statusarray:%@",statusarray1);
     BFLog(@"starttime:%@",starting_time);*/
    //SLog(@"duration:%@",duration);
    //  [_perdayList reloadData];
    
    int length=(int)[dataarray2 count];
    for (int k=0; k<length; k++) {
        if ((k%2==0)&&(k!=length-1)) {
            
            NSString *event = dataarray2[k][1];
            NSString *start = dataarray2[k][0];
            NSString *end = dataarray2[k+1][0];
            float start_float = [self getPosx:start];
            float end_float = [self getPosx:end];
            float diff = end_float-start_float;
            NSString *duration = [self getDuration:diff];
            NSArray *details = @[start,event,duration,end];
            [statusarray addObject:details];
            
            
        }
        
        
        
    }
    
    BFLog(@"Status Array new:%@",statusarray);

}
-(void)getDeviceData:(NSString*)date1{
    //getting authCode
    NSString *query0 = [NSString stringWithFormat:@"select * from authDetails;"];
    NSArray *authData = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query0]];
    NSString *authCode = @"";
    if([authData count]!=0){
        authCode = authData[0][1];
        
    }
    //---
    NSString *path = [[NSUserDefaults standardUserDefaults] valueForKey:@"path"];
    NSString *urlString1 = [NSString stringWithFormat:@"%@/hosapp/getDeviceData_app.php?master=%@&user=%@&date=%@&authCode=%@",path,masterUser,user_menu,date1,authCode];
    NSURL *url = [NSURL URLWithString:urlString1];
    NSData *data = [NSData dataWithContentsOfURL:url];
    
    if (data) {
        NSURLSession *session1 = [NSURLSession sharedSession];
        NSURLSessionDataTask *dataTask1 = [session1 dataTaskWithURL:[NSURL URLWithString:urlString1] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            if(data!=nil){
            deviceData1 = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            
            BFLog(@"Data From File:%@",deviceData);
            }
            
        }];
        [dataTask1 resume];
    }
    
    
}
-(float)getPosx:(NSString *)time1{
    NSString *x12=time1;
    NSArray *time = [x12 componentsSeparatedByString:@":"];
    int x4 =(int) [time[1] integerValue]/15;
    int x2 =(int) [time[0] integerValue]*4+x4;
    float x1 = 12.25*x2;
    float posx = x1+40.0;
    return posx;
    
}
-(void)saveToServer:(NSMutableDictionary *)driver_data date1:(NSString*)dateActual{
    
//    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
//    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration];
//    //getting authCode
//    NSString *query0 = [NSString stringWithFormat:@"select * from authDetails;"];
//    NSArray *authData = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query0]];
//    NSString *authCode = @"";
//    if([authData count]!=0){
//        authCode = authData[0][1];
//        
//    }
//    //---------------------------
//   NSString *urlString = [NSString stringWithFormat:@"http://www.gpstracking-server1.com/gpstracking/hosapp/saveHosGraphData.php?masterUser=%@&user=%@&date=%@&approve=0&authCode=%@",master_menu,user_menu,dateActual,authCode];
//    // NSString *urlString = [NSString stringWithFormat:@"http://23.239.28.100/mydealership/saveHosGraphData_app.php?masterUser=%@&user=%@&date=%@&approve=0",master_menu,user_menu,current_date];
//    NSURL *url = [NSURL URLWithString:urlString];
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
//    [request setHTTPMethod:@"HEAD"];
//    NSURLResponse *response;
//    NSError *error;
//    NSData *mydata = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
//    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
//    if(httpResponse!=nil){
//        if ([httpResponse statusCode] ==200 ) {
//            BFLog(@"Device connected to the internet");
//            NSURL *url = [NSURL URLWithString:urlString];
//            
//            //NSError *error;
//            
//            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
//                                                                   cachePolicy:NSURLRequestUseProtocolCachePolicy
//                                                               timeoutInterval:60.0];
//            
//            [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
//            [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
//            
//            [request setHTTPMethod:@"POST"];
//            /* NSDictionary *mapData = [[NSDictionary alloc] initWithObjectsAndKeys: @"TEST IOS", @"name",
//             @"IOS TYPE", @"typemap",
//             nil];*/
//            
//            //NSMutableDictionary *currentStatus=[[NSMutableDictionary alloc]init];
//            //[currentStatus setValue:status forKey:@"Status"];*/
//            /*  NSMutableArray *inner = [[NSMutableArray alloc] initWithArray:@[@"00:00",@"OFF"]];
//             NSMutableDictionary *data_dict = [[NSMutableDictionary alloc] init];
//             [data_dict setObject:inner forKey:@"payload"];
//             NSError *error;*/
//            NSData *postData = [NSJSONSerialization dataWithJSONObject:driver_data options:0 error:&error];
//            [request setHTTPBody:postData];
//            
//            NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
//                
//                NSDictionary *responseData = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
//                
//            }];
//            
//            [postDataTask resume];
//        }else if([httpResponse statusCode]==403){
//            if(autoLoginCount<=1){
//                insertDutyCycle *obj = [insertDutyCycle new];
//                BOOL success =  [obj autoLogin];
//                
//                if(success){
//                    [self saveToServer:driver_data date1:dateActual];
//                    
//                }else{
//                    KeychainItemWrapper *credentials = [[KeychainItemWrapper alloc] initWithIdentifier:@"credentials" accessGroup:nil];
//                    [credentials resetKeychainItem];
//                    UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Session Expired!!"
//                                                                     message:@"Please Login Again"
//                                                                    delegate:self
//                                                           cancelButtonTitle:@"OK"
//                                                           otherButtonTitles:nil, nil];
//                    alert1.tag = 4;
//                    alert1.delegate = self;
//                    
//                    [alert1 show];
//                    menuScreenViewController *obj = [menuScreenViewController new];
//                    [obj syncServerLater:driver_data date:dateActual];
//                }
//            }else{
//                KeychainItemWrapper *credentials = [[KeychainItemWrapper alloc] initWithIdentifier:@"credentials" accessGroup:nil];
//                [credentials resetKeychainItem];
//                UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Session Expired!!"
//                                                                 message:@"Please Login Again"
//                                                                delegate:self
//                                                       cancelButtonTitle:@"OK"
//                                                       otherButtonTitles:nil, nil];
//                alert1.tag = 2;
//                alert1.delegate = self;
//                
//                [alert1 show];
//                menuScreenViewController *obj = [menuScreenViewController new];
//                [obj syncServerLater:driver_data date:dateActual];
//                
//                
//            }
//            
//        }else if([httpResponse statusCode]==503){
//            BFLog(@"Device not connected to the internet");
//            UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Service Unavailable"
//                                                             message:@"No connectivity with server at this time,it will sync automatically"
//                                                            delegate:self
//                                                   cancelButtonTitle:@"OK"
//                                                   otherButtonTitles:nil, nil];
//            
//            [alert1 show];
//            menuScreenViewController *obj = [menuScreenViewController new];
//            [obj syncServerLater:driver_data date:dateActual];
//            
//        }
//    }
//    
}



/*-(void)saveToServer:(NSData *)data date1:(NSString*)dateActual{
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration];
    //getting authCode
    NSString *query0 = [NSString stringWithFormat:@"select * from authDetails;"];
    NSArray *authData = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query0]];
    NSString *authCode = @"";
    if([authData count]!=0){
        authCode = authData[0][1];
        
    }
    //---------------------------
    NSString *urlString = [NSString stringWithFormat:@"http://www.gpstracking-server1.com/gpstracking/hosapp1/saveHosGraphData.php?masterUser=%@&user=%@&date=%@&approve=0&authCode=%@",master_menu,user_menu,dateActual,authCode];
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"HEAD"];
    NSURLResponse *response;
    NSError *error;
    NSData *mydata = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
    if(httpResponse!=nil){
        if ([httpResponse statusCode] ==200 ) {
        BFLog(@"Device connected to the internet");
        NSURL *url = [NSURL URLWithString:urlString];
        
        //NSError *error;
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:60.0];
        
        [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
        
        [request setHTTPMethod:@"POST"];
        /* NSDictionary *mapData = [[NSDictionary alloc] initWithObjectsAndKeys: @"TEST IOS", @"name",
         @"IOS TYPE", @"typemap",
         nil];*/
        
        /*  NSMutableDictionary *currentStatus=[[NSMutableDictionary alloc]init];
         [currentStatus setValue:status forKey:@"Status"];
         
         
        //  NSData *postData = [NSJSONSerialization dataWithJSONObject:data options:0 error:&error];
        [request setHTTPBody:data];
        
        
        NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            
            
            
        }];
        
        [postDataTask resume];
    }
    else if([httpResponse statusCode]==503){
        BFLog(@"Device not connected to the internet");
        UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Service Unavailable"
                                                         message:@"No connectivity with server at this time,it will sync automatically"
                                                        delegate:self
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles:nil, nil];
        
        [alert1 show];
        
    }
    
    }
}*/

-(void)saveToDB:(NSMutableArray *)array status:(NSString *)status_current date:(NSString*)date1{
    
    
    
    
    int id1;
    if ([array count]!=0) {
        id1 =(int) [array count]-1;
    }else{
        
        id1=0;
    }
    
    // if ([location_array count]==0) {
    
    //  [location_array addObject:dict1];
    //}else{
    
    
    
    //}
    NSMutableArray *outer_array = [[NSMutableArray alloc] init];
    NSMutableDictionary *data_dict = [[NSMutableDictionary alloc] init];
    
    for (int i=0; i<[array count]; i++) {
        
        NSString *x12=array[i][0];
        NSArray *time = [x12 componentsSeparatedByString:@":"];
        int x4 =(int) [time[1] integerValue]/15;
        int x2 =(int) [time[0] integerValue]*4+x4;
        float x1 = 12.25*x2;
        float posx = x1+40.0;
        int posy;
        BFLog(@"posx:%.2f",posx);
        if ([array[i][1] isEqualToString:@"OFF"]) {
            posy = 65;
        }else if([array[i][1] isEqualToString:@"ON"]){
            posy = 245;
        }else if([array[i][1] isEqualToString:@"D"]){
            posy=185;
        }else {
            posy=125;
        }
        
        NSMutableDictionary *cont1 = [[NSMutableDictionary alloc] init];
        [cont1 setValue:@(i) forKey:@"id"];
        [cont1 setValue:@(posx) forKey:@"posx"];
        [cont1 setValue:@(posy) forKey:@"posy"];
        
        [outer_array addObject:cont1];
        
        
        
    }
    
    [data_dict setObject:outer_array forKey:@"payload"];
    if (location_prev!=NULL && location_prev!=(id)[NSNull null]  && location_prev!=nil && [location_prev count]!=0) {
        
        [data_dict setObject:location_prev forKey:@"location"];

        
    }
     if (notes_prev!=NULL && notes_prev!=(id)[NSNull null]  && notes_prev!=nil && [notes_prev count]!=0) {
         [data_dict setObject:notes_prev forKey:@"notes"];
     }
    BFLog(@"dictionary:%@",data_dict);
    NSData *data = [NSJSONSerialization dataWithJSONObject:data_dict options:NSJSONWritingPrettyPrinted error:nil];
   
    NSString *jsonStr = [[NSString alloc] initWithData:data
                                              encoding:NSUTF8StringEncoding];
    BFLog(@"JSON:%@",jsonStr);
    NSString *dateString=date1;
    
   // [self saveToServer:data_dict date1:dateString];
    NSString *query1 = [NSString stringWithFormat:@"select * from TempTable where date='%@'",dateString];
    
    // Get the results.
    
    NSArray *values = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query1]];
    
    if ([values count]!=0) {
        
        
        
        NSString *query_update = [NSString stringWithFormat:@"UPDATE TempTable SET data='%@',last_status='%@' WHERE date='%@'",jsonStr,status_current,dateString];
        
        // Execute the query.
        
        //NSString *query_del = @"delete from TempTable";
        [dbManager executeQuery:query_update];
        
        // If the query was successfully executed then pop the view controller.
        if (dbManager.affectedRows != 0) {
            BFLog(@"Query was executed successfully. Affected rows = %d", dbManager.affectedRows);
            
        }
        else{
            BFLog(@"Could not execute the query");
        }
    }else{
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat=@"MM/dd/yyyy";
        [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
        NSDate *dateData=[formatter dateFromString:date1];
        NSTimeInterval interval=[dateData timeIntervalSince1970]*1000;
        BFLog(@"Timeinterval:%f",interval);
        
        
        NSString *query_insert1 = [NSString stringWithFormat:@"insert into TempTable values(null, '%@', '%@', 'OFF' ,'%d','%d','%f')", dateString,jsonStr,0,1,interval];
        
               
        //NSString *query_del = @"delete from TempTable";
        [dbManager executeQuery:query_insert1];
        BFLog(@"%d",dbManager.affectedRows );
        // If the query was successfully executed then pop the view controller.
        if (dbManager.affectedRows != 0) {
            //BFLog(@"Query was inserted the data successfully. Affected rows = %d", dbManager.affectedRows);
            UIAlertController *alert1 = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Modification Saved" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *ok1= [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                [alert1 dismissViewControllerAnimated:YES completion:nil];
            }];
            
            
            [alert1 addAction:ok1];
            [self presentViewController:alert1 animated:YES completion:nil];
            
            
        }
        else{
            BFLog(@"Could not execute the query");
        }
        
        

        
    }
    
    
}



-(NSString *)getDuration:(float)difference{
    //minnu
    float x1;
    // if(difference>40.0){
    x1 = difference ;//- 40.0;
    //}
    //  else
    //  x1=40.0-difference;
    //minnu
    int x2 = x1/12.25;
    int x3 = x2/4.0;//+1;
    int x4 = x2%4;
    
    NSString *minutes;
    if (15*x4 <15) {
        minutes=@"00";
    }
    else if(15*x4 >=15 && 15*x4 <30){
        minutes=@"15";
    }else if(15*x4 >=30 && 15*x4 <45){
        minutes=@"30";
    }else if(15*x4 >=45 && 15*x4 <60){
        minutes=@"45";
    }
    NSString *posx_1 = [NSString stringWithFormat:@"%d:%@hr",x3,minutes];
    return posx_1;
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    NSInteger sectionCount=1;
    if([violationArray count]==0){
        
        sectionCount=1;
    }else{
        
        sectionCount=2;
        
    }
    return sectionCount;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    int rows;
    if([violationArray count]==0){
        
        rows =(int) [statusarray count]+1;

        
    }else{
         if (section==0) {
             
             rows=(int)[violationArray count];
         }else{
             
              rows =(int) [statusarray count]+1;
         }
        
        
        
    }

    
    
    
    return rows;
}
-(void)clearcell:(UITableViewCell *)cell{
    
    for (UIView * view in[cell.contentView subviews]){
        [view removeFromSuperview];
    }
    cell.textLabel.text=nil;
    cell.accessoryView=nil;
    
    
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *cellIdentifier = @"cell";
    //UIImageView *image2 = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"navigation.png"]];
    
   
    
    

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    [self clearcell:cell];
  
    //cell.textLabel.text=@"sample";
    
    //if (sections==2) {
//    cell.textLabel.minimumScaleFactor=8./cell.textLabel.font.pointSize;
//    cell.textLabel.adjustsFontSizeToFitWidth=YES;
//    cell.detailTextLabel.minimumScaleFactor=8./cell.detailTextLabel.font.pointSize;
//    cell.detailTextLabel.adjustsFontSizeToFitWidth=YES;
//    BFLog(@"section number:%ld",(long)indexPath.section);
//     if ([violationArray count]==0) {
//
//         if ([statusarray count]==0) {
//
//             cell.textLabel.text=@"no details";
//             cell.detailTextLabel.text=@"";
//         }else{
//
//             if (indexPath.row%2==0) {
//                 cell.backgroundColor=[UIColor colorWithRed:0.78 green:0.91 blue:0.22 alpha:1.0];
//             }else{
//
//                 cell.backgroundColor=[UIColor colorWithRed:0.60 green:0.83 blue:0.13 alpha:1.0];
//             }
//             if(indexPath.row==0){
//                 CGFloat labelwidth = [tableView bounds].size.width/7;
//                 UILabel *serialNumber = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, labelwidth, cell.contentView.bounds.size.height)];
//                 [serialNumber setText:@"Sr"];
//                 serialNumber.textAlignment = NSTextAlignmentCenter;
//                 [cell.contentView addSubview:serialNumber];
//
//                 UILabel *status = [[UILabel alloc] initWithFrame:CGRectMake(serialNumber.frame.origin.x+labelwidth, 0, labelwidth, cell.contentView.bounds.size.height)];
//                 [status setText:@"Status"];
//                 status.textAlignment = NSTextAlignmentCenter;
//                 status.adjustsFontSizeToFitWidth=YES;
//
//                 [cell.contentView addSubview:status];
//
//                 UILabel *start = [[UILabel alloc] initWithFrame:CGRectMake(status.frame.origin.x+labelwidth, 0, labelwidth, cell.contentView.bounds.size.height)];
//                 [start setText:@"Start"];
//                 start.textAlignment = NSTextAlignmentCenter;
//                 start.adjustsFontSizeToFitWidth=YES;
//                 [cell.contentView addSubview:start];
//
//                 UILabel *end = [[UILabel alloc] initWithFrame:CGRectMake(start.frame.origin.x+labelwidth, 0, labelwidth, cell.contentView.bounds.size.height)];
//                 [end setText:@"End"];
//                 end.adjustsFontSizeToFitWidth=YES;
//                 end.textAlignment = NSTextAlignmentCenter;
//                 [cell.contentView addSubview:end];
//
//                 UILabel *notes = [[UILabel alloc] initWithFrame:CGRectMake(end.frame.origin.x+labelwidth, 0, labelwidth, cell.contentView.bounds.size.height)];
//                 [notes setText:@"Notes"];
//                 notes.textAlignment = NSTextAlignmentCenter;
//                 notes.adjustsFontSizeToFitWidth=YES;
//                 notes.numberOfLines=2;
//                 [cell.contentView addSubview:notes];
//
//                 UILabel *type = [[UILabel alloc] initWithFrame:CGRectMake(notes.frame.origin.x+labelwidth, 0, labelwidth, cell.contentView.bounds.size.height)];
//                 [type setText:@"Oirgin"];
//                 type.textAlignment = NSTextAlignmentCenter;
//                 //CALayer *leftBorder = [CALayer layer];
//                 //leftBorder.frame = CGRectMake(0.0f, 0.0f, 1.0f, type.frame.size.height);
//                 //leftBorder.backgroundColor = [[UIColor blackColor] CGColor];
//                 //[type.layer addSublayer:leftBorder];
//                 type.adjustsFontSizeToFitWidth=YES;
//                 [cell.contentView addSubview:type];
//
//
//                 UILabel *edit = [[UILabel alloc] initWithFrame:CGRectMake(type.frame.origin.x+labelwidth, 0, labelwidth, cell.contentView.bounds.size.height)];
//                 [edit setText:@"Edit"];
//                 edit.adjustsFontSizeToFitWidth=YES;
//                 edit.textAlignment=NSTextAlignmentCenter;
//                 [cell.contentView addSubview:edit];
//
//
//             }
//             for (int i=0; i<[statusarray count]; i++) {
//                 CGFloat labelwidth = [tableView bounds].size.width/7;
//                 UILabel *serialNumber = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, labelwidth, cell.contentView.bounds.size.height)];
//                 if (indexPath.row==i+1)
//                 {
//                     [serialNumber setText:[NSString stringWithFormat:@"%ld",(long)indexPath.row]];
//                     serialNumber.textAlignment = NSTextAlignmentCenter;
//                     serialNumber.adjustsFontSizeToFitWidth=YES;
//                     [cell.contentView addSubview:serialNumber];
//
//                     UILabel *status = [[UILabel alloc] initWithFrame:CGRectMake(serialNumber.frame.origin.x+labelwidth, 0, labelwidth, cell.contentView.bounds.size.height)];
//
//                     status.textAlignment = NSTextAlignmentCenter;
//                     status.adjustsFontSizeToFitWidth=YES;
//                     [cell.contentView addSubview:status];
//
//                     UILabel *start = [[UILabel alloc] initWithFrame:CGRectMake(status.frame.origin.x+labelwidth, 0, labelwidth, cell.contentView.bounds.size.height)];
//                     start.adjustsFontSizeToFitWidth=YES;
//                     start.textAlignment = NSTextAlignmentCenter;
//                     [cell.contentView addSubview:start];
//
//                     UILabel *end = [[UILabel alloc] initWithFrame:CGRectMake(start.frame.origin.x+labelwidth, 0, labelwidth, cell.contentView.bounds.size.height)];
//                     end.adjustsFontSizeToFitWidth=YES;
//                     end.textAlignment = NSTextAlignmentCenter;
//                     [cell.contentView addSubview:end];
//
//                     UILabel *notesLabel = [[UILabel alloc] initWithFrame:CGRectMake(end.frame.origin.x+labelwidth, 0, labelwidth, cell.contentView.bounds.size.height)];
//                     notesLabel.lineBreakMode = NSLineBreakByWordWrapping;
//                     notesLabel.adjustsFontSizeToFitWidth=YES;
//                     notesLabel.numberOfLines = 2;
//                     //notesLabel.textAlignment = NSTextAlignmentCenter;
//                     [cell.contentView addSubview:notesLabel];
//
//                     UILabel *type = [[UILabel alloc] initWithFrame:CGRectMake(notesLabel.frame.origin.x+labelwidth, 0, labelwidth, cell.contentView.bounds.size.height)];
//                     type.adjustsFontSizeToFitWidth=YES;
//                     type.textAlignment = NSTextAlignmentCenter;
//                     //CALayer *leftBorder = [CALayer layer];
//                     //leftBorder.frame = CGRectMake(0.0f, 0.0f, 1.0f, type.frame.size.height);
//                     //leftBorder.backgroundColor = [[UIColor blackColor] CGColor];
//                     //[type.layer addSublayer:leftBorder];
//                     [cell.contentView addSubview:type];
//                     UIView *edit = [[UIView alloc] initWithFrame:CGRectMake(type.frame.origin.x+labelwidth, 0, labelwidth, cell.contentView.frame.size.height)];
//                       [cell.contentView addSubview:edit];
//                     if ([statusarray[i][1] isEqualToString:@"D"]) {
//                         [status setText:@"D"];
//                        // UIView *right = [[UIView alloc]initWithFrame:CGRectMake(cell.contentView.bounds.size.width-50, 0, 50, 20)];
//                         CGFloat editViewWidth = edit.frame.size.width/2;
//                         CGFloat editViewHeight = edit.frame.size.height/2;
//
//                         UIImageView *image3;
//                         if(editViewWidth>20){
//                             image3 = [[UIImageView alloc]initWithFrame:CGRectMake(0, (editViewHeight-20)/2+15, 20, 20)];
//                         }else{
//                            image3 = [[UIImageView alloc]initWithFrame:CGRectMake(0, (editViewHeight-20)/2+15, editViewWidth, 20)];
//
//
//                         }
//                         [image3 setImage:[UIImage imageNamed:@"editicon1.png"]];
//                         image3.tag = indexPath.row;
//                         image3.contentMode=UIViewContentModeScaleAspectFit;
//                         UITapGestureRecognizer *showEdit1 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(showEdit:)];
//
//
//                         showEdit1.numberOfTapsRequired=1;
//                         [image3 setUserInteractionEnabled:YES];
//                         [image3 addGestureRecognizer:showEdit1];
//                         [edit addSubview:image3];
//
//                         UIImageView *image4 = [[UIImageView alloc]initWithFrame:CGRectMake(image3.frame.origin.x+editViewWidth, 15, 20, 20)];
//
//                         [image4 setImage:[UIImage imageNamed:@"remove.png"]];
//                         image4.contentMode=UIViewContentModeScaleAspectFit;
//                         [image4 setTag:indexPath.row];
//                         UITapGestureRecognizer *removeEvent = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clearEvent:)];
//                         [image4 setUserInteractionEnabled:YES];
//                         [image4 addGestureRecognizer:removeEvent];
//
//                         [edit addSubview:image4];
//                           //cell.accessoryView=right;
//                         //cell.imageView.image=[UIImage imageNamed:@"images/driving.png"];
//
//                         //NSString *textcell = [NSString stringWithFormat:@"Driving From %@ | To %@ | Duration:%@",statusarray[i][0],statusarray[i][3],statusarray[i][2]];
//                        // cell.textLabel.text=textcell;cell.textLabel.font=[UIFont systemFontOfSize:12.0];
//                         [start setText:[NSString stringWithFormat:@"%@",statusarray[i][0]]];
//                         [end setText:[NSString stringWithFormat:@"%@",statusarray[i][3]]];
//                         [type setText:[NSString stringWithFormat:@"%d",1]];
//                         NSMutableString *notes ;
//                        /* if (location_prev!=nil && location_prev!=(id)[NSNull null] && location_prev!=NULL && [location_prev count]!=0) {
//
//
//                             for (int j=0; j<[location_prev count]; j++) {
//                                  if(![[location_prev[j] valueForKey:@"id"] isKindOfClass:[NSNull class]]){
//                                 if ([[location_prev[j] valueForKey:@"id"] integerValue]==i*2) {
//                                     notes =[NSMutableString stringWithFormat:@"Location:%@",[location_prev[j] valueForKey:@"value"]];
//                                 }
//                                  }
//                             }
//                         }*/
//                         if (notes_prev!=nil && notes_prev!=NULL && notes_prev!=(id)[NSNull null] &&[notes_prev count]!=0) {
//
//
//                             for (int j=0; j<[notes_prev count]; j++) {
//                                 if(![[notes_prev[j] valueForKey:@"id"] isKindOfClass:[NSNull class]]){
//                                 if ([[notes_prev[j] valueForKey:@"id"] integerValue]==i*2) {
//                                     notes =[NSMutableString stringWithFormat:@"%@",notes,[notes_prev[j] valueForKey:@"value"]];
//
//                                 }
//                                 }
//                             }
//                         }
//                         [notesLabel setText:notes];
//                         //cell.detailTextLabel.text=notes;
//
//
//
//
//                     }
//                     else if ([statusarray[i][1] isEqualToString:@"OFF"]) {
//                         UIView *right = [[UIView alloc]initWithFrame:CGRectMake(cell.contentView.bounds.size.width-50, 0, 50, 20)];
//                         UIImageView *image3 = [[UIImageView alloc]initWithFrame:CGRectMake(35, 0, 20, 20)];
//                         [image3 setImage:[UIImage imageNamed:@"editicon1.png"]];
//                         image3.tag = indexPath.row;
//
//                         image3.contentMode=UIViewContentModeScaleAspectFit;
//                         UITapGestureRecognizer *showEdit1 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(showEdit:)];
//                         showEdit1.numberOfTapsRequired=1;
//                         [image3 setUserInteractionEnabled:YES];
//                         [image3 addGestureRecognizer:showEdit1];
//                         [right addSubview:image3];
//                         cell.accessoryView=right;
//
//                        // cell.imageView.image=[UIImage imageNamed:@"icons/offduty.png"];
//                         BFLog(@"before");
//                         //NSString *textcell = [NSString stringWithFormat:@"Off Duty From %@ | To %@ | Duration:%@",statusarray[i][0],statusarray[i][3],statusarray[i][2]];
//                         BFLog(@"After");
//                         //cell.textLabel.text=textcell;cell.textLabel.font=[UIFont systemFontOfSize:12.0];
//                         [status setText:@"OFF"];
//                         [start setText:[NSString stringWithFormat:@"%@",statusarray[i][0]]];
//                         [end setText:[NSString stringWithFormat:@"%@",statusarray[i][3]]];
//                         [type setText:[NSString stringWithFormat:@"%d",2]];
//                         NSMutableString *notes ;
//                        /* if (location_prev!=nil && location_prev!=(id)[NSNull null] && location_prev!=NULL && [location_prev count]!=0) {
//
//
//                             for (int j=0; j<[location_prev count]; j++) {
//                                  if(![[location_prev[j] valueForKey:@"id"] isKindOfClass:[NSNull class]]){
//                                 if ([[location_prev[j] valueForKey:@"id"] integerValue]==i*2) {
//                                     notes =[NSMutableString stringWithFormat:@"Location:%@",[location_prev[j] valueForKey:@"value"]];
//                                 }
//                                  }
//                             }
//                         }*/
//                         if (notes_prev!=nil && notes_prev!=(id)[NSNull null] && notes_prev!=NULL &&[notes_prev count]!=0) {
//
//
//                             for (int j=0; j<[notes_prev count]; j++) {
//                                 if(![[notes_prev[j] valueForKey:@"id"] isKindOfClass:[NSNull class]]){
//                                 if ([[notes_prev[j] valueForKey:@"id"] integerValue]==i*2) {
//                                     notes =[NSMutableString stringWithFormat:@"%@",notes,[notes_prev[j] valueForKey:@"value"]];
//
//                                 }
//                                 }
//                             }
//                         }
//
//                          [notesLabel setText:notes];
//
//                     }
//                     else if ([statusarray[i][1] isEqualToString:@"SB"]) {
//
//
//                         [status setText:@"SB"];
//                         [start setText:[NSString stringWithFormat:@"%@",statusarray[i][0]]];
//                         [end setText:[NSString stringWithFormat:@"%@",statusarray[i][3]]];
//                         [type setText:[NSString stringWithFormat:@"%d",2]];
//                         CGFloat editViewWidth = edit.frame.size.width/2;
//                         CGFloat editViewHeight = edit.frame.size.height/2;
//
//                         UIImageView *image3;
//                         if(editViewWidth>20){
//                             image3 = [[UIImageView alloc]initWithFrame:CGRectMake(0, (editViewHeight-20)/2+15, 20, 20)];
//                         }else{
//                             image3 = [[UIImageView alloc]initWithFrame:CGRectMake(0, (editViewHeight-20)/2+15, editViewWidth, 20)];
//
//
//                         }
//                         [image3 setImage:[UIImage imageNamed:@"editicon1.png"]];
//                         image3.tag = indexPath.row;
//                         image3.contentMode=UIViewContentModeScaleAspectFit;
//                         UITapGestureRecognizer *showEdit1 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(showEdit:)];
//
//
//                         showEdit1.numberOfTapsRequired=1;
//                         [image3 setUserInteractionEnabled:YES];
//                         [image3 addGestureRecognizer:showEdit1];
//                         [edit addSubview:image3];
//
//                         UIImageView *image4 = [[UIImageView alloc]initWithFrame:CGRectMake(image3.frame.origin.x+editViewWidth, 15, 20, 20)];
//
//                         [image4 setImage:[UIImage imageNamed:@"remove.png"]];
//                         image4.contentMode=UIViewContentModeScaleAspectFit;
//                         [image4 setTag:indexPath.row];
//                         UITapGestureRecognizer *removeEvent = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clearEvent:)];
//                         [image4 setUserInteractionEnabled:YES];
//                         [image4 addGestureRecognizer:removeEvent];
//
//                         [edit addSubview:image4];
//                          // cell.accessoryView=right;
//                         //cell.imageView.image=[UIImage imageNamed:@"icons/SB.png"];
//
//                        // NSString *textcell = [NSString stringWithFormat:@"Sleeping From %@ | To %@ | Duration:%@",statusarray[i][0],statusarray[i][3],statusarray[i][2]];                                                cell.textLabel.text=textcell;
//                         //cell.textLabel.font=[UIFont systemFontOfSize:12.0];
//                         NSMutableString *notes ;
//                        /* if (location_prev!=nil && location_prev!=(id)[NSNull null] && location_prev!=NULL && [location_prev count]!=0) {
//
//
//                             for (int j=0; j<[location_prev count]; j++) {
//                                  if(![[location_prev[j] valueForKey:@"id"] isKindOfClass:[NSNull class]]){
//                                 if ([[location_prev[j] valueForKey:@"id"] integerValue]==i*2) {
//                                     notes =[NSMutableString stringWithFormat:@"Location:%@",[location_prev[j] valueForKey:@"value"]];
//                                 }
//                                  }
//                             }
//                         }*/
//                         if (notes_prev!=nil && notes_prev!=(id)[NSNull null] && notes_prev!=NULL &&[notes_prev count]!=0) {
//
//
//                             for (int j=0; j<[notes_prev count]; j++) {
//                                 if(![[notes_prev[j] valueForKey:@"id"] isKindOfClass:[NSNull class]]){
//                                 if ([[notes_prev[j] valueForKey:@"id"] integerValue]==i*2) {
//                                     notes =[NSMutableString stringWithFormat:@"%@",notes,[notes_prev[j] valueForKey:@"value"]];
//
//                                 }
//                                 }
//                             }
//                         }
//
//                         //cell.detailTextLabel.text=notes;
//                         [notesLabel setText:notes];
//
//
//                     }
//                     else if ([statusarray[i][1] isEqualToString:@"ON"]) {
//
//                         [status setText:@"ON"];
//                         [start setText:[NSString stringWithFormat:@"%@",statusarray[i][0]]];
//                         [end setText:[NSString stringWithFormat:@"%@",statusarray[i][3]]];
//                         [type setText:[NSString stringWithFormat:@"%d",2]];
//                         CGFloat editViewWidth = edit.frame.size.width/2;
//                         CGFloat editViewHeight = edit.frame.size.height/2;
//
//                         UIImageView *image3;
//                         if(editViewWidth>20){
//                             image3 = [[UIImageView alloc]initWithFrame:CGRectMake(0, (editViewHeight-20)/2+15, 20, 20)];
//                         }else{
//                             image3 = [[UIImageView alloc]initWithFrame:CGRectMake(0, (editViewHeight-20)/2+15, editViewWidth, 20)];
//
//
//                         }
//                         [image3 setImage:[UIImage imageNamed:@"editicon1.png"]];
//                         image3.tag = indexPath.row;
//                         image3.contentMode=UIViewContentModeScaleAspectFit;
//                         UITapGestureRecognizer *showEdit1 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(showEdit:)];
//
//
//                         showEdit1.numberOfTapsRequired=1;
//                         [image3 setUserInteractionEnabled:YES];
//                         [image3 addGestureRecognizer:showEdit1];
//                         [edit addSubview:image3];
//
//                         UIImageView *image4 = [[UIImageView alloc]initWithFrame:CGRectMake(image3.frame.origin.x+editViewWidth, 15, 20, 20)];
//
//                         [image4 setImage:[UIImage imageNamed:@"remove.png"]];
//                         image4.contentMode=UIViewContentModeScaleAspectFit;
//                         [image4 setTag:indexPath.row];
//                         UITapGestureRecognizer *removeEvent = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clearEvent:)];
//                         [image4 setUserInteractionEnabled:YES];
//                         [image4 addGestureRecognizer:removeEvent];
//
//                         [edit addSubview:image4];
//                         /*UIView *right = [[UIView alloc]initWithFrame:CGRectMake(cell.contentView.bounds.size.width-50, 0, 50, 20)];
//                         UIImageView *image3 = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 20, 20)];
//                         [image3 setImage:[UIImage imageNamed:@"editicon1.png"]];
//                         image3.tag = indexPath.row;
//
//                         image3.contentMode=UIViewContentModeScaleAspectFit;
//                         UITapGestureRecognizer *showEdit1 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(showEdit:)];
//                         showEdit1.numberOfTapsRequired=1;
//                         [image3 setUserInteractionEnabled:YES];
//                         [image3 addGestureRecognizer:showEdit1];
//                         UIImageView *image4 = [[UIImageView alloc]initWithFrame:CGRectMake(image3.bounds.origin.x+image3.bounds.size.width+10, 0, 20, 20)];
//
//                         [image4 setImage:[UIImage imageNamed:@"remove.png"]];
//                         image4.contentMode=UIViewContentModeScaleAspectFit;
//                         [image4 setTag:indexPath.row];
//                         UITapGestureRecognizer *removeEvent = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clearEvent:)];
//                         [image4 setUserInteractionEnabled:YES];
//                         [image4 addGestureRecognizer:removeEvent];
//                         [right addSubview:image3];
//                         [right addSubview:image4];
//                           cell.accessoryView=right;
//                         cell.imageView.image=[UIImage imageNamed:@"icons/onduty.png"];
//
//                         NSString *textcell = [NSString stringWithFormat:@"On Duty From %@ | To %@ | Duration:%@",statusarray[i][0],statusarray[i][3],statusarray[i][2]];
//                         cell.textLabel.text=textcell;*/
//                         NSMutableString *notes ;
//                         /*if (location_prev!=nil && location_prev!=(id)[NSNull null] && location_prev!=NULL && [location_prev count]!=0) {
//
//
//                             for (int j=0; j<[location_prev count]; j++) {
//                                  if(![[location_prev[j] valueForKey:@"id"] isKindOfClass:[NSNull class]]){
//                                 if ([[location_prev[j] valueForKey:@"id"] integerValue]==i*2) {
//                                     notes =[NSMutableString stringWithFormat:@"Location:%@",[location_prev[j] valueForKey:@"value"]];
//                                 }
//                                  }
//                             }
//                         }*/
//                         if (notes_prev!=nil && notes_prev!=(id)[NSNull null] && notes_prev!=NULL &&[notes_prev count]!=0) {
//
//
//                             for (int j=0; j<[notes_prev count]; j++) {
//                                 if(![[notes_prev[j] valueForKey:@"id"] isKindOfClass:[NSNull class]]){
//                                 if ([[notes_prev[j] valueForKey:@"id"] integerValue]==i*2) {
//                                     notes =[NSMutableString stringWithFormat:@"%@",notes,[notes_prev[j] valueForKey:@"value"]];
//
//                                 }
//                                 }
//                             }
//                         }
//
//                         //cell.detailTextLabel.text=notes;
//
//                         [notesLabel setText:notes];
//                         //cell.textLabel.font=[UIFont systemFontOfSize:12.0];
//
//                     }
//
//
//
//
//                 }//end if
//
//
//             }//end of for
//
//
//
//
//
//         }
//
//
//
//     }else{
//
//         if (indexPath.section == 0) {
//
//
//
//                    cell.textLabel.text=violationArray[indexPath.row];
//
//                     cell.textLabel.font=[UIFont fontWithName:@"Courier-Bold" size:16];
//                     cell.textLabel.textColor=[UIColor redColor];
//                     cell.textLabel.alpha=0;
//                     [UIView animateWithDuration:1.0 delay:0.0 options:UIViewAnimationOptionRepeat | UIViewAnimationOptionAutoreverse animations:^{
//                         cell.textLabel.alpha=1;
//                     } completion:nil];
//                     cell.detailTextLabel.text=@"";
//                     cell.imageView.image=[UIImage imageNamed:@"icons/violations.png"];
//
//
//
//
//
//
//
//
//         }else if(indexPath.section ==1){
//
//
//
//             if ([statusarray count]==0) {
//
//                 cell.textLabel.text=@"no details";
//                 cell.detailTextLabel.text=@"";
//             }else{
//
//                 if (indexPath.row%2==0) {
//                     cell.backgroundColor=[UIColor colorWithRed:0.78 green:0.91 blue:0.22 alpha:1.0];
//                 }else{
//
//                     cell.backgroundColor=[UIColor colorWithRed:0.60 green:0.83 blue:0.13 alpha:1.0];
//                 }
//                 if(indexPath.row==0){
//                     CGFloat labelwidth = [tableView bounds].size.width/7;
//                     UILabel *serialNumber = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, labelwidth, cell.contentView.bounds.size.height)];
//                     [serialNumber setText:@"Sr"];
//                     serialNumber.textAlignment = NSTextAlignmentCenter;
//                     [cell.contentView addSubview:serialNumber];
//
//                     UILabel *status = [[UILabel alloc] initWithFrame:CGRectMake(serialNumber.frame.origin.x+labelwidth, 0, labelwidth, cell.contentView.bounds.size.height)];
//                     [status setText:@"Status"];
//                     status.textAlignment = NSTextAlignmentCenter;
//                     status.adjustsFontSizeToFitWidth=YES;
//
//                     [cell.contentView addSubview:status];
//
//                     UILabel *start = [[UILabel alloc] initWithFrame:CGRectMake(status.frame.origin.x+labelwidth, 0, labelwidth, cell.contentView.bounds.size.height)];
//                     [start setText:@"Start"];
//                     start.textAlignment = NSTextAlignmentCenter;
//                     start.adjustsFontSizeToFitWidth=YES;
//                     [cell.contentView addSubview:start];
//
//                     UILabel *end = [[UILabel alloc] initWithFrame:CGRectMake(start.frame.origin.x+labelwidth, 0, labelwidth, cell.contentView.bounds.size.height)];
//                     [end setText:@"End"];
//                     end.adjustsFontSizeToFitWidth=YES;
//                     end.textAlignment = NSTextAlignmentCenter;
//                     [cell.contentView addSubview:end];
//
//                     UILabel *notes = [[UILabel alloc] initWithFrame:CGRectMake(end.frame.origin.x+labelwidth, 0, labelwidth, cell.contentView.bounds.size.height)];
//                     [notes setText:@"Notes"];
//                     notes.textAlignment = NSTextAlignmentCenter;
//                     notes.adjustsFontSizeToFitWidth=YES;
//                     notes.numberOfLines=2;
//                     [cell.contentView addSubview:notes];
//
//                     UILabel *type = [[UILabel alloc] initWithFrame:CGRectMake(notes.frame.origin.x+labelwidth, 0, labelwidth, cell.contentView.bounds.size.height)];
//                     [type setText:@"Oirgin"];
//                     type.textAlignment = NSTextAlignmentCenter;
//                     //CALayer *leftBorder = [CALayer layer];
//                     //leftBorder.frame = CGRectMake(0.0f, 0.0f, 1.0f, type.frame.size.height);
//                     //leftBorder.backgroundColor = [[UIColor blackColor] CGColor];
//                     //[type.layer addSublayer:leftBorder];
//                     type.adjustsFontSizeToFitWidth=YES;
//                     [cell.contentView addSubview:type];
//
//
//                     UILabel *edit = [[UILabel alloc] initWithFrame:CGRectMake(type.frame.origin.x+labelwidth, 0, labelwidth, cell.contentView.bounds.size.height)];
//                     [edit setText:@"Edit"];
//                     edit.adjustsFontSizeToFitWidth=YES;
//                     edit.textAlignment=NSTextAlignmentCenter;
//                     [cell.contentView addSubview:edit];
//
//
//                 }
//
//                 for (int i=0; i<[statusarray count]; i++) {
//                     CGFloat labelwidth = [tableView bounds].size.width/7;
//                     UILabel *serialNumber = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, labelwidth, cell.contentView.bounds.size.height)];
//                     if (indexPath.row==i+1)
//                     {
//                         [serialNumber setText:[NSString stringWithFormat:@"%ld",(long)indexPath.row]];
//                         serialNumber.textAlignment = NSTextAlignmentCenter;
//                         serialNumber.adjustsFontSizeToFitWidth=YES;
//                         [cell.contentView addSubview:serialNumber];
//
//                         UILabel *status = [[UILabel alloc] initWithFrame:CGRectMake(serialNumber.frame.origin.x+labelwidth, 0, labelwidth, cell.contentView.bounds.size.height)];
//
//                         status.textAlignment = NSTextAlignmentCenter;
//                         status.adjustsFontSizeToFitWidth=YES;
//                         [cell.contentView addSubview:status];
//
//                         UILabel *start = [[UILabel alloc] initWithFrame:CGRectMake(status.frame.origin.x+labelwidth, 0, labelwidth, cell.contentView.bounds.size.height)];
//                         start.adjustsFontSizeToFitWidth=YES;
//                         start.textAlignment = NSTextAlignmentCenter;
//                         [cell.contentView addSubview:start];
//
//                         UILabel *end = [[UILabel alloc] initWithFrame:CGRectMake(start.frame.origin.x+labelwidth, 0, labelwidth, cell.contentView.bounds.size.height)];
//                         end.adjustsFontSizeToFitWidth=YES;
//                         end.textAlignment = NSTextAlignmentCenter;
//                         [cell.contentView addSubview:end];
//
//                         UILabel *notesLabel = [[UILabel alloc] initWithFrame:CGRectMake(end.frame.origin.x+labelwidth, 0, labelwidth, cell.contentView.bounds.size.height)];
//                         notesLabel.lineBreakMode = NSLineBreakByWordWrapping;
//                         notesLabel.adjustsFontSizeToFitWidth=YES;
//                         notesLabel.numberOfLines = 2;
//                         //notesLabel.textAlignment = NSTextAlignmentCenter;
//                         [cell.contentView addSubview:notesLabel];
//
//                         UILabel *type = [[UILabel alloc] initWithFrame:CGRectMake(notesLabel.frame.origin.x+labelwidth, 0, labelwidth, cell.contentView.bounds.size.height)];
//                         type.adjustsFontSizeToFitWidth=YES;
//                         type.textAlignment = NSTextAlignmentCenter;
//                         //CALayer *leftBorder = [CALayer layer];
//                         //leftBorder.frame = CGRectMake(0.0f, 0.0f, 1.0f, type.frame.size.height);
//                         //leftBorder.backgroundColor = [[UIColor blackColor] CGColor];
//                         //[type.layer addSublayer:leftBorder];
//                         [cell.contentView addSubview:type];
//                         UIView *edit = [[UIView alloc] initWithFrame:CGRectMake(type.frame.origin.x+labelwidth, 0, labelwidth, cell.contentView.frame.size.height)];
//                         [cell.contentView addSubview:edit];
//                         if ([statusarray[i][1] isEqualToString:@"D"]) {
//                             [status setText:@"D"];
//                             // UIView *right = [[UIView alloc]initWithFrame:CGRectMake(cell.contentView.bounds.size.width-50, 0, 50, 20)];
//                             CGFloat editViewWidth = edit.frame.size.width/2;
//                             CGFloat editViewHeight = edit.frame.size.height/2;
//
//                             UIImageView *image3;
//                             if(editViewWidth>20){
//                                 image3 = [[UIImageView alloc]initWithFrame:CGRectMake(0, (editViewHeight-20)/2+15, 20, 20)];
//                             }else{
//                                 image3 = [[UIImageView alloc]initWithFrame:CGRectMake(0, (editViewHeight-20)/2+15, editViewWidth, 20)];
//
//
//                             }
//                             [image3 setImage:[UIImage imageNamed:@"editicon1.png"]];
//                             image3.tag = indexPath.row;
//                             image3.contentMode=UIViewContentModeScaleAspectFit;
//                             UITapGestureRecognizer *showEdit1 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(showEdit:)];
//
//
//                             showEdit1.numberOfTapsRequired=1;
//                             [image3 setUserInteractionEnabled:YES];
//                             [image3 addGestureRecognizer:showEdit1];
//                             [edit addSubview:image3];
//
//                             UIImageView *image4 = [[UIImageView alloc]initWithFrame:CGRectMake(image3.frame.origin.x+editViewWidth, 15, 20, 20)];
//
//                             [image4 setImage:[UIImage imageNamed:@"remove.png"]];
//                             image4.contentMode=UIViewContentModeScaleAspectFit;
//                             [image4 setTag:indexPath.row];
//                             UITapGestureRecognizer *removeEvent = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clearEvent:)];
//                             [image4 setUserInteractionEnabled:YES];
//                             [image4 addGestureRecognizer:removeEvent];
//
//                             [edit addSubview:image4];
//                             //cell.accessoryView=right;
//                             //cell.imageView.image=[UIImage imageNamed:@"images/driving.png"];
//
//                             //NSString *textcell = [NSString stringWithFormat:@"Driving From %@ | To %@ | Duration:%@",statusarray[i][0],statusarray[i][3],statusarray[i][2]];
//                             // cell.textLabel.text=textcell;cell.textLabel.font=[UIFont systemFontOfSize:12.0];
//                             [start setText:[NSString stringWithFormat:@"%@",statusarray[i][0]]];
//                             [end setText:[NSString stringWithFormat:@"%@",statusarray[i][3]]];
//                             [type setText:[NSString stringWithFormat:@"%d",1]];
//                             NSMutableString *notes ;
//                             /* if (location_prev!=nil && location_prev!=(id)[NSNull null] && location_prev!=NULL && [location_prev count]!=0) {
//
//
//                              for (int j=0; j<[location_prev count]; j++) {
//                              if(![[location_prev[j] valueForKey:@"id"] isKindOfClass:[NSNull class]]){
//                              if ([[location_prev[j] valueForKey:@"id"] integerValue]==i*2) {
//                              notes =[NSMutableString stringWithFormat:@"Location:%@",[location_prev[j] valueForKey:@"value"]];
//                              }
//                              }
//                              }
//                              }*/
//                             if (notes_prev!=nil && notes_prev!=NULL && notes_prev!=(id)[NSNull null] &&[notes_prev count]!=0) {
//
//
//                                 for (int j=0; j<[notes_prev count]; j++) {
//                                     if(![[notes_prev[j] valueForKey:@"id"] isKindOfClass:[NSNull class]]){
//                                         if ([[notes_prev[j] valueForKey:@"id"] integerValue]==i*2) {
//                                             notes =[NSMutableString stringWithFormat:@"%@",notes,[notes_prev[j] valueForKey:@"value"]];
//
//                                         }
//                                     }
//                                 }
//                             }
//                             [notesLabel setText:notes];
//                             //cell.detailTextLabel.text=notes;
//
//
//
//
//                         }
//                         else if ([statusarray[i][1] isEqualToString:@"OFF"]) {
//                             UIView *right = [[UIView alloc]initWithFrame:CGRectMake(cell.contentView.bounds.size.width-50, 0, 50, 20)];
//                             UIImageView *image3 = [[UIImageView alloc]initWithFrame:CGRectMake(35, 0, 20, 20)];
//                             [image3 setImage:[UIImage imageNamed:@"editicon1.png"]];
//                             image3.tag = indexPath.row;
//
//                             image3.contentMode=UIViewContentModeScaleAspectFit;
//                             UITapGestureRecognizer *showEdit1 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(showEdit:)];
//                             showEdit1.numberOfTapsRequired=1;
//                             [image3 setUserInteractionEnabled:YES];
//                             [image3 addGestureRecognizer:showEdit1];
//                             [right addSubview:image3];
//                             cell.accessoryView=right;
//
//                             // cell.imageView.image=[UIImage imageNamed:@"icons/offduty.png"];
//                             BFLog(@"before");
//                             //NSString *textcell = [NSString stringWithFormat:@"Off Duty From %@ | To %@ | Duration:%@",statusarray[i][0],statusarray[i][3],statusarray[i][2]];
//                             BFLog(@"After");
//                             //cell.textLabel.text=textcell;cell.textLabel.font=[UIFont systemFontOfSize:12.0];
//                             [status setText:@"OFF"];
//                             [start setText:[NSString stringWithFormat:@"%@",statusarray[i][0]]];
//                             [end setText:[NSString stringWithFormat:@"%@",statusarray[i][3]]];
//                             [type setText:[NSString stringWithFormat:@"%d",2]];
//                             NSMutableString *notes ;
//                             /* if (location_prev!=nil && location_prev!=(id)[NSNull null] && location_prev!=NULL && [location_prev count]!=0) {
//
//
//                              for (int j=0; j<[location_prev count]; j++) {
//                              if(![[location_prev[j] valueForKey:@"id"] isKindOfClass:[NSNull class]]){
//                              if ([[location_prev[j] valueForKey:@"id"] integerValue]==i*2) {
//                              notes =[NSMutableString stringWithFormat:@"Location:%@",[location_prev[j] valueForKey:@"value"]];
//                              }
//                              }
//                              }
//                              }*/
//                             if (notes_prev!=nil && notes_prev!=(id)[NSNull null] && notes_prev!=NULL &&[notes_prev count]!=0) {
//
//
//                                 for (int j=0; j<[notes_prev count]; j++) {
//                                     if(![[notes_prev[j] valueForKey:@"id"] isKindOfClass:[NSNull class]]){
//                                         if ([[notes_prev[j] valueForKey:@"id"] integerValue]==i*2) {
//                                             notes =[NSMutableString stringWithFormat:@"%@",notes,[notes_prev[j] valueForKey:@"value"]];
//
//                                         }
//                                     }
//                                 }
//                             }
//
//                             [notesLabel setText:notes];
//
//                         }
//                         else if ([statusarray[i][1] isEqualToString:@"SB"]) {
//
//
//                             [status setText:@"SB"];
//                             [start setText:[NSString stringWithFormat:@"%@",statusarray[i][0]]];
//                             [end setText:[NSString stringWithFormat:@"%@",statusarray[i][3]]];
//                             [type setText:[NSString stringWithFormat:@"%d",2]];
//                             CGFloat editViewWidth = edit.frame.size.width/2;
//                             CGFloat editViewHeight = edit.frame.size.height/2;
//
//                             UIImageView *image3;
//                             if(editViewWidth>20){
//                                 image3 = [[UIImageView alloc]initWithFrame:CGRectMake(0, (editViewHeight-20)/2+15, 20, 20)];
//                             }else{
//                                 image3 = [[UIImageView alloc]initWithFrame:CGRectMake(0, (editViewHeight-20)/2+15, editViewWidth, 20)];
//
//
//                             }
//                             [image3 setImage:[UIImage imageNamed:@"editicon1.png"]];
//                             image3.tag = indexPath.row;
//                             image3.contentMode=UIViewContentModeScaleAspectFit;
//                             UITapGestureRecognizer *showEdit1 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(showEdit:)];
//
//
//                             showEdit1.numberOfTapsRequired=1;
//                             [image3 setUserInteractionEnabled:YES];
//                             [image3 addGestureRecognizer:showEdit1];
//                             [edit addSubview:image3];
//
//                             UIImageView *image4 = [[UIImageView alloc]initWithFrame:CGRectMake(image3.frame.origin.x+editViewWidth, 15, 20, 20)];
//
//                             [image4 setImage:[UIImage imageNamed:@"remove.png"]];
//                             image4.contentMode=UIViewContentModeScaleAspectFit;
//                             [image4 setTag:indexPath.row];
//                             UITapGestureRecognizer *removeEvent = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clearEvent:)];
//                             [image4 setUserInteractionEnabled:YES];
//                             [image4 addGestureRecognizer:removeEvent];
//
//                             [edit addSubview:image4];
//                             // cell.accessoryView=right;
//                             //cell.imageView.image=[UIImage imageNamed:@"icons/SB.png"];
//
//                             // NSString *textcell = [NSString stringWithFormat:@"Sleeping From %@ | To %@ | Duration:%@",statusarray[i][0],statusarray[i][3],statusarray[i][2]];                                                cell.textLabel.text=textcell;
//                             //cell.textLabel.font=[UIFont systemFontOfSize:12.0];
//                             NSMutableString *notes ;
//                             /* if (location_prev!=nil && location_prev!=(id)[NSNull null] && location_prev!=NULL && [location_prev count]!=0) {
//
//
//                              for (int j=0; j<[location_prev count]; j++) {
//                              if(![[location_prev[j] valueForKey:@"id"] isKindOfClass:[NSNull class]]){
//                              if ([[location_prev[j] valueForKey:@"id"] integerValue]==i*2) {
//                              notes =[NSMutableString stringWithFormat:@"Location:%@",[location_prev[j] valueForKey:@"value"]];
//                              }
//                              }
//                              }
//                              }*/
//                             if (notes_prev!=nil && notes_prev!=(id)[NSNull null] && notes_prev!=NULL &&[notes_prev count]!=0) {
//
//
//                                 for (int j=0; j<[notes_prev count]; j++) {
//                                     if(![[notes_prev[j] valueForKey:@"id"] isKindOfClass:[NSNull class]]){
//                                         if ([[notes_prev[j] valueForKey:@"id"] integerValue]==i*2) {
//                                             notes =[NSMutableString stringWithFormat:@"%@",notes,[notes_prev[j] valueForKey:@"value"]];
//
//                                         }
//                                     }
//                                 }
//                             }
//
//                             //cell.detailTextLabel.text=notes;
//                             [notesLabel setText:notes];
//
//
//                         }
//                         else if ([statusarray[i][1] isEqualToString:@"ON"]) {
//
//                             [status setText:@"ON"];
//                             [start setText:[NSString stringWithFormat:@"%@",statusarray[i][0]]];
//                             [end setText:[NSString stringWithFormat:@"%@",statusarray[i][3]]];
//                             [type setText:[NSString stringWithFormat:@"%d",2]];
//                             CGFloat editViewWidth = edit.frame.size.width/2;
//                             CGFloat editViewHeight = edit.frame.size.height/2;
//
//                             UIImageView *image3;
//                             if(editViewWidth>20){
//                                 image3 = [[UIImageView alloc]initWithFrame:CGRectMake(0, (editViewHeight-20)/2+15, 20, 20)];
//                             }else{
//                                 image3 = [[UIImageView alloc]initWithFrame:CGRectMake(0, (editViewHeight-20)/2+15, editViewWidth, 20)];
//
//
//                             }
//                             [image3 setImage:[UIImage imageNamed:@"editicon1.png"]];
//                             image3.tag = indexPath.row;
//                             image3.contentMode=UIViewContentModeScaleAspectFit;
//                             UITapGestureRecognizer *showEdit1 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(showEdit:)];
//
//
//                             showEdit1.numberOfTapsRequired=1;
//                             [image3 setUserInteractionEnabled:YES];
//                             [image3 addGestureRecognizer:showEdit1];
//                             [edit addSubview:image3];
//
//                             UIImageView *image4 = [[UIImageView alloc]initWithFrame:CGRectMake(image3.frame.origin.x+editViewWidth, 15, 20, 20)];
//
//                             [image4 setImage:[UIImage imageNamed:@"remove.png"]];
//                             image4.contentMode=UIViewContentModeScaleAspectFit;
//                             [image4 setTag:indexPath.row];
//                             UITapGestureRecognizer *removeEvent = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clearEvent:)];
//                             [image4 setUserInteractionEnabled:YES];
//                             [image4 addGestureRecognizer:removeEvent];
//
//                             [edit addSubview:image4];
//                             /*UIView *right = [[UIView alloc]initWithFrame:CGRectMake(cell.contentView.bounds.size.width-50, 0, 50, 20)];
//                              UIImageView *image3 = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 20, 20)];
//                              [image3 setImage:[UIImage imageNamed:@"editicon1.png"]];
//                              image3.tag = indexPath.row;
//
//                              image3.contentMode=UIViewContentModeScaleAspectFit;
//                              UITapGestureRecognizer *showEdit1 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(showEdit:)];
//                              showEdit1.numberOfTapsRequired=1;
//                              [image3 setUserInteractionEnabled:YES];
//                              [image3 addGestureRecognizer:showEdit1];
//                              UIImageView *image4 = [[UIImageView alloc]initWithFrame:CGRectMake(image3.bounds.origin.x+image3.bounds.size.width+10, 0, 20, 20)];
//
//                              [image4 setImage:[UIImage imageNamed:@"remove.png"]];
//                              image4.contentMode=UIViewContentModeScaleAspectFit;
//                              [image4 setTag:indexPath.row];
//                              UITapGestureRecognizer *removeEvent = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clearEvent:)];
//                              [image4 setUserInteractionEnabled:YES];
//                              [image4 addGestureRecognizer:removeEvent];
//                              [right addSubview:image3];
//                              [right addSubview:image4];
//                              cell.accessoryView=right;
//                              cell.imageView.image=[UIImage imageNamed:@"icons/onduty.png"];
//
//                              NSString *textcell = [NSString stringWithFormat:@"On Duty From %@ | To %@ | Duration:%@",statusarray[i][0],statusarray[i][3],statusarray[i][2]];
//                              cell.textLabel.text=textcell;*/
//                             NSMutableString *notes ;
//                             /*if (location_prev!=nil && location_prev!=(id)[NSNull null] && location_prev!=NULL && [location_prev count]!=0) {
//
//
//                              for (int j=0; j<[location_prev count]; j++) {
//                              if(![[location_prev[j] valueForKey:@"id"] isKindOfClass:[NSNull class]]){
//                              if ([[location_prev[j] valueForKey:@"id"] integerValue]==i*2) {
//                              notes =[NSMutableString stringWithFormat:@"Location:%@",[location_prev[j] valueForKey:@"value"]];
//                              }
//                              }
//                              }
//                              }*/
//                             if (notes_prev!=nil && notes_prev!=(id)[NSNull null] && notes_prev!=NULL &&[notes_prev count]!=0) {
//
//
//                                 for (int j=0; j<[notes_prev count]; j++) {
//                                     if(![[notes_prev[j] valueForKey:@"id"] isKindOfClass:[NSNull class]]){
//                                         if ([[notes_prev[j] valueForKey:@"id"] integerValue]==i*2) {
//                                             notes =[NSMutableString stringWithFormat:@"%@",notes,[notes_prev[j] valueForKey:@"value"]];
//
//                                         }
//                                     }
//                                 }
//                             }
//
//                             //cell.detailTextLabel.text=notes;
//
//                             [notesLabel setText:notes];
//                             //cell.textLabel.font=[UIFont systemFontOfSize:12.0];
//
//                         }
//
//
//
//
//                     }//end if
//
//
//                 }//end of for
//
//
//
//
//
//             }
//
//
//
//
//
//         }
//
//
//
//
//     }
    
    
  ///----------------------------
    
   /* if (indexPath.section == 0) {
       
        if ([violationArray count]==0) {
            CGFloat y = CGRectGetMinY(cell.frame);
           cellHeight=tableY+tableHeight+2*cell.frame.size.height+y;

            cell.textLabel.text=@"Insert Past Duty Status";
            cell.detailTextLabel.text=@"";
            cell.accessoryView=image2;
        }else{
            
            if (indexPath.row==0) {
                
                CGFloat y = CGRectGetMinY(cell.frame);
                cellHeight=tableY+tableHeight+2*cell.frame.size.height+y;

                cell.textLabel.text=@"Insert Past Duty Status";
                cell.detailTextLabel.text=@"";
                cell.accessoryView=image2;
            }else{
                
                cell.textLabel.text=violationArray[indexPath.row-1];
             
                cell.textLabel.font=[UIFont fontWithName:@"Courier-Bold" size:16];
                cell.textLabel.textColor=[UIColor redColor];
                cell.textLabel.alpha=0;
                [UIView animateWithDuration:1.0 delay:0.0 options:UIViewAnimationOptionRepeat | UIViewAnimationOptionAutoreverse animations:^{
                    cell.textLabel.alpha=1;
                } completion:nil];
                cell.detailTextLabel.text=@"";
                cell.imageView.image=[UIImage imageNamed:@"icons/violations.png"];
               
                
                
            }
            
            
            
        }
        
    }
    else if(indexPath.section ==1){
        
        
        if ([statusarray count]==0) {
            
            cell.textLabel.text=@"no details";
            cell.detailTextLabel.text=@"";
        }
        else{
            if (indexPath.row%2==0) {
                cell.backgroundColor=[UIColor colorWithRed:0.78 green:0.91 blue:0.22 alpha:1.0];
            }else{
                
                cell.backgroundColor=[UIColor colorWithRed:0.60 green:0.83 blue:0.13 alpha:1.0];
            }
            UIButton *edit = [UIButton buttonWithType:UIButtonTypeCustom];
            [edit setTitle:@"Edit" forState:UIControlStateNormal];
            edit.frame=CGRectMake(0, 10, 50, 20);
            //[cell addSubview:edit];
            for (int i=0; i<[statusarray count]; i++) {
                if (indexPath.row==i)
                {
                    
                    if ([statusarray[i][1] isEqualToString:@"D"]) {
                        cell.imageView.image=[UIImage imageNamed:@"images/driving.png"];
                        
                        NSString *textcell = [NSString stringWithFormat:@"Driving From %@ | To %@ | Duration:%@",statusarray[i][0],statusarray[i][3],statusarray[i][2]];
                        cell.textLabel.text=textcell;cell.textLabel.font=[UIFont systemFontOfSize:12.0];
                        NSMutableString *notes ;
                        if (location_prev!=nil && location_prev!=(id)[NSNull null] && location_prev!=NULL && [location_prev count]!=0) {
                            
                            
                            for (int j=0; j<[location_prev count]; j++) {
                                if ([[location_prev[j] valueForKey:@"id"] integerValue]==i*2) {
                                   notes =[NSMutableString stringWithFormat:@"Location:%@",[location_prev[j] valueForKey:@"value"]];
                                                                    }
                            }
                        }
                        if (notes_prev!=nil && notes_prev!=NULL && notes_prev!=(id)[NSNull null] &&[notes_prev count]!=0) {
                            
                            
                            for (int j=0; j<[notes_prev count]; j++) {
                                if ([[notes_prev[j] valueForKey:@"id"] integerValue]==i*2) {
                                    notes =[NSMutableString stringWithFormat:@"%@|notes:%@",notes,[notes_prev[j] valueForKey:@"value"]];
                                    
                                }
                            }
                        }

                        cell.detailTextLabel.text=notes;
                        
                        
                        
                        
                    }
                    else if ([statusarray[i][1] isEqualToString:@"OFF"]) {
                        cell.imageView.image=[UIImage imageNamed:@"icons/offduty.png"];
                        BFLog(@"before");
                        NSString *textcell = [NSString stringWithFormat:@"Off Duty From %@ | To %@ | Duration:%@",statusarray[i][0],statusarray[i][3],statusarray[i][2]];
                        BFLog(@"After");
                        cell.textLabel.text=textcell;cell.textLabel.font=[UIFont systemFontOfSize:12.0];
                        NSMutableString *notes ;
                        if (location_prev!=nil && location_prev!=(id)[NSNull null] && location_prev!=NULL && [location_prev count]!=0) {
                            
                            
                            for (int j=0; j<[location_prev count]; j++) {
                                if ([[location_prev[j] valueForKey:@"id"] integerValue]==i*2) {
                                    notes =[NSMutableString stringWithFormat:@"Location:%@",[location_prev[j] valueForKey:@"value"]];
                                }
                            }
                        }
                        if (notes_prev!=nil && notes_prev!=(id)[NSNull null] && notes_prev!=NULL &&[notes_prev count]!=0) {
                            
                            
                            for (int j=0; j<[notes_prev count]; j++) {
                                if ([[notes_prev[j] valueForKey:@"id"] integerValue]==i*2) {
                                    notes =[NSMutableString stringWithFormat:@"%@|notes:%@",notes,[notes_prev[j] valueForKey:@"value"]];
                                    
                                }
                            }
                        }
                        
                        cell.detailTextLabel.text=notes;
                        
                    }
                    else if ([statusarray[i][1] isEqualToString:@"SB"]) {
                        cell.imageView.image=[UIImage imageNamed:@"icons/SB.png"];
                        
                        NSString *textcell = [NSString stringWithFormat:@"Sleeping From %@ | To %@ | Duration:%@",statusarray[i][0],statusarray[i][3],statusarray[i][2]];                                                cell.textLabel.text=textcell;
                        cell.textLabel.font=[UIFont systemFontOfSize:12.0];
                        NSMutableString *notes ;
                        if (location_prev!=nil && location_prev!=(id)[NSNull null] && location_prev!=NULL && [location_prev count]!=0) {
                            
                            
                            for (int j=0; j<[location_prev count]; j++) {
                                if ([[location_prev[j] valueForKey:@"id"] integerValue]==i*2) {
                                    notes =[NSMutableString stringWithFormat:@"Location:%@",[location_prev[j] valueForKey:@"value"]];
                                }
                            }
                        }
                        if (notes_prev!=nil && notes_prev!=(id)[NSNull null] && notes_prev!=NULL &&[notes_prev count]!=0) {
                            
                            
                            for (int j=0; j<[notes_prev count]; j++) {
                                if ([[notes_prev[j] valueForKey:@"id"] integerValue]==i*2) {
                                    notes =[NSMutableString stringWithFormat:@"%@|notes:%@",notes,[notes_prev[j] valueForKey:@"value"]];
                                    
                                }
                            }
                        }
                        
                        cell.detailTextLabel.text=notes;
                        
                        
                    }
                    else if ([statusarray[i][1] isEqualToString:@"ON"]) {
                        cell.imageView.image=[UIImage imageNamed:@"icons/onduty.png"];
                        
                        NSString *textcell = [NSString stringWithFormat:@"On Duty From %@ | To %@ | Duration:%@",statusarray[i][0],statusarray[i][3],statusarray[i][2]];
                        cell.textLabel.text=textcell;
                        NSMutableString *notes ;
                        if (location_prev!=nil && location_prev!=(id)[NSNull null] && location_prev!=NULL && [location_prev count]!=0) {
                            
                            
                            for (int j=0; j<[location_prev count]; j++) {
                                if ([[location_prev[j] valueForKey:@"id"] integerValue]==i*2) {
                                    notes =[NSMutableString stringWithFormat:@"Location:%@",[location_prev[j] valueForKey:@"value"]];
                                }
                            }
                        }
                        if (notes_prev!=nil && notes_prev!=(id)[NSNull null] && notes_prev!=NULL &&[notes_prev count]!=0) {
                            
                            
                            for (int j=0; j<[notes_prev count]; j++) {
                                if ([[notes_prev[j] valueForKey:@"id"] integerValue]==i*2) {
                                    notes =[NSMutableString stringWithFormat:@"%@|notes:%@",notes,[notes_prev[j] valueForKey:@"value"]];
                                    
                                }
                            }
                        }
                        
                        cell.detailTextLabel.text=notes;
                        

                        cell.textLabel.font=[UIFont systemFontOfSize:12.0];
                        
                    }
                    
                    
                }
                
                
            }
        }
        
    }
    
    
    }
     else{
     
     if ([statusarray1 count]==0) {
     
     cell.textLabel.text=@"no details";
     }
     else{
     for (int i=0; i<[statusarray1 count]; i++) {
     if (indexPath.row==i)
     {
     
     
     
     if ([statusarray1[i] isEqualToString:@"D"]) {
     cell.imageView.image=[UIImage imageNamed:@"images/driving.png"];
     
     
     NSString *textcell = [NSString stringWithFormat:@"Driving from %@",[starting_time objectAtIndex:i]];
     cell.textLabel.text=textcell;
     
     }
     else if ([statusarray1[i] isEqualToString:@"OFF"]) {
     cell.imageView.image=[UIImage imageNamed:@"offduty.jpg"];
     BFLog(@"before");
     NSString *textcell = [NSString stringWithFormat:@"Off Duty from %@",[starting_time objectAtIndex:i]];
     BFLog(@"After");
     cell.textLabel.text=textcell;
     
     }
     else if ([statusarray1[i] isEqualToString:@"SB"]) {
     cell.imageView.image=[UIImage imageNamed:@"sleeping.png"];
     
     NSString *textcell = [NSString stringWithFormat:@"Sleeping from %@",[starting_time objectAtIndex:i]];
     cell.textLabel.text=textcell;
     
     }
     else if ([statusarray1[i] isEqualToString:@"ON"]) {
     cell.imageView.image=[UIImage imageNamed:@"onduty.jpg"];
     
     NSString *textcell = [NSString stringWithFormat:@"On Duty from %@",[starting_time objectAtIndex:i]];
     cell.textLabel.text=textcell;
     
     }
     
     }
     
     
     
     }
     
     
     
     
     
     }}*/
    
    Section *objSec = [arrayOfTableData objectAtIndex:indexPath.section];
    SectionRows *objRow = [objSec.arrayRows objectAtIndex:indexPath.row];
    
    
    
    
       return cell;
    
    
}

/*-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
 UILabel *label = [[UILabel alloc] init];
 if (section==1) {
 
 
 label.text=@"Duration";
 label.textAlignment=NSTextAlignmentRight;
 
 }
 return label;
 
 }*/
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    //if (sections==0) {
   /* if (indexPath.section==0) {
        if (indexPath.row==0) {
            
        
        UIStoryboard *storyBoard = self.storyboard;
        UIViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"insertpast"];
        [self presentViewController:vc animated:YES completion:nil];
        //}
        }
        
    }else{
        
        
        
    }*/
    
    
    
}
-(void)removeEvent:(NSInteger)row{
    
    NSInteger dataIndex = 2*row;
    NSMutableArray *data = [dataarray2 mutableCopy];
    NSMutableArray *sam = [data[dataIndex] mutableCopy];
    NSMutableArray *sam1 = [data[dataIndex+1] mutableCopy];
    sam[1]=@"OFF";
    sam1[1]=@"OFF";
    data[dataIndex]=sam;
    data[dataIndex+1]=sam1;
    [self cleanseData:data];
    
    
}
-(void)clearEvent:(UITapGestureRecognizer*)tapGesture{
    
    NSInteger row = tapGesture.view.tag;
    BFLog(@"Editing:%ld",(long)row);
    deleteRow = row;
    UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@""
                                                     message:@"Do You want to Delete this Event?"
                                                    delegate:self
                                           cancelButtonTitle:@"Delete"
                                           otherButtonTitles:@"Keep", nil];
    
    [alert1 setTag:3];
    [alert1 show];
    
    
    
}
-(void)cleanseData:(NSMutableArray*)data{
    NSMutableArray *dummy = [[NSMutableArray alloc] init];
    [dummy addObject:data[0]];
    [dummy addObject:data[1]];
    for(int i=2;i<[data count];i=i+2){
        NSInteger lastDummy = [dummy count]-1;
        if(dummy[lastDummy][1]==data[i][1]){
            
             if([self calculatePosx:dummy[lastDummy][0]] <= [self calculatePosx:data[i+1][0]]){
                 
                 NSMutableArray *sam = [data[i+1] mutableCopy];
                 dummy[lastDummy]=sam;
                               
               }
            
        }else{
            
            [dummy addObject:data[i]];
            [dummy addObject:data[i+1]];
            
            
            
        }
        
        
        
    }
    
    BFLog(@"array:%@",dummy);
    [dataarray2 removeAllObjects];
    dataarray2=dummy;
    [self saveToDB:dataarray2 status:dataarray2[[dataarray2 count]-1][1] date:dateGlobal];
    [statusarray removeAllObjects];
    [violationArray removeAllObjects];
    int length=(int)[dataarray2 count];
    for (int k=0; k<length; k++) {
        if ((k%2==0)&&(k!=length-1)) {
            
            NSString *event = dataarray2[k][1];
            NSString *start = dataarray2[k][0];
            NSString *end = dataarray2[k+1][0];
            float start_float = [self getPosx:start];
            float end_float = [self getPosx:end];
            float diff = end_float-start_float;
            NSString *duration = [self getDuration:diff];
            NSArray *details = @[start,event,duration,end];
            [statusarray addObject:details];
            
            
        }
        
        
        
    }
    
    //[_perdayList reloadData];
    [_collecionViewLogEventInfo reloadData];

    
   /* UIAlertView *alert2 = [[UIAlertView alloc] initWithTitle:@""
                                                     message:@"Event deleted successfully"
                                                    delegate:self
                                           cancelButtonTitle:@"OK"
                                           otherButtonTitles:nil, nil];
    //[alert1 setTag:2];
    [alert2 show];*/
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [graph setNeedsDisplay];
    });

    
}

-(void)showEdit:(id)sender{
    UITapGestureRecognizer *tapper = (UITapGestureRecognizer*)sender;
    UIImageView *image = (UIImageView *)tapper.view;
    selected_row = image.tag;
    UIStoryboard *storyBoard = self.storyboard;
    editStatus *vc = (editStatus *) [storyBoard instantiateViewControllerWithIdentifier:@"editstatus"];
    [self presentViewController:vc animated:YES completion:nil];
    
}


-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
 
 return 35;
 }



-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    UIView *header = [[UIView alloc]initWithFrame:CGRectMake(0, 10, tableView.bounds.size.width, 200) ];
    header.backgroundColor = [UIColor lightGrayColor];
    UILabel *label1= [[UILabel alloc] initWithFrame:CGRectMake(10, 0, tableView.bounds.size.width/2, 30)];
    label1.textColor=[UIColor whiteColor];
    [label1 setFont:[UIFont fontWithName:@"Gill Sans Bold" size:15]];
    if([violationArray count]==0){
//        label1.text=@"Event Info";
//        UIView *rightView = [[UIView alloc] initWithFrame:CGRectMake(tableView.bounds.size.width/2, 0, tableView.bounds.size.width/2, 30)];
//        UIButton *addEvent = [UIButton buttonWithType:UIButtonTypeCustom];
//        addEvent.frame = CGRectMake(rightView.bounds.origin.x, 0, rightView.bounds.size.width/2, 35);
//       // [addEvent setImage:[UIImage imageNamed:@"addEvent.png"] forState:UIControlStateNormal];
//        NSDictionary *dict = @{NSUnderlineStyleAttributeName:@(NSUnderlineStyleSingle),NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[UIFont fontWithName:@"GillSans-Bold" size:15]};
//        NSMutableAttributedString *title1 = [[NSMutableAttributedString alloc] initWithString:@"Add Event" attributes:dict];
//       // [title1 addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInt:1] range:(NSRange){0,[title1 length]}];
//        [addEvent setAttributedTitle:title1 forState:UIControlStateNormal];
//        [addEvent addTarget:self action:@selector(addEvents:) forControlEvents:UIControlEventTouchUpInside];
//        [addEvent.titleLabel setFont:[UIFont fontWithName:@"Gill Sans Bold" size:15]];
//        UIButton *clearAll = [UIButton buttonWithType:UIButtonTypeCustom];
//        clearAll.frame = CGRectMake(rightView.bounds.size.width/2, 0, rightView.bounds.size.width/2, 35);
//        //[clearAll setImage:[UIImage imageNamed:@"deleteAll.png"] forState:UIControlStateNormal];
//        [clearAll addTarget:self action:@selector(clearAllData:) forControlEvents:UIControlEventTouchUpInside];
//        NSMutableAttributedString *title = [[NSMutableAttributedString alloc] initWithString:@"Clear All" attributes:dict];
//       // [title addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInt:1] range:(NSRange){0,[title length]}];
//
//
//         [clearAll setAttributedTitle:title forState:UIControlStateNormal];
//        //[clearAll setBackgroundColor:[UIColor darkGrayColor]];
//        clearAll.titleLabel.textAlignment = NSTextAlignmentRight;
//        [clearAll.titleLabel setFont:[UIFont fontWithName:@"Gill Sans Bold" size:15]];
//        [rightView addSubview:addEvent];
//        [rightView addSubview:clearAll];
//        [header addSubview:rightView];
        
    
    }else{
        if(section==0){
            label1.text= @"Violations";
        }else{
//            label1.text=@"Event Info";
//            UIView *rightView = [[UIView alloc] initWithFrame:CGRectMake(tableView.bounds.size.width/2, 0, tableView.bounds.size.width/2, 30)];
//            UIButton *addEvent = [UIButton buttonWithType:UIButtonTypeCustom];
//            addEvent.frame = CGRectMake(rightView.bounds.origin.x, 0, rightView.bounds.size.width/2, 30);
//            //[addEvent setTitle:@"Add Event" forState:UIControlStateNormal];
//            NSDictionary *dict = @{NSUnderlineStyleAttributeName:@(NSUnderlineStyleSingle),NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[UIFont fontWithName:@"GillSans-Bold" size:15]};
//            NSMutableAttributedString *title1 = [[NSMutableAttributedString alloc] initWithString:@"Add Event" attributes:dict];
//            // [title1 addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInt:1] range:(NSRange){0,[title1 length]}];
//            [addEvent setAttributedTitle:title1 forState:UIControlStateNormal];
//            [addEvent addTarget:self action:@selector(addEvents:) forControlEvents:UIControlEventTouchUpInside];
//
//            UIButton *clearAll = [UIButton buttonWithType:UIButtonTypeCustom];
//            clearAll.frame = CGRectMake(rightView.bounds.size.width/2, 0, rightView.bounds.size.width/2, 30);
//            [clearAll addTarget:self action:@selector(clearAllData:) forControlEvents:UIControlEventTouchUpInside];
//            NSMutableAttributedString *title = [[NSMutableAttributedString alloc] initWithString:@"Clear All" attributes:dict];
//            // [title addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInt:1] range:(NSRange){0,[title length]}];
//
//
//            [clearAll setAttributedTitle:title forState:UIControlStateNormal];
//            clearAll.titleLabel.textAlignment = NSTextAlignmentRight;
//            [rightView addSubview:addEvent];
//            [rightView addSubview:clearAll];
//            [header addSubview:rightView];
            
        }
        
        
        
    }
   
    [header addSubview:label1];
    return header;

}
-(IBAction)clearAllData:(id)sender{
    UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@""
                                                     message:@"Are you sure you want to delete?"
                                                    delegate:self
                                           cancelButtonTitle:@"YES"
                                           otherButtonTitles:@"NO", nil];
    [alert1 setTag:1];
    [alert1 show];
    
    
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if(alertView.tag ==1){
        
        if(buttonIndex==0){
            //minnu
            DBManager *objDB= [[DBManager alloc] initWithDatabaseFilename:@"hosDB.sql"];
            NSArray *userInfoArray=[[NSArray alloc]initWithArray: [objDB getUserId]];
            int userId = 0;
            if([userInfoArray count]!=0)
                userId = [userInfoArray[0][0] intValue];
            loggraph *objLog=[loggraph new];
            NSString* charStr=[@"" stringByPaddingToLength:96 withString:@"1" startingAtIndex:0];
            commonDataHandler *common=[commonDataHandler new];
            if([selected_previous isEqualToString:@""])
                charStr=[common modifyData:charStr date:selected_date];
            else
                charStr=[common modifyData:charStr date:selected_previous];
            for(int i=0;i<charStr.length;i++){
                objLog.slot=[NSString stringWithFormat:@"%d",(int)(1+i)];
                objLog.driverStatus=[charStr substringWithRange:NSMakeRange(i, 1)];;
                objLog.calculatedStatus=[charStr substringWithRange:NSMakeRange(i, 1)];;
                objLog.date=dateValue;
                objLog.userId=[NSString stringWithFormat:@"%d",userId];
                objLog.insert=[NSString stringWithFormat:@"%d",0];
                [objDB insertPastStatus:objLog];
            }
            graphDataOperations *obj=[graphDataOperations new];
            [dataarray2 removeAllObjects];
            [statusarray removeAllObjects];
            NSMutableArray *dict=[obj generateGraphPoints:charStr];
            for (int i=0; i<[dict count]; i++) {
                
//                NSLog(@"posx:%@,%d",[dict[0] objectForKey:@"posx"],i);
                float posx=[[dict[i] objectForKey:@"posx"] floatValue];
                float x1 = posx - 40.0;
                int x2 = x1/12.25;
                int x3 = x2/4.0;
                int x4 = x2%4;
                
                NSString *minutes;
                if (15*x4 <15) {
                    minutes=@"00";
                }
                else if(15*x4 >=15 && 15*x4 <30){
                    minutes=@"15";
                }else if(15*x4 >=30 && 15*x4 <45){
                    minutes=@"30";
                }else if(15*x4 >=45 && 15*x4 <60){
                    minutes=@"45";
                }
                NSString *posx_1 = [NSString stringWithFormat:@"%d:%@",x3,minutes];
                
                // float time = [posx_1 floatValue];
                int event_no =(int)[[dict[i] objectForKey:@"posy"] integerValue];
                NSString * event;
                if (event_no == 245) {
                    event=@"ON";
                }
                else if(event_no == 125){
                    event=@"SB";
                    
                }else if (event_no == 185){
                    event = @"D";
                }else if (event_no == 65){
                    event = @"OFF";
                }
                NSArray *final_data = [[NSArray alloc] initWithObjects:posx_1,event, nil];
                [dataarray2 addObject:final_data];
            }
            
            //minnu
            
           
            [violationArray removeAllObjects];
            if([selected_date isEqual:@""]||selected_date==nil){
                
                [self checkViolation:selected_previous];
            }else{
                
                 [self checkViolation:selected_date];
            }
       //     [self saveToDB:dataarray2 status:@"OFF" date:dateGlobal];
            NSString *event = dataarray2[0][1];
            NSString *start = dataarray2[0][0];
            NSString *end = dataarray2[1][0];
            float start_float = [self getPosx:start];
            float end_float = [self getPosx:end];
            float diff = end_float-start_float;
            NSString *duration = [self getDuration:diff];
            NSArray *details = @[start,event,duration,end];
            [statusarray addObject:details];
            [self createTableForClearAll];

          
            UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@""
                                                             message:@"All the data deleted successfully"
                                                            delegate:self
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles:nil, nil];
            //[alert1 setTag:2];
            [alert1 show];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [graph setNeedsDisplay];
                [_collecionViewLogEventInfo reloadData];
            });
  
        }
    }else if(alertView.tag==3){
        
        if(buttonIndex==0){
       
            [self removeEvent:deleteRow];
        }
        
    }
    else if(alertView.tag ==4 || alertView.tag ==2){
        
        if(buttonIndex==[alertView cancelButtonIndex]){
            
            
            dispatch_async(dispatch_get_main_queue(), ^{
                UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                UIViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"loginscreen"];
                [self presentViewController:vc animated:YES completion:nil];
            });
            
        }
        
        
    }
    else if(alertView.tag ==5)
    [self saveDataAfterDelete];

    
    
    
}
-(IBAction)addEvents:(id)sender{
    
    
    UIStoryboard *storyBoard = self.storyboard;
    UIViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"insertpast"];
    [self presentViewController:vc animated:YES completion:nil];
    
}
 /* -(NSArray<UITableViewRowAction *> *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath{
    
  UITableViewRowAction *edit = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:@"Edit" handler:^(UITableViewRowAction *action, NSIndexPath *indexpath){
        BFLog(@"Editing Style");
        selected_section=(int)indexPath.section;
        selected_row=(int)indexPath.row;
        UIStoryboard *storyBoard = self.storyboard;
        UIViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"editstatus"];
        [self presentViewController:vc animated:YES completion:nil];
        
    }];
    UITableViewRowAction *delete = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:@"Delete" handler:^(UITableViewRowAction *action, NSIndexPath *indexpath){
        BFLog(@"Editing Style");
        selected_section=(int)indexPath.section;
        selected_row=(int)indexPath.row;
        UIStoryboard *storyBoard = self.storyboard;
        UIViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"editstatus"];
        [self presentViewController:vc animated:YES completion:nil];
        
    }];

    edit.backgroundColor=[UIColor greenColor];
    delete.backgroundColor=[UIColor redColor];
    return @[edit,delete];
    
}*/


-(void)getSuggestionTableData:(NSString *)date{
    [arrayOfSuggestionDB removeAllObjects];
   // [dataarray2 removeAllObjects];
    current_date=date;
//    NSLog(@"Current date:%@",current_date);
    DBManager *objDB = [[DBManager alloc] initWithDatabaseFilename:@"hosDB.sql"];
    NSDictionary *token;
    ;
    NSMutableDictionary *whereToken;
    NSArray *userInfoArray=[[NSArray alloc]initWithArray: [objDB getUserId]];
    if([userInfoArray count]!=0){
        NSString *strUserName = userInfoArray[0][1];
        token=@{@"username":strUserName,@"date":[NSString stringWithFormat:@"%@",date]};
        whereToken=[[NSMutableDictionary alloc] initWithDictionary:token];
        NSArray* dataFromDB=[[NSArray alloc]initWithArray:[objDB fetchSuggestionWithwhereToken:whereToken]];
        if(dataFromDB.count){
            for (NSArray *arraySuggestion in dataFromDB) {
                Suggestion *objSuggestion = [Suggestion new];
                objSuggestion.username = [NSString stringWithFormat:@"%@", arraySuggestion[1]];
                objSuggestion.date = [NSString stringWithFormat:@"%@", arraySuggestion[2]];
                objSuggestion.slot = [NSString stringWithFormat:@"%@", arraySuggestion[3]];
                objSuggestion.repeat = [NSString stringWithFormat:@"%@", arraySuggestion[4]];
                objSuggestion.status = [self getSuggestionStatus:[NSString stringWithFormat:@"%@", arraySuggestion[5]]];
                objSuggestion.suggestedby = [NSString stringWithFormat:@"%@", arraySuggestion[6]];
                [arrayOfSuggestionDB addObject:objSuggestion];
            }
        }
    }
    [_collecionViewLogEventInfo reloadData];
    
}
-(NSString *)getSuggestionStatus:(NSString *)status{
    /*Driving - 3, 7 , 9
     OnDuty - 4, 8, A, C
     OffDuty - 1, 5, B
     Sleeper - 2, 6*/
    if([status isEqualToString:@"3"] || [status isEqualToString:@"7"] || [status isEqualToString:@"9"]){
        return @"D";
    }else if ([status isEqualToString:@"4"] || [status isEqualToString:@"8"] || [status isEqualToString:@"A"] || [status isEqualToString:@"C"]){
        return @"ON";
    }else if ([status isEqualToString:@"1"] || [status isEqualToString:@"5"] || [status isEqualToString:@"B"]){
        return @"OFF";
    }else if ([status isEqualToString:@"2"] || [status isEqualToString:@"6"]){
        return @"SB";
    }else{
        return @"SB";
    }
}
#pragma mark    - <UICollectionView Delegates & Datasource>
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return arrayOfTableData.count;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    Section *objsec = [arrayOfTableData objectAtIndex:section];
    return objsec.arrayRows.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    Section *objsec = [arrayOfTableData objectAtIndex:indexPath.section];
    SectionRows *objRow = [objsec.arrayRows objectAtIndex:indexPath.item];
    if(objRow.rowType == row_type_log_edit_OFF || objRow.rowType == row_type_log_edit_suggestion || objRow.rowType == row_type_log_edit_Others){
        LogEventEditCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"LogEventEditCollectionViewCell" forIndexPath:indexPath];
        cell.delegate = self;
        [cell setCellWithData:objRow];
        return cell;
    }else{
        LogEventInfoCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"LogEventInfoCollectionViewCell" forIndexPath:indexPath];
        [cell setCellWithData:objRow];
        return cell;
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{

    Section *objSec = [arrayOfTableData objectAtIndex:indexPath.section];
    return CGSizeMake(CGRectGetWidth(collectionView.frame)/objSec.arrayRows.count, 35);
}
#pragma mark    - <Clear & Add Events Action>
- (IBAction)AddEventAction:(id)sender{
    UIStoryboard *storyBoard = self.storyboard;
    UIViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"insertpast"];
    [self presentViewController:vc animated:YES completion:nil];
}
- (IBAction)ClearAllEventAction:(id)sender{
    UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@""
                                                     message:@"Are you sure you want to delete?"
                                                    delegate:self
                                           cancelButtonTitle:@"YES"
                                           otherButtonTitles:@"NO", nil];
    [alert1 setTag:1];
    [alert1 show];
}
#pragma mark    - <Create Table Data>
-(void)createTableForClearAll{
    
    Section *objSec = [Section new];
    SectionRows *objRow ;
    
    objSec.sectionType = section_type_other;
    objRow = [SectionRows new];
    objRow.rowType =  row_type_log_sr;
    objRow.isMandatory = YES;
    objRow.strRowTitle = @"1";
    [objSec.arrayRows addObject:objRow];
    
    objRow = [SectionRows new];
    objRow.rowType =  row_type_log_status;
    objRow.isMandatory = YES;
    objRow.strRowTitle = @"OFF";
    [objSec.arrayRows addObject:objRow];
    
    objRow = [SectionRows new];
    objRow.rowType =  row_type_log_start;
    objRow.isMandatory = YES;
    objRow.strRowTitle = @"00:00";
    [objSec.arrayRows addObject:objRow];
    
    objRow = [SectionRows new];
    objRow.rowType =  row_type_log_end;
    objRow.isMandatory = NO;
    NSDate *date = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm"];
    // [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    NSDictionary*token;NSMutableDictionary *whereToken;
    NSString* timezone;
    DBManager *objDB = [[DBManager alloc] initWithDatabaseFilename:@"hosDB.sql"];
    NSArray *userInfoArray=[[NSArray alloc]initWithArray: [objDB getUserId]];
    if([userInfoArray count]!=0){
        int userId = [userInfoArray[0][0] intValue];
        token=@{@"columnName":@"id",@"entry":[NSString stringWithFormat:@"%d",userId]};
        ;
        whereToken=[[NSMutableDictionary alloc] initWithDictionary:token];
        NSArray * value=[[NSArray alloc]initWithArray:[objDB getlogSettings:whereToken]];
        if([value count]!=0){
            timezone=value[0][2];
            NSString *strTimeZone = [timezone NullCheck];
            if(strTimeZone.length > 0)
                [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:timezone]];
            else{
                timezone = [[NSTimeZone localTimeZone] name];
                [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
            }
        }
        else{
            timezone = [[NSTimeZone localTimeZone] name];
            [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        }
    }else{
        timezone = [[NSTimeZone localTimeZone] name];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    }
    NSString *dateString = [dateFormatter stringFromDate:date];
    NSDateFormatter *dateFormatterCurrent = [[NSDateFormatter alloc] init];
    [dateFormatterCurrent setDateFormat:@"MM/dd/yyyy"];
    [dateFormatterCurrent setTimeZone:[NSTimeZone timeZoneWithAbbreviation:timezone]];
    NSDate *dateCurrent = [dateFormatterCurrent dateFromString:current_date];
    BOOL today = [[NSCalendar currentCalendar] isDateInToday:dateCurrent];
    if(today){
        NSArray *arrayMinutes = [dateString componentsSeparatedByString:@":"];
        int hour, minute;
        hour = 0;
        minute = 0;
        if(arrayMinutes.count > 1){
            hour = [arrayMinutes[0] intValue];
            minute = [arrayMinutes[1] intValue];
            if(minute>0 && minute <15)
                minute = 00;
            else if (minute > 15 && minute <30)
                minute = 15;
            else if (minute > 30 && minute <45)
                minute = 30;
            else if (minute > 45 && minute <=59)
                minute = 45;
            dateString = [NSString stringWithFormat:@"%02d:%02d",hour,minute];
        }
        objRow.strRowTitle = dateString;
    }
    else
        objRow.strRowTitle = @"24:00";
    [objSec.arrayRows addObject:objRow];
    

    objRow = [SectionRows new];
    objRow.rowType =  row_type_log_notes;
    objRow.isMandatory = YES;
    objRow.strRowTitle = @"-";
    [objSec.arrayRows addObject:objRow];
    
    objRow = [SectionRows new];
    objRow.rowType =  row_type_log_origin;
    objRow.isMandatory = NO;
    objRow.strRowTitle = @"-";
    [objSec.arrayRows addObject:objRow];
    
    objRow = [SectionRows new];
    objRow.rowType = row_type_log_edit_OFF;
    objRow.isMandatory = NO;
    [objSec.arrayRows addObject:objRow];
//    [arrayOfTableData addObject:objSec];
    [arrayOfTableData insertObject:objSec atIndex:1];
//    [arrayTemp addObjectsFromArray:[arrayOfTableData mutableCopy]];
//    [arrayTemp insertObject:objSec atIndex:1];
    
//    for (int i = 2; i<arrayOfTableData.count; i++) {
//        [arrayOfTableData removeObjectAtIndex:i];
//    }
    
//    for (int i = (int)arrayTemp.count; i>=2; i--) {
//        Section *objSec = [arrayTemp objectAtIndex:i];
//        if(objSec.sectionType == section_type_other){
//            if([arrayOfTableData containsObject:objSec]){
//                [arrayOfTableData removeObject:objSec];
//            }
//        }
//    }
   
    NSMutableArray *discardedItems = [NSMutableArray array];
    for (int i = 2; i<arrayOfTableData.count; i++){
        Section *objSec = [arrayOfTableData objectAtIndex:i];
        if(objSec.sectionType == section_type_other)
            [discardedItems addObject:objSec];
    }
    
    [arrayOfTableData removeObjectsInArray:discardedItems];
    
    
//    if(arrayTemp .count > 0){
//    Section *objHeadSec = [arrayTemp objectAtIndex:0];
//    [arrayOfTableData addObject:objHeadSec];
//    Section *objOffSec = [arrayTemp objectAtIndex:1];
//    [arrayOfTableData addObject:objOffSec];
//    }
    
//    for (Section *objSec in arrayTemp) {
//        if(objSec.sectionType == section_type_suggestion){
//            if(![arrayOfTableData containsObject:objSec]){
//                [arrayOfTableData addObject:objSec];
//            }
//        }
//    }
    
    
    for (int i =1; i<arrayOfTableData.count; i++) {
        Section *objSec = [arrayOfTableData objectAtIndex:i];
        if(objSec.sectionType == section_type_suggestion)
        {
            for (int j = 0; j < objSec.arrayRows.count ; j++) {
                SectionRows *objro = [objSec.arrayRows objectAtIndex:j];
                if(objro.rowType == row_type_log_sr)
                    objro.strRowTitle = [NSString stringWithFormat:@"%d", i];
                [objSec.arrayRows replaceObjectAtIndex:j withObject:objro];
            }
        }
        
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [_collecionViewLogEventInfo reloadData];
    });
//    [_collecionViewLogEventInfo reloadData];
}
-(void)createTableData{
    
    arrayOfTableData = [NSMutableArray new];
    Section *objSec;
    objSec = [Section new];
    SectionRows *objRow;
    objRow = [SectionRows new];
    objRow.rowType =  row_type_log_sr_title;
    objRow.isMandatory = YES;
    objRow.strRowTitle = @"Sr";
    [objSec.arrayRows addObject:objRow];
    
    objRow = [SectionRows new];
    objRow.rowType =  row_type_log_status_title;
    objRow.isMandatory = NO;
    objRow.strRowTitle = @"Status";
    [objSec.arrayRows addObject:objRow];
    
    objRow = [SectionRows new];
    objRow.rowType = row_type_log_start_title;
    objRow.isMandatory = YES;
    objRow.strRowTitle = @"Start";
    [objSec.arrayRows addObject:objRow];
    
    objRow = [SectionRows new];
    objRow.rowType = row_type_log_end_title;
    objRow.isMandatory = NO;
    objRow.strRowTitle = @"End";
    [objSec.arrayRows addObject:objRow];
    
    objRow = [SectionRows new];
    objRow.rowType = row_type_log_notes_title;
    objRow.isMandatory = YES;
    objRow.strRowTitle = @"Notes";
    [objSec.arrayRows addObject:objRow];
    
    objRow = [SectionRows new];
    objRow.rowType = row_type_log_origin_title;
    objRow.isMandatory = NO;
    objRow.strRowTitle = @"Origin";
    [objSec.arrayRows addObject:objRow];
    
    objRow = [SectionRows new];
    objRow.rowType = row_type_log_edit_title;
    objRow.isMandatory = NO;
    objRow.strRowTitle = @"Edit";
    [objSec.arrayRows addObject:objRow];
    [arrayOfTableData addObject:objSec];
    
    for (int i= 0; i < statusarray.count; i++) {
        objSec = [Section new];
//        if([[NSString stringWithFormat:@"%@",statusarray[i][1]] isEqualToString:@"OFF"])
//            objSec.sectionType = section_type_off;
//        else
            objSec.sectionType = section_type_other;
        objRow = [SectionRows new];
        objRow.rowType =  row_type_log_sr;
        objRow.isMandatory = YES;
        objRow.strRowTitle = [NSString stringWithFormat:@"%d", i+1];
        [objSec.arrayRows addObject:objRow];
        
        objRow = [SectionRows new];
        objRow.rowType =  row_type_log_status;
        objRow.isMandatory = YES;
        objRow.strRowTitle = [NSString stringWithFormat:@"%@",statusarray[i][1]];
        [objSec.arrayRows addObject:objRow];
        
        objRow = [SectionRows new];
        objRow.rowType =  row_type_log_start;
        objRow.isMandatory = YES;
        objRow.strRowTitle = [NSString stringWithFormat:@"%@",statusarray[i][0]];
        [objSec.arrayRows addObject:objRow];
        
        objRow = [SectionRows new];
        objRow.rowType =  row_type_log_end;
        objRow.isMandatory = NO;
        objRow.strRowTitle = [NSString stringWithFormat:@"%@",statusarray[i][3]];
        [objSec.arrayRows addObject:objRow];
        
        NSString *strNotes;
        strNotes = @"";
        if (notes_prev!=nil && notes_prev!=NULL && notes_prev!=(id)[NSNull null] &&[notes_prev count]!=0) {
            
            
            for (int j=0; j<[notes_prev count]; j++) {
                if(![[notes_prev[j] valueForKey:@"id"] isKindOfClass:[NSNull class]]){
                    if ([[notes_prev[j] valueForKey:@"id"] integerValue]==i*2) {
                        strNotes =[NSMutableString stringWithFormat:@"%@",[notes_prev[j] valueForKey:@"value"]];
                        
                    }
                }
            }
        }
        objRow = [SectionRows new];
        objRow.rowType =  row_type_log_notes;
        objRow.isMandatory = YES;
        objRow.strRowTitle = strNotes;
        [objSec.arrayRows addObject:objRow];
        
        objRow = [SectionRows new];
        objRow.rowType =  row_type_log_origin;
        objRow.isMandatory = NO;
        objRow.strRowTitle = @"-";
        [objSec.arrayRows addObject:objRow];
        
        objRow = [SectionRows new];
        if([[NSString stringWithFormat:@"%@",statusarray[i][1]] isEqualToString:@"OFF"])
            objRow.rowType = row_type_log_edit_OFF;
        else
            objRow.rowType = row_type_log_edit_Others;
        objRow.isMandatory = NO;
        objRow.strRowTitle = [NSString stringWithFormat:@"%@",statusarray[i][0]];//start Time
         objRow.strPlaceHolder = [NSString stringWithFormat:@"%@",statusarray[i][3]];//end Time
        [objSec.arrayRows addObject:objRow];
        [arrayOfTableData addObject:objSec];
    }
    for (int i = 0; i < arrayOfSuggestionDB.count; i++) {
        Suggestion *objSugg = [arrayOfSuggestionDB objectAtIndex:i];
        objSec = [Section new];
        objSec.sectionType = section_type_suggestion;
        
        objRow = [SectionRows new];
        objRow.rowType =  row_type_log_sr;
        objRow.isMandatory = YES;
        objRow.strRowTitle = [NSString stringWithFormat:@"%d", (int)(statusarray.count+i+1)];
        objRow.strRowContent = @"suggestion";
        [objSec.arrayRows addObject:objRow];
        
        objRow = [SectionRows new];
        objRow.rowType = row_type_log_status;
        objRow.strRowTitle = objSugg.status;
        objRow.strRowContent = @"suggestion";
        [objSec.arrayRows addObject:objRow];
        
        objRow = [SectionRows new];
        objRow.rowType =  row_type_log_start;
        objRow.isMandatory = YES;
        objRow.strRowTitle = objSugg.start;//[NSString stringWithFormat:@"%@",statusarray[i][0]];
        objRow.strRowContent = @"suggestion";
        [objSec.arrayRows addObject:objRow];
        
        objRow = [SectionRows new];
        objRow.rowType =  row_type_log_end;
        objRow.isMandatory = NO;
        objRow.strRowTitle = objSugg.end;
        objRow.strRowContent = @"suggestion";
        [objSec.arrayRows addObject:objRow];
        
        objRow = [SectionRows new];
        objRow.rowType =  row_type_log_notes;
        objRow.isMandatory = YES;
        objRow.strRowTitle = objSugg.suggestedby;
        objRow.strRowContent = @"suggestion";
        [objSec.arrayRows addObject:objRow];
        
        objRow = [SectionRows new];
        objRow.rowType =  row_type_log_origin;
        objRow.isMandatory = NO;
        objRow.strRowTitle = @"-";
        objRow.strRowContent = @"suggestion";
        [objSec.arrayRows addObject:objRow];
        
        objRow = [SectionRows new];
        objRow.rowType = row_type_log_edit_suggestion;
        objRow.isMandatory = NO;
        objRow.strRowContent = @"suggestion";
        objRow.strRowTitle = objSugg.start;//start Time
        objRow.strPlaceHolder = objSugg.end;//end Time
        [objSec.arrayRows addObject:objRow];
        [arrayOfTableData addObject:objSec];
    }
    arrayTemp = [NSMutableArray new];
    arrayTemp = arrayOfTableData;
    [_collecionViewLogEventInfo reloadData];
}
#pragma mark    - <Custom delegates>
- (void)AcceptLogActionWithRow:(SectionRows *)objRow ForCell:(LogEventEditCollectionViewCell *)objCell{
    
}
- (void)rejectLogActionWithRow:(SectionRows *)objRow ForCell:(LogEventEditCollectionViewCell *)objCell{
    
}
- (void)editLogActionWithRow:(SectionRows *)objRow ForCell:(LogEventEditCollectionViewCell *)objCell{
    NSIndexPath *index = [_collecionViewLogEventInfo indexPathForCell:objCell];
    selected_row = index.section;
    selected_row=selected_row-1;
    UIStoryboard *storyBoard = self.storyboard;
    editStatus *vc = (editStatus *)[storyBoard instantiateViewControllerWithIdentifier:@"editstatus"];
    vc.objEditRow = objRow;
    [self presentViewController:vc animated:YES completion:nil];
}
- (void)deleteLogActionWithRow:(SectionRows *)objRow ForCell:(LogEventEditCollectionViewCell *)objCell{
    NSIndexPath *index = [_collecionViewLogEventInfo indexPathForCell:objCell];
    selected_row = index.section;
    UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@""
                                                     message:@"Do You want to Delete this Event?"
                                                    delegate:self
                                           cancelButtonTitle:@"Delete"
                                           otherButtonTitles:@"Keep", nil];
    
    [alert1 setTag:5];
    [alert1 show];
    

}

-(void)saveDataAfterDelete{
    
    NSInteger index_pos;
    [newline removeAllObjects];
    newline=[[NSMutableArray alloc]initWithArray:dataarray2];
    [dataarray2 removeAllObjects];
    //minnu
    selected_row=selected_row-1;
    if(selected_row!=0)
        index_pos=selected_row*2;
    else
        index_pos=selected_row;
    NSString *start=newline[index_pos][0];
    NSString *end=newline[index_pos+1][0];
    NSString *init=@"0:0";
    NSArray *initTime=[init componentsSeparatedByString:@":"];
    NSArray *startTime=[start componentsSeparatedByString:@":"];
    NSArray *endTime=[end componentsSeparatedByString:@":"];
    int duration=([endTime[0]intValue]*60+[endTime[1] intValue])-([startTime[0]intValue]*60+[startTime[1] intValue]);
    int repeat=(int)duration/15;
    duration=([startTime[0]intValue]*60+[startTime[1] intValue])-([initTime[0]intValue]*60+[initTime[1] intValue]);
    int  slot=(int)duration/15;
    slot=slot+1;
    NSString *statusNew=@"1";
    annotation *obj=[annotation new];
    NSString* charStr=statusString;
    NSMutableString *charStrWithSlash=[NSMutableString stringWithFormat:@""];
    for(int i=0;i<charStr.length;i++){
        NSUInteger len=1;
        [charStrWithSlash appendFormat:@"%@|",[charStr substringWithRange:NSMakeRange(i, len)]];
    }
    NSString *tempStr;
        tempStr=[obj updateStr:charStrWithSlash index1:slot index2:repeat notes:statusNew];
    
     newCharStr=@"";
     newCharStr=[tempStr stringByReplacingOccurrencesOfString:@"|" withString:@""];
     NSArray *statusArray=[[NSArray alloc] initWithArray:[tempStr componentsSeparatedByString:@"|"]];
    [newline removeAllObjects];
    graphDataOperations *objGraph=[graphDataOperations new];
    NSMutableArray *dict=[objGraph generateGraphPoints:newCharStr];
    for (int i=0; i<[dict count]; i++) {
        
//        NSLog(@"posx:%@,%d",[dict[0] objectForKey:@"posx"],i);
        float posx=[[dict[i] objectForKey:@"posx"] floatValue];
        float x1 = posx - 40.0;
        int x2 = x1/12.25;
        int x3 = x2/4.0;
        int x4 = x2%4;
        
        NSString *minutes;
        if (15*x4 <15) {
            minutes=@"00";
        }
        else if(15*x4 >=15 && 15*x4 <30){
            minutes=@"15";
        }else if(15*x4 >=30 && 15*x4 <45){
            minutes=@"30";
        }else if(15*x4 >=45 && 15*x4 <60){
            minutes=@"45";
        }
        NSString *posx_1 = [NSString stringWithFormat:@"%d:%@",x3,minutes];
        
        // float time = [posx_1 floatValue];
        int event_no =(int)[[dict[i] objectForKey:@"posy"] integerValue];
        NSString * event;
        if (event_no == 245) {
            event=@"ON";
        }
        else if(event_no == 125){
            event=@"SB";
            
        }else if (event_no == 185){
            event = @"D";
        }else if (event_no == 65){
            event = @"OFF";
        }
        NSArray *final_data = [[NSArray alloc] initWithObjects:posx_1,event, nil];
        [dataarray2 addObject:final_data];
    }
    
    [self saveChanges:(NSArray *)statusArray];
}

-(void)saveChanges:(NSArray *)statusArray{
    DBManager *objDB= [[DBManager alloc] initWithDatabaseFilename:@"hosDB.sql"];
    NSArray *userInfoArray=[[NSArray alloc]initWithArray: [objDB getUserId]];
    int userId = 0;
    if([userInfoArray count]!=0)
        userId = [userInfoArray[0][0] intValue];
    loggraph *objLog=[loggraph new];
   
    for(int i=0;i<[statusArray count];i++){
        objLog.slot=[NSString stringWithFormat:@"%d",(int)(1+i)];
        objLog.driverStatus=[statusArray objectAtIndex:i];
        objLog.calculatedStatus=[statusArray objectAtIndex:i];
        objLog.date=dateValue;
        objLog.userId=[NSString stringWithFormat:@"%d",userId];
        objLog.insert=[NSString stringWithFormat:@"%d",0];
        [objDB insertPastStatus:objLog];
    }
    [statusarray removeAllObjects];
    [violationArray removeAllObjects];
    int length=(int)[dataarray2 count];
    for (int k=0; k<length; k++) {
        if ((k%2==0)&&(k!=length-1)) {
            
            NSString *event = dataarray2[k][1];
            NSString *start = dataarray2[k][0];
            NSString *end = dataarray2[k+1][0];
            float start_float = [self getPosx:start];
            float end_float = [self getPosx:end];
            float diff = end_float-start_float;
            NSString *duration = [self getDuration:diff];
            NSArray *details = @[start,event,duration,end];
            [statusarray addObject:details];
        }}

    [self createTableData];
    [_collecionViewLogEventInfo reloadData];
    
    
    UIAlertView *alert2 = [[UIAlertView alloc] initWithTitle:@""
                                                     message:@"Event deleted successfully"
                                                    delegate:self
                                           cancelButtonTitle:@"OK"
                                           otherButtonTitles:nil, nil];
    //[alert1 setTag:2];
    [alert2 show];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [graph setNeedsDisplay];
        statusString=newCharStr;
    });
    

}
@end

