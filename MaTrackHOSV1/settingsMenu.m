//
//  settingsMenu.m
//  MaTrackHOS
//
//  Created by Raghee Chandran M on 7/12/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//

#import "settingsMenu.h"
#import "menuScreenViewController.h"
#import "CarrierSettings.h"
#import "accountSettings.h"
#import "NotificationHandler.h"
#import "AppDelegate.h"
#import "commonDataHandler.h"
#import "Header.h"
#import "MobileCheckIn.h"
#import "MBProgressHUD.h"

NSString *property_carrying;
NSString *passenger_carrying;
NSString *oilandgas;
NSString *carrierName;
NSString *usa70hr;
NSString *usa60hr;
NSString *texas70hr;
NSString *californiasouth80hr;
//NSString *others;
NSDictionary *cargo;
NSDictionary *carrier;
NSDictionary *cycle;
NSDictionary *restart;
NSDictionary *breakdetails;
NSDictionary *driver;
NSDictionary *logs;
NSDictionary *odometer;
NSArray *s_menu;
NSArray *s_images;
UIView *transparentView_settings;
@interface settingsMenu ()<UIAlertViewDelegate>
{
    BOOL isReAuth;
    UIAlertView *  alert;
}
@end

@implementation settingsMenu
@synthesize settings_menu;
@synthesize menu_settings,dbManager;
@synthesize objCarierFromMenu;
- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadSettings];
    settings_menu.delegate=self;
    settings_menu.dataSource=self;
    s_menu=@[@"Driver Details",@"Carrier Details",@"Timezone and Cycle Rule",@"Company Connection",@"Change Your Password",@"Diagnostics",@"Preferences",@"Logout"];
    s_images=@[@"icons/account.png",@"icons/carrier.png",@"icons/cyclesAndRules.png",@"icons/connection.jpg",@"icons/password.png",@"icons/diagnostics.png",@"icons/preference.png",@"icons/logouticon.png"];
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(handleUpdatedData:)
//                                                 name:@"DataUpdated"
//                                               object:nil];
    
}
- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidAppear:YES];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"DataUpdated" object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleUpdatedData:)
                                                 name:@"DataUpdated"
                                               object:nil];
}
-(void)handleUpdatedData:(NSNotification *)notification {
    NSDictionary* userInfo = notification.userInfo;
    driver_status drvStat;
    int driverStat;
    driverStat = [[userInfo valueForKey:@"driverStatus"] intValue];
    if(driverStat == 1)
        drvStat = driver_status_driving;
    else if (driverStat == 2)
        drvStat = driver_status_onDuty;
    else if (driverStat == 3)
        drvStat = driver_status_offDuty;
    else if (driverStat == 4)
        drvStat = driver_status_none;
    else if (driverStat == 6)
        drvStat = driver_status_onDuty_immediate;
    else
        drvStat = driver_status_connect_back;
//    if(driverStat == 2){
//        dispatch_async(dispatch_get_main_queue(), ^{
//            alert = [[UIAlertView alloc]initWithTitle:@"Status Change" message:@"Do you want to continue driving or change your duty status" delegate:self cancelButtonTitle:@"Continue Driving" otherButtonTitles:@"Change Status", nil];
//            alert.tag=1005;
//            alert.delegate=self;
//            [alert show];
//            [self performSelector:@selector(dismissAlertView) withObject:nil afterDelay:60.0];
//        });
//    }else{
        NotificationHandler *objHelper = [NotificationHandler new];
        [objHelper recievePushNotification:self withDriverStatus:drvStat];
  //  }
}

- (void)dismissAlertView {
        NotificationHandler *objHelper = [NotificationHandler new];
        [objHelper recievePushNotification:self withDriverStatus:driver_status_onDuty_immediate];
    [alert dismissWithClickedButtonIndex:-1 animated:YES];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [s_menu count];
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    // UIImageView *image2 = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"icons/navigation.png"]];
    static NSString *identifier = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath ];
    if (cell==nil) {
        cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identifier];
    }
    cell.textLabel.text=s_menu[indexPath.row];
    cell.backgroundView.backgroundColor=[UIColor cyanColor];
    cell.textLabel.font=[UIFont fontWithName:@"Arial" size:20.0];
    //cell.textLabel.textAlignment=NSTextAlignmentCenter;
    //cell.detailTextLabel.text=@">";
    cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
    cell.imageView.image=[UIImage imageNamed:s_images[indexPath.row]];
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.row) {
        case 0:
        {
            UIStoryboard *storyBoard = self.storyboard;
            //UIViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"account"];
            //[self presentViewController:vc animated:YES completion:nil];
            accountSettings *vc=(accountSettings *)[storyBoard instantiateViewControllerWithIdentifier:@"account"];
            vc.accountData=_objUserFromMenu;
            [self presentViewController:vc animated:YES completion:nil];
            break;
        }
        case 1:
        {
            UIStoryboard *storyBoard = self.storyboard;
            //            UIViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"carrier"];
            //            [self presentViewController:vc animated:YES completion:nil];
            CarrierSettings *vc = (CarrierSettings *)[storyBoard instantiateViewControllerWithIdentifier:@"carrier"];
            vc.carrierData = objCarierFromMenu;
            [self presentViewController:vc animated:YES completion:nil];
            break;
        }
        case 2:
        {
            
            UIStoryboard *storyBoard = self.storyboard;
            LogSettings *vc = (LogSettings *)[storyBoard instantiateViewControllerWithIdentifier:@"log"];
            vc.logSettingsData = _objRulesFromMenu;
            
            //UIViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"log"];
            [self presentViewController:vc animated:YES completion:nil];
            break;
        }
        case 3:{
            
            UIStoryboard *storyBoard = self.storyboard;
            UIViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"connection"];
            [self presentViewController:vc animated:YES completion:nil];
            break;
        }
        case 4:{
            
            UIStoryboard *storyBoard = self.storyboard;
            UIViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"resetOldPassword"];
            [self presentViewController:vc animated:YES completion:nil];
            break;
        }
        case 5:{
            //            UIStoryboard *storyBoard = self.storyboard;
            //            UIViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"diagnostics"];
            //            [self presentViewController:vc animated:YES completion:nil];
            //            break;
            [[[UIAlertView alloc] initWithTitle:@"Diagnostics"
                                        message:@"Coming soon"
                                       delegate:self
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil, nil] show];
            
            break;
            
        }
        case 6:{
            UIStoryboard *storyBoard = self.storyboard;
            UIViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"preference"];
            [self presentViewController:vc animated:YES completion:nil];
            break;
            
            
        }
        case 7:{
            UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@""
                                                             message:@"Do You want to logout?"
                                                            delegate:self
                                                   cancelButtonTitle:@"YES"
                                                   otherButtonTitles:@"NO", nil];
            alert1.tag = 1001;
            
            [alert1 show];

        }
            
        default:
            break;
    }
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 70;
    
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if(alertView.tag == 1001){
        if (buttonIndex==0) {
            isReAuth = YES;
            [self callLogOutAPI];
        }
    }
    if(alertView.tag == 1005){
        if(buttonIndex==1){
            
            UIStoryboard *storyBoard = self.storyboard;
            UIViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"changedutystatus"];
            [self presentViewController:vc animated:YES completion:nil];
        }
    }
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    // UITouch *touch = [touches anyObject];
    if([[self.view subviews] containsObject:transparentView_settings]){
        [transparentView_settings removeFromSuperview];
        
    }
}









-(void)showHelp{
    
    sourceVC = [self valueForKey:@"storyboardIdentifier"];
    UIStoryboard *storyBoard = self.storyboard;
    UIViewController *vc1 = [storyBoard instantiateViewControllerWithIdentifier:@"helpScreen"];
    [self presentViewController:vc1 animated:YES completion:nil];
    
    
    
}
//minnu
-(void)loadSettings{
    DBManager *objDB= [[DBManager alloc] initWithDatabaseFilename:@"hosDB.sql"];
    
   NSDictionary *token;
    ;
    NSMutableDictionary *whereToken;
    NSArray *userInfoArray=[[NSArray alloc]initWithArray: [objDB getUserId]];
    if([userInfoArray count]!=0){
        int userId = [userInfoArray[0][0] intValue];
        token=@{@"columnName":@"id",@"entry":[NSString stringWithFormat:@"%d",userId]};
        ;
        
    whereToken=[[NSMutableDictionary alloc] initWithDictionary:token];
   // NSArray *user = [[NSArray alloc] initWithArray:[objDB selectFromTable:objCarrier whereToken:whereToken]];
    User *objUser=[User new];
    NSArray *user = [[NSArray alloc] initWithArray:[objDB selectFromTable:objUser whereToken:whereToken]];
    _objUserFromMenu=[User new];
    _objUserFromMenu.username=user[0][1];
    _objUserFromMenu.password=user[0][2];
    _objUserFromMenu.firstname=user[0][3];
    _objUserFromMenu.lastname=user[0][4];
    _objUserFromMenu.email=user[0][5];
    _objUserFromMenu.mobile=user[0][6];
    _objUserFromMenu.driverid=user[0][7];
    _objUserFromMenu.dotNo=user[0][8];
token=@{@"columnName":@"userid",@"entry":[NSString stringWithFormat:@"%d",userId]};
        ;
        
whereToken=[[NSMutableDictionary alloc] initWithDictionary:token];
        
NSArray * carrierIdInfo= [[NSArray alloc] initWithArray:[objDB getCarrierId:whereToken]];
        if([carrierIdInfo count]!=0){
            token=@{@"columnName":@"id",@"entry":[NSString stringWithFormat:@"%@", carrierIdInfo[0][0]]};
            ;whereToken=[[NSMutableDictionary alloc] initWithDictionary:token];
    Carrier *objCarrier=[Carrier new];
    NSArray *carrier = [[NSArray alloc] initWithArray:[objDB selectFromTable:objCarrier whereToken:whereToken]];
            if([carrier count]!=0){
    objCarierFromMenu=[Carrier new];
    objCarierFromMenu.name=carrier[0][1];
    objCarierFromMenu.homeAddress=carrier[0][6];
    objCarierFromMenu.homeCity=carrier[0][7];
    objCarierFromMenu.homeState=carrier[0][8];
    objCarierFromMenu.homeZip=carrier[0][9];
    objCarierFromMenu.mainAddress=carrier[0][2];
    objCarierFromMenu.mainCity=carrier[0][3];
    objCarierFromMenu.mainState=carrier[0][4];
    objCarierFromMenu.mainZip=carrier[0][5];
            }
        }
    preferences *objPref=[preferences new];
    token=@{@"columnName":@"userid",@"entry":[NSString stringWithFormat:@"%d",userId]};
    ;
    whereToken=[[NSMutableDictionary alloc] initWithDictionary:token];
    
    NSArray *prefs=[[NSArray alloc]initWithArray:[objDB selectFromTable:objPref whereToken:whereToken]];
    NSMutableDictionary *prefDict=[[NSMutableDictionary alloc]init];
    for(int i=0;i<[prefs count];i++){
        
            [prefDict setObject:prefs[i][3] forKey:prefs[i][2]];
        
    }
    NSArray *timezoneCycle=[[NSArray alloc]initWithArray:[objDB getlogSettings:whereToken]];
        if([timezoneCycle count]!=0){
    _objRulesFromMenu=[rules new];
    _objRulesFromMenu.cycle=timezoneCycle[0][1];
    _objRulesFromMenu.timezone=timezoneCycle[0][2];
    _objRulesFromMenu.restart=timezoneCycle[0][3];
    _objRulesFromMenu.breakhour=timezoneCycle[0][4];
    _objRulesFromMenu.cargotype=timezoneCycle[0][5];
    _objRulesFromMenu.odounit=timezoneCycle[0][6];
        }
}
}
//minnu

/*-(void)loadSettings{
    dbManager = [[DBManager1 alloc] initWithDatabaseFilename:@"HOSDatabase.sql"];
    
    NSString *master = [[NSUserDefaults standardUserDefaults] objectForKey:@"masteruser"];
    NSString *USer = [[NSUserDefaults standardUserDefaults] objectForKey:@"user"];
    //getting authCode
    NSString *query0 = [NSString stringWithFormat:@"select * from authDetails;"];
    NSArray *authData = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query0]];
    NSString *authCode = @"";
    if([authData count]!=0){
        authCode = authData[0][1];
        
    }
    //---------------------------
    NSString *urlString = [NSString stringWithFormat:@"http://www.gpstracking-server1.com/gpstracking/hosapp/getHosSettings.php?masterUser=%@&user=%@&authCode=%@",master,USer,authCode];
    NSURL *url = [NSURL URLWithString:urlString];
    NSData *data = [NSData dataWithContentsOfURL:url];
    if (data) {
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:urlString] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            //  BFLog(@"Retrieved Data:%@,%@",data,response);
            BFLog(@"---------------------------------------");
            BFLog(@"error:%@",error);
            // if (error.code) {
            
            if(data){
                NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                if(json!=nil && [json count]!=0){
                //if (error == nil) {
                cargo=json[@"Cargo"];
                carrier=[json[@"Carrier"] mutableCopy];
                cycle=json[@"Cycle"];
                restart=json[@"Restart"];
                breakdetails=json[@"Break"];
                driver=json[@"Driver"];
                logs=json[@"Logs"];
                odometer=json[@"Odometer"];
                
                
                
                if([cycle[@"CycleType"] isEqualToString:@"USA70hours"]){
                    
                    
                    cycleinfo=@"USA 70 hours/8 days";
                    
                }else if([cycle[@"CycleType"] isEqualToString:@"USA60hours"]){
                    
                    cycleinfo=@"USA 60 hours/7 days";
                    
                    
                    
                }else if([cycle[@"CycleType"] isEqualToString:@"Texas70hours"]){
                    
                    cycleinfo=@"Texas 70 hours/7 days";
                    
                    
                    
                }else if([cycle[@"CycleType"] isEqualToString:@"California80hours"]){
                    
                    cycleinfo=@"California 80 hours/8 days";
                    
                    
                }else if([cycle[@"CycleType"] isEqualToString:@"Others"]){
                    
                    cycleinfo=@"Others";
                    
                    
                }
                
                if ([cargo[@"CargoType"] isEqualToString:@"PropertyCarrying"]) {
                    
                    cargoinfo=@"Property";
                    
                }else if ([cargo[@"CargoType"] isEqualToString:@"PassengerCarrying"]){
                    
                    cargoinfo=@"Passenger";
                    
                }else if ([cargo[@"CargoType"] isEqualToString:@"OilandGas"]){
                    
                    cargoinfo=@"Oil and Gas";
                }
                
                    NSString *query_del5 =[NSString stringWithFormat:@"delete from Carrier_Sett;"];
                    [dbManager executeQuery:query_del5];
                    
                    NSString *query_del7 =[NSString stringWithFormat:@"delete from AccountSettings;"];
                    [dbManager executeQuery:query_del7];
                    NSString *query_del8 =[NSString stringWithFormat:@"delete from LogSettings;"];
                    [dbManager executeQuery:query_del8];
                
                
                NSString *query_insert1 = [NSString stringWithFormat:@"insert into LogSettings values(null, '%@', '%@', '%@','%@','%@','%@')",logs[@"Timezone"],cycle[@"CycleType"],cargo[@"CargoType"],odometer[@"Odometer"],restart[@"RestartType"],breakdetails[@"BreakType"]];
                
                // Execute the query.
                
                //NSString *query_del = @"delete from TempTable";
                [dbManager executeQuery:query_insert1];
                
                // If the query was successfully executed then pop the view controller.
                if (dbManager.affectedRows != 0) {
                    BFLog(@"Query was inserted the data successfully. Affected rows = %d", dbManager.affectedRows);
                    //c_flag=1;
                }
                else{
                    BFLog(@"Could not execute the query");
                }
                NSString *query_insert2 = [NSString stringWithFormat:@"insert into AccountSettings values(null, '%@','%@' ,'%@', '%@','%@','%@')",driver[@"FirstName"],driver[@"LastName"],driver[@"LicenceNumber"],driver[@"Phone"],driver[@"Email"],driver[@"dotNumber"]];
                
                // Execute the query.
                
                //NSString *query_del = @"delete from TempTable";
                [dbManager executeQuery:query_insert2];
                
                // If the query was successfully executed then pop the view controller.
                if (dbManager.affectedRows != 0) {
                    BFLog(@"Query was inserted the data successfully. Affected rows = %d", dbManager.affectedRows);
                    //c_flag=1;
                }
                else{
                    BFLog(@"Could not execute the query");
                }
                
                NSString *query_insert3 = [NSString stringWithFormat:@"insert into Carrier_Sett values(null, '%@', '%@', '%@')", carrier[@"Carrier"],[NSString stringWithFormat:@"%@,%@,%@,%@", carrier[@"offAddressline1"],carrier[@"offAddressline2"],carrier[@"offAddressline3"],carrier[@"offZip"]],[NSString stringWithFormat:@"%@,%@,%@,%@",carrier[@"homeAddressline1"],carrier[@"homeAddressline2"],carrier[@"homeAddressline3"],carrier[@"homeZip"]] ];
                
                // Execute the query.
                
                //NSString *query_del = @"delete from TempTable";
                [dbManager executeQuery:query_insert3];
                
                // If the query was successfully executed then pop the view controller.
                if (dbManager.affectedRows != 0) {
                    BFLog(@"Query was inserted the data successfully. Affected rows = %d", dbManager.affectedRows);
                    //c_flag=1;
                }
                else{
                    BFLog(@"Could not execute the query");
                }
                }
            }else{
                UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                                 message:[NSString stringWithFormat:@"%ld",error.code]
                                                                delegate:self
                                                       cancelButtonTitle:@"Cancel"
                                                       otherButtonTitles:@"OK", nil];
                
                [alert1 show];
                
            }
            
        }];
        [dataTask resume];
    }else{
        BFLog(@"Device not connected to the internet");
        UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                         message:@"Please check your internet connectivity!!!"
                                                        delegate:self
                                               cancelButtonTitle:@"Cancel"
                                               otherButtonTitles:@"OK", nil];
        
        [alert1 show];
=======
    NSArray *userInfoArray=[[NSArray alloc]initWithArray: [objDB getUserId:whereToken]];
    if([userInfoArray count]!=0){
        NSInteger userId = [userInfoArray[0][0] integerValue];
        token=@{@"columnName":@"id",@"entry":[NSString stringWithFormat:@"%ld",(long)userId]};
        ;
        whereToken=[[NSMutableDictionary alloc] initWithDictionary:token];
        // NSArray *user = [[NSArray alloc] initWithArray:[objDB selectFromTable:objCarrier whereToken:whereToken]];
        User *objUser=[User new];
        NSArray *user = [[NSArray alloc] initWithArray:[objDB selectFromTable:objUser whereToken:whereToken]];
        _objUserFromMenu=[User new];
        _objUserFromMenu.username=user[0][1];
        _objUserFromMenu.password=user[0][2];
        _objUserFromMenu.firstname=user[0][3];
        _objUserFromMenu.lastname=user[0][4];
        _objUserFromMenu.email=user[0][5];
        _objUserFromMenu.mobile=user[0][6];
>>>>>>> b7ad7dbd593cf4bb5aa96d4081efebccfb28664c
        
        
        NSInteger *carrierId= [objDB getCarrierId:whereToken];
        token=@{@"columnName":@"id",@"entry":[NSString stringWithFormat:@"%@",carrierId]};
        ;
        whereToken=[[NSMutableDictionary alloc] initWithDictionary:token];
        Carrier *objCarrier=[Carrier new];
        NSArray *carrier = [[NSArray alloc] initWithArray:[objDB selectFromTable:objCarrier whereToken:whereToken]];
        objCarierFromMenu=[Carrier new];
        objCarierFromMenu.name=carrier[0][1];
        objCarierFromMenu.homeAddress=carrier[0][6];
        objCarierFromMenu.homeCity=carrier[0][7];
        objCarierFromMenu.homeState=carrier[0][8];
        objCarierFromMenu.homeZip=carrier[0][9];
        objCarierFromMenu.mainAddress=carrier[0][2];
        objCarierFromMenu.mainCity=carrier[0][3];
        objCarierFromMenu.mainState=carrier[0][4];
        objCarierFromMenu.mainZip=carrier[0][5];
    }
}
//minnu

/*-(void)loadSettings{
 dbManager = [[DBManager1 alloc] initWithDatabaseFilename:@"HOSDatabase.sql"];
 
 NSString *master = [[NSUserDefaults standardUserDefaults] objectForKey:@"masteruser"];
 NSString *USer = [[NSUserDefaults standardUserDefaults] objectForKey:@"user"];
 //getting authCode
 NSString *query0 = [NSString stringWithFormat:@"select * from authDetails;"];
 NSArray *authData = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query0]];
 NSString *authCode = @"";
 if([authData count]!=0){
 authCode = authData[0][1];
 
 }
 //---------------------------
 NSString *urlString = [NSString stringWithFormat:@"http://www.gpstracking-server1.com/gpstracking/hosapp/getHosSettings.php?masterUser=%@&user=%@&authCode=%@",master,USer,authCode];
 NSURL *url = [NSURL URLWithString:urlString];
 NSData *data = [NSData dataWithContentsOfURL:url];
 if (data) {
 NSURLSession *session = [NSURLSession sharedSession];
 NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:urlString] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
 //  BFLog(@"Retrieved Data:%@,%@",data,response);
 BFLog(@"---------------------------------------");
 BFLog(@"error:%@",error);
 // if (error.code) {
 
 if(data){
 NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
 if(json!=nil && [json count]!=0){
 //if (error == nil) {
 cargo=json[@"Cargo"];
 carrier=[json[@"Carrier"] mutableCopy];
 cycle=json[@"Cycle"];
 restart=json[@"Restart"];
 breakdetails=json[@"Break"];
 driver=json[@"Driver"];
 logs=json[@"Logs"];
 odometer=json[@"Odometer"];
 
 
 
 if([cycle[@"CycleType"] isEqualToString:@"USA70hours"]){
 
 
 cycleinfo=@"USA 70 hours/8 days";
 
 }else if([cycle[@"CycleType"] isEqualToString:@"USA60hours"]){
 
 cycleinfo=@"USA 60 hours/7 days";
 
 
 
 }else if([cycle[@"CycleType"] isEqualToString:@"Texas70hours"]){
 
 cycleinfo=@"Texas 70 hours/7 days";
 
 
 
 }else if([cycle[@"CycleType"] isEqualToString:@"California80hours"]){
 
 cycleinfo=@"California 80 hours/8 days";
 
 
 }else if([cycle[@"CycleType"] isEqualToString:@"Others"]){
 
 cycleinfo=@"Others";
 
 
 }
 
 if ([cargo[@"CargoType"] isEqualToString:@"PropertyCarrying"]) {
 
 cargoinfo=@"Property";
 
 }else if ([cargo[@"CargoType"] isEqualToString:@"PassengerCarrying"]){
 
 cargoinfo=@"Passenger";
 
 }else if ([cargo[@"CargoType"] isEqualToString:@"OilandGas"]){
 
 cargoinfo=@"Oil and Gas";
 }
 
 NSString *query_del5 =[NSString stringWithFormat:@"delete from Carrier_Sett;"];
 [dbManager executeQuery:query_del5];
 
 NSString *query_del7 =[NSString stringWithFormat:@"delete from AccountSettings;"];
 [dbManager executeQuery:query_del7];
 NSString *query_del8 =[NSString stringWithFormat:@"delete from LogSettings;"];
 [dbManager executeQuery:query_del8];
 
 
 NSString *query_insert1 = [NSString stringWithFormat:@"insert into LogSettings values(null, '%@', '%@', '%@','%@','%@','%@')",logs[@"Timezone"],cycle[@"CycleType"],cargo[@"CargoType"],odometer[@"Odometer"],restart[@"RestartType"],breakdetails[@"BreakType"]];
 
 // Execute the query.
 
 //NSString *query_del = @"delete from TempTable";
 [dbManager executeQuery:query_insert1];
 
 // If the query was successfully executed then pop the view controller.
 if (dbManager.affectedRows != 0) {
 BFLog(@"Query was inserted the data successfully. Affected rows = %d", dbManager.affectedRows);
 //c_flag=1;
 }
 else{
 BFLog(@"Could not execute the query");
 }
 NSString *query_insert2 = [NSString stringWithFormat:@"insert into AccountSettings values(null, '%@','%@' ,'%@', '%@','%@','%@')",driver[@"FirstName"],driver[@"LastName"],driver[@"LicenceNumber"],driver[@"Phone"],driver[@"Email"],driver[@"dotNumber"]];
 
 // Execute the query.
 
 //NSString *query_del = @"delete from TempTable";
 [dbManager executeQuery:query_insert2];
 
 // If the query was successfully executed then pop the view controller.
 if (dbManager.affectedRows != 0) {
 BFLog(@"Query was inserted the data successfully. Affected rows = %d", dbManager.affectedRows);
 //c_flag=1;
 }
 else{
 BFLog(@"Could not execute the query");
 }
 
 NSString *query_insert3 = [NSString stringWithFormat:@"insert into Carrier_Sett values(null, '%@', '%@', '%@')", carrier[@"Carrier"],[NSString stringWithFormat:@"%@,%@,%@,%@", carrier[@"offAddressline1"],carrier[@"offAddressline2"],carrier[@"offAddressline3"],carrier[@"offZip"]],[NSString stringWithFormat:@"%@,%@,%@,%@",carrier[@"homeAddressline1"],carrier[@"homeAddressline2"],carrier[@"homeAddressline3"],carrier[@"homeZip"]] ];
 
 // Execute the query.
 
 //NSString *query_del = @"delete from TempTable";
 [dbManager executeQuery:query_insert3];
 
 // If the query was successfully executed then pop the view controller.
 if (dbManager.affectedRows != 0) {
 BFLog(@"Query was inserted the data successfully. Affected rows = %d", dbManager.affectedRows);
 //c_flag=1;
 }
 else{
 BFLog(@"Could not execute the query");
 }
 }
 }else{
 UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Attention"
 message:[NSString stringWithFormat:@"%ld",error.code]
 delegate:self
 cancelButtonTitle:@"Cancel"
 otherButtonTitles:@"OK", nil];
 
 [alert1 show];
 
 }
 
 }];
 [dataTask resume];
 }else{
 BFLog(@"Device not connected to the internet");
 UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Attention"
 message:@"Please check your internet connectivity!!!"
 delegate:self
 cancelButtonTitle:@"Cancel"
 otherButtonTitles:@"OK", nil];
 
 [alert1 show];
 
 
 }
 
 
 
 
 
 }
 
 
 */







- (IBAction)helpAlert:(id)sender {
    CGRect screenSize = [[UIScreen mainScreen] bounds];
    transparentView_settings  = [[UIView alloc] initWithFrame:screenSize];
    transparentView_settings.backgroundColor=[[UIColor blackColor] colorWithAlphaComponent:0.6];
    UIView *view1=[menu_settings valueForKey:@"view"];
    int tableHeight = [settings_menu frame].origin.y;
    
    UIButton *tutorialLink = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    tutorialLink.frame = CGRectMake(screenSize.origin.x+(screenSize.size.width/3), screenSize.size.height-40, screenSize.size.width/3, 40);
    [tutorialLink addTarget:self action:@selector(showHelp) forControlEvents:UIControlEventTouchUpInside];
    tutorialLink.tintColor = [UIColor yellowColor];
    [tutorialLink setTitle:@"<<Know More>>" forState:UIControlStateNormal];
    
    [transparentView_settings addSubview:tutorialLink];
    
    
    
    UIImageView *arrow1=[[UIImageView alloc]initWithFrame:CGRectMake(view1.frame.origin.x+view1.frame.size.width, view1.frame.origin.y, 50, 50)];
    [arrow1 setImage:[UIImage imageNamed:@"icons/arrow2.png"]];
    UILabel *dashboard  = [[UILabel alloc] initWithFrame:CGRectMake(arrow1.frame.origin.x+arrow1.frame.size.width+20, arrow1.frame.origin.y+arrow1.frame.size.height/2, screenSize.size.width/2, 20)];
    dashboard.text=@"Dashboard";
    dashboard.numberOfLines=2;
    dashboard.textColor=[UIColor whiteColor];
    dashboard.font=[UIFont fontWithName:@"Arial-BoldMT" size:20.0];
    dashboard.minimumScaleFactor=10./dashboard.font.pointSize;
    dashboard.adjustsFontSizeToFitWidth=YES;
    
    UIImageView *arrow2=[[UIImageView alloc]initWithFrame:CGRectMake(screenSize.size.width-50, tableHeight, 50, 50)];
    [arrow2 setImage:[UIImage imageNamed:@"icons/arrow2.png"]];
    
    UILabel *accontLabel  = [[UILabel alloc] initWithFrame:CGRectMake(screenSize.origin.x+50,tableHeight+20 , screenSize.size.width-50, 20)];
    accontLabel.text=@"Edit your account details here";
    accontLabel.numberOfLines=2;
    accontLabel.textColor=[UIColor whiteColor];
    accontLabel.font=[UIFont fontWithName:@"Arial-BoldMT" size:20.0];
    accontLabel.minimumScaleFactor=10./accontLabel.font.pointSize;
    accontLabel.adjustsFontSizeToFitWidth=YES;
    UIImageView *arrow3=[[UIImageView alloc]initWithFrame:CGRectMake(screenSize.size.width-50, tableHeight+70, 50, 50)];
    [arrow3 setImage:[UIImage imageNamed:@"icons/arrow2.png"]];
    
    UILabel *passwordLabel  = [[UILabel alloc] initWithFrame:CGRectMake(screenSize.origin.x+50,tableHeight+70+20 , screenSize.size.width-50, 20)];
    passwordLabel.text=@"Change your password here";
    passwordLabel.numberOfLines=2;
    passwordLabel.textColor=[UIColor whiteColor];
    passwordLabel.font=[UIFont fontWithName:@"Arial-BoldMT" size:20.0];
    passwordLabel.minimumScaleFactor=10./passwordLabel.font.pointSize;
    passwordLabel.adjustsFontSizeToFitWidth=YES;
    UIImageView *arrow4=[[UIImageView alloc]initWithFrame:CGRectMake(screenSize.size.width-50, tableHeight+2*70, 50, 50)];
    [arrow4 setImage:[UIImage imageNamed:@"icons/arrow2.png"]];
    UILabel *carrierLabel  = [[UILabel alloc] initWithFrame:CGRectMake(screenSize.origin.x+50,tableHeight+2*70+20 , screenSize.size.width-50, 20)];
    carrierLabel.text=@"Edit your carrier details here";
    carrierLabel.numberOfLines=2;
    carrierLabel.textColor=[UIColor whiteColor];
    carrierLabel.font=[UIFont fontWithName:@"Arial-BoldMT" size:20.0];
    carrierLabel.minimumScaleFactor=10./carrierLabel.font.pointSize;
    carrierLabel.adjustsFontSizeToFitWidth=YES;
    
    UIImageView *arrow5=[[UIImageView alloc]initWithFrame:CGRectMake(screenSize.size.width-50, tableHeight+3*70, 50, 50)];
    [arrow5 setImage:[UIImage imageNamed:@"icons/arrow2.png"]];
    UILabel *cycleLabel  = [[UILabel alloc] initWithFrame:CGRectMake(screenSize.origin.x+50,tableHeight+3*70+20 , screenSize.size.width-50, 20)];
    cycleLabel.text=@"Edit your cycles details here";
    cycleLabel.numberOfLines=2;
    cycleLabel.textColor=[UIColor whiteColor];
    cycleLabel.font=[UIFont fontWithName:@"Arial-BoldMT" size:20.0];
    cycleLabel.minimumScaleFactor=10./cycleLabel.font.pointSize;
    cycleLabel.adjustsFontSizeToFitWidth=YES;
    UIImageView *arrow6=[[UIImageView alloc]initWithFrame:CGRectMake(screenSize.size.width-50, tableHeight+4*70, 50, 50)];
    [arrow6 setImage:[UIImage imageNamed:@"icons/arrow2.png"]];
    UILabel *searchLabel  = [[UILabel alloc] initWithFrame:CGRectMake(screenSize.origin.x+50,tableHeight+4*70+20 , screenSize.size.width-50, 20)];
    searchLabel.text=@"Search carrier here";
    searchLabel.numberOfLines=2;
    searchLabel.textColor=[UIColor whiteColor];
    searchLabel.font=[UIFont fontWithName:@"Arial-BoldMT" size:20.0];
    searchLabel.minimumScaleFactor=10./searchLabel.font.pointSize;
    searchLabel.adjustsFontSizeToFitWidth=YES;
    [transparentView_settings addSubview:searchLabel];
    [transparentView_settings addSubview:cycleLabel];
    [transparentView_settings addSubview:carrierLabel];
    [transparentView_settings addSubview:passwordLabel];
    [transparentView_settings addSubview:accontLabel];
    [transparentView_settings addSubview:arrow6];
    [transparentView_settings addSubview:arrow5];
    [transparentView_settings addSubview:arrow4];
    [transparentView_settings addSubview:arrow3];
    [transparentView_settings addSubview:arrow2];
    [transparentView_settings addSubview: arrow1];
    [transparentView_settings addSubview:dashboard];
    [self.view addSubview:transparentView_settings];
}

#pragma mark    - <API call>
-(void)callLogOutAPI{
    if([commonDataHandler sharedInstance].strUserName.length <= 0 || [commonDataHandler sharedInstance].strAuthCode.length <= 0){
        DBManager *objDB= [[DBManager alloc] initWithDatabaseFilename:@"hosDB.sql"];
        NSArray *userInfoArray=[[NSArray alloc]initWithArray: [objDB getUserId]];
        NSArray *detailsArray = [userInfoArray firstObject];
        [commonDataHandler sharedInstance].strUserId = detailsArray[0];
        [commonDataHandler sharedInstance].strUserName = detailsArray[1];
        if([commonDataHandler sharedInstance].strAuthCode.length <= 0)
            [commonDataHandler sharedInstance].strAuthCode = detailsArray[[detailsArray count]-2];
    }
    
    NSMutableDictionary *requestDict = [[NSMutableDictionary alloc] init];
    [requestDict setValue:OPERATION_LOGOUT forKey:OPERATION];
    [requestDict setValue:[commonDataHandler sharedInstance].strUserName forKey:USER_NAME];
    [requestDict setValue:[Bugfender deviceIdentifier] forKey:DEVICE_ID];
    [requestDict setValue:[commonDataHandler sharedInstance].strAuthCode forKey:AUTH_CODE];
    NSString *strParam =  [[commonDataHandler sharedInstance] constructInput:requestDict];
    [self getConnection:[NSString stringWithFormat:@"%@%@",WEB_ROOT, LOGOUT_URL] withParams:strParam];
}
-(void)getConnection:(NSString *)urlString withParams:(NSString*)paramString{
    __block NSString *responseString = @"";
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSString *urlStringWithParam = [NSString stringWithFormat:@"payload=%@",paramString];
    NSString *enodedURl = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [NSURL URLWithString:enodedURl] ;
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    
    [request setHTTPBody:[urlStringWithParam dataUsingEncoding:NSUTF8StringEncoding]];
    NSOperationQueue *queue1 = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:request queue:queue1 completionHandler:^(NSURLResponse * _Nullable response, NSData * _Nullable data, NSError * _Nullable connectionError) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        });
        responseString = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
        if(![responseString isEqualToString:@""]){
            NSMutableArray *arrayResponse = [[NSMutableArray alloc] init];
            NSMutableArray *arrayMobileCheckIn   = [NSMutableArray new];
            [arrayMobileCheckIn removeAllObjects];
            [arrayResponse removeAllObjects];
            arrayResponse =[[commonDataHandler sharedInstance] parseResponse:responseString];
            
            if(arrayResponse!=nil && [arrayResponse count]!=0){
                
                for (NSMutableDictionary *objDict in arrayResponse) {
                    MobileCheckIn *objMobileCheckIn = [MobileCheckIn new];
                    [objMobileCheckIn getDataFromAPI:objDict];
                    [arrayMobileCheckIn addObject:objMobileCheckIn];
                }
                if(arrayMobileCheckIn.count > 0){
                    MobileCheckIn *objMob = [arrayMobileCheckIn firstObject];
                    if([objMob.operation isEqualToString:OPERATION_LOGOUT]){
                        if([objMob.result isEqualToString:@"failed"]){
//                            if([objMob.reason isEqualToString:AUTH_CODE_INVALID]){
//                                if(isReAuth){
//                                    isReAuth = NO;
//                                    [[commonDataHandler sharedInstance] reAuthenticateUser:^(BOOL finished) {
//                                        [self callLogOutAPI];
//                                    }];
//                                }
//                            }else{
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@""
                                                                                     message:objMob.reason
                                                                                    delegate:nil
                                                                           cancelButtonTitle:@"OK"
                                                                           otherButtonTitles:nil, nil];
                                    [alert1 show];
                                });
//                            }
                        }else{
                            [[NSUserDefaults standardUserDefaults] removeObjectForKey:SELECTED_VEHICLE];
                            
                            NSUserDefaults *userdata = [NSUserDefaults standardUserDefaults];
                            NSString *previous_user = [userdata objectForKey:@"username"];
                            [userdata setValue:previous_user forKey:@"previousUser"];
                            [userdata removeObjectForKey:@"username"];
                            [userdata removeObjectForKey:@"password"];
                            [userdata removeObjectForKey:@"masteruser"];
                            [userdata removeObjectForKey:@"user"];
                            [commonDataHandler sharedInstance].strUserId = @"";
                            [commonDataHandler sharedInstance].strAuthCode = @"";
                            [commonDataHandler sharedInstance].strUserName = @"";
                            [[NSUserDefaults standardUserDefaults] synchronize];
                            [credentials resetKeychainItem];
                            popup = 0;
                            BFLog(@"username n password deleted");
                            DBManager *objDB= [[DBManager alloc] initWithDatabaseFilename:@"hosDB.sql"];
                            NSArray *userInfoArray=[[NSArray alloc]initWithArray: [objDB getUserId]];
                            int userId = [userInfoArray[0][0] intValue];;
                            [objDB logout:userId];
                        //    [self dismissModalStack];

                            dispatch_async(dispatch_get_main_queue(), ^{
                                UIStoryboard *storyBoard = self.storyboard;
                                UIViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"main1"];
                                [self presentViewController:vc animated:YES completion:nil];
                            });
                           
                        }
                    }
                }
            }
        }
    }];
}
-(void)dismissModalStack {
    dispatch_async(dispatch_get_main_queue(), ^{

        UIViewController *vc = self.presentingViewController;
        while (vc.presentingViewController) {
            vc = vc.presentingViewController;
        }
        [vc dismissViewControllerAnimated:YES completion:^{
           
        }];
        
        [self dismissViewControllerAnimated:YES completion:^{
            UIStoryboard *storyBoard = self.storyboard;
            UIViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"main1"];
            [self presentViewController:vc animated:YES completion:nil];
        }];
    });
   

}
@end
