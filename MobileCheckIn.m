//
//  MobileCheckIn.m
//  MaTrackHOSV1
//
//  Created by sieva networks on 10/27/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import "MobileCheckIn.h"
#import "Header.h"
#import "NSString+NullCheck.h"

@implementation MobileCheckIn
- (instancetype)init
{
    self = [super init];
    if (self) {
        [self initializeData];
    }
    return self;
}
-(void)initializeData{
    self.operation           = @"";
    self.date                = @"";
    self.vehiclestatus       = @"";
    self.associatedvehicle   = @"";
    self.odo                 = @"";
    self.userName            = @"";
    self.userId              = @"";
    self.deviceidentifier    = @"";
    self.slot                = @"";
    self.ack                 = @"";
    self.result              = @"";
    self.reason              = @"";
    self.calculatedstatus    = @"";
    self.driverstatus        = @"";
}
- (void)getDataFromAPI:(NSMutableDictionary *)responseDict{
    self.operation          = [[responseDict valueForKey:OPERATION] NullCheck];
    self.date               = [[responseDict valueForKey:RESPONSE_DATE] NullCheck];
    self.vehiclestatus      = [[responseDict valueForKey:VEHICLE_STATUS] NullCheck];
    self.associatedvehicle  = [[responseDict valueForKey:ASSO_VEHICLE] NullCheck];
    self.odo                = [[responseDict valueForKey:ODO_NUMBER] NullCheck];
    self.userName             = [[responseDict valueForKey:USER_NAME] NullCheck];
    self.deviceidentifier   = [[responseDict valueForKey:DEVICE_ID] NullCheck];
    self.slot               = [[responseDict valueForKey:SLOT] NullCheck];
    self.ack                = [[responseDict valueForKey:ACK] NullCheck];
    self.result             = [[responseDict valueForKey:RESULT] NullCheck];
    self.reason             = [[responseDict valueForKey:REASON] NullCheck];
}
- (void)getDataFromAPILogin:(NSMutableDictionary *)responseDict{
    self.operation          = [[responseDict valueForKey:OPERATION] NullCheck];
    self.authCode           = [[responseDict valueForKey:AUTH_CODE] NullCheck];
    self.result             = [[responseDict valueForKey:RESULT] NullCheck];
    self.reason             = [[responseDict valueForKey:REASON] NullCheck];
}
@end

