//
//  NotificationHandler.m
//  MaTrackHOSV1
//
//  Created by sieva networks on 10/20/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import "NotificationHandler.h"
#import "commonDataHandler.h"

@implementation NotificationHandler

-(void)recievePushNotification:(UIViewController *)currentVC withDriverStatus:(driver_status)driverStat{
    switch (driverStat) {
        case driver_status_driving:
        {
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            StatusViewController *objVC=[storyboard instantiateViewControllerWithIdentifier:@"drivingProgress"];
            objVC.driverStatus = driver_status_driving;
            objVC.isFromPush = YES;
            current_status = @"D";
            [self saveData:@"D"];
            [currentVC presentViewController:objVC animated:YES completion:nil];
        }
            break;
        case driver_status_onDuty:
        {
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            logScreenViewController *objVC=[storyboard instantiateViewControllerWithIdentifier:@"logscreen"];
            objVC.isFromPush = YES;
            objVC.isImmediate = NO;
            objVC.driverStatus = driver_status_onDuty;
//            current_status = @"ON";
//            [self saveData:@"ON"];
            [currentVC presentViewController:objVC animated:YES completion:nil];
        }
            break;
        case driver_status_offDuty:
        {
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            logScreenViewController *objVC=[storyboard instantiateViewControllerWithIdentifier:@"logscreen"];
            objVC.isFromPush = YES;
            objVC.driverStatus = driver_status_offDuty;
            current_status = @"OFF";
            [self saveData:@"OFF"];
            [currentVC presentViewController:objVC animated:YES completion:nil];
        }
            break;
        case driver_status_onDuty_immediate:
        {
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            logScreenViewController *objVC=[storyboard instantiateViewControllerWithIdentifier:@"logscreen"];
            objVC.isFromPush = YES;
            objVC.isImmediate = YES;
            objVC.driverStatus = driver_status_onDuty_immediate;
            current_status = @"ON";
            [self saveData:@"ON"];
            [currentVC presentViewController:objVC animated:YES completion:nil];
        }
            break;
        
        case driver_status_none:
        {
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            logScreenViewController *objVC=[storyboard instantiateViewControllerWithIdentifier:@"logscreen"];
            objVC.isFromPush = YES;
            objVC.driverStatus = driver_status_none;
            current_status = @"NONE";
            [self saveData:@"NONE"];
            [currentVC presentViewController:objVC animated:YES completion:nil];
        }
            break;
        case driver_status_connect_back:
        {
//            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//            StatusViewController *objVC=[storyboard instantiateViewControllerWithIdentifier:@"drivingProgress"];
//            objVC.driverStatus = driverStat;
//            objVC.isFromPush = YES;
//            [currentVC presentViewController:objVC animated:YES completion:nil];
            
            //Call to the server
        }
            break;
        default:
            break;
    }
    
   
}
-(void)saveData:(NSString *)x1{
    DBManager *objDB = [[DBManager alloc] initWithDatabaseFilename:@"hosDB.sql"];
    NSDictionary *token;
    ;
    NSMutableDictionary *whereToken;
    NSArray *userInfoArray=[[NSArray alloc]initWithArray: [objDB getUserId]];
    if([userInfoArray count]!=0){
        int userId = [userInfoArray[0][0] intValue];
        token=@{@"columnName":@"id",@"entry":[NSString stringWithFormat:@"%d",userId]};
        ;
        whereToken=[[NSMutableDictionary alloc] initWithDictionary:token];
        
        //[objDB getLogEditData:whereToken];
        commonDataHandler *common=[commonDataHandler new];
        NSString * date=[common getCurrentTimeInLocalTimeZone];
        token=@{@"userid":[NSString stringWithFormat:@"%d",userId],@"date":[NSString stringWithFormat:@"%@",date]};
        ;
        whereToken=[[NSMutableDictionary alloc] initWithDictionary:token];
        NSArray* dataFromDB=[[NSArray alloc]initWithArray:[objDB getLogEditData:whereToken]];
        NSString *statusString=@"";
        for(int j=0;j<[dataFromDB count];j++){
            statusString= [statusString stringByAppendingString:[NSString stringWithFormat:@"%@",[[dataFromDB objectAtIndex:j] objectAtIndex:1]]];
        }
        
        NSString* charStr=[@"" stringByPaddingToLength:96 withString:@"1" startingAtIndex:0];
        if([dataFromDB count]==0)
            statusString=charStr;
        else
        {
            if([statusString isEqualToString:@""])
                statusString=charStr;
            
        }
        statusString=[common modifyData:statusString date:date];
        NSString *statusStringTemp=[statusString stringByReplacingOccurrencesOfString:@"0" withString:@""];
        int currentSlot=(int)statusStringTemp.length;
        statusString=[statusString stringByReplacingCharactersInRange:NSMakeRange((int)(currentSlot-1), 1) withString:[self getStatusCode:x1]];
        
        loggraph *objLog=[loggraph new];
        for(int i=0;i<statusString.length;i++){
            objLog.slot=[NSString stringWithFormat:@"%d",(int)(1+i)];
            objLog.driverStatus=[statusString substringWithRange:NSMakeRange(i, 1)];
            objLog.calculatedStatus=[statusString substringWithRange:NSMakeRange(i, 1)];
            objLog.date=date;
            objLog.userId=[NSString stringWithFormat:@"%d",userId];
            objLog.insert=[NSString stringWithFormat:@"%d",1];
            [objDB insertPastStatus:objLog];
        }
        if(!objDB.affectedRows){
            current_status=x1;
        }
        
    }
    
}
-(NSString *)getStatusCode:(NSString *)status_new{
    NSString *statusNew;
    if([status_new isEqualToString:@"D"])
        statusNew=@"7";
    else if([status_new isEqualToString:@"SB"])
        statusNew=@"6";
    else if([status_new isEqualToString:@"OFF"] || [status_new isEqualToString:@"PC"])
        statusNew=@"5";
    else if([status_new isEqualToString:@"ON"] || [status_new isEqualToString:@"YM"])
        statusNew=@"8";
    return statusNew;
}
@end
