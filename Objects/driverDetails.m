//
//  driverDetails.m
//  hos
//
//  Created by Minnu Mohandas on 25/09/17.
//  Copyright © 2017 Minnu Mohandas. All rights reserved.
//

#import "driverDetails.h"

@implementation driverDetails
@synthesize fname,lname,license,dotNum,email,phone, dbManager;

-(NSString*)getFirstName{
    return fname;
}

-(NSString*)getLastName{
    return lname;
}

-(NSString*)getLicense{
    return license;
}

-(NSString*)getDotNum{
    return dotNum;
}

-(NSString*)getEmail{
    return email;
}

-(NSString*)getPhone{
    return phone;
}

-(void)setFirstName:(NSString *)firstName{
    fname=firstName;
}

-(void)setLastName:(NSString *)lastName{
    lname=lastName;
}

-(void)setLicense:(NSString *)licenseId{
    license=licenseId;
}

-(void)setDotNum:(NSString *)dotNumber{
    dotNum=dotNumber;
}

-(void)setEmail:(NSString *)emailId{
    email=emailId;
}

-(void)setPhone:(NSString *)phoneNum{
    phone=phoneNum;
}

-(void)insertIntoTable:(NSMutableArray*)objectArray{
    
    for (NSInteger i=0; i<[objectArray count]; i++) {
        
        dbManager = [[DBManager1 alloc] initWithDatabaseFilename:@"hosDB.sqlite"];
        
        NSString *query_insert = [NSString stringWithFormat:@"INSERT INTO driverDetails ('firstName','lastName','license',dot, email, phone) VALUES ('%@','%@','%@','%@','%@','%@')",[objectArray[i] getFirstName],[objectArray[i] getLastName],[objectArray[i] getLicense],[objectArray[i] getDotNum],[objectArray[i] getEmail],[objectArray[i] getPhone]];
        
        // Execute the query.
        
        
        [dbManager executeQuery:query_insert];
        
        
        if (dbManager.affectedRows != 0) {
            NSLog(@"Inserted Succesfully");
        }else{
            NSLog(@"Error");
            
        }
        
    }
    
    }

-(void)deleteFromTable:(NSString*)license{
        
        dbManager = [[DBManager1 alloc] initWithDatabaseFilename:@"hosDB.sqlite"];
        
        NSString *query_delete= [NSString stringWithFormat:@"DELETE FROM driverDetails where license=%@",license];
        
        // Execute the query.
        
        
        [dbManager executeQuery:query_delete];
        
        
        if (dbManager.affectedRows != 0) {
            NSLog(@"Deleted Succesfully");
        }else{
            NSLog(@"Error");
            
        }
    
}


@end

