//
//  Carrier.h
//  MaTrackHOSV1
//
//  Created by Minnu Mohandas on 12/10/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Carrier : NSObject
@property (nonatomic, strong) NSString *identifier;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *mainAddress;
@property (nonatomic, strong) NSString *mainCity;
@property (nonatomic, strong) NSString *mainState;
@property (nonatomic, strong) NSString *homeAddress;
@property (nonatomic, strong) NSString *mainZip;
@property (nonatomic, strong) NSString *homeCity;
@property (nonatomic, strong) NSString *homeState;
@property (nonatomic, strong) NSString *homeZip;
@end
