//
//  Carrier.h
//  MaTrackHOSV1
//
//  Created by Minnu Mohandas on 12/10/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DVIR : NSObject
@property (nonatomic, strong) NSString *identifier;
@property (nonatomic, strong) NSString *logid;
@property (nonatomic, strong) NSString *type;
@property (nonatomic, strong) NSString *defectCode;
@property (nonatomic, strong) NSString *driverSign;
@property (nonatomic, strong) NSString *mechSign;
@end
