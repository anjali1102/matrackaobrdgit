//
//  Carrier.m
//  MaTrackHOSV1
//
//  Created by Minnu Mohandas on 12/10/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import "DVIR2Defects.h"


@implementation DVIR2Defects
@synthesize identifier, driverid, reason, dvirProperty;

-(NSString*)getId{
    return identifier;
}

-(NSString*)getDriverId{
    return driverid;
}

-(NSString*)getReason{
    return reason;
}

-(NSString*)getDvirProperty{
    return dvirProperty;
}

-(NSString*)setId{
    return identifier;
}

-(NSString*)setDriverId{
    return driverid;
}

-(NSString*)setReason{
    return reason;
}

-(NSString*)setDvirProperty{
    return dvirProperty;
}

@end
