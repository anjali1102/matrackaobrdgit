//
//  Carrier.m
//  MaTrackHOSV1
//
//  Created by Minnu Mohandas on 12/10/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import "DVIRDefects.h"

@implementation DVIRDefects

@synthesize identifier, defectdescription;

-(NSString*)getId{
    return identifier;
}

-(NSString*)getDefectDescription{
    return defectdescription;
}

-(void)setId:(NSString *)Id{
    identifier=Id;
}

-(void)setDefectdescription:(NSString *)Defectdescription{
    defectdescription=Defectdescription;
}


@end
