//
//  Carrier.h
//  MaTrackHOSV1
//
//  Created by Minnu Mohandas on 12/10/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Log : NSObject
@property (nonatomic, strong) NSString *identifier;
@property (nonatomic, strong) NSString *userid;
@property (nonatomic, strong) NSString *carrierid;
@property (nonatomic, strong) NSString *distance;
@property (nonatomic, strong) NSString *codriver;
@property (nonatomic, strong) NSString *destination;
@property (nonatomic, strong) NSString *notes;
@property (nonatomic, strong) NSString *date;
@property (nonatomic, strong) NSString *timezone;
@property (nonatomic, strong) NSString *driverStatus;
@property (nonatomic, strong) NSString *annotation;
@property (nonatomic, strong) NSString *vehicleStatus;
@property (nonatomic, strong) NSString *calculateStatus;
@property (nonatomic, strong) NSString *orgin;
@property (nonatomic, strong) NSString *carriername;
@property (nonatomic, strong) NSString *carrierhome;
@property (nonatomic, strong) NSString *carriermain;
@end
