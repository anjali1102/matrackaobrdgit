//
//  Carrier.m
//  MaTrackHOSV1
//
//  Created by Minnu Mohandas on 12/10/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import "Log.h"


@implementation Log
@synthesize identifier;
@synthesize  userid;
@synthesize carrierid;
@synthesize distance;
@synthesize codriver;
@synthesize destination;
@synthesize notes;
@synthesize date;
@synthesize timezone;
@synthesize driverStatus;
@synthesize annotation;
@synthesize vehicleStatus;
@synthesize calculateStatus;
@synthesize orgin;
@synthesize carriermain;
@synthesize carrierhome;
@synthesize carriername;
-(NSString*)getId{
    return identifier;
}

-(NSString*)getUserid{
    return userid;
}

-(NSString*)getCarrierid{
    return carrierid;
}

-(NSString*)getDistance{
    return distance;
}

-(NSString*)getCodriver{
    return codriver;
}

-(NSString*)getDestination{
    return destination;
}

-(NSString*)getNotes{
    return notes;
}

-(NSString*)getDate{
    return date;
}

-(NSString*)getTimezone{
    return timezone;
}

-(NSString*)getAnnotation{
    return annotation;
}

-(NSString*)getDriverStatus{
    return driverStatus;
}

-(NSString*)getVehicleStatus{
    return vehicleStatus;
}

-(NSString*)getOrgin{
    return orgin;
}

-(void)setDate:(NSString *)Date{
    date=Date;
}

-(void)setOrgin:(NSString *)Orgin{
    orgin=Orgin;
}
@end
