//
//  Carrier.h
//  MaTrackHOSV1
//
//  Created by Minnu Mohandas on 12/10/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject
@property (nonatomic, strong) NSString *identifier;
@property (nonatomic, strong) NSString *username;
@property (nonatomic, strong) NSString *password;
@property (nonatomic, strong) NSString *firstname;
@property (nonatomic, strong) NSString *lastname;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *mobile;
@property (nonatomic, strong) NSString *driverid;
@property (nonatomic, strong) NSString *homeTimezone;
@property (nonatomic, strong) NSString *cycle;
@property (nonatomic, strong) NSString *licenseNo;
@property (nonatomic, strong) NSString *dotNo;
@property (nonatomic, strong) NSString *authCode;
@property (nonatomic, strong) NSString *loggedIn;
@property (nonatomic, strong) NSString *restartHour;
@property (nonatomic, strong) NSString *breakHour;
@end
