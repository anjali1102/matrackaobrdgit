//
//  Carrier.m
//  MaTrackHOSV1
//
//  Created by Minnu Mohandas on 12/10/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import "user2carrier.h"

@implementation user2carrier
@synthesize identifier, userid, carrierid;

-(NSString*)getId{
    return identifier;
}

-(NSString*)getUserid{
    return userid;
}

-(NSString*)getCarrierid{
    return carrierid;
}

-(void)setId:(NSString *)Id{
    identifier=Id;
}

-(void)setUserId:(NSString *)Userid{
    userid=Userid;
}

-(void)setCarrierId:(NSString *)Carrierid{
    carrierid=Carrierid;
}


@end
