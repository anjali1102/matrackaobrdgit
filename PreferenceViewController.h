//
//  PreferenceViewController.h
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 7/29/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "DBManager1.h"
#import "DBManager/DBManager.h"
#import "KeychainItemWrapper.h"
#import "preferences.h"
#import "DBManager1.h"
#import "loginViewController.h"
#import "AppDelegate.h"
@interface PreferenceViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>{
    UITextField *location;
    UITextField *orientation;
    
}
@property (weak, nonatomic) IBOutlet UITableView *preferenceList;
- (IBAction)savePreference:(id)sender;
//@property (nonatomic, strong) DBManager1 *dbManager;
@end
