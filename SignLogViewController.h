//
//  SignLogViewController.h
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 7/28/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SignaturePad.h"
#import "previousDayViewController.h"
#import "DBManager1.h"
#import "CaptureSignatureViewController.h"
#import "logsmenuscreen.h"
#import "HOSRecap.h"
#import "DOT/Recap/Recap.h"
#import "DOT/DOTInspection/DOTInspection.h"
#import "GraphPoint.h"
#import "approve.h"
extern CGRect saveBtnY,saveBtnHeight;
extern CGRect editBtnY,editBtnHeight;
extern CGRect signAreaY;
@interface SignLogViewController : UIViewController<UIAlertViewDelegate>
- (IBAction)save:(id)sender;
@property (nonatomic, strong) DBManager1 *dbManager;
@property (weak, nonatomic) IBOutlet UIImageView *signatureImageView;
@property (weak, nonatomic) IBOutlet UIButton *editBtn;
@property (weak, nonatomic) IBOutlet UIButton *saveBtn;
@property (weak, nonatomic) IBOutlet UILabel *certifyLabel;

@end
