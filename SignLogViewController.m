//
//  SignLogViewController.m
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 7/28/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//

#import "SignLogViewController.h"
#import "KeychainItemWrapper.h"
#import "insertDutyCycle.h"
#import "Header.h"
#import "commonDataHandler.h"
#import "DOTInspection.h"
#import "TestDataXML.h"
#import "MBProgressHUD.h"

@interface SignLogViewController ()
{
    NSString *strXMTemplate;
    BOOL isReAuth;
}
@end
NSMutableArray *data_to_finaldb;
NSMutableDictionary *data_dict;
UIImage *sign_image;
CGRect saveBtnY,saveBtnHeight;
CGRect editBtnY,editBtnHeight;
CGRect signAreaY;
@implementation SignLogViewController
@synthesize dbManager;
@synthesize saveBtn;
@synthesize editBtn;
@synthesize certifyLabel;

- (void)viewDidLoad {
    [super viewDidLoad];
    saveBtn.layer.cornerRadius=20.0;
    editBtn.layer.cornerRadius=20.0;
    saveBtnY=saveBtn.frame;
    editBtnY=editBtn.frame;
    certifyLabel.minimumScaleFactor=8./certifyLabel.font.pointSize;
    certifyLabel.adjustsFontSizeToFitWidth=YES;

    signAreaY=_signatureImageView.frame;
    NSUserDefaults *sign=[NSUserDefaults standardUserDefaults];
    if ([sign objectForKey:@"sign"]!=nil) {
        NSString *sign_string = [sign objectForKey:@"sign"];
        NSData *data=[[NSData alloc]initWithBase64EncodedString:sign_string options:NSDataBase64DecodingIgnoreUnknownCharacters];
        sign_image=[UIImage imageWithData:data];
    }
    
    data_to_finaldb = [[NSMutableArray alloc] init];
//  clearSign = [[SignaturePad alloc] init];
    dbManager = [[DBManager1 alloc] initWithDatabaseFilename:@"HOSDatabase.sql"];
    BFLog(@"date:%@",current_date);
    
    CGFloat borderWidth = 2.0f;
    
    self.signatureImageView.layer.borderColor = [UIColor grayColor].CGColor;
    self.signatureImageView.layer.borderWidth = borderWidth;
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    NSUserDefaults *sign=[NSUserDefaults standardUserDefaults];
    if ([sign objectForKey:@"sign"]!=nil) {
        _signatureImageView.image=sign_image;}
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//implementation of delegate method
- (void)processCompleted:(UIImage*)signImage
{
   // _signatureImageView.image = signImage;
    [super viewDidLoad];
    NSUserDefaults *sign=[NSUserDefaults standardUserDefaults];
    if ([sign objectForKey:@"sign"]!=nil) {
        _signatureImageView.image=sign_image;}
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier hasPrefix:@"view_to_capture"]) {
        CaptureSignatureViewController *destination = segue.destinationViewController;
        destination.delegate = self;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)getthedata:(NSString *)date{


    NSString *query1 = [NSString stringWithFormat:@"select * from TempTable where date='%@'",date];
    
    // Get the results.
    NSArray *data = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query1]];
    BFLog(@"Result:%@",data);
    if ([data count]!=0) {
        NSString *dbresult = data[[data count]-1][2];
        
        
        
        BFLog(@"string:%@",dbresult);
        
        
        //BFLog(@"timezone:%lu",(unsigned long)[dbresult count]);
        NSData *data2= [dbresult dataUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *json_log1 = [NSJSONSerialization JSONObjectWithData:data2 options:0 error:nil];
        BFLog(@"Dictionary of db data:%@",json_log1);
        if ([json_log1 count]==0) {
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@" " withString:@""];
            dbresult = [NSString stringWithFormat:@"[%@]",dbresult];
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@"\";" withString:@";"];
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@"=\"" withString:@"="];
            
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@"location=(" withString:@"\"location\":["];
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@"payload=(" withString:@"\"payload\":["];
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@");" withString:@"],"];
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@";" withString:@"\","];
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@"=" withString:@":\""];
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@"id" withString:@"\"id\""];
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@"value" withString:@"\"value\""];
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@"posx" withString:@"\"posx\""];
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@"posy" withString:@"\"posy\""];
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@"timezone" withString:@"\"timezone\""];
            NSData *data2= [dbresult dataUsingEncoding:NSUTF8StringEncoding];
            json_log1 = [NSJSONSerialization JSONObjectWithData:data2 options:0 error:nil];
            BFLog(@"Dictionary of db data2:%@",json_log1);
            
        }
        
        
        //[location_array removeAllObjects];
        //   BFLog(@"data portion:%@",[json_log1 allKeys]);
        // NSMutableArray *dict=[[NSMutableArray alloc] init];
        NSMutableArray *dict = [json_log1 objectForKey:@"payload"];
        location_array = [json_log1 objectForKey:@"location"];
        //BFLog(@"Location array:%@",location_array);
        int json_size = (int)[dict count];
        BFLog(@"inner json count:%d",json_size);
        [data_to_finaldb removeAllObjects];
        for (int i=0; i<json_size; i++) {
            
            BFLog(@"posx:%@,%d",[dict[0] objectForKey:@"posx"],i);
            float posx=[[dict[i] objectForKey:@"posx"] floatValue];
            float x1 = posx - 40.0;
            int x2 = x1/12.25;
            int x3 = x2/4.0;
            int x4 = x2%4;
            
            /* NSString *minutes;
             if (15*x4 <15) {
             minutes=@"00";
             }
             else if(15*x4 >=15 && 15*x4 <30){
             minutes=@"15";
             }else if(15*x4 >=30 && 15*x4 <45){
             minutes=@"30";
             }else if(15*x4 >=45 && 15*x4 <60){
             minutes=@"45";
             }*/
            NSString *posx_1 = [NSString stringWithFormat:@"%d:%d",x3,15*x4];
            
            // float time = [posx_1 floatValue];
            int event_no =(int)[[dict[i] objectForKey:@"posy"] integerValue];
            NSString * event;
            if (event_no == 245) {
                event=@"ON";
            }
            else if(event_no == 125){
                event=@"SB";
                
            }else if (event_no == 185){
                event = @"D";
            }else if (event_no == 65){
                event = @"OFF";
            }
            NSArray *final_data = [[NSArray alloc] initWithObjects:posx_1,event, nil];
            [data_to_finaldb addObject:final_data];
            
            
            
        }
        NSString *last_status =data_to_finaldb[[data_to_finaldb count]-1][1];
        [data_to_finaldb removeLastObject];
        NSArray *last = [[NSArray alloc] initWithObjects:@"24:00",last_status, nil];
        [data_to_finaldb addObject:last];
        
    }
    BFLog(@"data array 4:%@",data_to_finaldb);
    
        
    
}
-(BOOL)checkRequired{
    BOOL general = NO;
    BOOL carrier = NO;
    
    NSString *query1 = [NSString stringWithFormat:@"select * from GeneralSettings"];
    NSArray *values = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query1]];
    
    NSString *query2 = [NSString stringWithFormat:@"select * from Carrier_Sett"];
    
    // Get the results.
    
    NSArray *values1 = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query2]];

    if([values count]!=0 && [values1 count]!=0){
        
        NSArray *carrierhome = [values1[0][2] componentsSeparatedByString:@","];
         NSArray *carriermain = [values1[0][3] componentsSeparatedByString:@","];
        if([carrierhome containsObject:@""] || [carriermain containsObject:@""] || [values1[0][1] isEqualToString:@""]){
            
            carrier =NO;
        }else{
            
            carrier = YES;
        }
        
        if([values[0][1] isEqualToString:@""] || [values[0][3] isEqualToString:@""] || [values[0][5] isEqualToString:@""]){
            
            
            general=NO;
            
        }else{
            general = YES;
        }
        
        
        
    }else{
        
        general = NO;
        carrier = NO;
    }
    
    if(general && carrier){
        
     return YES;
    }else{
        
        
         return NO;
    }
   
    
    
}

- (IBAction)save:(id)sender {
    BOOL requiredFilled = [self checkRequired];
    
    /*(if(!requiredFilled){
        UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@""
                                                         message:@"Required fields cannot be empty!"
                                                        delegate:self
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles:nil, nil];
        
        [alert1 show];

        
    }else{
    
    if(_signatureImageView.image!=nil){
      */  UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Driver's Certification of Own Records" message:@"I hereby certify that my data entries and my record of duty status for this 24-hour period are true and correct." delegate:self cancelButtonTitle:@"Agree" otherButtonTitles:@"Not Ready",nil];
        [alert show];
        
      /*  }else{
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:@"Please sign before approving the log!!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil,nil];
        [alert show];
        
        
    }
    
    }
*/
    
}


-(void)savetoFinal:(NSMutableArray *)status{
    autoLoginCount=0;
    int flag_t1=0;
    int flag_f1=0;
    
    BFLog(@"data:%@",status);
    NSString *status1=status[[status count]-1][1];
   // NSString *location = _location.text;
   // int id1 =(int) [dataarray count]-1;
    // if ([location_array count]==0) {
    //NSMutableDictionary *dict1 = [[NSMutableDictionary alloc] init];
    //[dict1 setValue:@(id1) forKey:@"id"];
    //[dict1 setValue:location forKey:@"location"];
    //  [location_array addObject:dict1];
    //}else{
    
    
    
    //}
    NSMutableArray *outer_array = [[NSMutableArray alloc] init];
    NSMutableArray *location_array = [[NSMutableArray alloc] init];
    NSMutableDictionary *data_dict = [[NSMutableDictionary alloc] init];
    
    for (int i=0; i<[status count]; i++) {
        
        NSString *x12=status[i][0];
        NSArray *time = [x12 componentsSeparatedByString:@":"];
        int x4 =(int) [time[1] integerValue]/15;
        int x2 =(int) [time[0] integerValue]*4+x4;
        float x1 = 12.25*x2;
        float posx = x1+40.0;
        int posy = 65;
        BFLog(@"posx:%.2f",posx);
        if ([status[i][1] isEqualToString:@"OFF"]) {
            posy = 65;
        }else if([status[i][1] isEqualToString:@"ON"]){
            posy = 245;
        }else if([status[i][1] isEqualToString:@"D"]){
            posy=185;
        }else if ([status[i][1] isEqualToString:@"SB"]){
            posy=125;
        }
        
        NSMutableDictionary *cont1 = [[NSMutableDictionary alloc] init];
        [cont1 setValue:@(i) forKey:@"id"];
        [cont1 setValue:@(posx) forKey:@"posx"];
        [cont1 setValue:@(posy) forKey:@"posy"];
        
        [outer_array addObject:cont1];
        
        
        
    }
    
    [data_dict setObject:outer_array forKey:@"payload"];
    [data_dict setObject:location_array forKey:@"location"];
    
    BFLog(@"dictionary:%@",data_dict);
    NSData *data = [NSJSONSerialization dataWithJSONObject:data_dict options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonStr = [[NSString alloc] initWithData:data
                                              encoding:NSUTF8StringEncoding];
    BFLog(@"JSON:%@",jsonStr);
  /*  NSString *query1 = [NSString stringWithFormat:@"select * from FinalTable where date='%@'",current_date];
    
    // Get the results.
    
    NSArray *values = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query1]];
    
    if ([values count]!=0) {
        
        
        
        NSString *query_update = [NSString stringWithFormat:@"UPDATE FinalTable SET data='%@',last_status='%@',flag='%@' WHERE date='%d'",jsonStr,status1,current_date,1];
        
        // Execute the query.
        
        //NSString *query_del = @"delete from TempTable";
        [dbManager executeQuery:query_update];
        
        // If the query was successfully executed then pop the view controller.
        if (dbManager.affectedRows != 0) {
            BFLog(@"Query was executed successfully. Affected rows = %d", dbManager.affectedRows);
            flag_f1=1;
            
        }
        else{
            BFLog(@"Could not execute the query");
        }}
    
    else{
        
        NSString *query_insert1 = [NSString stringWithFormat:@"insert into FinalTable values(null, '%@', '%@', '%@' ,'%d','%d')", current_date,jsonStr,status1,1,0];
        
        // Execute the query.
        
        //NSString *query_del = @"delete from TempTable";
        [dbManager executeQuery:query_insert1];
        
        // If the query was successfully executed then pop the view controller.
        if (dbManager.affectedRows != 0) {
            BFLog(@"Query was executed successfully. Affected rows = %d", dbManager.affectedRows);
            flag_f1=1;
            
        }
        else{
            BFLog(@"Could not execute the query");
        }
        
        
        
    }
    */
    NSString *query_update = [NSString stringWithFormat:@"UPDATE TempTable SET flag='%d' WHERE date='%@'",1,current_date];
    
    // Execute the query.
    
    //NSString *query_del = @"delete from TempTable";
    [dbManager executeQuery:query_update];
    
    // If the query was successfully executed then pop the view controller.
    if (dbManager.affectedRows != 0) {
        BFLog(@"Query was executed successfully. Affected rows = %d", dbManager.affectedRows);
        flag_t1=1;
        
    }
    else{
        BFLog(@"Could not execute the query");
    }
    
    if (flag_t1==1) {
        
        
        [self saveCurrent:data_dict date:current_date];
    }


}
-(void)saveToServer:(NSMutableDictionary *)driver_data date:(NSString *)date_graph{
    
//    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
//    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration];
//    //getting authCode
//    NSString *query0 = [NSString stringWithFormat:@"select * from authDetails;"];
//    NSArray *authData = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query0]];
//    NSString *authCode = @"";
//    if([authData count]!=0){
//        authCode = authData[0][1];
//        
//    }
//    NSString *urlString = [NSString stringWithFormat:@"http://www.gpstracking-server1.com/gpstracking/hosapp/saveHosGraphData.php?masterUser=%@&user=%@&date=%@&approve=1&authCode=%@",masterUser,user,date_graph,authCode];
//    // NSString *urlString = [NSString stringWithFormat:@"http://23.239.28.100/mydealership/saveHosGraphData_app.php?masterUser=%@&user=%@&date=%@&approve=0",master_menu,user_menu,current_date];
//    NSURL *url = [NSURL URLWithString:urlString];
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
//    [request setHTTPMethod:@"HEAD"];
//    NSURLResponse *response;
//    NSError *error;
//    NSData *mydata = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
//    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
//    if(httpResponse!=nil){
//        if ([httpResponse statusCode] ==200 ) {
//            BFLog(@"Device connected to the internet");
//            NSURL *url = [NSURL URLWithString:urlString];
//            
//            //NSError *error;
//            
//            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
//                                                                   cachePolicy:NSURLRequestUseProtocolCachePolicy
//                                                               timeoutInterval:60.0];
//            
//            [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
//            [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
//            
//            [request setHTTPMethod:@"POST"];
//            /* NSDictionary *mapData = [[NSDictionary alloc] initWithObjectsAndKeys: @"TEST IOS", @"name",
//             @"IOS TYPE", @"typemap",
//             nil];*/
//            
//            //NSMutableDictionary *currentStatus=[[NSMutableDictionary alloc]init];
//            //[currentStatus setValue:status forKey:@"Status"];*/
//            /*  NSMutableArray *inner = [[NSMutableArray alloc] initWithArray:@[@"00:00",@"OFF"]];
//             NSMutableDictionary *data_dict = [[NSMutableDictionary alloc] init];
//             [data_dict setObject:inner forKey:@"payload"];
//             NSError *error;*/
//            NSData *postData = [NSJSONSerialization dataWithJSONObject:driver_data options:0 error:&error];
//            [request setHTTPBody:postData];
//            
//            NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
//                
//                NSDictionary *responseData = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
//                if(error==nil){
//                    
//                    UIAlertController *alert1 = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Modification Saved" preferredStyle:UIAlertControllerStyleAlert];
//                    UIAlertAction *ok1= [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
//                        [alert1 dismissViewControllerAnimated:YES completion:nil];
//                    }];
//                    
//                    
//                    [alert1 addAction:ok1];
//                   // [self presentViewController:alert1 animated:YES completion:nil];
//                    
//                    
//                    
//                }
//
//                
//            }];
//            
//            [postDataTask resume];
//        }else if([httpResponse statusCode]==403){
//            if(autoLoginCount<=1){
//                insertDutyCycle *obj = [insertDutyCycle new];
//                BOOL success =  [obj autoLogin];
//                
//                if(success){
//                    [self saveToServer:driver_data date:date_graph];
//                    
//                }else{
//                    KeychainItemWrapper *credentials = [[KeychainItemWrapper alloc] initWithIdentifier:@"credentials" accessGroup:nil];
//                    [credentials resetKeychainItem];
//                    UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Session Expired!!"
//                                                                     message:@"Please Login Again"
//                                                                    delegate:self
//                                                           cancelButtonTitle:@"OK"
//                                                           otherButtonTitles:nil, nil];
//                    alert1.tag = 1;
//                    alert1.delegate = self;
//                    
//                    [alert1 show];
//                    menuScreenViewController *obj = [menuScreenViewController new];
//                    [obj syncServerLater:driver_data date:date_graph];
//                    
//                }
//            }else{
//                KeychainItemWrapper *credentials = [[KeychainItemWrapper alloc] initWithIdentifier:@"credentials" accessGroup:nil];
//                [credentials resetKeychainItem];
//                UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Session Expired!!"
//                                                                 message:@"Please Login Again"
//                                                                delegate:self
//                                                       cancelButtonTitle:@"OK"
//                                                       otherButtonTitles:nil, nil];
//                alert1.tag = 2;
//                alert1.delegate = self;
//                
//                [alert1 show];
//                menuScreenViewController *obj = [menuScreenViewController new];
//                [obj syncServerLater:driver_data date:date_graph];
//                
//                
//                
//            }
//            
//        }else if([httpResponse statusCode]==503){
//            BFLog(@"Device not connected to the internet");
//            UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Service Unavailable"
//                                                             message:@"No connectivity with server at this time,it will sync automatically"
//                                                            delegate:self
//                                                   cancelButtonTitle:@"OK"
//                                                   otherButtonTitles:nil, nil];
//            
//            [alert1 show];
//            menuScreenViewController *obj = [menuScreenViewController new];
//            [obj syncServerLater:driver_data date:date_graph];
//            
//        }
//    }
//    
}
-(void)getDataForDOT:(NSString *)dateValue withIsApproved:(NSString *)isApproved{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0), ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        });
        //call the database insertion method here
        NSDictionary *token;
        NSMutableDictionary *whereToken;
        DBManager *objDB= [[DBManager alloc] initWithDatabaseFilename:@"hosDB.sql"];
        NSArray *userInfoArray=[[NSArray alloc]initWithArray: [objDB getUserId]];
        if([userInfoArray count]!=0){
            int userId = [userInfoArray[0][0] intValue];
            token=@{@"date":[NSString stringWithFormat:@"%@",dateValue],@"id":[NSString stringWithFormat:@"%d",userId],};
            whereToken=[[NSMutableDictionary alloc] initWithDictionary:token];
            NSMutableDictionary * dotValues=[[NSMutableDictionary alloc]initWithDictionary:[objDB getDOTInspectionData:whereToken]];
            token=@{@"columnName":@"id",@"entry":[NSString stringWithFormat:@"%d",userId]};
            ;
            whereToken=[[NSMutableDictionary alloc] initWithDictionary:token];
            
            token=@{@"userid":[NSString stringWithFormat:@"%d",userId],@"date":[NSString stringWithFormat:@"%@",dateValue]};
            ;
            whereToken=[[NSMutableDictionary alloc] initWithDictionary:token];
            approve *appObj=[approve new];
            appObj.approve=[NSString stringWithFormat:@"1"];
            appObj.date=[NSString stringWithFormat:@"%@",dateValue];
            appObj.userid=[NSString stringWithFormat:@"%d",userId];
            NSArray* dataFromDB=[[NSArray alloc]initWithArray:[objDB getLogEditData:whereToken]];
            NSString *statusString=@"";
            for(int j=0;j<[dataFromDB count];j++){
                if([dataFromDB[j][1] isEqualToString:@""])
                    statusString= [statusString stringByAppendingString:[NSString stringWithFormat:@"1"]];
                else
                    statusString= [statusString stringByAppendingString:[NSString stringWithFormat:@"%@",[self getStatusToSend:[[dataFromDB objectAtIndex:j] objectAtIndex:1]]]];
                
            }
            commonDataHandler *common=[commonDataHandler new];
            statusString=[common modifyData:statusString date:dateValue];
            NSString* charStr=[@"" stringByPaddingToLength:96 withString:@"1" startingAtIndex:0];
            if([dataFromDB count]==0)
                statusString=charStr;
            else
            {
                if([statusString isEqualToString:@""])
                    statusString=charStr;
                
            }
            HOSRecap *objRecap=[HOSRecap new];
            NSArray*dateList=[[NSArray alloc] initWithArray:[dateValue componentsSeparatedByString:@"/"]];
            [objRecap getRecap:[NSString stringWithFormat:@"%@-%@-%@ 00:00:00",dateList[2],dateList[0],dateList[1]]];
            NSMutableDictionary * recapForCycle=[[NSMutableDictionary alloc]initWithDictionary:[objRecap returnRecapData:[NSString stringWithFormat:@"%@-%@-%@ 00:00:00",dateList[2],dateList[0],dateList[1]]]];
            NSString * statusWithoutFuture=@"";
            for(int i=0;i<96;i++){
                NSString *status=[statusString substringWithRange:NSMakeRange(i, 1)];
                if([status isEqualToString:@"0"]){
                    statusWithoutFuture=[statusWithoutFuture stringByAppendingString:[NSString stringWithFormat:@""]];
                }
                else
                    statusWithoutFuture=[statusWithoutFuture stringByAppendingString:status];
                
            }
            
            DOTInspection *objDOT = [DOTInspection new];
            [objDOT getTestDataWithCommonData:dotValues recapData:recapForCycle andGraphPoint:statusWithoutFuture HavingDBValue:dataFromDB forDate:dateValue];
            TestDataXML *objTestXML = [TestDataXML new];
            strXMTemplate = [objTestXML saveDOTInspToXML:objDOT];
            [self dotInspectionAPICall:isApproved];
            loggraph *objLog=[loggraph new];
            for(int i=0;i<statusString.length;i++){
                objLog.slot=[NSString stringWithFormat:@"%d",(int)(1+i)];
                objLog.driverStatus=[statusString substringWithRange:NSMakeRange(i, 1)];
                objLog.calculatedStatus=[statusString substringWithRange:NSMakeRange(i, 1)];
                objLog.date=dateValue;
                objLog.userId=[NSString stringWithFormat:@"%d",userId];
                objLog.insert=[NSString stringWithFormat:@"%d",0];
                if(i==0)
                    objLog.approve=[NSString stringWithFormat:@"%d",1];
                [objDB insertPastStatus:objLog];
            }
            
            
        }
    });

}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if([[alertView buttonTitleAtIndex:buttonIndex] isEqualToString:@"Agree"]){

        [self getthedata:current_date];
        // [self savetoFinal:data_to_finaldb];
        // DOTInspection *objDOT = [DOTInspection new];
        //[objDOT getTestData];
        //TestDataXML *objTestXML = [TestDataXML new];
        //strXMTemplate = [objTestXML saveDOTInspToXML:objDOT];
        //fetch data
        [self getDataForDOT:dateValue withIsApproved:@"true"];
        //fetch data
      //  [self dotInspectionAPICall];
    }
    else{
        if(alertView.tag ==1 || alertView.tag ==2){
            
            if(buttonIndex==[alertView cancelButtonIndex]){
                
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                    UIViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"loginscreen"];
                    [self presentViewController:vc animated:YES completion:nil];
                });
                
            }
            
            
        }
        if([[alertView buttonTitleAtIndex:buttonIndex] isEqualToString:@"Not Ready"]){

            [self getthedata:current_date];
            [self getDataForDOT:dateValue withIsApproved:@"false"];
        }
        
        
    }
}
-(void)saveCurrent:(NSMutableDictionary *)data date:(NSString *)date_graph{
      // dispatch_group_t group = dispatch_group_create();
    dispatch_queue_t queue = dispatch_queue_create("com.SievaNetworks.app.queue", DISPATCH_QUEUE_CONCURRENT);
dispatch_async(queue, ^{
    
    [self saveToServer:data date:date_graph];
    
});
   /* NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration];
    //getting authCode
    NSString *query0 = [NSString stringWithFormat:@"select * from authDetails;"];
    NSArray *authData = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query0]];
    NSString *authCode = @"";
    if([authData count]!=0){
        authCode = authData[0][1];
        
    }
    //---------------------------
    NSString *urlString = [NSString stringWithFormat:@"http://www.gpstracking-server1.com/gpstracking/hosapp1/saveHosGraphData.php?masterUser=%@&user=%@&date=%@&approve=1&authCode=%@",masterUser,user,date_graph,authCode];
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"HEAD"];
    NSURLResponse *response;
    NSError *error;
    NSData *mydata = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
    if(httpResponse!=nil){
        if ([httpResponse statusCode] ==200 ) {
        
        BFLog(@"Device connected to the internet");
    NSURL *url = [NSURL URLWithString:urlString];
     
   
        
    
                        NSError *error;
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    [request setHTTPMethod:@"POST"];
    /* NSDictionary *mapData = [[NSDictionary alloc] initWithObjectsAndKeys: @"TEST IOS", @"name",
     @"IOS TYPE", @"typemap",
     nil];*/
    
  /*  NSMutableDictionary *currentStatus=[[NSMutableDictionary alloc]init];
    [currentStatus setValue:status forKey:@"Status"];
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:data options:0 error:&error];
    [request setHTTPBody:postData];
    
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
       
        /*if (error!=nil) {
             BFLog(@"error:%@,%@",error,data);
        }else{
            BFLog(@"Error:%ld",(long)error.localizedDescription);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:error.localizedDescription delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
        
    }];
    
    [postDataTask resume];
                       
                   });*/
        
        dispatch_barrier_sync(queue, ^{
        UIAlertController *alert1 = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Your Data is Saved" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ok1= [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            [alert1 dismissViewControllerAnimated:YES completion:nil];
        }];
        
        
        [alert1 addAction:ok1];
        [self presentViewController:alert1 animated:YES completion:nil];
           /* NSString *query_update = [NSString stringWithFormat:@"UPDATE FinalTable SET server_sync='%d' WHERE date='%@'",1,current_date];
            
            // Execute the query.
            
            //NSString *query_del = @"delete from TempTable";
            [dbManager executeQuery:query_update];
            
            // If the query was successfully executed then pop the view controller.
            if (dbManager.affectedRows != 0) {
                BFLog(@"Query was executed successfully. Affected rows = %d", dbManager.affectedRows);
                
            }
            else{
                BFLog(@"Could not execute the query");
            }*/

        });

        
    /*}
       else if([httpResponse statusCode]==503){
       BFLog(@"Device not connected to the internet");
      UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                         message:@"No connectivity with server at this time,it will sync automatically"
                                                        delegate:self
                                               cancelButtonTitle:@"Cancel"
                                               otherButtonTitles:@"OK", nil];
        
        [alert1 show];
        */
       /* NSString *query_update = [NSString stringWithFormat:@"UPDATE FinalTable SET server_sync='%d' WHERE date='%@'",0,current_date];
        
        // Execute the query.
        
        //NSString *query_del = @"delete from TempTable";
        [dbManager executeQuery:query_update];
        
        // If the query was successfully executed then pop the view controller.
        if (dbManager.affectedRows != 0) {
            BFLog(@"Query was executed successfully. Affected rows = %d", dbManager.affectedRows);
            
        }
        else{
            BFLog(@"Could not execute the query");
        }*/

        
  //  }
    //}
    
}
#pragma mark    - <Send DOT inspection report>
-(void)dotInspectionAPICall:(NSString *)strIsApproved{
    
    if([commonDataHandler sharedInstance].strUserName.length <= 0 || [commonDataHandler sharedInstance].strAuthCode.length <= 0){
        DBManager *objDB= [[DBManager alloc] initWithDatabaseFilename:@"hosDB.sql"];
        NSArray *userInfoArray=[[NSArray alloc]initWithArray: [objDB getUserId]];
        NSArray *detailsArray = [userInfoArray firstObject];
        [commonDataHandler sharedInstance].strUserId = detailsArray[0];
        [commonDataHandler sharedInstance].strUserName = detailsArray[1];
        if([commonDataHandler sharedInstance].strAuthCode.length <= 0){
            [commonDataHandler sharedInstance].strAuthCode = detailsArray[[detailsArray count]-2];
        }
    }
    
    NSMutableDictionary *requestDict = [[NSMutableDictionary alloc] init];
    [requestDict setValue:OPERATION_DOT forKey:OPERATION];
    [requestDict setValue:[commonDataHandler sharedInstance].strUserName forKey:USER_NAME];
    [requestDict setValue:[Bugfender deviceIdentifier] forKey:DEVICE_ID];
    [requestDict setValue:[commonDataHandler sharedInstance].strAuthCode forKey:AUTH_CODE];
//    [requestDict setValue:@"true" forKey:IS_APPROVED];
     [requestDict setValue:strIsApproved forKey:IS_APPROVED];
    [requestDict setValue:strXMTemplate forKey:XML_TEMPLATE];
    [requestDict setValue:[[commonDataHandler sharedInstance] getCurrentTimeInLocalTimeZone] forKey:DATE_IN_CURRENT_TZ];
    NSString *stringParams = [[commonDataHandler sharedInstance] constructInput:requestDict];
    [self getConnection:[NSString stringWithFormat:@"%@%@", WEB_ROOT, SEND_SIGNED_DOT_URL] withParams:stringParams];
    
}

-(void)getConnection:(NSString *)urlString withParams:(NSString*)paramString{
    __block NSString *responseString = @"";
    NSString *urlStringWithParam = [NSString stringWithFormat:@"payload=%@",paramString];
    NSString *enodedURl = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [NSURL URLWithString:enodedURl] ;
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    
    [request setHTTPBody:[urlStringWithParam dataUsingEncoding:NSUTF8StringEncoding]];
    NSOperationQueue *queue1 = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:request queue:queue1 completionHandler:^(NSURLResponse * _Nullable response, NSData * _Nullable data, NSError * _Nullable connectionError) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        });
        responseString = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
        if(![responseString isEqualToString:@""]){
            NSMutableArray *arrayResponse = [[NSMutableArray alloc] init];
            NSMutableArray *arrayMobileCheckIn   = [NSMutableArray new];
            [arrayMobileCheckIn removeAllObjects];
            [arrayResponse removeAllObjects];
            arrayResponse =[[commonDataHandler sharedInstance] parseResponse:responseString];
            
            if(arrayResponse!=nil && [arrayResponse count]!=0){
                
                for (NSMutableDictionary *objDict in arrayResponse) {
                    MobileCheckIn *objMobileCheckIn = [MobileCheckIn new];
                    [objMobileCheckIn getDataFromAPI:objDict];
                    [arrayMobileCheckIn addObject:objMobileCheckIn];
                }
                
                if(arrayMobileCheckIn.count > 0){
                    MobileCheckIn *objMobileCheckIn = [arrayMobileCheckIn firstObject];
                    if([objMobileCheckIn.result isEqualToString:@"failed"]){
                        if([objMobileCheckIn.reason isEqualToString:AUTH_CODE_INVALID]){
                            //Re-auth
                            if(isReAuth){
                                isReAuth = NO;
//                                [[commonDataHandler sharedInstance] reAuthenticateUser:^(BOOL finished) {
//                                    [self dotInspectionAPICall];
//                                }];
                            }
                        }else{
                            dispatch_async(dispatch_get_main_queue(), ^{
                                UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"DOT Inspection Report"
                                                                                 message:objMobileCheckIn.reason
                                                                                delegate:nil
                                                                       cancelButtonTitle:@"OK"
                                                                       otherButtonTitles:nil, nil];
                                [alert1 show];
                            });
                        }
                    }else{
                        //Call success
                        dispatch_async(dispatch_get_main_queue(), ^{
                            UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Send DOT Inspection Report"
                                                                             message:objMobileCheckIn.result
                                                                            delegate:nil
                                                                   cancelButtonTitle:@"OK"
                                                                   otherButtonTitles:nil, nil];
                            [alert1 show];
                        });
                    }
                }
            }
        }
        
        
    }];
    
}

-(NSString *)getStatusToSend:(NSString *)status{
    NSString *statusNew=@"";
   if([status isEqualToString:@"5"]||[status isEqualToString:@"B"]||[status isEqualToString:@"1"])
       statusNew=@"1";
    else if([status isEqualToString:@"6"]||[status isEqualToString:@"2"])
        statusNew=@"2";
    else if([status isEqualToString:@"9"]||[status isEqualToString:@"7"]||[status isEqualToString:@"3"])
        statusNew=@"3";
    if([status isEqualToString:@"A"]||[status isEqualToString:@"C"]||[status isEqualToString:@"8"]||[status isEqualToString:@"4"])
        statusNew=@"4";
    else if([status isEqualToString:@"0"])
        statusNew=@"0";
        return statusNew;
}
@end
