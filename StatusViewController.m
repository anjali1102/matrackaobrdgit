//
//  StatusViewController.m
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 9/25/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import "StatusViewController.h"
#import "NotificationHandler.h"
#import "logScreenViewController.h"
#import "commonDataHandler.h"
@interface StatusViewController ()
{
    UIAlertView *alert;
    BOOL isAutoDismiss;
}
@end

@implementation StatusViewController
@synthesize buttonClick,detailView,deviceStatusLabel,buttonImage;
- (void)viewDidLoad
{
    
    [super viewDidLoad];
    isAutoDismiss = NO;
    detailView.layer.borderWidth = 1.0;
    detailView.layer.borderColor = [UIColor colorWithRed:0.125 green:0.525 blue:0.125 alpha:1.0].CGColor;

    buttonClick.layer.borderColor = [UIColor colorWithRed:0.125 green:0.525 blue:0.125 alpha:1.0].CGColor;
    buttonClick.layer.borderWidth = 1.0;
    buttonClick.layer.cornerRadius = 15;
    
    _lblDriverStatus.text = @"DRIVING";
    current_status = @"D";
    [self saveData:current_status];
    UITapGestureRecognizer *clickButton = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(changeStatus)];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"DataUpdated" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleUpdatedData:)
                                                 name:@"DataUpdated"
                                               object:nil];
    
    
    if(_driverStatus != driver_status_driving ||  ! _isFromPush){
        [clickButton setNumberOfTapsRequired:1];
        [buttonClick setUserInteractionEnabled:YES];
        [buttonClick addGestureRecognizer:clickButton];
    }else{
        _isFromPush = NO;
    }

    globalTimer = 600;
    BOOL stationary = false;
    if(stationary){
        deviceStatusLabel.text=@"Stationary";
        deviceStatusLabel.textColor = [UIColor redColor];
        buttonImage.image=[UIImage imageNamed:@"icons/drive.png"];
        [buttonClick addGestureRecognizer:clickButton];
        
    }else{
        
        buttonImage.image=[UIImage imageNamed:@"icons/password1.png"];
    }
    [self updateCircularProgressBar];
    
    
    [self startTimer];
}
- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidAppear:YES];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark    - <Navigation>
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:@"goToLogScreen"]){
    logScreenViewController *objVC = (logScreenViewController *)segue.destinationViewController;
    objVC.isFromPush = YES;
        if([current_status isEqualToString:@"YM"]){
            objVC.driverStatus = driver_status_yard_move;
        }else if ([current_status isEqualToString:@"PC"]){
            objVC.driverStatus = driver_status_personal_convey;
        }else{
            objVC.driverStatus = driver_status_driving;
        }
    }
    
}
-(void)handleUpdatedData:(NSNotification *)notification {
    NSDictionary* userInfo = notification.userInfo;
    driver_status drvStat;
    int driverStat;
    driverStat = [[userInfo valueForKey:@"driverStatus"] intValue];
    if(driverStat == 1){
        drvStat = driver_status_driving;
        current_status = @"D";
    }
    else if (driverStat == 2){
        drvStat = driver_status_onDuty;
    }
    else if (driverStat == 3){
        drvStat = driver_status_offDuty;
        current_status = @"OFF";
    }
    else if (driverStat == 4){
        drvStat = driver_status_none;
        current_status = @"NONE";
    }else if (driverStat == 6){
        drvStat = driver_status_onDuty_immediate;
        current_status = @"ON";
    }
    else
        drvStat = driver_status_connect_back;
    if(driverStat != 1){
        
        if(driverStat == 2){
            [self changeStatusOnPush];
        }else{
            NotificationHandler *objHelper = [NotificationHandler new];
            [objHelper recievePushNotification:self withDriverStatus:drvStat];
        }
    }else{
        [self saveData:current_status];
    }
    
}
-(void)changeStatusOnPush{
    dispatch_async(dispatch_get_main_queue(), ^{
        alert = [[UIAlertView alloc]initWithTitle:@"Status Change" message:@"Do you want to continue driving or change your duty status" delegate:self cancelButtonTitle:@"Continue Driving" otherButtonTitles:@"Change Status", nil];
        alert.tag=1;
        alert.delegate=self;
        [alert show];
        [self performSelector:@selector(dismissAlertView) withObject:nil afterDelay:60.0];
    });
}
-(void)changeStatus{
    dispatch_async(dispatch_get_main_queue(), ^{
        alert = [[UIAlertView alloc]initWithTitle:@"Status Change" message:@"Do you want to continue driving or change your duty status" delegate:self cancelButtonTitle:@"Continue Driving" otherButtonTitles:@"Change Status", nil];
        alert.tag=1;
        alert.delegate=self;
        [alert show];
    });
}
- (void)dismissAlertView {
    NotificationHandler *objHelper = [NotificationHandler new];
    [objHelper recievePushNotification:self withDriverStatus:driver_status_onDuty_immediate];
    [alert dismissWithClickedButtonIndex:-1 animated:YES];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(buttonIndex==1){
        
        if(alertView.tag==1){
            
            UIStoryboard *storyBoard = self.storyboard;
            UIViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"changedutystatus"];
            [self presentViewController:vc animated:YES completion:nil];
        }
    }
    
}

// Draws the progress circles on top of the already painted backgroud
- (void)drawCircularProgressBarWithMinutesLeft:(NSInteger)minutes secondsLeft:(NSInteger)seconds
{
    // Removing unused view to prevent them from stacking up
//    for (id subView in [self.view subviews]) {
//        if ([subView isKindOfClass:[CircularProgressTimer class]]) {
//            [subView removeFromSuperview];
//        }
//    }
    
    // Init our view and set current circular progress bar value
    CGRect progressBarFrame = CGRectMake(0, 200, 180, 180);
    _progressTimerView = [[CircularProgressTimer alloc] initWithFrame:progressBarFrame];
    [_progressTimerView setCenter:CGPointMake(210, 250)];
    [_progressTimerView setPercent:seconds];
    if (minutes == 0 && seconds == 0) {
        [_progressTimerView setInstanceColor:[UIColor redColor]];
    }
    
    // Here, setting the minutes left before adding it to the parent view
    [_progressTimerView setMinutesLeft:12];
    [_progressTimerView setSecondsLeft:00];
   // [self.view addSubview:progressTimerView];
    //progressTimerView = nil;
}

- (void)startTimer
{
    timer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                             target:self
                                           selector:@selector(updateCircularProgressBar)
                                           userInfo:nil
                                            repeats:YES];
}

- (void)updateCircularProgressBar
{
    // Values to be passed on to Circular Progress Bar
    if (globalTimer > 0 && globalTimer <= 1200) {
        globalTimer--;
        minutesLeft = 04;
        secondsLeft = 10;
        
        [self drawCircularProgressBarWithMinutesLeft:minutesLeft secondsLeft:secondsLeft];
//        NSLog(@"Time left: %02ld:%02ld", (long)minutesLeft, (long)secondsLeft);
    } else {
        [self drawCircularProgressBarWithMinutesLeft:0 secondsLeft:0];
        [timer invalidate];
    }
}
#pragma mark    - <Toggle Switch Action For PC & YM>
-(IBAction)toggleButtonAction:(id)sender{
     UISwitch *toggleSwitch = (UISwitch *)sender;
    if(toggleSwitch.tag == 2000){//PC Switch
        if(toggleSwitch.on){
            [_switchYM  setOn:NO animated:NO];
            _lblDriverStatus.text = @"OFF DUTY";
            current_status = @"PC";
        }else{
            _lblDriverStatus.text = @"DRIVING";
            current_status = @"D";
        }
        [self saveData:current_status];
    }else if (toggleSwitch.tag == 3000){//YM Switch
        if(toggleSwitch.on){
            [_switchPC  setOn:NO animated:NO];
            _lblDriverStatus.text = @"ON DUTY";
            current_status = @"YM";
        }else{
            _lblDriverStatus.text = @"DRIVING";
            current_status = @"D";
        }
         [self saveData:current_status];
    }
}
-(IBAction)backAction:(id)sender{
    [self performSegueWithIdentifier:@"goToLogScreen" sender:self];
}
-(void)saveData:(NSString *)x1{
    DBManager *objDB = [[DBManager alloc] initWithDatabaseFilename:@"hosDB.sql"];
    NSDictionary *token;
    ;
    NSMutableDictionary *whereToken;
    NSArray *userInfoArray=[[NSArray alloc]initWithArray: [objDB getUserId]];
    if([userInfoArray count]!=0){
        int userId = [userInfoArray[0][0] intValue];
        token=@{@"columnName":@"id",@"entry":[NSString stringWithFormat:@"%d",userId]};
        ;
        whereToken=[[NSMutableDictionary alloc] initWithDictionary:token];
        
        //[objDB getLogEditData:whereToken];
        commonDataHandler *common=[commonDataHandler new];
        NSString * date=[common getCurrentTimeInLocalTimeZone];
        token=@{@"userid":[NSString stringWithFormat:@"%d",userId],@"date":[NSString stringWithFormat:@"%@",date]};
        ;
        whereToken=[[NSMutableDictionary alloc] initWithDictionary:token];
        NSArray* dataFromDB=[[NSArray alloc]initWithArray:[objDB getLogEditData:whereToken]];
        NSString *statusString=@"";
        for(int j=0;j<[dataFromDB count];j++){
            if([dataFromDB[j][1] isEqualToString:@""])
                statusString= [statusString stringByAppendingString:[NSString stringWithFormat:@"1"]];
            else
                statusString= [statusString stringByAppendingString:[NSString stringWithFormat:@"%@",[[dataFromDB objectAtIndex:j] objectAtIndex:1]]];
        }
        
        NSString* charStr=[@"" stringByPaddingToLength:96 withString:@"1" startingAtIndex:0];
        if([dataFromDB count]==0)
            statusString=charStr;
        else
        {
            if([statusString isEqualToString:@""])
                statusString=charStr;
            
        }
        statusString=[common modifyData:statusString date:date];
        NSString *statusStringTemp=[statusString stringByReplacingOccurrencesOfString:@"0" withString:@""];
        int currentSlot=(int)statusStringTemp.length;
        statusString=[statusString stringByReplacingCharactersInRange:NSMakeRange((int)(currentSlot-1), 1) withString:[self getStatusCode:x1]];
        
        loggraph *objLog=[loggraph new];
        for(int i=0;i<statusString.length;i++){
            objLog.slot=[NSString stringWithFormat:@"%d",(int)(1+i)];
            objLog.driverStatus=[statusString substringWithRange:NSMakeRange(i, 1)];
            objLog.calculatedStatus=[statusString substringWithRange:NSMakeRange(i, 1)];
            objLog.date=date;
            objLog.userId=[NSString stringWithFormat:@"%d",userId];
            objLog.insert=[NSString stringWithFormat:@"%d",1];
            [objDB insertPastStatus:objLog];
        }
        if(!objDB.affectedRows){
            current_status=x1;
        }
        
    }
    
}
-(NSString *)getStatusCode:(NSString *)status_new{
    NSString *statusNew;
    if([status_new isEqualToString:@"D"])
        statusNew=@"7";
    else if([status_new isEqualToString:@"SB"])
        statusNew=@"6";
    else if([status_new isEqualToString:@"OFF"]|| [status_new isEqualToString:@"PC"])
        statusNew=@"5";
    else if([status_new isEqualToString:@"ON"]|| [status_new isEqualToString:@"YM"])
        statusNew=@"8";
    return statusNew;
}
@end
