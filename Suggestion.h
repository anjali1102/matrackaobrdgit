//
//  Suggestion.h
//  MaTrackHOSV1
//
//  Created by sieva networks on 10/27/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Suggestion : NSObject
@property (nonatomic, strong) NSString *operation;
@property (nonatomic, strong) NSString *username;
@property (nonatomic, strong) NSString *deviceidentifier;
@property (nonatomic, strong) NSString *date;
@property (nonatomic, strong) NSString *slot;
@property (nonatomic, strong) NSString *repeat;
@property (nonatomic, strong) NSString *status;
@property (nonatomic, strong) NSString *suggestedby;
@property (nonatomic, strong) NSString *ack;
@property (nonatomic, strong) NSString *start;
@property (nonatomic, strong) NSString *end;
-(void)getDataFromAPI:(NSMutableDictionary *)dict;
@end
