//
//  TIMERUIApplication.m
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 4/6/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import "TIMERUIApplication.h"

@implementation TIMERUIApplication

-(void)sendEvent:(UIEvent *)event{
    
    [super sendEvent:event];
    if (!myidleTimer) {
        [self resetIdleTimer];
    }
    NSSet *allTouches = [event allTouches];
    if ([allTouches count] > 0) {
        UITouchPhase phase = ((UITouch *)[allTouches anyObject]).phase;
        if (phase == UITouchPhaseBegan) {
            [self resetIdleTimer];
        }
    }
    
    
}


-(void)resetIdleTimer{
    
    if(myidleTimer){
        
        [myidleTimer invalidate];
        
    }
    int timeout = kApplicationTimeoutInMinutes *60;
    myidleTimer = [NSTimer scheduledTimerWithTimeInterval:timeout target:self selector:@selector(idleTimerExceeded) userInfo:nil repeats:NO];
    
    
}

-(void)idleTimerExceeded{
    
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kApplicationDidTimeoutNotification object:nil];
}
@end
