//
//  Trailer_Defects.m
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 9/12/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//

#import "Trailer_Defects.h"
#import "DVIR_Main.h"

NSArray *trailer_defects;
int trailer1;
@interface Trailer_Defects ()

@end

@implementation Trailer_Defects
@synthesize trilerDefects;
@synthesize scroller_trailer;

- (void)viewDidLoad {
    [super viewDidLoad];
    trailer1=1;
    trailer_defects=@[@"1 - Air Brake System",@"3 - Cargo Securement",@"4 - Coupling Devices",@"5 - Dangerous Goods",@"8 - Eletric Brakes",@"10 - Exhaust System",@"11 - Frame & Cargo Body",@"17 - Hydraulic Brakes",@"18 - Lamps/Reflectors",@"20 - Suspension System",@"21 - Tires",@"22 - Wheels,Hubs,Fasteners"];
    trilerDefects.delegate=self;
    trilerDefects.dataSource=self;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [trailer_defects count];
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (action==1 && editedTrailerDefects==0) {
        //for(int i=0;i<[vehicle_defects count];i++){
        if([trailer_defects1 count]!=0){
            if([trailer_defects1 containsObject:trailer_defects[indexPath.row]]){
                [expandedCellsTrailers addObject:indexPath];
                
            }
        }
        
        
        //}
    }
    
    if([expandedCellsTrailers count]!=0){
        if([expandedCellsTrailers containsObject:indexPath]){
            
            return 180.00;
            
            
        }else{
            
            return 40;
            
        }
    }else{
        
        return 40;
        
    }
    
    
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    static NSString *identifier=@"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
   cell.textLabel.text=trailer_defects[indexPath.row];
    if([expandedCellsTrailers containsObject:indexPath]){
        
        
        [expandedCells addObject:indexPath];
        notes = [[UITextField alloc]initWithFrame:CGRectMake(tableView.bounds.size.width/2, 0, tableView.bounds.size.width/2, 80) ];
        notes.placeholder=@"Add Notes Here";
        notes.tag = indexPath.row;
        notes.layer.borderWidth = 2.0;
        notes.delegate = self;
        //es [notes addTarget:self action:@selector(notesAdded:) forControlEvents:UIControlEventEditingDidEnd];
        notes.layer.borderColor = [[UIColor lightGrayColor] CGColor];
        cell.backgroundColor = [UIColor whiteColor];
        cell.accessoryView = notes;
        cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
        cell.textLabel.numberOfLines = 3;
        cell.textLabel.minimumScaleFactor=10./cell.textLabel.font.pointSize;
        cell.textLabel.adjustsFontSizeToFitWidth=YES;
        
        
        if([defectsDummyTrailers count]!=0){
            
            for(int i =0;i<[defectsDummyTrailers count];i++){
                NSMutableDictionary *sam = defectsDummyTrailers[i];
                
                if([[sam valueForKey:@"defects"] isEqualToString:trailer_defects[indexPath.row]]){
                    
                    notes.text = [sam valueForKey:@"info"];
                    
                }
                
            }
        }
    }
    //  [addNotes addTarget:self action:@selector(addExpandedCells:) forControlEvents:UIControlEventTouchUpInside];
    //cell.accessoryView = addNotes;
    
    
    
    if ([trailer_defects1 containsObject:trailer_defects[indexPath.row]]) {
          cell.imageView.image=[UIImage imageNamed:@"icons/checked.png"];
    }else{
    cell.imageView.image=[UIImage imageNamed:@"icons/uncheck.png"];
    }
    return cell;
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    editedTrailerDefects=1;
    UITableViewCell *cell =[tableView cellForRowAtIndexPath:indexPath];
    
    
    if([expandedCellsTrailers containsObject:indexPath]){
        [expandedCellsTrailers removeObject:indexPath];
        notes.layer.borderColor = [[UIColor whiteColor] CGColor];
        [notes setHidden:YES];
        
        
    }else{
        
        [expandedCellsTrailers addObject:indexPath];
        notes = [[UITextField alloc]initWithFrame:CGRectMake(tableView.bounds.size.width/2, 0, tableView.bounds.size.width/2, 80) ];
        notes.placeholder=@"Add Notes Here";
        notes.tag = indexPath.row;
        notes.layer.borderWidth = 2.0;
        notes.delegate = self;
        //es [notes addTarget:self action:@selector(notesAdded:) forControlEvents:UIControlEventEditingDidEnd];
        notes.layer.borderColor = [[UIColor lightGrayColor] CGColor];
        cell.backgroundColor = [UIColor whiteColor];
        cell.accessoryView = notes;
        cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
        cell.textLabel.numberOfLines = 3;
        cell.textLabel.minimumScaleFactor=10./cell.textLabel.font.pointSize;
        cell.textLabel.adjustsFontSizeToFitWidth=YES;
    }
    
    [trilerDefects beginUpdates];
    [trilerDefects endUpdates];
    NSData *cellImage= UIImageJPEGRepresentation(cell.imageView.image, 1.0);
    NSData *uncheck = UIImageJPEGRepresentation([UIImage imageNamed:@"icons/uncheck.png"], 1.0);
    if ([cellImage isEqualToData:uncheck]) {
        cell.imageView.image=[UIImage imageNamed:@"icons/checked.png"];
        NSMutableDictionary *defectsInfo = [[NSMutableDictionary alloc]init];
        NSArray *parts = [trailer_defects[indexPath.row] componentsSeparatedByString:@"-"];
        [defectsInfo setValue:parts[0] forKey:@"id"];
        [defectsInfo  setValue:trailer_defects[indexPath.row] forKey:@"defects"];
        [defectsInfo setValue:@"No Notes" forKey:@"info"];
        [defectsDummyTrailers addObject:defectsInfo];
        [trailer_defects1 addObject:trailer_defects[indexPath.row]];

        
        
    }else{
        cell.imageView.image=[UIImage imageNamed:@"icons/uncheck.png"];
        NSString *selected = trailer_defects[indexPath.row];
        if ([trailer_defects1 containsObject:selected]) {
            [trailer_defects1 removeObject:selected];
        }
        
    }
    
    
    
        
    
    
    
    
    
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:textField.tag inSection:0];
    BOOL defectFound = false;
    if([defectsDummyTrailers count]!=0){
        
        for(int i =0;i<[defectsDummyTrailers count];i++){
            NSMutableDictionary *sam = defectsDummyTrailers[i];
            
            if([[sam valueForKey:@"defects"] isEqualToString:trailer_defects[indexPath.row]]){
                
                [sam setValue:textField.text forKey:@"info"];
                defectFound = true;
                
            }
            
        }
        
        if(!defectFound){
            
            
            NSMutableDictionary *defectsInfo = [[NSMutableDictionary alloc]init];
            NSArray *parts = [trailer_defects[indexPath.row] componentsSeparatedByString:@"-"];
            [defectsInfo setValue:parts[0] forKey:@"id"];
            [defectsInfo  setValue:trailer_defects[indexPath.row] forKey:@"defects"];
            [defectsInfo setValue:@"No Notes" forKey:@"info"];
            [defectsDummyTrailers addObject:defectsInfo];
            
        }
        
        
        
        
    }else{
        
        NSMutableDictionary *defectsInfo = [[NSMutableDictionary alloc]init];
        NSArray *parts = [trailer_defects[indexPath.row] componentsSeparatedByString:@"-"];
        [defectsInfo setValue:parts[0] forKey:@"id"];
        [defectsInfo  setValue:trailer_defects[indexPath.row] forKey:@"defects"];
        [defectsInfo setValue:@"No Notes" forKey:@"info"];
        [defectsDummyTrailers addObject:defectsInfo];
    }
    //BFLog(@"%@",textField.text);
    [textField resignFirstResponder];
    
    return YES;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)doneBtn:(id)sender {
    UIAlertController *alert1 = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Modification Saved" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *ok1= [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        [alert1 dismissViewControllerAnimated:YES completion:nil];
    }];
    
    
    [alert1 addAction:ok1];
    [self presentViewController:alert1 animated:YES completion:nil];

}
@end
