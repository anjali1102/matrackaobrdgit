//
//  VehicleSelection.m
//  MaTrackHOSV1
//
//  Created by sieva networks on 10/27/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import "VehicleSelection.h"
#import "NSString+NullCheck.h"
#import "Header.h"
@implementation VehicleSelection
- (instancetype)init
{
    self = [super init];
    if (self) {
        [self initializeData];
    }
    return self;
}
-(void)initializeData{
    _strOperation = @"";
    _strReason = @"";
    _strResult = @"";
}
- (void)getDataFromAPI:(NSMutableDictionary *)objDict{
    _strOperation = [[objDict valueForKey:OPERATION] NullCheck];
    _strResult = [objDict valueForKey:@"result"];
    _strReason = [objDict valueForKey:@"reason"];
    NSString *strVehicle = [objDict valueForKey:@"vehiclelist"];
    _arrayPossibleVehicles = [strVehicle componentsSeparatedByString:@"|"];
    
}
@end
