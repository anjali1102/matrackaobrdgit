//
//  Vehicle_Defects.m
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 9/12/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//

#import "Vehicle_Defects.h"
#import "DVIR_Main.h"
#import "DVIRMenu.h"



@interface Vehicle_Defects ()

@end

NSArray *defects;
int selected=0;
int vehicle1=0;
int vehicleNum=0;

@implementation Vehicle_Defects
@synthesize defects_list;

- (void)viewDidLoad {
    [super viewDidLoad];
   // defectsDummy = [[NSMutableArray alloc] init];
    defects_list.delegate=self;
       vehicle1=1;
    //action=0;
    defects_list.dataSource=self;
    defects=@[@"0 - Other",@"1 - Air Brake System",@"2 - Cab",@"3 - Cargo Securement",@"4 - Coupling Devices",@"5 - Dangerous Goods",@"6 - Driver Controls",@"7 - Driver seat",@"8 - Eletric Brakes",@"9 - Emergency Equipment & Safety Devices",@"10 - Exhaust System",@"11 - Frame & Cargo Body", @"12 - Fuel Systems",@"13 - General",@"14 - Glass & Mirrors",@"15 - Heater/Defroster",@"16 - Horn",@"17 - Hydraulic Brakes",@"18 - Lamps/Reflectors",@"19 - Steering",@"20 - Suspension System",@"21 - Tires",@"22 - Wheels,Hubs,Fasteners",@"23 - Windshield Wiper/Washer"];
    
    //demo 
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [defects count];
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier=@"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
    cell.textLabel.text=defects[indexPath.row];
    if([expandedCells containsObject:indexPath]){
        
        
        [expandedCells addObject:indexPath];
        notes = [[UITextField alloc]initWithFrame:CGRectMake(tableView.bounds.size.width/2, 0, tableView.bounds.size.width/2, 80) ];
        notes.placeholder=@"Add Notes Here";
        notes.tag = indexPath.row;
        notes.layer.borderWidth = 2.0;
        notes.delegate = self;
        //es [notes addTarget:self action:@selector(notesAdded:) forControlEvents:UIControlEventEditingDidEnd];
        notes.layer.borderColor = [[UIColor lightGrayColor] CGColor];
        cell.backgroundColor = [UIColor whiteColor];
        cell.accessoryView = notes;
        cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
        cell.textLabel.numberOfLines = 3;
        cell.textLabel.minimumScaleFactor=10./cell.textLabel.font.pointSize;
        cell.textLabel.adjustsFontSizeToFitWidth=YES;
        
        
    if([defectsDummy count]!=0){
        
        for(int i =0;i<[defectsDummy count];i++){
            NSMutableDictionary *sam = defectsDummy[i];
            
            if([[sam valueForKey:@"defects"] isEqualToString:defects[indexPath.row]]){
                
                notes.text = [sam valueForKey:@"info"];
                
            }
            
        }
    }
    }
  //  [addNotes addTarget:self action:@selector(addExpandedCells:) forControlEvents:UIControlEventTouchUpInside];
    //cell.accessoryView = addNotes;
    
    if ([vehicle_defects count]!=0) {
        if ([vehicle_defects containsObject: defects[indexPath.row]]) {
             cell.imageView.image=[UIImage imageNamed:@"icons/checked.png"];
            
        }
        else{
            cell.imageView.image=[UIImage imageNamed:@"icons/uncheck.png"];
        }

    }else{
    cell.imageView.image=[UIImage imageNamed:@"icons/uncheck.png"];
    }
   
    return cell;
    
}

/*-(void)addExpandedCells:(UIButton*)addNotes{
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:addNotes.tag inSection:0];
    if([expandedCells containsObject:indexPath]){
        [expandedCells removeObject:indexPath];
        
    }else{
        
        [expandedCells addObject:indexPath];
        
    }
    
    [defects_list beginUpdates];
    [defects_list endUpdates];
    
}*/
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (action==1 && editedVehicleDefects==0) {
        //for(int i=0;i<[vehicle_defects count];i++){
        if([vehicle_defects count]!=0){
            if([vehicle_defects containsObject:defects[indexPath.row]]){
                [expandedCells addObject:indexPath];
                
            }
        }
        
            
        //}
    }
    
    if([expandedCells count]!=0){
    if([expandedCells containsObject:indexPath]){
        
        return 180.00;
        
        
    }else{
        
        return 40;
        
    }
    }else{
        
        return 40;
        
    }
    
    
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    editedVehicleDefects=1;

    UITableViewCell *cell =[tableView cellForRowAtIndexPath:indexPath];
    if([expandedCells containsObject:indexPath]){
        [expandedCells removeObject:indexPath];
        notes.layer.borderColor = [[UIColor whiteColor] CGColor];
        [notes setHidden:YES];
        
        
    }else{
        
        [expandedCells addObject:indexPath];
        notes = [[UITextField alloc]initWithFrame:CGRectMake(tableView.bounds.size.width/2, 0, tableView.bounds.size.width/2, 80) ];
        notes.placeholder=@"Add Notes Here";
        notes.tag = indexPath.row;
        notes.layer.borderWidth = 2.0;
        notes.delegate = self;
       //es [notes addTarget:self action:@selector(notesAdded:) forControlEvents:UIControlEventEditingDidEnd];
        notes.layer.borderColor = [[UIColor lightGrayColor] CGColor];
        cell.backgroundColor = [UIColor whiteColor];
        cell.accessoryView = notes;
        cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
        cell.textLabel.numberOfLines = 3;
        cell.textLabel.minimumScaleFactor=10./cell.textLabel.font.pointSize;
        cell.textLabel.adjustsFontSizeToFitWidth=YES;
    }
    
    [defects_list beginUpdates];
    [defects_list endUpdates];
    NSData *cellImage= UIImageJPEGRepresentation(cell.imageView.image, 1.0);
    NSData *uncheck = UIImageJPEGRepresentation([UIImage imageNamed:@"icons/uncheck.png"], 1.0);
    if ([cellImage isEqualToData:uncheck]) {
        cell.imageView.image=[UIImage imageNamed:@"icons/checked.png"];
        NSMutableDictionary *defectsInfo = [[NSMutableDictionary alloc]init];
        NSArray *parts = [defects[indexPath.row] componentsSeparatedByString:@"-"];
        [defectsInfo setValue:parts[0] forKey:@"id"];
        [defectsInfo  setValue:defects[indexPath.row] forKey:@"defects"];
        [defectsInfo setValue:@"No Notes" forKey:@"info"];
        [defectsDummy addObject:defectsInfo];
        [vehicle_defects addObject:defects[indexPath.row]];

        
    }else{
          cell.imageView.image=[UIImage imageNamed:@"icons/uncheck.png"];
        NSString *selected = defects[indexPath.row];
        if ([vehicle_defects containsObject:selected]) {
            [vehicle_defects removeObject:selected];
        }
        
    }
    //NSIndexPath *indexPath = [NSIndexPath indexPathForRow:addNotes.tag inSection:0];
    
    
    
    
   

    
}
/*-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    [self performSelector:@selector(notesAdded:) withObject:textField afterDelay:10.0];
    
    return YES;
}*/
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:textField.tag inSection:0];
    BOOL defectFound = false;
    if([defectsDummy count]!=0){
        
        for(int i =0;i<[defectsDummy count];i++){
            NSMutableDictionary *sam = defectsDummy[i];
            
            if([[sam valueForKey:@"defects"] isEqualToString:defects[indexPath.row]]){
                
                [sam setValue:textField.text forKey:@"info"];
                defectFound = true;
                
            }
            
        }
        
        if(!defectFound){
            
            
            NSMutableDictionary *defectsInfo = [[NSMutableDictionary alloc]init];
            NSArray *parts = [defects[indexPath.row] componentsSeparatedByString:@"-"];
            [defectsInfo setValue:parts[0] forKey:@"id"];
            [defectsInfo  setValue:defects[indexPath.row] forKey:@"defects"];
            [defectsInfo setValue:@"No Notes" forKey:@"info"];
            [defectsDummy addObject:defectsInfo];
            
        }
        
        
        
        
    }else{
        
        NSMutableDictionary *defectsInfo = [[NSMutableDictionary alloc]init];
        NSArray *parts = [defects[indexPath.row] componentsSeparatedByString:@"-"];
        [defectsInfo setValue:parts[0] forKey:@"id"];
        [defectsInfo  setValue:defects[indexPath.row] forKey:@"defects"];
        [defectsInfo setValue:@"No Notes" forKey:@"info"];
        [defectsDummy addObject:defectsInfo];
    }
    //BFLog(@"%@",textField.text);
    [textField resignFirstResponder];
    
    return YES;
}
-(void)notesAdded:(UITextField*)notesField{
    
    //NSIndexPath *indexPath = [NSIndexPath indexPathForRow:notesField.tag inSection:0];
    //BFLog(@"%@",notesField.text);
    
    
}



- (IBAction)done:(id)sender {
    ////BFLog(@"Selected:%@",vehicle_defects);
    UIAlertController *alert1 = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Modification Saved" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *ok1= [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        [alert1 dismissViewControllerAnimated:YES completion:nil];
    }];
    
    
    [alert1 addAction:ok1];
    [self presentViewController:alert1 animated:YES completion:nil];

}
@end
