//
//  callSupport.m
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 9/22/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//

#import "callSupport.h"
#import "loginViewController.h"
#import <BugfenderSDK/BugfenderSDK.h>

@interface callSupport ()

@end
UIView *transparentView_support;
@implementation callSupport
@synthesize supportView;

@synthesize menu_support;
@synthesize call_support;
@synthesize emailsupport;
@synthesize debugSwitch,deviceId;

- (void)viewDidLoad {
    [super viewDidLoad];
    deviceId.text =[NSString stringWithFormat:@"%@",[Bugfender deviceIdentifier]];
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"enableDebug"]!=nil){
        
        if([[NSUserDefaults standardUserDefaults] boolForKey:@"enableDebug"]==YES){
            
            [debugSwitch setOn:YES];
            
            
        }else{
            
            [debugSwitch setOn:NO];
            
        }
        
        
        
    }else{
        
        [Bugfender setForceEnabled:NO];
    }
    emailsupport.layer.borderColor=[[UIColor whiteColor] CGColor];
    emailsupport.layer.borderWidth = 2.0;
    emailsupport.layer.cornerRadius = 10.0;
    call_support.layer.borderColor = [[UIColor whiteColor] CGColor];
    call_support.layer.borderWidth = 2.0;
    call_support.layer.cornerRadius=10.0;
    
   
   NSString *urlString = [NSString stringWithFormat:@"http://www.gpstracking-server1.com/helpPage_IOSApp/indexNew.html"];
    
    NSURL *url = [NSURL URLWithString:urlString];
    NSData *data = [NSData dataWithContentsOfURL:url];
    if (data) {
        

    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    [supportView loadRequest:urlRequest];
        supportView.delegate=self;
    }else{
        
        UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                         message:@"Please check your internet connectivity!!!"
                                                        delegate:self
                                               cancelButtonTitle:@"Cancel"
                                               otherButtonTitles:@"OK", nil];
        
       [alert1 show];
        
    }
    // Do any additional setup after loading the view.

    
}


-(void)viewWillAppear:(BOOL)animated{
    NSString *urlString = [NSString stringWithFormat:@"http://www.gpstracking-server1.com/helpPage_IOSApp/indexNew.html"];
    
    NSURL *url = [NSURL URLWithString:urlString];
    NSData *data = [NSData dataWithContentsOfURL:url];
    if (data) {
        
        
        NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
        [supportView loadRequest:urlRequest];
        supportView.delegate=self;
    }else{
        
        UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                         message:@"Please check your internet connectivity!!!"
                                                        delegate:self
                                               cancelButtonTitle:@"Cancel"
                                               otherButtonTitles:@"OK", nil];
        
        [alert1 show];
        
    }
    

    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    
    if(navigationType == UIWebViewNavigationTypeLinkClicked){
        
        [[UIApplication sharedApplication] openURL:[request URL]];
        return NO;
    }
    return YES;
}
- (IBAction)callSupport:(id)sender {
    
    NSString *urlString = [@"tel:" stringByAppendingString:@"8722562669"];
    NSURL *url = [NSURL URLWithString:urlString];
    [[UIApplication sharedApplication] openURL:url];
}
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    // UITouch *touch = [touches anyObject];
    if([[self.view subviews] containsObject:transparentView_support]){
        [transparentView_support removeFromSuperview];
        
    }
}












- (IBAction)helpPage:(id)sender {
    
    CGRect screenSize = [[UIScreen mainScreen] bounds];
    transparentView_support  = [[UIView alloc] initWithFrame:screenSize];
    transparentView_support.backgroundColor=[[UIColor blackColor] colorWithAlphaComponent:0.6];
    UIView *view1=[menu_support valueForKey:@"view"];
    
    
    
    
    UIImageView *arrow1=[[UIImageView alloc]initWithFrame:CGRectMake(view1.frame.origin.x+view1.frame.size.width, view1.frame.origin.y, 50, 50)];
    [arrow1 setImage:[UIImage imageNamed:@"icons/arrow2.png"]];
    UILabel *dashboard  = [[UILabel alloc] initWithFrame:CGRectMake(arrow1.frame.origin.x+arrow1.frame.size.width+20, arrow1.frame.origin.y+arrow1.frame.size.height/2, screenSize.size.width/2, 20)];
    dashboard.text=@"Dashboard";
    dashboard.numberOfLines=2;
    dashboard.textColor=[UIColor whiteColor];
    dashboard.font=[UIFont fontWithName:@"Arial-BoldMT" size:20.0];
    dashboard.minimumScaleFactor=10./dashboard.font.pointSize;
    dashboard.adjustsFontSizeToFitWidth=YES;
    UIImageView *arrow2=[[UIImageView alloc]initWithFrame:CGRectMake(emailsupport.frame.size.width/2 ,emailsupport.frame.origin.y-emailsupport.frame.size.height, 50, 50)];
    [arrow2 setImage:[UIImage imageNamed:@"icons/arrow6.png"]];
    UILabel *sendDot  = [[UILabel alloc] initWithFrame:CGRectMake(screenSize.origin.x+10, arrow2.frame.origin.y-arrow2.frame.size.height, emailsupport.frame.size.width, 20)];
    sendDot.text=@"Email to us if u have any queries";
    sendDot.numberOfLines=2;
    sendDot.textColor=[UIColor whiteColor];
    sendDot.font=[UIFont fontWithName:@"Arial-BoldMT" size:20.0];
    sendDot.minimumScaleFactor=10./sendDot.font.pointSize;
    sendDot.adjustsFontSizeToFitWidth=YES;
    
    UIImageView *arrow3=[[UIImageView alloc]initWithFrame:CGRectMake(call_support.frame.origin.x+call_support.frame.size.width/2,call_support.frame.origin.y-call_support.frame.size.height, 50, 50)];
    [arrow3 setImage:[UIImage imageNamed:@"icons/arrow6.png"]];
    UILabel *callSuport  = [[UILabel alloc] initWithFrame:CGRectMake(call_support.frame.origin.x, (arrow3.frame.origin.y-arrow3.frame.size.height)+10, emailsupport.frame.size.width, 20)];
    callSuport.text=@"Call us if u have any queries";
    callSuport.numberOfLines=2;
    callSuport.textColor=[UIColor whiteColor];
    callSuport.font=[UIFont fontWithName:@"Arial-BoldMT" size:20.0];
    callSuport.minimumScaleFactor=10./callSuport.font.pointSize;
   callSuport.adjustsFontSizeToFitWidth=YES;
    

    [transparentView_support addSubview:arrow3];
    [transparentView_support addSubview:callSuport];
    [transparentView_support addSubview:sendDot];
    [transparentView_support addSubview:arrow2];
    [transparentView_support addSubview: arrow1];
    [transparentView_support addSubview:dashboard];
    [self.view addSubview:transparentView_support];
}
- (IBAction)changeDebug:(id)sender {
    
    if([debugSwitch isOn]){
        
        UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Warning"
                                                         message:@"Enabling debug flag will perform remote logging which will utilize mobile data/wifi and battery. It is recommended to connect to wifi to avoid mobile data usage. After debugging make sure to disable the debug switch!"
                                                        delegate:self
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles:nil, nil];
        
        [alert1 show];
        

        
        [Bugfender setForceEnabled:YES];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"enableDebug"];
        //BFLog(@"Debugging is Enabled");
        
    }else{
        
        [Bugfender setForceEnabled:NO];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"enableDebug"];
        //BFLog(@"Debugging Disabled");
        
        
    }
}
@end
