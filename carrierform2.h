//
//  carrierform2.h
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 9/1/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "logForm.h"
#import "DBManager1.h"
#import "DBManager.h"
#import "Log.h"
#import "logsmenuscreen.h"
#import "FormCommonTableViewCell.h"
#import "CarrierTwoTextTableViewCell.h"

extern NSString *carrier_name1;
extern NSString *main_street1;
extern NSString *main_city1;
extern NSString *main_state1;
extern NSString *main_zip1;
extern NSString *home_street1;
extern NSString *home_city1;
extern NSString *home_state1;
extern NSString *home_zip1;

@interface carrierform2 : UIViewController<UITextFieldDelegate, UIPickerViewDataSource, UIPickerViewDelegate, UITableViewDelegate, UITableViewDataSource>{
    
    UIPickerView *mainPicker;
    UIPickerView *homePicker;
}
@property (nonatomic, strong) DBManager1 *dbManager;

@property (strong, nonatomic) NSArray *startes;
@property (strong, nonatomic) NSArray *statecodes;
@property (weak, nonatomic) IBOutlet UITextField *carrier;

@property (weak, nonatomic) IBOutlet UITextField *main_street;
@property (weak, nonatomic) IBOutlet UITextField *main_city;
@property (weak, nonatomic) IBOutlet UITextField *main_state;
@property (weak, nonatomic) IBOutlet UILabel *carrierLabel;
@property (weak, nonatomic) IBOutlet UILabel *mainLabel;

@property (weak, nonatomic) IBOutlet UITextField *main_zip;
@property (weak, nonatomic) IBOutlet UITextField *home_street;
@property (weak, nonatomic) IBOutlet UILabel *homeLabel;

@property (weak, nonatomic) IBOutlet UITextField *home_city;
@property (weak, nonatomic) IBOutlet UITextField *home_state;
@property (strong, nonatomic) IBOutlet UIScrollView *scroller02;

@property (weak, nonatomic) IBOutlet UITextField *home_zip;
- (IBAction)DoneCarrier:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *done_carrier;
@property (nonatomic, strong) IBOutlet UITableView *tblCarrierForm;
-(IBAction)saveCarrierDetails:(id)sender;
@end
