//
//  companyConnection.h
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 9/7/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//

#import <UIKit/UIKit.h>
extern NSString *dotNumber;
extern NSMutableArray *carrier_array;
extern NSMutableArray *city_array;
@interface companyConnection : UIViewController<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *dot_Num;
- (IBAction)search_DOT:(id)sender;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
@property (weak, nonatomic) IBOutlet UILabel *textDot;


@end
