//
//  companyConnection.m
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 9/7/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//

#import "companyConnection.h"
NSString *dotNumber;
NSMutableArray *carrier_array;
NSMutableArray *city_array;
int flag0=0;
@interface companyConnection ()

@end

@implementation companyConnection
@synthesize dot_Num;
@synthesize textDot;
@synthesize indicator;

- (void)viewDidLoad {
    [super viewDidLoad];
    textDot.minimumScaleFactor=10./textDot.font.pointSize;
    textDot.adjustsFontSizeToFitWidth=YES;
    dot_Num.delegate=self;
   
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if ([textField.text length]<=10) {
        return YES;
    }else{
        
        return NO;
        
    }
    
    
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)search_DOT:(id)sender {
    
    
    if ([dot_Num.text isEqualToString:@""]) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:@"Please enter a DOT#" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }else{
        
        dotNumber=dot_Num.text;
        carrier_array = [[NSMutableArray alloc] init];
        city_array = [[NSMutableArray alloc] init];
        [indicator startAnimating];
        
        NSString *urlString = [NSString stringWithFormat:@"https://mobile.fmcsa.dot.gov/qc/services/carriers/%@?webKey=e943fae222cffa3c7f183c12de605068773db043",dotNumber];
        //NSURL *url = [NSURL URLWithString:urlString];
        //NSData *data = [NSData dataWithContentsOfURL:url];
        // if (data) {
        //BFLog(@"Device connected to the internet");
        NSURLSession *session = [NSURLSession sharedSession];
       
        NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:urlString] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            //  BFLog(@"Retrieved Data:%@,%@",data,response);
           // BFLog(@"---------------------------------------");
            //BFLog(@"error:%@",error);
            if(data){
                
                if(!error){
                    
                    
                    
                    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                    //if (error == nil) {
                    //NSString *text = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                   // BFLog(@"%@",json[@"content"]);
                    NSString *str = [NSString stringWithFormat:@"%@",json[@"content"]];
                    
                    
                    
                    if ([str isKindOfClass:[NSNull class]]||[str hasPrefix:@"Failed to convert"]) {
                        [indicator stopAnimating];
                        UIAlertView *alert2 = [[UIAlertView alloc] initWithTitle:@""
                                                                         message:@"Please check ur DOT#"
                                                                        delegate:self
                                                               cancelButtonTitle:@"OK"
                                                               otherButtonTitles:nil, nil];
                        
                        //  dispatch_async(dispatch_get_main_queue(), ^{
                        
                        [alert2 show];
                          //});
                    }else{
                        
                        //minnu
                        if(![json[@"content"] isKindOfClass:[NSNull class]]){
                            //minnu
                            
                           // BFLog(@"%@",json[@"content"][@"carrier"][@"legalName"]);
                            //BFLog(@"%@",json[@"content"][@"carrier"][@"phyCity"]);
                            [carrier_array addObject:json[@"content"][@"carrier"][@"legalName"]];
                            [city_array addObject:json[@"content"][@"carrier"][@"phyCity"]];
                          
                            [self moveToScrren];
                        }else{
                            
                            
                           // BFLog(@"Invalid Number");
                            flag0=1;
                            [self moveToScrren];
                        }
                        
                    }}else{
                         [indicator stopAnimating];
                        UIAlertView *alert2 = [[UIAlertView alloc] initWithTitle:@"Something went wrong"
                                                                         message:@"Please try again"
                                                                        delegate:self
                                                               cancelButtonTitle:@"OK"
                                                               otherButtonTitles:nil, nil];
                        
                        [alert2 show];
                        
                        
                        
                    }
                
                
            }else{
                [indicator stopAnimating];
                UIAlertView *alert2 = [[UIAlertView alloc] initWithTitle:@""
                                                                 message:@"Please check ur internet connectivity"
                                                                delegate:self
                                                       cancelButtonTitle:@"OK"
                                                       otherButtonTitles:nil, nil];
                
                [alert2 show];
                
            }
            
            //}}
            
        }];
        [dataTask resume];
        
        
        
        
    }
    
    
}
-(void)rotateLayerInfinite:(CALayer *)layer
{
    dispatch_async(dispatch_get_main_queue(), ^{
    CABasicAnimation *rotation;
    rotation = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
    rotation.fromValue = [NSNumber numberWithFloat:0];
    rotation.toValue = [NSNumber numberWithFloat:(2 * M_PI)];
    rotation.duration = 5.0f; // Speed
    rotation.repeatCount = HUGE_VALF; // Repeat forever. Can be a finite number.
    [layer removeAllAnimations];
    [layer addAnimation:rotation forKey:@"Spin"];
     });
}

-(void)moveToScrren{
   [indicator stopAnimating];
    dispatch_async(dispatch_get_main_queue(), ^{
        if(flag0==1)
        {UIAlertView *alert2 = [[UIAlertView alloc] initWithTitle:@""
                                                          message:@"Please check ur DOT#"
                                                         delegate:self
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles:nil, nil];
            
            [alert2 show];
            
        } else{
            
            UIStoryboard *storyBoard = self.storyboard;
            UIViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"findCarriers"];
            [self presentViewController:vc animated:YES completion:nil];
        }
        flag0=0;
    });
    
}
@end
