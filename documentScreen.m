//
//  documentScreen.m
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 8/26/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//

#import "documentScreen.h"
#import "TODocumentPickerViewController.h"
#import "TODocumentsDataSource.h"
#import "logScreenViewController.h"
#import "menuScreenViewController.h"
#import "documentMenuViewController.h"


NSInteger selectedFileIndex1;
NSMutableArray *documentlist;
NSMutableArray *documentlist1;
UIView *transparentView_doc;
NSData *syncResData1;
NSMutableURLRequest *request1;
NSMutableDictionary *docsdetails;
NSDate *doc_date;
NSString *doc_date1;


NSData *docData;
@interface documentScreen () <TODocumentPickerViewControllerDelegate>

@end

@implementation documentScreen
@synthesize scroller_doc;
@synthesize doc_list;
@synthesize dbManager;
@synthesize menu_doc;
@synthesize addDoc;
@synthesize toolBar;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    documentlist=[[NSMutableArray alloc] init];
    documentlist1=[[NSMutableArray alloc] init];

    docsdetails=[[NSMutableDictionary alloc] init];
    dbManager = [[DBManager1 alloc] initWithDatabaseFilename:@"HOSDatabase.sql"];
    NSString *query_docs = [NSString stringWithFormat:@"select * from Docs;"];
    
    // Get the results.
    
    NSArray *docs_db = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query_docs]];
   // BFLog(@"Result:%@",docs_db);
    for (int j=0; j<[docs_db count]; j++) {
        NSString *id1 = docs_db[j][0];
        NSData *data2= [docs_db[j][1] dataUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *json_log1 = [NSJSONSerialization JSONObjectWithData:data2 options:0 error:nil];
        //BFLog(@"Dictionary of db data:%@",json_log1);
        NSString *date = [json_log1 objectForKey:@"date"];
        NSArray *doc_names = [json_log1 objectForKey:@"docs"];
        
        for (int i=0; i<[doc_names count]; i++) {
            NSArray *con1 = @[date,doc_names[i],id1];
            [documentlist addObject:con1];
        }
        
    }
    
    doc_list.delegate=self;
    doc_list.dataSource=self;
    /*[scroller_doc setScrollEnabled:YES];
    [scroller_doc setContentSize:(CGSizeMake(600, 1000))];
    CGPoint bottomOffset = CGPointMake(0, 1000-scroller_doc.bounds.size.height);
    [scroller_doc setContentOffset:bottomOffset animated:YES];*/
    doc_date = [NSDate date];
    NSDateFormatter *outputFormatter_log = [[NSDateFormatter alloc] init];
    [outputFormatter_log setDateFormat:@"MM/dd/YYYY"];
    doc_date1 = [outputFormatter_log stringFromDate:doc_date];

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (IBAction)buttonTapping:(id)sender {
    
    TODocumentPickerViewController *documentPicker = [[TODocumentPickerViewController alloc] initWithFilePath:nil];
    documentPicker.dataSource = [[TODocumentsDataSource alloc] init];
    documentPicker.documentPickerDelegate = self;
    
    UINavigationController *controller = [[UINavigationController alloc] initWithRootViewController:documentPicker];
    controller.modalPresentationStyle = UIModalPresentationFormSheet;
    
    [self presentViewController:controller animated:YES completion:nil];
}
- (void)documentPickerViewController:(TODocumentPickerViewController *)documentPicker didSelectItems:(nonnull NSArray<TODocumentPickerItem *> *)items inFilePath:(NSString *)filePath
{
    [self dismissViewControllerAnimated:YES completion:nil];
    
    NSMutableArray *absoluteItemPaths = [NSMutableArray array];
    for (TODocumentPickerItem *item in items) {
        NSString *absoluteFilePath = [filePath stringByAppendingPathComponent:item.fileName];
        [absoluteItemPaths addObject:absoluteFilePath];
    }
    
    //BFLog(@"Paths for items selected: %@", absoluteItemPaths);
    for (int i=0; i<[absoluteItemPaths count]; i++) {
        NSString *query_docs = [NSString stringWithFormat:@"select MAX(id) from Docs;"];
        
        // Get the results.
        
        NSArray *docId = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query_docs]];
        NSString *fileId=@"";
        if([docId count]!=0){
         fileId= [NSString stringWithFormat:@"%ld",[docId[0][0] integerValue]+1];
        }else{
        fileId = [NSString stringWithFormat:@"%d",1];
            
        }
        NSArray *con2 = @[doc_date1,absoluteItemPaths[i],fileId];
        [documentlist addObject:con2];
        [documentlist1 addObject:absoluteItemPaths[i]];
    }
    //doc_date1=@"08/01/2016";
   
    [doc_list reloadData];
    [docsdetails setValue:doc_date1 forKey:@"date"];
    [docsdetails setObject:documentlist1 forKey:@"docs"];
    // BFLog(@"docs:%@",documentlist);
    [self saveToDB:docsdetails date:doc_date1];
}

-(void)saveToDB:(NSMutableDictionary *)data_docs date:(NSString *)date1{
    NSString *file=[data_docs valueForKey:@"docs"][0];
    file = [file stringByReplacingOccurrencesOfString:@"\\" withString:@""];
    NSArray *fileParts = [file componentsSeparatedByString:@"."];
    
  
    NSData *data = [NSJSONSerialization dataWithJSONObject:data_docs options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonStr = [[NSString alloc] initWithData:data
                                              encoding:NSUTF8StringEncoding];
    
        NSString *query_insert1 = [NSString stringWithFormat:@"insert into Docs values(null, '%@', '%@')",jsonStr,date1];
        
        // Execute the query.
        
        //NSString *query_del = @"delete from TempTable";
        [dbManager executeQuery:query_insert1];
        
        // If the query was successfully executed then pop the view controller.
        if (dbManager.affectedRows != 0) {
           // BFLog(@"Query was inserted the data successfully. Affected rows = %d", dbManager.affectedRows);
            
        }
        else{
            //BFLog(@"Could not execute the query");
        }
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
    
    //} NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    // Get the resource path and read the file using NSData
    NSString *filePath = [documentsDirectory stringByAppendingString:[NSString stringWithFormat:@"%@",file]];
    
    if([[NSFileManager defaultManager]fileExistsAtPath:filePath]){
        
        docData = [[NSFileManager defaultManager] contentsAtPath:filePath];
        [self createConnectionRequestToURL:fileParts[0] extension:fileParts[1]];
        
    }else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"File not exists" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        
        
    }
    
}

-(BOOL) setParams:(NSString*)filename extension:(NSString *)ext{
    //getting authCode
    NSString *query0 = [NSString stringWithFormat:@"select * from authDetails;"];
    NSArray *authData = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query0]];
    NSString *authCode = @"";
    if([authData count]!=0){
        authCode = authData[0][1];
        
    }
    //---
    NSString *urlString =[NSString stringWithFormat:@"http://www.gpstracking-server1.com/gpstracking/hosapp1/saveDocs.php?user=%@&authCode=%@",user_menu,authCode];
    NSURL *url = [NSURL URLWithString:urlString];
    //NSData *data = [NSData dataWithContentsOfURL:url];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"HEAD"];
    NSURLResponse *response;
    NSError *error;
    NSData *mydata = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
    
    
if(docData != nil){
        
    if(httpResponse!=nil){
        
        if ([httpResponse statusCode] ==200 ) {
        
        request1 = [NSMutableURLRequest new];
        request1.timeoutInterval = 20.0;
        [request1 setURL:[NSURL URLWithString:urlString]];
        [request1 setHTTPMethod:@"POST"];
        
        NSString *boundary = @"---------------------------14737809831466499882746641449";
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        [request1 addValue:contentType forHTTPHeaderField: @"Content-Type"];
        [request1 setValue:@"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8" forHTTPHeaderField:@"Accept"];
        [request1 setValue:@"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/536.26.14 (KHTML, like Gecko) Version/6.0.1 Safari/536.26.14" forHTTPHeaderField:@"User-Agent"];
        
        NSMutableData *body = [NSMutableData data];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"uploadedfile\"; filename=\"%@.%@\"\r\n", filename,ext] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[NSData dataWithData:docData]];
        
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        
        [request1 setHTTPBody:body];
        [request1 addValue:[NSString stringWithFormat:@"%lu", (unsigned long)[body length]] forHTTPHeaderField:@"Content-Length"];
        
        return TRUE;
            
        }else{
            
            
            UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                             message:@"Please check your internet connectivity!!!"
                                                            delegate:self
                                                   cancelButtonTitle:@"Cancel"
                                                   otherButtonTitles:@"OK", nil];
            
            [alert1 show];
            
            return FALSE;

        }
        
    }
}
  //  }else{
        
      
        
        return FALSE;
    //}
  
}

-(void)createConnectionRequestToURL:(NSString *)filename extension:(NSString *)ext{
    
    if( [self setParams:filename extension:ext]){
        
        NSError *error = nil;
        NSURLResponse *responseStr = nil;
        syncResData1 = [NSURLConnection sendSynchronousRequest:request1 returningResponse:&responseStr error:&error];
              
        if(error == nil){
           // BFLog(@"%@",returnString);
        }
        
        
        
    }
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
#pragma mark NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    // A response has been received, this is where we initialize the instance var you created
    // so that we can append data to it in the didReceiveData method
    // Furthermore, this method is called each time there is a redirect so reinitializing it
    // also serves to clear it
    _responseData1 = [[NSMutableData alloc] init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    // Append the new data to the instance variable you declared
    [_responseData1 appendData:data];
}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection
                  willCacheResponse:(NSCachedURLResponse*)cachedResponse {
    // Return nil to indicate not necessary to store a cached response for this connection
    return nil;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    // The request is complete and data has been received
    // You can parse the stuff in your instance variable now
    
    
    
  //  BFLog(@"_responseData %@", [[NSString alloc] initWithData:_responseData1 encoding:NSUTF8StringEncoding]);
    
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = FALSE;
    
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    // The request has failed for some reason!
    // Check the error var
    
   // BFLog(@"didFailWithError %@", error);
    
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    int rows;
    if ([documentlist count]==0) {
        rows=1;
    }else{
        rows=(int)[documentlist count];
    }
    return rows;
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *cellIdentifier = @"cell";
   UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    cell.detailTextLabel.textColor=[UIColor blueColor];
    
    if ([documentlist count]==0) {
        cell.textLabel.text=@"No Saved Documents";
    }else{
        
        NSString *doc_name = [documentlist[indexPath.row][1] stringByReplacingOccurrencesOfString:@"/" withString:@""];
        UIButton *removeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        removeBtn.frame = CGRectMake(tableView.bounds.size.width-50, 0, 50, 50);
        selectedFileIndex1=[documentlist[indexPath.row][2] integerValue];
        [removeBtn addTarget:self action:@selector(removeDoc:) forControlEvents:UIControlEventTouchUpInside];
        [removeBtn setImage:[UIImage imageNamed:@"remove.png"] forState:UIControlStateNormal];
        cell.accessoryView=removeBtn;
        cell.textLabel.text=doc_name;
        cell.imageView.image=[UIImage imageNamed:@"docs.png"];
        NSString *detail = [NSString stringWithFormat:@"Uploaded on %@",documentlist[indexPath.row][0]];
        cell.detailTextLabel.text=detail;
    }
    
    
    
    
    return cell;
    
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if(alertView.tag==1){
        if(buttonIndex==0){
            
           
            NSInteger fileId =selectedFileIndex1;
            BFLog(@"fileId:%ld",fileId);
            NSString *query_del8 =[NSString stringWithFormat:@"delete from Docs where id='%ld';",(long)fileId];
            [dbManager executeQuery:query_del8];
            [documentlist removeAllObjects];
            [documentlist1 removeAllObjects];
            NSString *query_docs = [NSString stringWithFormat:@"select * from Docs;"];
            
            // Get the results.
            
            NSArray *docs_db = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query_docs]];
            // BFLog(@"Result:%@",docs_db);
            for (int j=0; j<[docs_db count]; j++) {
                NSString *id1 = docs_db[j][0];
                NSData *data2= [docs_db[j][1] dataUsingEncoding:NSUTF8StringEncoding];
                NSMutableDictionary *json_log1 = [NSJSONSerialization JSONObjectWithData:data2 options:0 error:nil];
                //BFLog(@"Dictionary of db data:%@",json_log1);
                NSString *date = [json_log1 objectForKey:@"date"];
                NSArray *doc_names = [json_log1 objectForKey:@"docs"];
                
                for (int i=0; i<[doc_names count]; i++) {
                    NSArray *con1 = @[date,doc_names[i],id1];
                    [documentlist addObject:con1];
                }
                
            }
            UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@""
                                                             message:@"Successfully Removed"
                                                            delegate:self
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles:nil, nil];
            [alert1 setTag:2];
            
            [alert1 show];
            [doc_list reloadData];

                
                
                
                
                        
            
            
            
        }
        
        
        
        
        
    }
    
    
    
}

-(void)removeDoc:(UIButton*)sender{
    
    UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@""
                                                     message:@"Do you want to delete this file?"
                                                    delegate:self
                                           cancelButtonTitle:@"Delete"
                                           otherButtonTitles:@"Keep", nil];
    
    [alert1 setTag:1];
    [alert1 show];
    
    
    
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if([documentlist count]!=0){
    NSString *file=documentlist[indexPath.row][1];
    filenameSelected=[file stringByReplacingOccurrencesOfString:@"/" withString:@""];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    // Get the resource path and read the file using NSData
    NSString *filePath = [documentsDirectory stringByAppendingString:[NSString stringWithFormat:@"%@",file]];
     UIStoryboard *storyBoard = self.storyboard;
    selectedFile=filePath;
    if(![file  isEqual: @""])
    {
        UIViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"showDocuments"];
        [self presentViewController:vc animated:YES completion:nil];
    }
    }
    
}
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    // UITouch *touch = [touches anyObject];
    if([[self.view subviews] containsObject:transparentView_doc]){
        [transparentView_doc removeFromSuperview];
        
    }
}



-(void)showHelp{
    
    sourceVC = [self valueForKey:@"storyboardIdentifier"];
    UIStoryboard *storyBoard = self.storyboard;
    UIViewController *vc1 = [storyBoard instantiateViewControllerWithIdentifier:@"helpScreen"];
    [self presentViewController:vc1 animated:YES completion:nil];
    
    
    
}






    






- (IBAction)helpAlert:(id)sender {
    CGRect screenSize = [[UIScreen mainScreen] bounds];
    transparentView_doc  = [[UIView alloc] initWithFrame:screenSize];
    transparentView_doc.backgroundColor=[[UIColor blackColor] colorWithAlphaComponent:0.6];
    UIView *view1=[menu_doc valueForKey:@"view"];
     UIView *view2=[addDoc valueForKey:@"view"];
    UIButton *tutorialLink = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    tutorialLink.frame = CGRectMake(screenSize.origin.x+(screenSize.size.width/3), screenSize.size.height-40, screenSize.size.width/3, 40);
    [tutorialLink addTarget:self action:@selector(showHelp) forControlEvents:UIControlEventTouchUpInside];
    tutorialLink.tintColor = [UIColor yellowColor];
    [tutorialLink setTitle:@"<<Know More>>" forState:UIControlStateNormal];
    
    [transparentView_doc addSubview:tutorialLink];
    

    
    
    
    UIImageView *arrow1=[[UIImageView alloc]initWithFrame:CGRectMake(view1.frame.origin.x+view1.frame.size.width, view1.frame.origin.y, 50, 50)];
    [arrow1 setImage:[UIImage imageNamed:@"icons/arrow2.png"]];
    UILabel *dashboard  = [[UILabel alloc] initWithFrame:CGRectMake(arrow1.frame.origin.x+arrow1.frame.size.width+20, arrow1.frame.origin.y+arrow1.frame.size.height/2, screenSize.size.width/2, 20)];
    dashboard.text=@"Dashboard";
    dashboard.numberOfLines=2;
    dashboard.textColor=[UIColor whiteColor];
    dashboard.font=[UIFont fontWithName:@"Arial-BoldMT" size:20.0];
    dashboard.minimumScaleFactor=10./dashboard.font.pointSize;
    dashboard.adjustsFontSizeToFitWidth=YES;
    
    
    if ([documentlist count]!=0) {
        UIImageView *arrow2=[[UIImageView alloc]initWithFrame:CGRectMake(screenSize.size.width/2 ,doc_list.frame.origin.y+[doc_list sectionHeaderHeight]+[doc_list rowHeight]*2+50*[documentlist count], 50, 50)];
        [arrow2 setImage:[UIImage imageNamed:@"icons/arrow7.png"]];
        UILabel *sendDot  = [[UILabel alloc] initWithFrame:CGRectMake(screenSize.origin.x+10, arrow2.frame.origin.y+arrow2.frame.size.height, screenSize.size.width, 20)];
        sendDot.text=@"Click here to view the documents";
        sendDot.numberOfLines=2;
        sendDot.textColor=[UIColor whiteColor];
        sendDot.font=[UIFont fontWithName:@"Arial-BoldMT" size:20.0];
        sendDot.minimumScaleFactor=10./sendDot.font.pointSize;
        sendDot.adjustsFontSizeToFitWidth=YES;
            [transparentView_doc addSubview:sendDot];
    [transparentView_doc addSubview:arrow2];
    }
    UIImageView *arrow3=[[UIImageView alloc]initWithFrame:CGRectMake( view2.frame.origin.x-50,toolBar.frame.origin.y-toolBar.frame.size.height, 50, 50)];
    [arrow3 setImage:[UIImage imageNamed:@"icons/arrow6.png"]];
    UILabel *addBtn  = [[UILabel alloc] initWithFrame:CGRectMake(screenSize.origin.x+50, arrow3.frame.origin.y-10, screenSize.size.width, 20)];
    addBtn.text=@"Click here to add Documents";
   addBtn.numberOfLines=2;
    addBtn.textColor=[UIColor whiteColor];
    addBtn.font=[UIFont fontWithName:@"Arial-BoldMT" size:20.0];
    addBtn.minimumScaleFactor=10./addBtn.font.pointSize;
    addBtn.adjustsFontSizeToFitWidth=YES;
    
    [transparentView_doc addSubview:arrow3];
    [transparentView_doc addSubview:addBtn];
    [transparentView_doc addSubview: arrow1];
    [transparentView_doc addSubview:dashboard];
    [self.view addSubview:transparentView_doc];
}
@end
