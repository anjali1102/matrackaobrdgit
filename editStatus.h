//
//  editStatus.h
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 9/9/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "previousDayViewController.h"
#import "DBManager1.h"
#import "editView.h"
#import "menuScreenViewController.h"
#import "annotation.h"
#import "logScreenViewController.h"
#import "graphDataOperations.h"
#import "logsmenuscreen.h"
#import "DBManager.h"
#import "AppDelegate.h"
#import "commonDataHandler.h"
#import "HOSRecap.h"
#import "SectionRows.h"
@interface editStatus : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    NSString *statusString;
}
- (IBAction)saveBtn:(id)sender;
- (IBAction)backBtn:(id)sender;
@property (nonatomic, strong) DBManager1 *dbManager;
@property (strong, nonatomic) IBOutlet UITableView *edit_Table;
@property (nonatomic, strong) IBOutlet UILabel *lblDate;
@property (nonatomic, strong) IBOutlet UILabel *lblStartEndTime;
@property (nonatomic, strong) SectionRows *objEditRow;
@end
