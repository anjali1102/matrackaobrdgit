//
//  findCarrier.m
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 9/21/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//

#import "findCarrier.h"

@interface findCarrier ()

@end

@implementation findCarrier
@synthesize carrier_List;


- (void)viewDidLoad {
    [super viewDidLoad];
    carrier_List.delegate=self;
    carrier_List.dataSource=self;

}

-(void)viewWillAppear:(BOOL)animated{
    
    [carrier_List reloadData];
    
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    int rows=0;
    if ([carrier_array count]==0) {
        rows=1;
    }else{
        rows=(int)[carrier_array count];
    }
    
    return rows;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *identifier = @"cell";
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
    if ([carrier_array count]==0) {
        cell.textLabel.text=@"No Carrier Found";
        cell.detailTextLabel.text=@"";
        
    }else{
        cell.textLabel.text=carrier_array[indexPath.row];
        cell.detailTextLabel.text=city_array[indexPath.row];
        cell.imageView.image=[UIImage imageNamed:@"icons/trailer.png"];
        
    }
   
    return cell;
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
