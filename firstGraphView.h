//
//  firstGraphView.h
//  MaTrackHOS
//
//  Created by Raghee Chandran M on 7/12/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

extern CGRect current_rectfront;
extern int flag;
#define kkGraphHeight 75
//#define kkDefaultGraphWidth 355.5
//#define kkOffsetX 29
//#define kkStepX
#define kkGraphBottom 0
#define kkGraphTop 117
#define kkStepY 15
#define kkOffsetY 0
#define kkCircleRadius 1
@interface firstGraphView : UIView

@end
