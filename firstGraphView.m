//
//  firstGraphView.m
//  MaTrackHOS
//
//  Created by Raghee Chandran M on 7/12/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//

#import "firstGraphView.h"
#import "menuScreenViewController.h"

CGContextRef context_front;
CGRect current_rectfront;
NSString  *newDateString11;
float kkStepX2=0;
float kkUnit=0;
int multiplier=0;
NSMutableArray *data;

@implementation firstGraphView
- (void)drawLineGraphWithContext:(CGContextRef)ctx
{
    CGRect screenRect=[[UIScreen mainScreen] bounds];
    CGFloat screenWidth =screenRect.size.width;
    float kkOffsetX=(31.75/414)*screenWidth;
    ////BFLog(@"%@,%@",start_time,end_time);
   // //BFLog(@"graph loading......%@",current_status);
    CGContextSetLineWidth(ctx, 2.0);
    CGContextSetStrokeColorWithColor(ctx, [[UIColor yellowColor] CGColor]);
    
    //  if (flag==0) {
    
    // int maxGraphHeight = kGraphHeight - kOffsetY;
    //int y_origin=0;
    int y_offset=0;
    NSString *x1;
    CGContextBeginPath(ctx);
    if (![data[0][1]  isEqual: @""]) {
          x1=data[0][1];
    }else{
        
          x1=@"OFF";
    }
  
    //BFLog(@"current status:%@",x1);
    if([x1  isEqual: @"ON"]){
        y_origin3 = 5;
        CGContextMoveToPoint(ctx, kkOffsetX, ((y_origin3) * kkStepY));
        float x = kkOffsetX;
        y_offset=23/1.6;
        float y = ((y_origin3) * kkStepY)+y_offset;
        CGRect rect = CGRectMake(x - kkCircleRadius, y - kkCircleRadius, 2 * kkCircleRadius, 2 * kkCircleRadius);
        CGContextAddEllipseInRect(ctx, rect);
    }else  if([x1  isEqual: @"D"]){
        y_origin3 = 4;
        CGContextMoveToPoint(ctx, kkOffsetX, ((y_origin3) * kkStepY));
        float x = kkOffsetX;
        y_offset=23/2.5;
        float y = ((y_origin3) * kkStepY)+y_offset;
        CGRect rect = CGRectMake(x - kkCircleRadius, y - kkCircleRadius, 2 * kkCircleRadius, 2 * kkCircleRadius);
        CGContextAddEllipseInRect(ctx, rect);
    }else  if([x1  isEqual: @"SB"]){
        y_origin3 = 3;
        y_offset = 23/4;
        float x = kkOffsetX;
        float y = ((y_origin3) * kkStepY)+y_offset;
        CGRect rect = CGRectMake(x - kkCircleRadius, y - kkCircleRadius, 2 * kkCircleRadius, 2 * kkCircleRadius);
        CGContextAddEllipseInRect(ctx, rect);
        CGContextMoveToPoint(ctx, kkOffsetX, ((y_origin3) * kkStepY));
        
    }else  if([x1  isEqual: @"OFF"]){
        y_origin3 = 2;
        float x = kkOffsetX;
        float y = ((y_origin3) * kkStepY);
        CGRect rect = CGRectMake(x - kkCircleRadius, y - kkCircleRadius, 2 * kkCircleRadius, 2 * kkCircleRadius);
        CGContextAddEllipseInRect(ctx, rect);
        CGContextMoveToPoint(ctx, kkOffsetX, ((y_origin3) * kkStepY));
    }
    //BFLog(@"y origin:%d",y_origin3);
    int data_size =(int) [data count];
    //BFLog(@"array size:%d",data_size);
 for (int i = 0; i < data_size; i++)
   {
        int y_pos=0;
        NSArray *time = [data[i][0] componentsSeparatedByString:@":"];
        NSString *second_part=@"";
       
        int float_part =(int) [time[1] integerValue];
        if (float_part>=0 && float_part<15) {
            second_part=@"00";
            multiplier=0;
        }else if (float_part>=15 && float_part <30)    {
            second_part = @"15";
            multiplier=1;
            
        }else if (float_part>=30 && float_part<45){
            second_part=@"30";
            multiplier=2;
            
            
        }else if (float_part>=45){
            second_part=@"45";
            multiplier=3;
            
            
        }
        NSString *starttime = [NSString stringWithFormat:@"%@.%@",time[0],second_part];
        float x_one = [starttime floatValue];
       NSString *time_float = [NSString stringWithFormat:@"%@",time[0]];
       float x_one1 = [time_float floatValue];
        int y=0;
        NSString *x2 = data[i][1];
        if([x2  isEqual: @"ON"]){
            y_pos = 5;
            y_offset = 23/1.6;
            y = ((y_pos) * kkStepY)+y_offset;
            //NSString *posy = [NSString stringWithFormat:@"%d",y];
            //data[i][1]=posy;
            
        }else  if([x2  isEqual: @"D"]){
            y_pos = 4;
            y_offset = 23/2.5;
            y = ((y_pos) * kkStepY)+y_offset;
            // NSString *posy = [NSString stringWithFormat:@"%d",y];
            //data[i][1]=posy;             ;
        }else  if([x2  isEqual: @"SB"]){
            y_pos = 3;
            y_offset = 23/4;
            y = ((y_pos) * kkStepY)+y_offset;
            //NSString *posy = [NSString stringWithFormat:@"%d",y];
            //data[i][1]=posy;
            
        }else  if([x2  isEqual: @"OFF"]){
            y_pos = 2;
            y = ((y_pos) * kkStepY);
            //NSString *posy = [NSString stringWithFormat:@"%d",y];
            //data[i][1]=posy;
        }
        //BFLog(@"x,y:%f,%d",x_one,y);
       float x = kkOffsetX+(kkStepX2/4*(x_one1*4+multiplier));
        CGContextAddLineToPoint(ctx, (x+kkCircleRadius),y);
     

    //   float x = kkOffsetX+(kkStepX2/4*(x_one1*4+multiplier));
       float y1 = y;
       CGRect rect = CGRectMake(x - kkCircleRadius, y1 - kkCircleRadius, 2 * kkCircleRadius, 2 * kkCircleRadius);
       CGContextAddEllipseInRect(ctx, rect);
       CGContextSetFillColorWithColor(ctx, [[UIColor redColor] CGColor]);
          }
        ////BFLog(@"new array with new y pos%@",dataarray3);
    CGContextDrawPath(ctx, kCGPathStroke);
    
    
    
    ////BFLog(@"dta size:%lu",sizeof(data)/sizeof(float));
    //sample_data = self.data;
    //BFLog(@"data:%@",data);
    
    CGContextDrawPath(ctx, kCGPathFillStroke);
    
    
    
    
    //CGContextRetain(ctx);
    UIGraphicsPushContext(ctx);
    CGContextStrokePath(ctx);
    
    
    
}
-(void)drawAll:(CGContextRef)ctx{
    if([deviceData count]!=0){
        UIColor *lineColor;
        int y_offset=23/1.6;
        float y = ((5.5) * kkStepY)+y_offset;
        
        for(int i=0;i<[deviceData count];i++){
            
            if([deviceData[i][@"event"] isEqual:@"4002"]){
                
                
                lineColor = [UIColor whiteColor];
            }else if([deviceData[i][@"event"] isEqual:@"6045"]){
                
                
                lineColor = [UIColor greenColor];
            }else if([deviceData[i][@"event"] isEqual:@"4001"]){
                if([deviceData[i][@"speed"] floatValue]>0){
                    
                    lineColor = [UIColor colorWithRed:0.00 green:0.93 blue:0.00 alpha:1.0];
                    
                }else{
                    
                    
                    lineColor = [UIColor colorWithRed:0.93 green:0.00 blue:0.00 alpha:1.0];
                }
            }else if([deviceData[i][@"event"] isEqual:@"6011"] || [deviceData[i][@"event"] isEqual:@"6012"]){
                
                
                
                lineColor = [UIColor colorWithRed:0.99 green:0.99 blue:0.16 alpha:1.0];
            }else{
                
                lineColor = [UIColor whiteColor];
            }
            
            CGRect screenRect=[[UIScreen mainScreen] bounds];
            CGFloat screenWidth =screenRect.size.width;
            float kkOffsetX=(31.75/414)*screenWidth;
            CGContextSetLineWidth(ctx, 3.0);
            CGContextSetStrokeColorWithColor(ctx, [lineColor CGColor]);
            CGContextBeginPath(ctx);
            NSArray *time = [deviceData[i][@"starttime"] componentsSeparatedByString:@":"];
            NSString *second_part=@"";
            
            int float_part =(int) [time[1] integerValue];
            if (float_part>=0 && float_part<15) {
                second_part=@"00";
                multiplier=0;
            }else if (float_part>=15 && float_part <30)    {
                second_part = @"15";
                multiplier=1;
                
            }else if (float_part>=30 && float_part<45){
                second_part=@"30";
                multiplier=2;
                
                
            }else if (float_part>=45){
                second_part=@"45";
                multiplier=3;
                
                
            }
           // NSString *starttime = [NSString stringWithFormat:@"%@.%@",time[0],second_part];
           // float x_one = [starttime floatValue];
            NSString *time_float = [NSString stringWithFormat:@"%@",time[0]];
            float x_one1 = [time_float floatValue];
            
            float x1 = kkOffsetX+(kkStepX2/4*(x_one1*4+multiplier));
            CGContextMoveToPoint(ctx, x1, y);
            
            /*-------------------------------------------*/
            
            NSArray *time1 = [deviceData[i][@"endtime"] componentsSeparatedByString:@":"];
            NSString *second_part1=@"";
            
            int float_part1=(int) [time1[1] integerValue];
            if (float_part1>=0 && float_part1<15) {
                second_part1=@"00";
                multiplier=0;
            }else if (float_part1>=15 && float_part1 <30)    {
                second_part1 = @"15";
                multiplier=1;
                
            }else if (float_part1>=30 && float_part1<45){
                second_part1=@"30";
                multiplier=2;
                
                
            }else if (float_part1>=45){
                second_part1=@"45";
                multiplier=3;
                
                
            }
           // NSString *starttime1 = [NSString stringWithFormat:@"%@.%@",time1[0],second_part1];
            //float x_one2 = [starttime1 floatValue];
            NSString *time_float1 = [NSString stringWithFormat:@"%@",time1[0]];
            float x_one3 = [time_float1 floatValue];
            
            float x2 = kkOffsetX+(kkStepX2/4*(x_one3*4+multiplier));
            
            CGContextAddLineToPoint(ctx, (x2+kkCircleRadius),y);
            
            CGContextDrawPath(ctx, kCGPathStroke);
            
            CGContextDrawPath(ctx, kCGPathFillStroke);
            
            UIGraphicsPushContext(ctx);
            CGContextStrokePath(ctx);
            
            
            
            
        }
    }
    
}



-(void)drawRect:(CGRect)rect {
    //[dataarray3 removeAllObjects];
   // NSString *curr = @"SB";
    if(dataDictArray.count > 0){
        data=[[NSMutableArray alloc]initWithArray:[dataDictArray objectAtIndex:0]];
        CGRect screenRect=[[UIScreen mainScreen] bounds];
        CGFloat screenWidth =screenRect.size.width;
        float kkOffsetX=screenWidth*(31.25/414);
        float kkDefaultGraphWidth=screenWidth-screenWidth*(58.5/414);
        //BFLog(@"view refreshingg....:%@",NSStringFromCGRect(rect));
        current_rectfront = CGRectFromString(NSStringFromCGRect(rect));
        //flag=flag1;
        context_front = UIGraphicsGetCurrentContext();
        //current_context = UIGraphicsGetCurrentContext();
        //BFLog(@"current context:%@",context_front);
        CGContextSetLineWidth(context_front, 0.6);
        CGContextSetStrokeColorWithColor(context_front, [[UIColor lightGrayColor] CGColor]);
        //CGFloat dash[] = {2.0, 2.0};
        //CGContextSetLineDash(context, 0.0, dash, 2);    // How many lines?
        // int howManyk = (kkDefaultGraphWidth - kkOffsetX) / kkStepX22;
        int howManyk=24;
        kkStepX2=kkDefaultGraphWidth/howManyk;
        kkUnit=kkStepX2/4.0;
        
        //BFLog(@"vertical lines:%d",howManyk);
        // Here the lines go
        for (int i = 0; i <= howManyk; i++)
        {
            CGContextMoveToPoint(context_front, kkOffsetX + i * kkStepX2, kkGraphTop);
            CGContextAddLineToPoint(context_front, kkOffsetX + i * kkStepX2, kkGraphBottom);
        }
        //int howManyHorizontal = (kGraphBottom - kGraphTop - kOffsetY) / kStepY;
        int howManyHorizontalk = 4;
        //BFLog(@"horizontal lines:%d",howManyHorizontalk);
        for (int i = 0; i <howManyHorizontalk; i++)
        {
            //BFLog(@"%f,%d",kkOffsetX, kkGraphBottom - kkOffsetY - i * kkStepY);
            CGContextMoveToPoint(context_front, kkOffsetX, kkGraphBottom - kkOffsetY - i * kkStepY);
            ////BFLog(@"%d,%f",kDefaultGraphWidth, kGraphBottom - kOffsetY - i * kStepY);
            ////BFLog(@"------------------");
            CGContextAddLineToPoint(context_front, kkDefaultGraphWidth, kkGraphBottom - kkOffsetY - i * kkStepY);
        }
        CGContextStrokePath(context_front);
        CGContextSetLineDash(context_front, 0, NULL, 0); // Remove the dash
        
        
        UIImage *image = [UIImage imageNamed:@"graph_green.png"];
        //CGRect screenRect=[[UIScreen mainScreen] bounds];
        // CGFloat screenWidth =screenRect.size.width;
        CGRect imageRect = CGRectMake(0, 0,screenWidth,117);
        
        //BFLog(@"width--------------------%f",screenWidth);
        CGContextDrawImage(context_front, imageRect, image.CGImage);
        
        NSDate * now = [NSDate date];
        NSDateFormatter *outputFormatter1 = [[NSDateFormatter alloc] init];
        [outputFormatter1 setDateFormat:@"HH:mm"];
        newDateString11 = [outputFormatter1 stringFromDate:now];
        //NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
        /*  [outputFormatter setDateFormat:@"MM/dd/YYYY"];
         NSString *dateString = [outputFormatter stringFromDate:now];
         //BFLog(@"newDateString %@", dateString);*/
        // [self getData:dateString context:context_front];
        //if (flag != 0) {
        // flag = 0;
        if ([data count]!=0) {
            //BFLog(@"data array size:%lu",(unsigned long)[data count]);
            [self drawLineGraphWithContext:context_front];
            [self drawAll:context_front];
        }
        //}
    }
}


@end
