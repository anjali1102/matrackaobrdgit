//
//  formGeneral.m
//  MaTrackHOSV1
//
//  Created by Minnu Mohandas on 24/10/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import "formGeneral.h"

@implementation formGeneral
@synthesize userid;
@synthesize date;
@synthesize vehicle;
@synthesize trailer;
@synthesize distance;
@synthesize shippingdoc;
@synthesize firstname;
@synthesize lastname;
@synthesize driverid;
@end
