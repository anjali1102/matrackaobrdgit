 //
//  generalForm.h
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 9/1/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "logForm.h"
#import "DBManager1.h"
#import "formGeneral.h"
#import "logsmenuscreen.h"
#import "DBManager.h"
extern NSString *vehicleValue;
extern NSString *trailer;
extern NSString *distance_1;
extern NSString *shipping_docs;
extern NSString *driver_1;
@interface generalForm : UIViewController<UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, strong) DBManager1 *dbManager;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loader;
@property (nonatomic, strong) IBOutlet UITableView *tblGeneral;
- (IBAction)doneedit:(id)sender;
@end
