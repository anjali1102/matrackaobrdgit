//
//  annotation.h
//  annotation
//
//  Created by Minnu Mohandas on 10/10/17.
//  Copyright © 2017 Minnu Mohandas. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface annotation: NSObject
-(void) StringModify;
-(NSString *)updateStr:(NSString* )value index1:(NSInteger)index index2:(NSInteger)repeat notes:(NSString *) s;
@end
