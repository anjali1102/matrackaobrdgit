//
//  helpTutorialViewController.h
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 7/3/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface helpTutorialViewController : UIViewController<UIWebViewDelegate>

- (IBAction)backBuuton:(id)sender;
@property (weak, nonatomic) IBOutlet UIWebView *scroller_web;
@property (weak, nonatomic) IBOutlet UIScrollView *scroller_help;
@end
