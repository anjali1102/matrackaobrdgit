//
//  listFiles.m
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 8/30/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//

#import "listFiles.h"
NSArray *filePathsArray;
int selected_fileIndex=0;
@interface listFiles ()

@end
UIView *transparentView_listdot;
@implementation listFiles
@synthesize filelist;
@synthesize menu_listdot;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    filelist.delegate=self;
    filelist.dataSource=self;
    filelist.allowsMultipleSelectionDuringEditing=NO;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    
       filePathsArray =
    [[NSFileManager defaultManager] subpathsOfDirectoryAtPath:documentsDirectory
                                                        error:nil];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF BEGINSWITH 'dot'"];
    filePathsArray =  [filePathsArray filteredArrayUsingPredicate:predicate];
    if ([filePathsArray count]==0) {
        
    }
    //BFLog(@"\n\n Sorted files by extension %@",filePathsArray);
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return YES;
    
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    NSFileManager *filemanager = [NSFileManager defaultManager];
    if (editingStyle==UITableViewCellEditingStyleDelete) {
        int selected=(int)indexPath.row;
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        
        // Get the resource path and read the file using NSData
        NSString *filePath = [documentsDirectory stringByAppendingString:[NSString stringWithFormat:@"/%@",filePathsArray[selected]]];
      
        NSError *error;
        BOOL sucess=[filemanager removeItemAtPath:filePath error:&error];
       
        if (sucess) {
            NSMutableArray *dummy=[NSMutableArray arrayWithArray:filePathsArray];
            [dummy removeObjectAtIndex:indexPath.row];
            filePathsArray=[NSArray arrayWithArray:dummy];
            [tableView reloadData];
            UIAlertView *removed = [[UIAlertView alloc] initWithTitle:@"" message:@"Successfully Removed the Selected File!!!!" delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
            [removed show];
            
        }else{
            UIAlertView *removed = [[UIAlertView alloc] initWithTitle:@"Sorry!!something went wrong" message:@"Could not delete selected file" delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
            [removed show];
            
            
        }
    }
    
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    int rows;
    
    if ([filePathsArray count]!=0) {
        rows = (int) [filePathsArray count];
    }else{
        rows=1;
    }
    
    return rows;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UIImageView *image1 = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"icons/send.png"]];

    NSString *cellIdentifier = @"cell";
    // NSString *data=@"";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    //cell.detailTextLabel.backgroundColor=[UIColor grayColor];
    
    if ([filePathsArray count]!=0) {
        cell.imageView.image=[UIImage imageNamed:@"icons/pdf.png"];
        cell.detailTextLabel.textColor=[UIColor blackColor];
       // cell.detailTextLabel.backgroundColor= [UIColor cyanColor];
    //cell.detailTextLabel.text=@"Send";
        cell.accessoryView=image1;
    
    cell.textLabel.text=filePathsArray[indexPath.row];
    }else{
        
        cell.detailTextLabel.textColor=[UIColor whiteColor];
        cell.detailTextLabel.backgroundColor= [UIColor whiteColor];
        cell.textLabel.text = @"No DOT Reports are available";
        cell.detailTextLabel.text=@" ";

    }
    

    return cell;



}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    selected_fileIndex=(int)indexPath.row;
    UIStoryboard *storyBoard = self.storyboard;
    if([filePathsArray count]!=0)
    {
    UIViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"showReport"];
    [self presentViewController:vc animated:YES completion:nil];
    }
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    // UITouch *touch = [touches anyObject];
    if([[self.view subviews] containsObject:transparentView_listdot]){
        [transparentView_listdot removeFromSuperview];
        
    }
}









- (IBAction)helpAlert:(id)sender {
    CGRect screenSize = [[UIScreen mainScreen] bounds];
    transparentView_listdot  = [[UIView alloc] initWithFrame:screenSize];
    transparentView_listdot.backgroundColor=[[UIColor blackColor] colorWithAlphaComponent:0.6];
    UIView *view1=[menu_listdot valueForKey:@"view"];
    
    
    
    
    UIImageView *arrow1=[[UIImageView alloc]initWithFrame:CGRectMake(view1.frame.origin.x+view1.frame.size.width, view1.frame.origin.y, 50, 50)];
    [arrow1 setImage:[UIImage imageNamed:@"icons/arrow2.png"]];
    UILabel *dashboard  = [[UILabel alloc] initWithFrame:CGRectMake(arrow1.frame.origin.x+arrow1.frame.size.width+20, arrow1.frame.origin.y+arrow1.frame.size.height/2, screenSize.size.width/2, 20)];
    dashboard.text=@"Back to DOT Menu screen";
    dashboard.numberOfLines=2;
    dashboard.textColor=[UIColor whiteColor];
    dashboard.font=[UIFont fontWithName:@"Arial-BoldMT" size:20.0];
    dashboard.minimumScaleFactor=10./dashboard.font.pointSize;
    dashboard.adjustsFontSizeToFitWidth=YES;
    UIImageView *arrow2=[[UIImageView alloc]initWithFrame:CGRectMake(screenSize.size.width-80 ,filelist.frame.origin.y+[filelist sectionHeaderHeight], 50, 50)];
    [arrow2 setImage:[UIImage imageNamed:@"icons/arrow4.png"]];
    UILabel *sendDot  = [[UILabel alloc] initWithFrame:CGRectMake(screenSize.origin.x+10, filelist.frame.origin.y+[filelist sectionHeaderHeight]+50, screenSize.size.width, 20)];
    sendDot.text=@"Click here to email the DOT Report";
    sendDot.numberOfLines=2;
    sendDot.textColor=[UIColor whiteColor];
    sendDot.font=[UIFont fontWithName:@"Arial-BoldMT" size:20.0];
    sendDot.minimumScaleFactor=10./sendDot.font.pointSize;
    sendDot.adjustsFontSizeToFitWidth=YES;
    [transparentView_listdot addSubview:sendDot];
    [transparentView_listdot addSubview:arrow2];
    [transparentView_listdot addSubview: arrow1];
    [transparentView_listdot addSubview:dashboard];
    [self.view addSubview:transparentView_listdot];
}
@end
