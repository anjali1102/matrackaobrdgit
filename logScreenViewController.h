//
//  logScreenViewController.h
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 7/26/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "firstGraphView.h"
#import "DBManager1.h"
#import "VCFloatingActionButton.h"
#import "MGSwipeTableCell.h"
#import "Reachability.h"
#import <SystemConfiguration/SystemConfiguration.h>
#import "Carrier.h"
#import "DBManager/DBManager.h"
#import "graphDataOperations.h"
#import "AppDelegate.h"
#import "VehicleListViewController.h"
#import "StatusViewController.h"

extern NSString *selected_previous;
extern NSUInteger selected_index;
extern int refresh_flag;
extern NSString *masterUser;
extern NSString *user;
extern int section_index;
extern int row_index;
extern NSString *current_status;
extern NSString *driver_names;
extern NSString *carrier_name;
extern NSString *selected_date;
extern NSString *main_office;
@class Reachability;
@interface logScreenViewController : UIViewController<UITableViewDelegate, UITableViewDataSource, floatMenuDelegate,UIAlertViewDelegate,MGSwipeTableCellDelegate,UIScrollViewDelegate,UIAlertViewDelegate, VehicleListViewControllerDelegate>{
    UIDatePicker *picker;
    UIAlertView *alert;
    Reachability* internetReachable;
    Reachability* hostReachable;
}

@property (weak, nonatomic) IBOutlet UIScrollView *scroller;
- (IBAction)infoBtn:(id)sender;
@property (weak, nonatomic) IBOutlet UITableView *logtable;
@property (nonatomic, strong) DBManager1 *dbManager;
//@property (nonatomic, strong) DBManager *dbManager1;
@property (nonatomic, strong) NSArray *arrPeopleInfo;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *menu;
@property (strong, nonatomic) VCFloatingActionButton *addButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *info;
-(void)callChanges:(NSString*)authCode;
@property (weak, nonatomic) IBOutlet UINavigationItem *navBarItem;
@property (weak, nonatomic) IBOutlet UINavigationBar *navBar;
@property (nonatomic) BOOL isFromPush;
@property (nonatomic) BOOL isImmediate;
@property (nonatomic) driver_status driverStatus;
@end
