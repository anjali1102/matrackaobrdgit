//
//  loggraph.h
//  MaTrackHOSV1
//
//  Created by Minnu Mohandas on 27/10/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface loggraph : NSObject
@property (nonatomic, strong) NSString *driverStatus;
@property (nonatomic, strong) NSString *calculatedStatus;
@property (nonatomic, strong) NSString *slot;
@property (nonatomic, strong) NSString *location;
@property (nonatomic, strong) NSString *notes;
@property (nonatomic, strong) NSString *date;
@property (nonatomic, strong) NSString *userId;
@property (nonatomic, strong) NSString *insert;
@property (nonatomic, strong) NSString *approve;
@end
