//
//  loggraph.m
//  MaTrackHOSV1
//
//  Created by Minnu Mohandas on 27/10/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import "loggraph.h"

@implementation loggraph
@synthesize  driverStatus;
@synthesize calculatedStatus;
@synthesize  slot;
@synthesize location;
@synthesize notes;
@synthesize date;
@synthesize userId;
@synthesize insert;
@synthesize approve;
@end
