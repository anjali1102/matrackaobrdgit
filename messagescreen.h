//
//  messagescreen.h
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 8/23/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DBManager1.h"


@interface messagescreen : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) DBManager1 *dbManager;
- (IBAction)helpAlert:(id)sender;
@property (weak, nonatomic) IBOutlet UIScrollView *scroller_msg;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *menu_messages;
@property (weak, nonatomic) IBOutlet UITableView *msg_list;
@end
