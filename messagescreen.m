//
//  messagescreen.m
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 8/23/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//

#import "messagescreen.h"
#import "LogSettings.h"
#import "menuScreenViewController.h"
#import "HOSRecap.h"

@interface messagescreen ()

@end
int numCycles;
UIView  *transparentView_messages;
float break_tym=0.0;
float driving_hr=0.0;
float duty_hr=0.0;
float duty_window=0.0;
float restart_hr;
float brk_hr=0.0;
float cycle_tym=0.0;
float cycle_tot=0.0;
float offduty=0.0;
float onduty=0.0;
float driving=0.0;
float driving_cons=0.0;
NSArray *previousdates_1;
NSInteger numCycleStop=0;
BOOL restart_found=false;
BOOL duty_flag=false;
BOOL break_flag=false;
BOOL break_30=false;
BOOL break_long=false;
BOOL driving_flag=false;
BOOL finished=false;
NSMutableArray *violationInfo;
NSMutableArray *messages;
NSMutableArray *submessages;
@implementation messagescreen
@synthesize dbManager;
@synthesize scroller_msg;
@synthesize msg_list;
@synthesize menu_messages;

- (void)viewDidLoad {
    violationInfo = [[NSMutableArray alloc] init];
    NSMutableArray *previousdatesTemp;
    DBManager *objDB = [[DBManager alloc] initWithDatabaseFilename:@"hosDB.sql"];
    NSDictionary *token;
    NSMutableDictionary *whereToken;
    NSArray *userInfoArray=[[NSArray alloc]initWithArray: [objDB getUserId]];
    if([userInfoArray count]!=0){
        int userId = [userInfoArray[0][0] intValue];
        token=@{@"columnName":@"id",@"entry":[NSString stringWithFormat:@"%d",userId]};
        ;
        whereToken=[[NSMutableDictionary alloc] initWithDictionary:token];
        NSArray *timezoneCycle=[[NSArray alloc]initWithArray:[objDB getlogSettings:whereToken]];
        //        NSLog(@"timezon settings:%@",timezoneCycle);
        //minnu
        
    restart_hr=[timezoneCycle[0][3] floatValue];//minnu 34/24
    cycleinfo=timezoneCycle[0][1];
    if([cycleinfo isEqualToString:@"USA 70 hours/8 days"]||[cycleinfo isEqualToString:@"California 80 hours/8 days"]){   numCycles=8;
        cycle_tym=3470.00;//posx value of 70hr -- call [self calculatePosx:@"70:00"];
        
    }else{
        numCycles=7;
        cycle_tym=2980.00;//posx value for 60hr
    }
    
    if([cargoinfo isEqualToString:@"Passenger"]){
        driving_hr=530.00;//posx of 10:00
        duty_window=775.00;//posx of 15:00
        break_tym=[self calculatePosx:@"08:00"];
        
        
    }else{
        driving_hr=579.00;//posx of 11:00
        duty_window=726.00;//posx of 14:00
        break_tym=[self calculatePosx:@"10:00"];
        
        
    }

       dbManager = [[DBManager1 alloc] initWithDatabaseFilename:@"HOSDatabase.sql"];
    messages=[[NSMutableArray alloc] init];
    submessages=[[NSMutableArray alloc] init];

    [scroller_msg setScrollEnabled:YES];
    [scroller_msg setContentSize:(CGSizeMake(600, 1000))];
    CGPoint bottomOffset = CGPointMake(0, 1000-scroller_msg.bounds.size.height);
    [scroller_msg setContentOffset:bottomOffset animated:YES];

    [messages removeAllObjects];
    [submessages removeAllObjects];
    [violationInfo removeAllObjects];
    
    
    
    NSString *query0 = [NSString stringWithFormat:@"select date,data,flag from TempTable order by dateInSecs desc limit 15;"];
    
    // Get the results.
    //minnu
    
        NSMutableArray *dateList =[NSMutableArray array];
        for(int i=0;i<15;i++){
            
            NSArray *dateTemp=[[NSArray alloc]initWithArray: [objDB getLast14Days:i]];
            token=@{@"userid":[NSString stringWithFormat:@"%d",userId],@"date":[NSString stringWithFormat:@"%@",dateTemp[0][0]]};
            ;
            whereToken=[[NSMutableDictionary alloc] initWithDictionary:token];
            NSArray* dataFromDB=[[NSArray alloc]initWithArray:[objDB getLogEditData:whereToken]];
            NSString *statusString=@"";
            if([dateTemp count]!=0){
                for(int j=0;j<[dataFromDB count];j++){
                    if([dataFromDB[j][1] isEqualToString:@""])
                        statusString= [statusString stringByAppendingString:[NSString stringWithFormat:@"1"]];
                    else
                        statusString= [statusString stringByAppendingString:[NSString stringWithFormat:@"%@",[[dataFromDB objectAtIndex:j] objectAtIndex:1]]];
                }
                
                NSString* charStr=[@"" stringByPaddingToLength:96 withString:@"1" startingAtIndex:0];
                if([dataFromDB count]==0)
                    [dateList addObject:@{@"date":dateTemp[0][0],@"approved":@"0",@"data":charStr}];
                else
                {
                    if([statusString isEqualToString:@""])
                        statusString=charStr;
                    [dateList addObject:@{@"date":dateTemp[0][0],@"approved":dataFromDB[0][0],@"data":statusString}];
                }
            }
        }
        previousdatesTemp = [[NSMutableArray alloc] init];
        graphDataOperations *obj=[graphDataOperations new];
        for(int i=0;i<[dateList count];i++){
            NSMutableArray *dict=[[NSMutableArray alloc] initWithArray:[obj generateGraphPoints:[[dateList objectAtIndex:i] objectForKey:@"data"]]];
            
            [previousdatesTemp addObject:@{@"date":[[dateList objectAtIndex:i] objectForKey:@"date"], @"payload":dict}];
        }
//        NSLog(@"GeneratePoints :%@",previousdatesTemp);
    }
    
    
    //minnu
    previousdates_1 = [[NSArray alloc] initWithArray:previousdatesTemp];//[self.dbManager loadDataFromDB:query0]];
   // BFLog(@"Result:%@",previousdates_1);
    previousdates_1=[[previousdates_1 reverseObjectEnumerator] allObjects];
    numCycleStop=-1;
    if([previousdates_1 count]!=0){
            NSString *restartDate=[objDB returnRestart:whereToken];
            if(![restartDate isEqualToString:@""]){
                for(int i=0;i<[previousdates_1 count];i++)
                    if([[[previousdates_1 objectAtIndex:i] objectForKey:@"date"] isEqualToString:restartDate]){
                        numCycleStop=i;
                    }
                // cycleRestart =[self calculateRestart:previousdates];
                
            }
        
  //  numCycleStop = //[obj calculateRestart:previousdates_1];
        [self calculateViolations1:numCycleStop];
    }
 /*   for (int i=0; i<[previousdates_1 count]; i++) {
        
        NSString *dbresult = previousdates_1[i][1];
             NSData *data2= [dbresult dataUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *json_log1 = [NSJSONSerialization JSONObjectWithData:data2 options:0 error:nil];
        //BFLog(@"Dictionary of db data:%@",json_log1);
        if ([json_log1 count]==0) {
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@" " withString:@""];
            dbresult = [NSString stringWithFormat:@"[%@]",dbresult];
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@"\";" withString:@";"];
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@"=\"" withString:@"="];
            
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@"location=(" withString:@"\"location\":["];
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@"payload=(" withString:@"\"payload\":["];
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@");" withString:@"],"];
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@";" withString:@"\","];
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@"=" withString:@":\""];
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@"id" withString:@"\"id\""];
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@"value" withString:@"\"value\""];
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@"posx" withString:@"\"posx\""];
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@"posy" withString:@"\"posy\""];
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@"timezone" withString:@"\"timezone\""];
            NSData *data2= [dbresult dataUsingEncoding:NSUTF8StringEncoding];
            json_log1 = [NSJSONSerialization JSONObjectWithData:data2 options:0 error:nil];
           // BFLog(@"Dictionary of db data2:%@",json_log1);
            
        }
        
        NSArray *dict = [json_log1 objectForKey:@"payload"];
        int json_size = (int)[dict count];
        // int event_last =(int)[[dict[json_size-1] objectForKey:@"posy"] integerValue];
        for (int k=0; k<json_size; k=k+2) {
            int m=k;
            
            //BFLog(@"posx:%@",[dict[0] objectForKey:@"posx"]);
            int event_no =(int)[[dict[m] objectForKey:@"posy"] integerValue];
            if (event_no==65) {
                
                float posx1=[[dict[m] objectForKey:@"posx"] floatValue];
                float posx2=[[dict[m+1] objectForKey:@"posx"] floatValue];
                break_tym = break_tym+posx2-posx1;
                if (break_tym>=restart_hr) {
                    numCycleStop=i;
                    restart_found=true;
                    break;
                    
                }
                
                
            }else{
                break_tym=0.0;
            }
        }
        if (restart_found) {
            //BFLog(@"Cycle start:%d",numCycleStop1);
            if(i+numCycles>=[previousdates_1 count]){
                break;}
            else if(numCycleStop+numCycles<[previousdates_1 count]){
                restart_found=false;
                break_tym=0.0;
                
                
            }
            
            
            
        }
        if (i>=numCycles && !restart_found) {
            numCycleStop=numCycles;
            NSString *violation = [NSString stringWithFormat:@"34hr mandatory restart required"];
            if (![messages containsObject:violation]) {
                [messages addObject:violation];
                NSString *sub=@"Need to take 34hr or more off duty to restart the cycle!!!!";
                [submessages addObject:sub];
                NSArray *sam=@[@"",@"34hour"];
                [violationInfo addObject:sam];
            }
            //BFLog(@"34hour mandatory restart required");
            break;
        }
        
        
        
    }
    
    //BFLog(@"Cycle start:%d",numCycleStop1);
    int n=(int)[previousdates_1 count];
    if(numCycles+numCycleStop>15){
        
        n=(int)[previousdates_1 count];
        
    }
    
    if(15-numCycleStop>numCycles){
        
        //brkReq=8-(15-numCycleStop);
        
    }
    
    
  // BFLog(@"Cycle start:%d",numCycleStop);
    if(numCycleStop==0){
        numCycleStop=numCycles;
        
    }*/
   
   // [self calculateViolations:numCycleStop];
  //  [NSThread sleepForTimeInterval:5.0];
          //  BFLog(@"Messages:%@,submessages:%@,violation:%@",messages,submessages,violationInfo);
    
        dispatch_async(dispatch_get_main_queue(), ^{
            if (finished) {
                msg_list.delegate=self;
                msg_list.dataSource=self;
                    [msg_list reloadData];

            }
         
            
        });
    
 
}


-(void)calculateViolations1:(int)pos{
    int j=pos;
    if([previousdates_1 count]<pos){
        pos=(int)[previousdates_1 count]-1;
    }
    cycle_tot=0.0;
    for (j=pos;j<15;j++) { //for loop1
        driving_cons=0.0;
        driving=0.0;
        break_tym=0.0;
        duty_hr=0.0;
        BFLog(@"%@",previousdates_1[j]);
        NSString *date=[previousdates_1[j] objectForKey:@"date"];
        
        NSMutableArray *dict = [[previousdates_1[j] objectForKey:@"payload"] mutableCopy];
        NSDate * now_sta = [NSDate date];
        NSDateFormatter *outputFormatter_sta1 = [[NSDateFormatter alloc] init];
        [outputFormatter_sta1 setDateFormat:@"MM/dd/yyyy"];
        NSString *currentDate = [outputFormatter_sta1 stringFromDate:now_sta];
         int dicSize =(int)[dict count];
        if(![currentDate isEqualToString:date]){

       
        
        if([dict count]%2!=0){
            
            NSMutableDictionary *dic_new = [[NSMutableDictionary alloc] init];
            [dic_new setValue:[NSString stringWithFormat:@"%d",dicSize] forKey:@"id"];
            [dic_new setValue:@"1216" forKey:@"posx"];
            [dic_new setValue:dict[[dict count]-1][@"posy"] forKey:@"posy"];
            [dict addObject:dic_new];
            
        }
        }else{
            
            NSDate * now_sta = [NSDate date];
            NSDateFormatter *outputFormatter_sta1 = [[NSDateFormatter alloc] init];
            [outputFormatter_sta1 setDateFormat:@"HH:mm"];
            NSString *currentDate = [outputFormatter_sta1 stringFromDate:now_sta];
             if([dict count]%2!=0){
            NSMutableDictionary *dic_new = [[NSMutableDictionary alloc] init];
            [dic_new setValue:[NSString stringWithFormat:@"%d",dicSize] forKey:@"id"];
            [dic_new setValue:[NSString stringWithFormat:@"%f",[self calculatePosx:currentDate]] forKey:@"posx"];
            [dic_new setValue:dict[[dict count]-1][@"posy"] forKey:@"posy"];
            [dict addObject:dic_new];
             }
            
            
        }
        int json_size = (int)[dict count];
        for (int k=0; k<json_size; k=k+2) { //for loop2
            int i=k;
            
            //BFLog(@"posx:%@",[dict[0] objectForKey:@"posx"]);
            int event_no =(int)[[dict[i] objectForKey:@"posy"] integerValue];
            float posx1=[[dict[i] objectForKey:@"posx"] floatValue];
            float posx2=[[dict[i+1] objectForKey:@"posx"] floatValue];
            float duration=posx2-posx1;
            if(event_no==65||event_no==125){//if loop2
                
                driving_cons=0.0;
                
                if (event_no==65) {
                    
                    
                    offduty=offduty+duration;
                    brk_hr=brk_hr+duration;
                    
                    
                }
                if(event_no==125){
                    
                    
                    
                    brk_hr=brk_hr+duration;
                    
                    
                }
                
                
                
            }//if loop2
            else if(event_no==185||event_no==245){//if loop3
                
                if (break_flag) {
                    
                    offduty=0.0;
                    brk_hr=0.0;
                    break_flag=false;
                    
                }
                
                if(!duty_flag){
                    
                    duty_flag=true;
                }
                if (event_no==245) {
                    onduty=onduty+duration;
                    duty_hr=duty_hr+duration;
                    driving_cons=0.0;
                    driving_flag=false;
                    cycle_tot=cycle_tot+duration;
                    
                    
                }
                if (event_no==185) {
                    driving=driving+duration;
                    duty_hr=duty_hr+duration;
                    driving_cons=driving_cons+duration;
                    driving_flag=true;
                    cycle_tot=cycle_tot+duration;
                }

                
                
                
                
                
            }//if loop3
            
            
        
        }//for loop2
        //-----------------------------------------14hr/15hr
        if(duty_hr>duty_window){
            if (duty_window==726.00) {
                
                // BFLog(@"14 hour Rule violation");
                NSString *violation = [NSString stringWithFormat:@"14hr duty rule violation on %@",date];
                if (![messages containsObject:violation]) {
                    [messages addObject:violation];
                    NSString *sub=@"Maximum 14 consecutive duty hours allowed!!!!";
                    [submessages addObject:sub];
                    NSArray *sam=@[date,@"14hour"];
                    [violationInfo addObject:sam];
                }
                
            }else{
                
                // BFLog(@"15 hour Rule violation");
                NSString *violation = [NSString stringWithFormat:@"15hr duty rule violation on %@",date];
                if (![messages containsObject:violation]) {
                    [messages addObject:violation];
                    NSString *sub=@"Maximum 15 consecutive duty hours allowed!!!!";
                    [submessages addObject:sub];
                    NSArray *sam=@[date,@"15hour"];
                    [violationInfo addObject:sam];
                }
                
                
            }
        }
       //------------------------------------------
        BFLog(@"%f",[self calculatePosx:@"08:00"]);
        if (driving_cons>[self calculatePosx:@"08:00"]) {
            // BFLog(@"Maximum 8 hour driving is allowed");
            NSString *violation = [NSString stringWithFormat:@"8-hour driving rule violation  on %@",date];
            if (![messages containsObject:violation]) {
                NSString *sub=@"30minutes break required!!!!";
                [messages addObject:violation];
                [submessages addObject:sub];
                NSArray *sam=@[date,@"8hour"];
                [violationInfo addObject:sam];
            }
            
        }
        
        if (driving>driving_hr) {
            
            if (driving_hr==579.00) {
                // BFLog(@"11 hour driving rule");
                NSString *violation = [NSString stringWithFormat:@"11-hour driving rule violation  on %@",date];
                if (![messages containsObject:violation]) {
                    NSString *sub=@"Maximum 11 hours driving allowed in 14 hours duty cycle!!!!";
                    [submessages addObject:sub];
                    
                    [messages addObject:violation];
                    NSArray *sam=@[date,@"11hour"];
                    [violationInfo addObject:sam];
                }
            }else{
                // BFLog(@"10 hour driving rule");
                NSString *violation = [NSString stringWithFormat:@"10-hour driving rule violation  on %@",date];
                if (![messages containsObject:violation]) {
                    NSString *sub=@"Maximum 10 hours driving allowed in 14 hours duty cycle!!!!";
                    [submessages addObject:sub];
                    
                    [messages addObject:violation];
                    NSArray *sam=@[date,@"10hour"];
                    [violationInfo addObject:sam];
                    
                }
                
            }

        }
       //----------------------------------------
        
        
        
       //-----------------------------------------
    }//end of for loop1
    if(cycle_tot>cycle_tym){
        
        //  BFLog(@"Total maximum duty hour for the cycle %@ is",cycleinfo);
        NSString *violation = [NSString stringWithFormat:@"Maximum duty hour violation"];
        if (![messages containsObject:violation]) {
            [messages addObject:violation];
            NSString *sub=[NSString stringWithFormat:@"Maximum duty hour allowed for the current cycle is %@",[self calculateTime:cycle_tym]];
            [submessages addObject:sub];
        }
        
        
    }

    finished=true;
    
}

-(void)calculateViolations:(NSInteger)pos{
    
    NSInteger j=numCycleStop-1;
    
    for(j=numCycleStop-1;j>=0;j--){
         //[msg_list reloadData];
        NSString *dbresult = previousdates_1[j][1];
        NSString *date=previousdates_1[j][0];
        //BFLog(@"string:%@",dbresult);
        NSData *data2= [dbresult dataUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *json_log1 = [NSJSONSerialization JSONObjectWithData:data2 options:0 error:nil];
        //BFLog(@"Dictionary of db data:%@",json_log1);
        if ([json_log1 count]==0) {
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@" " withString:@""];
            dbresult = [NSString stringWithFormat:@"[%@]",dbresult];
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@"\";" withString:@";"];
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@"=\"" withString:@"="];
            
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@"location=(" withString:@"\"location\":["];
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@"payload=(" withString:@"\"payload\":["];
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@");" withString:@"],"];
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@";" withString:@"\","];
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@"=" withString:@":\""];
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@"id" withString:@"\"id\""];
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@"value" withString:@"\"value\""];
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@"posx" withString:@"\"posx\""];
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@"posy" withString:@"\"posy\""];
            dbresult = [dbresult stringByReplacingOccurrencesOfString:@"timezone" withString:@"\"timezone\""];
            NSData *data2= [dbresult dataUsingEncoding:NSUTF8StringEncoding];
            json_log1 = [NSJSONSerialization JSONObjectWithData:data2 options:0 error:nil];
            //BFLog(@"Dictionary of db data2:%@",json_log1);
            
        }
        NSArray *dict = [json_log1 objectForKey:@"payload"];
        int json_size = (int)[dict count];
        for (int k=0; k<json_size; k=k+2) {
            int i=k;
            
            //BFLog(@"posx:%@",[dict[0] objectForKey:@"posx"]);
            int event_no =(int)[[dict[i] objectForKey:@"posy"] integerValue];
            float posx1=[[dict[i] objectForKey:@"posx"] floatValue];
            float posx2=[[dict[i+1] objectForKey:@"posx"] floatValue];
            float duration=posx2-posx1;
            
            
            if(cycle_tot>cycle_tym){
                
              //  BFLog(@"Total maximum duty hour for the cycle %@ is",cycleinfo);
                NSString *violation = [NSString stringWithFormat:@"Maximum duty hour violation on %@",date];
                if (![messages containsObject:violation]) {
                    [messages addObject:violation];
                    NSString *sub=[NSString stringWithFormat:@"Maximum duty hour allowed for the current cycle is %@",[self calculateTime:cycle_tym]];
                    [submessages addObject:sub];
                }
                
                
            }
            if(event_no==65||event_no==125){
                
                if(duty_flag){
                    
                    onduty=0.0;
                    
                    
                }
                if(!break_flag){
                    break_flag=true;
                    
                }
                
                if (event_no==65) {
                    
                    
                    offduty=offduty+duration;
                    brk_hr=brk_hr+duration;
                    
                    
                }
                if(event_no==125){
                    
                    
                    
                    brk_hr=brk_hr+duration;
                    
                    
                }
                if (duty_flag) {
                    
                    if (brk_hr>=[self calculatePosx:@"10:00"]) {
                        duty_hr=0.0;
                        duty_flag=false;
                        driving=0.0;
                        
                        
                    }else{
                        
                        duty_hr=duty_hr+duration;
                        if(duty_hr>duty_window){
                            if (duty_window==726.00) {
                                
                               // BFLog(@"14 hour Rule violation");
                                NSString *violation = [NSString stringWithFormat:@"14hr duty rule violation on %@",date];
                                if (![messages containsObject:violation]) {
                                    [messages addObject:violation];
                                    NSString *sub=@"Maximum 14 consecutive duty hours allowed!!!!";
                                    [submessages addObject:sub];
                                    NSArray *sam=@[date,@"14hour"];
                                    [violationInfo addObject:sam];
                                }
                                
                            }else{
                                
                               // BFLog(@"15 hour Rule violation");
                                NSString *violation = [NSString stringWithFormat:@"15hr duty rule violation on %@",date];
                                if (![messages containsObject:violation]) {
                                    [messages addObject:violation];
                                    NSString *sub=@"Maximum 15 consecutive duty hours allowed!!!!";
                                    [submessages addObject:sub];
                                    NSArray *sam=@[date,@"15hour"];
                                    [violationInfo addObject:sam];
                                }
                                
                                
                            }
                            
                            break_long=true;
                            
                        }
                        
                        
                        
                        
                    }
                    
                }
                
                if (break_30) {
                    if (brk_hr<[self calculatePosx:@"00:30"]) {
                        
                        NSString *violation = [NSString stringWithFormat:@"30 min break violation  on %@",date];
                        if (![messages containsObject:violation]) {
                            [messages addObject:violation];
                            NSString *sub=@"Minimum 30 minutes break required!!!";
                            [submessages addObject:sub];
                            NSArray *sam=@[date,@"30mintuts"];
                            [violationInfo addObject:sam];
                        }
                        
                    }else{
                        break_30=false;
                        driving_cons=0.0;
                        driving_flag=false;
                        
                        
                    }
                    
                }
                
                if (break_long) {
                    
                    if (brk_hr<break_tym) {
                        
                        if (duty_window==[self calculatePosx:@"15:00"]) {
                            
                            //BFLog(@"8 hour break required!!!!");
                            NSString *violation = [NSString stringWithFormat:@"8 hour break violation on %@",date];
                            if (![messages containsObject:violation]) {
                                [messages addObject:violation];
                                NSString *sub=@"Minimum 8 hours consecutive break required!!!";
                                [submessages addObject:sub];
                                NSArray *sam=@[date,@"8hour"];
                                [violationInfo addObject:sam];
                            }
                            
                        }else{
                            
                           // BFLog(@"10 hour break required!!!!!!");
                            NSString *violation = [NSString stringWithFormat:@"10 hour break violation on %@",date];
                            if (![messages containsObject:violation]) {
                                [messages addObject:violation];
                                NSString *sub=@"Minimum 10 hours consecutive break required!!!";
                                [submessages addObject:sub];
                                NSArray *sam=@[date,@"10hour"];
                                [violationInfo addObject:sam];
                            }
                            
                        }
                        
                        
                        
                        
                    }else{
                        
                        break_long=false;
                    }
                    
                    
                    
                }
                
                
                
                
                
            }else if(event_no==185||event_no==245){
                
                if (break_flag) {
                    
                    offduty=0.0;
                    brk_hr=0.0;
                    break_flag=false;
                    
                }
                
                if(!duty_flag){
                    
                    duty_flag=true;
                }
                if (event_no==245) {
                    onduty=onduty+duration;
                    duty_hr=duty_hr+duration;
                    driving_cons=0.0;
                    driving_flag=false;
                    cycle_tot=cycle_tot+duration;
                    
                    
                }
                if (event_no==185) {
                    driving=driving+duration;
                    duty_hr=duty_hr+duration;
                    driving_cons=driving_cons+duration;
                    driving_flag=true;
                    cycle_tot=cycle_tot+duration;
                }
                
                if (driving_flag) {
                    if (driving_cons>[self calculatePosx:@"08:00"]) {
                       // BFLog(@"Maximum 8 hour driving is allowed");
                        NSString *violation = [NSString stringWithFormat:@"8-hour driving rule violation  on %@",date];
                        if (![messages containsObject:violation]) {
                            NSString *sub=@"Maximum 8 hours consecutive driving allowed!!!!";
                            [messages addObject:violation];
                            [submessages addObject:sub];
                            NSArray *sam=@[date,@"8hour"];
                            [violationInfo addObject:sam];
                        }
                        
                    }
                    
                    if (driving>driving_hr) {
                        
                        if (driving_hr==579.00) {
                           // BFLog(@"11 hour driving rule");
                            NSString *violation = [NSString stringWithFormat:@"11-hour driving rule violation  on %@",date];
                            if (![messages containsObject:violation]) {
                                NSString *sub=@"Maximum 11 hours driving allowed in 14 hours duty cycle!!!!";
                                [submessages addObject:sub];
                                
                                [messages addObject:violation];
                                NSArray *sam=@[date,@"11hour"];
                                [violationInfo addObject:sam];
                            }
                        }else{
                           // BFLog(@"10 hour driving rule");
                            NSString *violation = [NSString stringWithFormat:@"10-hour driving rule violation  on %@",date];
                            if (![messages containsObject:violation]) {
                                NSString *sub=@"Maximum 10 hours driving allowed in 14 hours duty cycle!!!!";
                                [submessages addObject:sub];
                                
                                [messages addObject:violation];
                                NSArray *sam=@[date,@"10hour"];
                                [violationInfo addObject:sam];
                                
                            }
                            
                        }
                    }
                    
                    if (duty_hr>duty_window) {
                        if (duty_window==[self calculatePosx:@"14:00"]) {
                            NSString *violation = [NSString stringWithFormat:@"14hr duty rule violation on %@",date];
                            if (![messages containsObject:violation]) {
                                [messages addObject:violation];
                                NSString *sub=@"Maximum 14 consecutive duty hours allowed!!!!";
                                [submessages addObject:sub];
                                NSArray *sam=@[date,@"14hour"];
                                [violationInfo addObject:sam];
                            }
                            
                        }else{
                            NSString *violation = [NSString stringWithFormat:@"15hr duty rule violation on %@",date];
                            if (![messages containsObject:violation]) {
                                [messages addObject:violation];
                                NSString *sub=@"Maximum 15 consecutive duty hours allowed!!!!";
                                [submessages addObject:sub];
                                NSArray *sam=@[date,@"15hour"];
                                [violationInfo addObject:sam];
                            }
                            
                            
                        }
                        break_long=true;
                    }
                    
                    //close duty_hr>duty_window
                    
                    
                    
                    
                    
                }
                
                
            }
            
            
        }
        
        
        // [self loadTable];
        
    }
    finished=true;
    
    
   
}
-(void)loadTable{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [msg_list reloadData];
        
    });

    
}
-(NSString*)calculateTime:(float)posx{
    
    int end=(posx-40)/12.25;
    int x1=end/4;
    int x2=end%4;
    NSString *decimalpart,*time;
    if (x2*15==0) {
        decimalpart=@"00";
    }else{
        
        decimalpart=[NSString stringWithFormat:@"%d",x2*15];
    }
    
    if (x1<10) {
        time=[NSString stringWithFormat:@"0%d:%@",x1,decimalpart];
    }else{
        
        time=[NSString stringWithFormat:@"%d:%@",x1,decimalpart];
    }
    
    return time;
    
}


-(float)calculatePosx:(NSString *)time{
    NSArray *parts=[time componentsSeparatedByString:@":"];
    float x1=4*[parts[0] integerValue]+([parts[1] integerValue]/15);
    float posx=x1*12.25+40;
    BFLog(@"from function:%f",posx);
    return posx;
    
}
    
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    int rows;
    if ([messages count]==0) {
        rows=1;
    }else{
    
    rows=(int)[messages count];
    }
    return rows;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString *cellIdentifier = @"cell";
    // NSString *cellIdentifier2 =@"more cell";
    // NSString *data=@"";
   // UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    
    cell.detailTextLabel.textColor=[UIColor redColor];
    cell.textLabel.minimumScaleFactor=8./cell.textLabel.font.pointSize;
    cell.textLabel.adjustsFontSizeToFitWidth=YES;
    cell.detailTextLabel.minimumScaleFactor=8./cell.detailTextLabel.font.pointSize;
    cell.detailTextLabel.adjustsFontSizeToFitWidth=YES;
    if ([messages count]==0) {
        cell.textLabel.text=@"Great you have no violations for the current cycle!!!";
         cell.imageView.image=[UIImage imageNamed:@"icons/ok.png"];
    }else{
        cell.textLabel.text=messages[indexPath.row];
        cell.imageView.image=[UIImage imageNamed:@"violations.png"];
        cell.detailTextLabel.text=submessages[indexPath.row];
        
        
    }
    
    
    return cell;
}
    
    
    

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    // UITouch *touch = [touches anyObject];
    if([[self.view subviews] containsObject:transparentView_messages]){
        [transparentView_messages removeFromSuperview];
        
    }
}




-(void)showHelp{
    
    sourceVC = [self valueForKey:@"storyboardIdentifier"];
    UIStoryboard *storyBoard = self.storyboard;
    UIViewController *vc1 = [storyBoard instantiateViewControllerWithIdentifier:@"helpScreen"];
    [self presentViewController:vc1 animated:YES completion:nil];
    
    
    
}






- (IBAction)helpAlert:(id)sender {
    
    CGRect screenSize = [[UIScreen mainScreen] bounds];
    transparentView_messages  = [[UIView alloc] initWithFrame:screenSize];
    transparentView_messages.backgroundColor=[[UIColor blackColor] colorWithAlphaComponent:0.6];
    UIView *view1=[menu_messages valueForKey:@"view"];
    UIButton *tutorialLink = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    tutorialLink.frame = CGRectMake(screenSize.origin.x+(screenSize.size.width/3), screenSize.size.height-40, screenSize.size.width/3, 40);
    [tutorialLink addTarget:self action:@selector(showHelp) forControlEvents:UIControlEventTouchUpInside];
    tutorialLink.tintColor = [UIColor yellowColor];
    [tutorialLink setTitle:@"<<Know More>>" forState:UIControlStateNormal];
    
    [transparentView_messages addSubview:tutorialLink];
    

    
    
    
    UIImageView *arrow1=[[UIImageView alloc]initWithFrame:CGRectMake(view1.frame.origin.x+view1.frame.size.width/2-10, view1.frame.size.height+view1.frame.origin.y*3 , 50, 50)];
    [arrow1 setImage:[UIImage imageNamed:@"icons/arrow7.png"]];
    UILabel *dashboard  = [[UILabel alloc] initWithFrame:CGRectMake(view1.frame.origin.x+view1.frame.size.width/2, arrow1.frame.origin.y+arrow1.frame.size.height, screenSize.size.width/2, 20)];
    dashboard.text=@"Dashboard";
    dashboard.numberOfLines=2;
    dashboard.textColor=[UIColor whiteColor];
    dashboard.font=[UIFont fontWithName:@"Arial-BoldMT" size:20.0];
    dashboard.minimumScaleFactor=10./dashboard.font.pointSize;
    dashboard.adjustsFontSizeToFitWidth=YES;
    
    
    UILabel *messages  = [[UILabel alloc] initWithFrame:CGRectMake(screenSize.origin.x+10,screenSize.size.height/2, screenSize.size.width-10, 20)];
    messages.text=@"You can see the violations of last cycle days here";
    messages.numberOfLines=2;
    messages.textColor=[UIColor whiteColor];
    messages.font=[UIFont fontWithName:@"Arial-BoldMT" size:20.0];
    messages.minimumScaleFactor=10./messages.font.pointSize;
    messages.adjustsFontSizeToFitWidth=YES;
    [transparentView_messages addSubview:arrow1];
    [transparentView_messages addSubview:dashboard];
    [transparentView_messages addSubview:messages];
    
    
    [self.view addSubview:transparentView_messages];
    

}
@end
