//
//  others.h
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 9/1/16.
//  Copyright © 2016 Raghee Chandran M. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "logForm.h"
#import "DBManager1.h"
#import "DBManager.h"
#import "Log.h"
#import "KeychainItemWrapper.h"
#import "logsmenuscreen.h"
#import "AppDelegate.h"
extern NSString *codriver;
extern NSString *origin_1;
extern NSString *dest;
extern NSString *notes_1;

@interface others : UIViewController<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *co_driver;
@property (weak, nonatomic) IBOutlet UITextField *origin;
@property (weak, nonatomic) IBOutlet UITextField *destination;
@property (strong, nonatomic) IBOutlet UIScrollView *scroller03;
@property (nonatomic, strong) DBManager1 *dbManager;

@property (weak, nonatomic) IBOutlet UITextField *notes;
- (IBAction)doneEdit:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *donebtn;
@end
