//
//  resetOldPasswordViewController.h
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 8/4/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DBManager1.h"
#import "user.h"

@interface resetOldPasswordViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *oldPassword;
@property (weak, nonatomic) IBOutlet UILabel *passwordNewLabel;

@property (weak, nonatomic) IBOutlet UITextField *oldPasswordVal;
@property (weak, nonatomic) IBOutlet UITextField *passwordNewvAl;

@property (weak, nonatomic) IBOutlet UILabel *confirmLabel;
- (IBAction)savePassword:(id)sender;
@property (nonatomic, strong) DBManager1 *dbManager;
@property (weak, nonatomic) IBOutlet UITextField *confirmPasswordVAl;
@end
