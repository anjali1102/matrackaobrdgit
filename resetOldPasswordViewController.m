//
//  resetOldPasswordViewController.m
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 8/4/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import "resetOldPasswordViewController.h"
#import "KeychainItemWrapper.h"
#import "insertDutyCycle.h"
#import "AppDelegate.h"
#import "commonDataHandler.h"
#import "Header.h"
#import "MBProgressHUD.h"
#import "MobileCheckIn.h"
@interface resetOldPasswordViewController ()

@end

@implementation resetOldPasswordViewController
@synthesize oldPassword,passwordNewLabel,confirmLabel;
@synthesize oldPasswordVal,passwordNewvAl,confirmPasswordVAl,dbManager;

- (void)viewDidLoad {
    [super viewDidLoad];
    dbManager = [[DBManager1 alloc] initWithDatabaseFilename:@"HOSDatabase.sql"];
    [self addRequired:oldPassword];
    [self addRequired:passwordNewLabel];
    [self addRequired:confirmLabel];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)addRequired:(UILabel*)label{
    
    NSMutableAttributedString *text =
    [[NSMutableAttributedString alloc]
     initWithAttributedString: label.attributedText];
    
    [text addAttribute:NSForegroundColorAttributeName
                 value:[UIColor redColor]
                 range:NSMakeRange([label.text length]-1, 1)];
    [label setAttributedText: text];
    
    
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)savePassword:(id)sender {
    autoLoginCount = 0;
    if([oldPasswordVal.text isEqualToString:@""] || [passwordNewvAl.text isEqualToString:@""] || [confirmPasswordVAl.text isEqualToString:@""]){
        
        UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@""
                                                         message:@"Required fields cannot be empty!"
                                                        delegate:self
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles:nil, nil];
        
        [alert1 show];
        

        
        
    }else if (![passwordNewvAl.text isEqualToString:confirmPasswordVAl.text]){
        
        UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@""
                                                         message:@"New Password and Confirm Password should match!"
                                                        delegate:self
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles:nil, nil];
        
        [alert1 show];
        

        
    }else{
//        NSMutableDictionary *mapData = [[NSMutableDictionary alloc] init];
//        [mapData setValue:oldPasswordVal.text forKey:@"passwordOld"];
//        [mapData setValue:passwordNewvAl.text forKey:@"passwordNew"];
        [self resetPasswordAPICall];
    }
    
}
-(void)resetPasswordAPICall{
    NSMutableDictionary *keyList = [[NSMutableDictionary alloc] init];
    [keyList setValue:OPERATION_RESET_PASSWORD forKey:OPERATION];
    [keyList setValue:[commonDataHandler sharedInstance].strUserName forKey:USER_NAME];
    [keyList setValue:[Bugfender deviceIdentifier] forKey:DEVICE_ID];
    [keyList setValue:oldPasswordVal.text forKey:OLD_PASSWORD];
     [keyList setValue:passwordNewvAl.text forKey:NEW_PASSWORD];
    NSString *stringParams = [[commonDataHandler sharedInstance] constructInput:keyList];
    __block NSString *responseString = @"";
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSString *urlString = [NSString stringWithFormat:@"%@%@", WEB_ROOT, RESET_PASSWORD_URL];
    NSString *urlStringWithParam = [NSString stringWithFormat:@"payload=%@",stringParams];
    NSString *enodedURl = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [NSURL URLWithString:enodedURl] ;
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    
    [request setHTTPBody:[urlStringWithParam dataUsingEncoding:NSUTF8StringEncoding]];
    NSOperationQueue *queue1 = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:request queue:queue1 completionHandler:^(NSURLResponse * _Nullable response, NSData * _Nullable data, NSError * _Nullable connectionError) {
        responseString = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        });
        if(![responseString isEqualToString:@""]){
            NSMutableArray *arrayResponse = [[NSMutableArray alloc] init];
            NSMutableArray *arrayVehicleList   = [NSMutableArray new];
            [arrayVehicleList removeAllObjects];
            [arrayResponse removeAllObjects];
            arrayResponse =[[commonDataHandler sharedInstance] parseResponse:responseString];
            
            if(arrayResponse!=nil && [arrayResponse count]!=0){
                
                for (NSMutableDictionary *objDict in arrayResponse) {
                    MobileCheckIn *objVehicle= [MobileCheckIn new];
                    [objVehicle getDataFromAPILogin:objDict];
                    [arrayVehicleList addObject:objVehicle];
                }
                
                if(arrayVehicleList.count > 0){
                    MobileCheckIn *objMobileCheckIn = [arrayVehicleList firstObject];
                    if([objMobileCheckIn.result isEqualToString:@"success"]){
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [credentials setObject:passwordNewvAl.text forKey:(id)kSecValueData];
                            [commonDataHandler sharedInstance].strAuthCode = objMobileCheckIn.authCode;
                            //minnu
                          /*  DBManager *objDB= [[DBManager alloc] initWithDatabaseFilename:@"hosDB.sql"];
                            User *objUser=[User new];
                             NSArray *userInfoArray=[[NSArray alloc]initWithArray: [objDB getUserId]];
                            if([userInfoArray count]!=0){
                                objUser.username=userInfoArray[0][1];
                                objUser.password=passwordNewvAl.text;
                                objUser.authCode=objMobileCheckIn.authCode;
                                objUser.loggedIn=@"1";
                                [objDB updateCred:objUser];
                            }*/
                            
                            
                           
                            
                            //minnu
                            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Success"
                                                        message:@"Reset password successfully"
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil, nil];
                            alert.tag = 1006;
                            [alert show];
                        });
                    }else{
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [[[UIAlertView alloc] initWithTitle:@"Error"
                                                        message:objMobileCheckIn.reason
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil, nil] show];
                        });
                    }
                }
            }
        }
        
        
    }];
    
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if(alertView.tag ==1 || alertView.tag ==2){
        
        if(buttonIndex==[alertView cancelButtonIndex]){
            
            
            dispatch_async(dispatch_get_main_queue(), ^{
                UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                UIViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"loginscreen"];
                [self presentViewController:vc animated:YES completion:nil];
            });            
        }
        
        
    }
    if (alertView.tag == 1006) {
        passwordNewvAl.text= @"";
        oldPasswordVal.text = @"";
        confirmPasswordVAl.text = @"";
    }
    
    
    
}

@end
