//
//  savedDocsViewController.m
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 7/28/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import "savedDocsViewController.h"
#import "documentMenuViewController.h"

NSArray *pdfArray;
NSMutableArray *pdfArray1;
NSInteger selectedFileIndex;
@interface savedDocsViewController ()

@end

@implementation savedDocsViewController
@synthesize pdfList;

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self getPdf];
    pdfList.delegate=self;
    pdfList.dataSource=self;
    // Do any additional setup after loading the view.
}
-(void)getPdf{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    
    pdfArray = [[NSFileManager defaultManager] subpathsOfDirectoryAtPath:documentsDirectory
                                                                   error:nil];
    
    [[NSFileManager defaultManager] subpathsOfDirectoryAtPath:documentsDirectory
                                                        error:nil];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF ENDSWITH '.pdf'"];
    pdfArray =  [pdfArray filteredArrayUsingPredicate:predicate];
    pdfArray1 = [[NSMutableArray alloc] initWithArray:pdfArray];
    
    
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSInteger rows=0;
    if([pdfArray1 count]!=0){
        rows = [pdfArray1 count];
        
    }else{
        rows=1;
    }
    
    return rows;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    if([pdfArray1 count]==0){
        
        
        cell.textLabel.text = @"No Saved Documents";
        cell.accessoryView =nil;
        
    }else{
        cell.textLabel.text = pdfArray1[indexPath.row];
        cell.imageView.image=[UIImage imageNamed:@"icons/pdf.png"];
        UIButton *removeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        removeBtn.frame = CGRectMake(tableView.bounds.size.width-50, 0, 50, 50);
        selectedFileIndex=indexPath.row;
        [removeBtn addTarget:self action:@selector(removeDoc:) forControlEvents:UIControlEventTouchUpInside];
        [removeBtn setImage:[UIImage imageNamed:@"remove.png"] forState:UIControlStateNormal];
        cell.accessoryView=removeBtn;
        
        
    }
    
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if([pdfArray1 count]!=0){
        
        NSString *file = pdfArray1[indexPath.row];
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        
        // Get the resource path and read the file using NSData
        NSString *filePath = [documentsDirectory stringByAppendingString:[NSString stringWithFormat:@"/%@",file]];
        selectedFile = filePath;
        filenameSelected = file;
        if(![file  isEqual: @""])
        {
            UIStoryboard *storyBoard = self.storyboard;
            UIViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"showDocuments"];
            [self presentViewController:vc animated:YES completion:nil];
        }
        
    }
    
    
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if(alertView.tag==1){
        if(buttonIndex==0){
            
            NSInteger fileIndex = selectedFileIndex;
            NSString *filename = pdfArray1[fileIndex];
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
            NSString *documentsDirectory = [paths objectAtIndex:0];
            NSString *filePath = [documentsDirectory stringByAppendingPathComponent:filename];
            NSError *error;
            BOOL success = [[NSFileManager defaultManager] removeItemAtPath:filePath error:&error];
            if(success){
                UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@""
                                                                 message:@"Successfully Removed"
                                                                delegate:self
                                                       cancelButtonTitle:@"OK"
                                                       otherButtonTitles:nil, nil];
                [alert1 setTag:2];
                
                [alert1 show];
                [pdfArray1 removeAllObjects];
                [self getPdf];
                [pdfList reloadData];
                
                
                
                
            }else{
                UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Oops"
                                                                 message:@"Couldn't Delete file!!"
                                                                delegate:self
                                                       cancelButtonTitle:@"OK"
                                                       otherButtonTitles:nil, nil];
                [alertView setTag:3];
                
                [alert1 show];
                
                
                
                
            }

            
            
            
            
        }
        
        
        
        
        
    }
    
    
    
}
-(void)removeDoc:(UIButton*)removeBtn{
    UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@""
                                                     message:@"Do you want to delete this file?"
                                                    delegate:self
                                           cancelButtonTitle:@"Delete"
                                           otherButtonTitles:@"Keep", nil];
    
    [alert1 setTag:1];
    [alert1 show];

    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
