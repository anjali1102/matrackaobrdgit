//
//  sendEmailViewController.h
//  MaTrackHOSV1
//
//  Created by sieva networks on 10/31/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface sendEmailViewController : UIViewController<UITextFieldDelegate>
{
    IBOutlet UITextField *txtToAddress;
    IBOutlet UITextField *txtCCAddress;
    IBOutlet UITextField *txtOptionalText;
    IBOutlet UILabel     *lblToAddress;
}
-(IBAction)sendEmail:(id)sender;
-(IBAction)backAction:(id)sender;
@end
