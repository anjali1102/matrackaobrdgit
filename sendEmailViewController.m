//
//  sendEmailViewController.m
//  MaTrackHOSV1
//
//  Created by sieva networks on 10/31/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import "sendEmailViewController.h"
#import "Header.h"
#import "commonDataHandler.h"
#import "MobileCheckIn.h"
#import "NSString+NullCheck.h"
@interface sendEmailViewController ()
{
    BOOL isReAuth;
    NSString *strToAddress;
    NSString *strCCAddress;
}
@end

@implementation sendEmailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    isReAuth = YES;
    NSString *strTo = @"To*";
    NSMutableAttributedString *text =[[NSMutableAttributedString alloc] initWithString:strTo];
    
    [text addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange([strTo length]-1, 1)];
    [lblToAddress setAttributedText: text];
    UIToolbar* keyboardToolbar = [[UIToolbar alloc] init];
    [keyboardToolbar sizeToFit];
    UIBarButtonItem *flexBarButton = [[UIBarButtonItem alloc]
                                      initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                      target:nil action:nil];
    UIBarButtonItem *doneBarButton = [[UIBarButtonItem alloc]
                                      initWithBarButtonSystemItem:UIBarButtonSystemItemCancel
                                      target:self action:@selector(cancelAction)];
    keyboardToolbar.items = @[flexBarButton, doneBarButton];
    txtToAddress.inputAccessoryView = keyboardToolbar;
    txtCCAddress.inputAccessoryView = keyboardToolbar;
    txtOptionalText.inputAccessoryView = keyboardToolbar;
    // Do any additional setup after loading the view.
}
-(void)cancelAction{
    [self.view endEditing:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark    - <UITextField Delegate>
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
//- (void)textFieldDidBeginEditing:(UITextField *)textField{
//    if(textField == txtToAddress){
//        txtToAddress.layer.borderColor = [UIColor redColor].CGColor;
//        txtCCAddress.layer.borderColor = [UIColor grayColor].CGColor;
//        txtOptionalText.layer.borderColor = [UIColor grayColor].CGColor;
//
//    }
//    else if (textField == txtCCAddress){
//        txtCCAddress.layer.borderColor = [UIColor redColor].CGColor;
//        txtOptionalText.layer.borderColor = [UIColor grayColor].CGColor;
//        txtToAddress.layer.borderColor = [UIColor grayColor].CGColor;
//
//    }
//    else if (textField == txtOptionalText){
//        txtOptionalText.layer.borderColor = [UIColor redColor].CGColor;
//        txtCCAddress.layer.borderColor = [UIColor grayColor].CGColor;
//        txtToAddress.layer.borderColor = [UIColor grayColor].CGColor;
//    }
//}
//- (void)textFieldDidEndEditing:(UITextField *)textField{
//    textField.layer.borderColor = [UIColor grayColor].CGColor;
//}
#pragma mark    - <send email>
-(IBAction)sendEmail:(id)sender
{
    [self.view endEditing:YES];
    
    if(txtToAddress.text.length > 0){
        NSString *str1 = txtToAddress.text;
        strToAddress = [str1 stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        if(txtCCAddress.text.length > 0){
            NSString *str2 = txtCCAddress.text;
            strCCAddress = [str2 stringByReplacingOccurrencesOfString:@" " withString:@""];
            if(![strCCAddress validateEmailWithString:strCCAddress]){
                
                [[[UIAlertView alloc] initWithTitle:@"Wrong Data"
                                            message:@"Please Enter valid CC Address"
                                           delegate:nil
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil, nil] show] ;
                [txtCCAddress becomeFirstResponder];
            }
        }
        if([strToAddress validateEmailWithString:strToAddress]){
            [self callSendEmailAPI];
        }else{
            [[[UIAlertView alloc] initWithTitle:@"Wrong Data"
                                        message:@"Please Enter valid To Address"
                                       delegate:nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil, nil] show] ;
            [txtToAddress becomeFirstResponder];
        }
    }else{
        [[[UIAlertView alloc] initWithTitle:@"Mandatory Field"
                                    message:@"Please Fill the Mandatory Fields"
                                   delegate:nil
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil, nil] show] ;
        [txtToAddress becomeFirstResponder];
    }
}
-(void)callSendEmailAPI{
    NSString *strOptional;
    if(strCCAddress.length <= 0)
        strCCAddress = @"NA";
    if(txtOptionalText.text.length <= 0)
        strOptional = @"NA";
    else
        strOptional = txtOptionalText.text;
    NSMutableDictionary *requestDict = [[NSMutableDictionary alloc] init];
    [requestDict setValue:OPERATION_SEND_EMAIL forKey:OPERATION];
    [requestDict setValue:[commonDataHandler sharedInstance].strUserName forKey:USER_NAME];
    [requestDict setValue:[Bugfender deviceIdentifier] forKey:DEVICE_ID];
    [requestDict setValue:[commonDataHandler sharedInstance].strAuthCode forKey:AUTH_CODE];
    [requestDict setValue:strToAddress forKey:TO_ADDRESS];
    [requestDict setValue:strCCAddress forKey:CC_ADDRESS];
    [requestDict setValue:strOptional forKey:OPTIONAL_TEXT];
    [requestDict setValue:[[commonDataHandler sharedInstance] getTimeDifferenceForGMT] forKey:TIME_DIFFERENCE];
    
    NSString *stringParams = [[commonDataHandler sharedInstance] constructInput:requestDict];
    
    __block NSString *responseString = @"";
    NSString *urlStringWithParam = [NSString stringWithFormat:@"payload=%@",stringParams];
    NSString *urlString = [NSString stringWithFormat:@"%@%@", WEB_ROOT, SEND_EMAIL_URL];
    NSString *enodedURl = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [NSURL URLWithString:enodedURl] ;
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    
    [request setHTTPBody:[urlStringWithParam dataUsingEncoding:NSUTF8StringEncoding]];
    NSOperationQueue *queue1 = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:request queue:queue1 completionHandler:^(NSURLResponse * _Nullable response, NSData * _Nullable data, NSError * _Nullable connectionError) {
        responseString = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
//        if([newString containsString:OPERATION]){
//            NSRange range = [newString rangeOfString:OPERATION];
//            responseString = [newString substringFromIndex:range.location];
//        }else
//            responseString = newString;

        if(![responseString isEqualToString:@""]){
            NSMutableArray *arrayResponse = [[NSMutableArray alloc] init];
            NSMutableArray *arrayMobileCheckIn   = [NSMutableArray new];
            [arrayMobileCheckIn removeAllObjects];
            [arrayResponse removeAllObjects];
            arrayResponse =[[commonDataHandler sharedInstance] parseResponse:responseString];
            
            if(arrayResponse!=nil && [arrayResponse count]!=0){
                
                for (NSMutableDictionary *objDict in arrayResponse) {
                    MobileCheckIn *objMobileCheckIn = [MobileCheckIn new];
                    [objMobileCheckIn getDataFromAPI:objDict];
                    [arrayMobileCheckIn addObject:objMobileCheckIn];
                }
                
                if(arrayMobileCheckIn.count > 0){
                    MobileCheckIn *objMobileCheckIn = [arrayMobileCheckIn firstObject];
                    if([objMobileCheckIn.result isEqualToString:@"failed"] && [objMobileCheckIn.operation isEqualToString:OPERATION_SEND_EMAIL]){
                        if([objMobileCheckIn.reason isEqualToString:AUTH_CODE_INVALID]){
                            //Re-auth
                            if(isReAuth){
                                isReAuth = NO;
//                                [[commonDataHandler sharedInstance] reAuthenticateUser:^(BOOL finished) {
//                                    [self callSendEmailAPI];
//                                }];
                            }
                        }else{
                            dispatch_async(dispatch_get_main_queue(), ^{
                                txtCCAddress.text = @"";
                                txtToAddress.text = @"";
                                txtOptionalText.text = @"";
                                [txtToAddress becomeFirstResponder];
                                UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Send Email"
                                                                                 message:objMobileCheckIn.reason
                                                                                delegate:nil
                                                                       cancelButtonTitle:@"OK"
                                                                       otherButtonTitles:nil, nil];
                                [alert1 show];
                            });
                        }
                    }else{
                        //Call success
                        dispatch_async(dispatch_get_main_queue(), ^{
                            UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Send Email"
                                                                             message:objMobileCheckIn.result
                                                                            delegate:nil
                                                                   cancelButtonTitle:@"OK"
                                                                   otherButtonTitles:nil, nil];
                            [alert1 show];
                        });
                    }
                }
            }else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[[UIAlertView alloc] initWithTitle:@"Send Email"
                                                                     message:@"Something went wrong!!!"
                                                                    delegate:nil
                                                           cancelButtonTitle:@"OK"
                                                           otherButtonTitles:nil, nil]show];
                });
            }
        }
    }];
    
}
#pragma mark    - <Back Action>
-(IBAction)backAction:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
