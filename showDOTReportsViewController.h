//
//  showDOTReportsViewController.h
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 1/11/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface showDOTReportsViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIBarButtonItem *menu_senddot;

@property (weak, nonatomic) IBOutlet UIWebView *fileView;
@property (weak, nonatomic) IBOutlet UIButton *sendBtn;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *helpAlert;
- (IBAction)helpMessage:(id)sender;
@end
