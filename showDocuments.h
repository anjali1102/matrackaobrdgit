//
//  showDocuments.h
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 1/25/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface showDocuments : UIViewController
@property (weak, nonatomic) IBOutlet UIWebView *showDoc;
@property (weak, nonatomic) IBOutlet UINavigationItem *navigation1;

@end
