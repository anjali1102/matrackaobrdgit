//
//  showDocuments.m
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 1/25/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import "showDocuments.h"
#import "documentMenuViewController.h"


@interface showDocuments ()

@end

@implementation showDocuments
@synthesize showDoc;
@synthesize navigation1;

- (void)viewDidLoad {
    [super viewDidLoad];
    navigation1.title=filenameSelected;
    NSURL *targetUrl = [NSURL fileURLWithPath:selectedFile];
    NSURLRequest *request = [NSURLRequest requestWithURL:targetUrl];
    [showDoc loadRequest:request];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
