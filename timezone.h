//
//  timezone.h
//  MaTrackHOSV1
//
//  Created by Minnu Mohandas on 19/10/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface timezoneTable : NSObject
@property (nonatomic, strong) NSString *identifier;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *timediff;

@end
