//
//  user2vehicle.h
//  MaTrackHOSV1
//
//  Created by Minnu Mohandas on 10/11/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface user2vehicle : NSObject
@property (nonatomic, strong) NSString *userid;
@property (nonatomic, strong) NSString *vehicleid;
@end
