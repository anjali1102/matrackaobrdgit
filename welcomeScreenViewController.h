//
//  welcomeScreenViewController.h
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 7/17/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface welcomeScreenViewController : UIViewController
@property NSUInteger pageIndex;
- (IBAction)skip:(id)sender;
- (IBAction)ok:(id)sender;
@end
