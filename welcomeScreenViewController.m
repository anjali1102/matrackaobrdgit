//
//  welcomeScreenViewController.m
//  MaTrackHOSV1
//
//  Created by Raghee Chandran M on 7/17/17.
//  Copyright © 2017 Raghee Chandran M. All rights reserved.
//

#import "welcomeScreenViewController.h"

@interface welcomeScreenViewController ()

@end

@implementation welcomeScreenViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)skip:(id)sender {
    UIStoryboard *storyBoard = self.storyboard;
    UIViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"menuscreen"];
    [self presentViewController:vc animated:YES completion:nil];
}

- (IBAction)ok:(id)sender {
    //[self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
    UIStoryboard *storyBoard = self.storyboard;
    UIViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"settingsMenu"];
    [self presentViewController:vc animated:YES completion:nil];
}
@end
